#!/usr/bin/env bash

#####
# @Author Oscar Makala
# @Date Friday June 2016
#
####
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo $DIR
. $DIR"/parse_yaml.sh"

pidfile="${DIR}/../recon.pid"
CONFIG="${DIR}/../app/config/parameters.yml"

eval $(parse_yaml ${CONFIG} "m_")


#script checks all numbers that are in registration table, but have no status. sends them to treg
mysql_conn="mysql -u${m_parameters__database_user} -p${m_parameters__database_password} -h${m_parameters__database_host} -ss -e"

$mysql_conn "select id from user where username not in ('754311023','754712584')" ${m_parameters__database_name}|while read id; do
echo $id;
$mysql_conn "delete From RefreshToken where user_id=$id" ${m_parameters__database_name}
$mysql_conn "delete From AccessToken where user_id=$id" ${m_parameters__database_name}
$mysql_conn "delete from device where user_id=$id" ${m_parameters__database_name}
$mysql_conn "delete From RegistrationStatus where registrationid in (select id From registration where owner_id=$id);" ${m_parameters__database_name}
$mysql_conn "delete From registration where owner_id=$id;" ${m_parameters__database_name}
$mysql_conn "delete from user where id=$id" ${m_parameters__database_name}
done