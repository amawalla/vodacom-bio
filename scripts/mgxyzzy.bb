  -p, --password[=name] 
                      Password to use when connecting to server. If password is
                      not given it's asked from the tty.
