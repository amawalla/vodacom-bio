#!/usr/bin/env bash

DIRECTORY=$1

PHP_COMMAND_DIR="/var/www/html/vodacom-staging"

ls ${DIRECTORY}/*.txt | while read filename;
do
    SOURCE_FILE_DIR=$(dirname "${filename}");

    echo "Processing file ${filename}"

    less ${filename} | while read msisdn;
    do
        echo ${msisdn}
        php ${PHP_COMMAND_DIR}/app/console rest_api:vodacom_processor_command deregister ${msisdn} &
        #sleep 1;
    done
    mv ${filename} ${SOURCE_FILE_DIR}/processed/
done

exit
