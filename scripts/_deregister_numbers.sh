#!/usr/bin/env bash

SOURCE_FILENAME=$1

SOURCE_FILE_DIR=$(dirname "${SOURCE_FILENAME}");

PHP_COMMAND_DIR="/var/www/html/vodacomEKYCLive"

less ${SOURCE_FILENAME} | while read msisdn;
do
    echo ${msisdn}
    php ${PHP_COMMAND_DIR}/app/console rest_api:vodacom_processor_command deregister ${msisdn} &
    #sleep 1;
done

mv ${SOURCE_FILENAME} ${SOURCE_FILE_DIR}/processed/

exit
