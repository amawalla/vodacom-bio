#!/usr/bin/env bash


DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. $DIR"/parse_yaml.sh"

LOGDIR="${DIR}/bashlogs/report_generator.log"
CONFIG="${DIR}/../app/config/parameters.yml"

eval $(parse_yaml ${CONFIG} "m_")


email=$1
title=$2
filename=$3


echo "`date` WWORKAROUND workaround email: ${email} with title: ${title} " >> $LOGDIR

php "${DIR}/mail/SendEmail.php" "http://172.26.2.108/euploads/reports/${filename}" "${email}" "${title}" >> $LOGDIR

exit
