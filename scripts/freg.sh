#!/bin/bash
#####
# @Author Oscar Makala
# @Date Tue May 2016
# 1. script checks all numbers that are in registration table, but have no status. sends them to treg
# 2. checks all numbers that have been sent to treg succesfully, but have not been sent to freg or have
# been only submitted
#
####
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo $DIR
. $DIR"/parse_yaml.sh"

pidfile="${DIR}/freg.pid"
LOGDIR="${DIR}/bashlogs/recon.log"
CONFIG="${DIR}/../app/config/parameters.yml"
TEMP="${DIR}/vodacom_mfreg.out"
num_files=20
echo "`date`    starting script at "; >> $LOGDIR;

eval $(parse_yaml ${CONFIG} "m_")


if test -e "$pidfile"
then
        #get the pid value from the file
        pid_id=`cat $pidfile`
        pid_count=`ps -ef|grep " $pid_id "|grep -v 'grep' | wc -l`
        if [ $pid_count -ge 1 ];then
                echo "`date` | another icap resend module is still running ($pid_id)" >> $LOGDIR
                exit 1
        fi
fi

countR=$(ps -ef|grep 'rest_api:vodacom_processor_command freg'|wc -l)
if [ $countR -gt 1 ];
then
                echo "`date` | Still running another batch of files. to avoid retry" >> $LOGDIR
                exit 1
fi

today=$(date +"%Y-%m-%d")


mysql_conn="mysql -u${m_parameters__database_user} -h${m_parameters__database_host_replica} -ss -e";

mysql_conn_primary="mysql -u${m_parameters__database_user} -h${m_parameters__database_host} -ss -e";


$mysql_conn_primary " UPDATE registration SET identificationType = CASE WHEN identificationType = 'National ID' THEN 3 WHEN identificationType = 'WEO/VEO/REF' THEN 14 WHEN identificationType = 'Voters ID' THEN 1 WHEN identificationType = 'Health Insurance ID' THEN 9 WHEN identificationType = 'Student ID' THEN 13 WHEN identificationType = 'Employer ID' THEN 10 WHEN identificationType = 'Drivers License' THEN 2 WHEN identificationType = 'Passport' THEN 4 WHEN identificationType = 'VAT Certificate' THEN 7 WHEN identificationType = 'Business Licence' THEN 5 WHEN identificationType IN('NSSF Pension Card', 'Pension Fund ID') THEN 11 WHEN identificationType = 'Certificate of Incorporation' THEN 8 ELSE identificationType END WHERE length(identificationType) > 2 and date(createdDate) >= '${today}'; "  ${m_parameters__database_name}


echo "$$" > $pidfile;
:> ${TEMP}
#checks all numbers that have been sent to treg succesfully, but have not been sent to freg or have
# been only submitted
#$mysql_conn "select registration.registrationid from RegistrationStatus inner join registration on RegistrationStatus.registrationId = registration.id where temporaryRegStatus in (2,6) and fullRegStatus in (0) and image_count >= 4 and RegistrationStatus.verifyState = 1 and left(RegistrationStatus.createdDate,10) >= '2016-11-01' limit 1000 " ${m_parameters__database_name} > ${TEMP}


#$mysql_conn "select registration.id from RegistrationStatus left join registration on RegistrationStatus.registrationId = registration.id where verifyState = 1 AND fullRegStatus in (0,1) and image_count >= 4 and icap_state = 0 and RegistrationStatus.createdDate >= '2017-08-19' ORDER BY registration.id DESC limit 1000 " ${m_parameters__database_name} > ${TEMP}

$mysql_conn "select registrationId from (SELECT registrationId, icap_state from RegistrationStatus WHERE verifyState = 1 AND fullRegStatus IN(0,1,7) AND RegistrationStatus.createdDate >= subdate(current_date, 3) ORDER BY id desc limit 1500000) as ss where ss.icap_state = 0 order by registrationId desc limit 1000; " ${m_parameters__database_name} > ${TEMP}

count=0
if [ -s ${TEMP} ];
then
total_lines=$(wc -l <${TEMP})
((lines_per_file = (total_lines + num_files - 1) / num_files))
split --lines=${lines_per_file} ${TEMP} fregxyzzy.
echo "Total lines     = ${total_lines}"
echo "Lines  per file = ${lines_per_file}"

for f in fregxyzzy.*;
do
 id=$(less ${f} | awk -vORS=, '{ print $1 }' | sed 's/,$/\n/')
 echo {$id};
 php /var/www/html/environments/simreg/dev/app/console -e=prod rest_api:vodacom_processor_command "freg" "${id}" &
 rm $f
done
fi

rm ${TEMP}
echo "ending script at `date`";

#if [[ "${hour}" -eq "00" ]];then
#if [[ "${minute}" -lt "10" ]];then
#`${DIR}/mqtt_clear_app_data.sh`
#fi

#fi
rm -f $pidfile
exit 1

