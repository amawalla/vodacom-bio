#!/usr/bin/env bash


DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. $DIR"/parse_yaml.sh"

LOGDIR="${DIR}/bashlogs/report_generator.log"
CONFIG="${DIR}/../app/config/parameters.yml"

eval $(parse_yaml ${CONFIG} "m_")

mysql_database_host="eregdatabase-replica.c1menozddi24.us-east-1.rds.amazonaws.com"

queryString=$1
email=$2
title=$3
heading=$4

report=`date +%Y%m%d%H%M`"_${title}.csv"
reportFile="${DIR}/${report}"
reportDirectory="${m_parameters__resource_path}/reports/"`date +%Y%m%d`
dummyReportPath="${m_parameters__resource_path}/euploads/reports/"`date +%Y%m%d`

echo "`date` params ${queryString} ${email} ${title}" >> $LOGDIR
echo "`date` Start generating ${title} report for ${email} with file ${reportFile}" >> $LOGDIR

echo "${heading}" >> ${reportFile}
mysql_conn="mysql -u${m_parameters__database_user} -h${m_parameters__database_host_replica} -ss -e";
$mysql_conn "${queryString}" ${m_parameters__database_name} | perl -pe 's/(\[?"?\}?\s?{"fieldN\s*(.*?)\s*Descr":")/\#/g and s/"\}\]//g' >> ${reportFile}


if [ -s "${reportFile}" ];
then
#turn to csv
`sed -i -e 's/\t/,/g' "${reportFile}"`
#move report file to report directory
if [ ! -d "${reportDirectory}" ];
then
mkdir -p "${reportDirectory}"
else
echo "`date` ${title} report for ${email} with file ${reportFile} starting move processs to ${reportDirectory}/${report}" >> $LOGDIR
`mv "$reportFile" "${reportDirectory}"`

sleep 1
if [ -s "${reportDirectory}/${report}" ];
then
echo "`date` ${title} report for ${email} with file ${reportFile} successfully moved to ${reportDirectory}/${report}" >> $LOGDIR
finalReportFile="${dummyReportPath}/${report}";
finalReportLink=$(echo "${finalReportFile/${m_parameters__resource_path}/${m_parameters__hostname}}" )
echo "`date` ${title} report for ${email} with file ${reportFile} generate link is  ${finalReportLink}" >> $LOGDIR
php "${DIR}/mail/SendEmail.php" "${finalReportLink}" "${email}" "${title}" >> $LOGDIR
fi
fi

fi
rm ${reportFile}
echo "`date` End generating ${title} report for ${email} with file ${reportFile}" >> $LOGDIR

exit
