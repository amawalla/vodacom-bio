#!/bin/bash
#####
# @Author  Kimuyu
# @Date Tue May 2016
# 1. scripts to call and rectify registrations with wrong image count
#
####
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo $DIR
. $DIR"/parse_yaml.sh"

pidfile="${DIR}/../recon_image.pid"
LOGDIR="${DIR}/../bashlogs/recon.log"
CONFIG="${DIR}/../app/config/parameters.yml"
TEMP="${DIR}/../vodacom_recon1.out"
today=$(date +"%Y-%m-%d")
yesterday=$(date -d "yesterday" '+%Y-%m-%d')

echo "`date`    starting script at "; >> $LOGDIR;

eval $(parse_yaml ${CONFIG} "m_")

if test -e "$pidfile"
then
	#get the pid value from the file
	pid_id=`cat $pidfile`
	pid_count=`ps -ef|grep " $pid_id "|grep -v 'grep' | wc -l`
	if [ $pid_count -ge 1 ];then
		echo "`date` | another correct_image_script module is still running ($pid_id)" >> $LOG
	    exit 1
	fi
fi

mysql_conn="mysql -u${m_parameters__database_user} -p${m_parameters__database_password} -h${m_parameters__database_host} -ss -e";

url=$(echo "${m_parameters__hostname/\'/}")
url=$(echo "${url/\'/}")

$mysql_conn "update registration left join (select reg_images.registration, count(reg_images.id) as total_images from reg_images where createdOn >= subdate(current_date, 3) group by reg_images.registration) as images on images.registration = registration.registrationid set registration.image_count = images.total_images where registration.image_count != images.total_images;" ${m_parameters__database_name}


$mysql_conn "update RegistrationStatus left join ( select registration.id as id, count(reg_images.id) as total_images, max(reg_images.createdOn) as imageDate from registration left join reg_images on registration.registrationid = reg_images.registration where registration.createdDate >= subdate(current_date, 3) group by registration.registrationid ) as rectified on rectified.id = RegistrationStatus.registrationId set allImageDate = rectified.imageDate where rectified.total_images >= 4 and RegistrationStatus.registrationId = rectified.id and allimageDate != imageDate;" ${m_parameters__database_name}


#empty the file
#:> ${TEMP}


hour=`date +%H`;
minute=`date +%M`;

#if [ "${hour}" -eq "22" ] && [ "${minute}" -ge "55" ]; then
#     $DIR"/mqtt_clear_app_data.sh"
#fi


rm -f $pidfile
exit
