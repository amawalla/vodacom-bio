#!/bin/bash
#####
# @Author  Kimuyu
# @Date Tue May 2016
# 1. scripts to clear old registrations 
#
####
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo $DIR
. $DIR"/parse_yaml.sh"

pidfile="${DIR}/../old_regs.pid"
LOGDIR="${DIR}/../bashlogs/recon.log"
CONFIG="${DIR}/../app/config/parameters.yml"
TEMP="${DIR}/../vodacom_recon1.out"

echo "`date`    starting script at "; >> $LOGDIR;

eval $(parse_yaml ${CONFIG} "m_")

if test -e "$pidfile"
then
	#get the pid value from the file
	pid_id=`cat $pidfile`
	pid_count=`ps -ef|grep " $pid_id "|grep -v 'grep' | wc -l`
	if [ $pid_count -ge 1 ];then
		echo "`date` | another decline_old_registrations module is still running ($pid_id)" >> $LOG
	    exit 1
	fi
fi

mysql_conn="mysql -u${m_parameters__database_user} -p${m_parameters__database_password} -h${m_parameters__database_host} -ss -e";


$mysql_conn "update registration r left join RegistrationStatus rs on r.id=rs.registrationId set rs.fullRegStatus = 3, fullRegDesc = 'Incomplete Information', verifyState = 3, verifyDescr = 'Incomplete Information', r.sim_serial= concat(sim_serial, '_', DATE_FORMAT(r.createdDate, '%m%d%Y%H%i%s')) where r.createdDate <= subdate(current_date, 3) and r.createdDate >= '2017-11-28' and verifyState = 0 and icap_state = 0 and fullRegStatus = 0;" ${m_parameters__database_name}

rm -f $pidfile
exit

