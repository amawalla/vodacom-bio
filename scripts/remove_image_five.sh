#!/bin/bash

#####################################################
#                                                   #
# @author Anthony M K                               #
# @date Tue October 2017                            #
#                                                   #
#####################################################


SCRIPT_DIRECTORY="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo ${SCRIPT_DIRECTORY}
. ${SCRIPT_DIRECTORY}"/parse_yaml.sh"


EKYC_DIRECTORY=${SCRIPT_DIRECTORY}/..
CONFIG="${EKYC_DIRECTORY}/app/config/parameters.yml"

eval $(parse_yaml ${CONFIG} "m_")


mysql_conn="mysql -u${m_parameters__database_user} -p${m_parameters__database_password} -h${m_parameters__database_host} -ss -e";


$mysql_conn "UPDATE RegistrationStatus set verifyState = 0 WHERE verifyState = 5 AND TIMESTAMPDIFF(MINUTE, verifyDate , NOW()) > 20 AND createdDate  >= '2018-01-01 00:00:00';" ${m_parameters__database_name}

exit 1
