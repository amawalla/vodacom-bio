#!/bin/bash
#####
# @Author Oscar Makala
# @Date Tue May 816
# 1. script checks all numbers that are in registration table, but have no status. sends them to treg
# 2. checks all numbers that have been sent to treg succesfully, but have not been sent to freg or have
# been only submitted
#
####
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo $DIR
. $DIR"/parse_yaml.sh"

pidfile="${DIR}/rerecon.pid"
LOGDIR="${DIR}/bashlogs/recon.log"
CONFIG="${DIR}/../app/config/parameters.yml"
TEMP="${DIR}/vodacom_recon1.out"
days=$1
num_files=20
echo "`date`    starting script at "; >> $LOGDIR;

eval $(parse_yaml ${CONFIG} "m_")


if test -e "$pidfile"
then
        #get the pid value from the file
        pid_id=`cat $pidfile`
        pid_count=`ps -ef|grep " $pid_id "|grep -v 'grep' | wc -l`
        if [ $pid_count -ge 1 ];then
                echo "`date` | another icap resend module is still running ($pid_id)" >> $LOGDIR
                exit 1
        fi
fi


:> ${TEMP}
mysql_conn="mysql -u${m_parameters__database_user} -p${m_parameters__database_password} -h${m_parameters__database_host} -ss -e";

echo "$$" > $pidfile;

:> ${TEMP}

#query
#select registrationId,mpesa_state from RegistrationStatus where mpesa_state not in (1,3) and icap_state =6 order
#
#script checks all numbers that are in registration table, but have no status. sends them to treg
$mysql_conn "select registrationId from RegistrationStatus where mpesa_state in (0,2) and icap_state =6  and left(createdDate,10)=subdate(current_date,${days})" ${m_parameters__database_name} > ${TEMP}
count=0

if [ -s ${TEMP} ];
then
total_lines=$(wc -l <${TEMP})
((lines_per_file = (total_lines + num_files - 1) / num_files))
split --lines=${lines_per_file} ${TEMP} tmgxyzzy.
echo "Total lines     = ${total_lines}"
echo "Lines  per file = ${lines_per_file}"

for f in tmgxyzzy.*;
do
id=$(less ${f} | awk -vORS=, '{ print $1 }' | sed 's/,$/\n/')
php /var/www/html/vodacomEKYCLive/app/console rest_api:vodacom_processor_command "mpesa" "${id}" &
rm $f
sleep 2
done
fi


#script checks all numbers that are in registration table, but have no status. sends them to treg
$mysql_conn "select id From registration where id not in (select registrationId from RegistrationStatus where registrationId is NOT NULL and left(createdDate,10) = subdate(current_date,${days})) and left(createdDate,10) = subdate(current_date,${days})  order  by id desc" ${m_parameters__database_name} > ${TEMP}
if [ -s ${TEMP} ];
then
total_lines=$(wc -l <${TEMP})
((lines_per_file = (total_lines + num_files - 1) / num_files))
split --lines=${lines_per_file} ${TEMP} txyzzy.
echo "Total lines     = ${total_lines}"
echo "Lines  per file = ${lines_per_file}"

for f in txyzzy.*;
do
id=$(less ${f} | awk -vORS=, '{ print $1 }' | sed 's/,$/\n/')
php /var/www/html/vodacomEKYCLive/app/console rest_api:vodacom_processor_command "treg" "${id}" &
rm $f
done
fi
#empty the file
:> ${TEMP}
#temporaryRegStatus

#sleep 5

count=0
$mysql_conn "select r.id from registration r inner join RegistrationStatus s on s.registrationId=r.id  where s.temporaryRegStatus in (1,0)   and left(r.createdDate,10)=subdate(current_date,${days}) order  by r.id desc " ${m_parameters__database_name} > ${TEMP}
if [ -s ${TEMP} ];
then
total_lines=$(wc -l <${TEMP})
((lines_per_file = (total_lines + num_files - 1) / num_files))
split --lines=${lines_per_file} ${TEMP} txyzzy.
echo "Total lines     = ${total_lines}"
echo "Lines  per file = ${lines_per_file}"

for f in txyzzy.*;
do
id=$(less ${f} | awk -vORS=, '{ print $1 }' | sed 's/,$/\n/')
php /var/www/html/vodacomEKYCLive/app/console rest_api:vodacom_processor_command "treg" "${id}" &
rm $f
done
fi

sleep 2
#empty the file
:> ${TEMP}

#checks all numbers that have been sent to treg succesfully, but have not been sent to freg or have
# been only submitted
$mysql_conn "select registration.id from RegistrationStatus inner join registration on RegistrationStatus.registrationId = registration.id where temporaryRegStatus in (2,6) and fullRegStatus in (1,0) and left(RegistrationStatus.createdDate,10) = subdate(current_date,${days}) " ${m_parameters__database_name} > ${TEMP}

count=0
if [ -s ${TEMP} ];
then
total_lines=$(wc -l <${TEMP})
((lines_per_file = (total_lines + num_files - 1) / num_files))
split --lines=${lines_per_file} ${TEMP} fxyzzy.
echo "Total lines     = ${total_lines}"
echo "Lines  per file = ${lines_per_file}"

for f in fxyzzy.*;
do
id=$(less ${f} | awk -vORS=, '{ print $1 }' | sed 's/,$/\n/')
php /var/www/html/vodacomEKYCLive/app/console rest_api:vodacom_processor_command "freg" "${id}" &
rm $f
done
fi

rm -f $pidfile
exit 1
