#!/usr/bin/env bash


DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. $DIR"/parse_yaml.sh"

LOGDIR="${DIR}/bashlogs/bulkpush-"`date +%d%m%Y`".log"
CONFIG="${DIR}/../app/config/parameters.yml"
TEMP="${DIR}/../outfile.out"
JSONFILE="${DIR}/../file.json"

topicmessage=$1;
deviceId=$2;
json=$3;

LOGDIR="${DIR}/bashlogs/bulkpush-"`date +%d%m%Y`".log"
echo "`date` start bulk publish for topic  : ${deviceId}" >> $LOGDIR
echo "`date` start bulk publish for event  :  ${topicmessage}" >> $LOGDIR
echo "`date` start bulk publish for message: ${json:0:100}" >> $LOGDIR




eval $(parse_yaml ${CONFIG} "m_")

eventPre=${m_parameters__mqtt_topic/"{2}"/"${topicmessage}"}

mysql_conn="mysql -u${m_parameters__database_user} -p${m_parameters__database_password} -h${m_parameters__database_host} -ss -e"

$mysql_conn "select deviceId from device where deviceId = 1490016264951 and deviceId is NOT NULL and isActive=1" ${m_parameters__database_name} > ${TEMP}
#$mysql_conn "select msisdn from registration limit 10" ${m_parameters__database_name} > ${TEMP}

if [ -s ${TEMP} ];
then
while read deviceId; do
echo $deviceId
topic=$(echo ${eventPre/"{1}"/"${deviceId}"}|sed "s/'//g")
echo "`date` topic for publishing :- ${topic}" >> $LOGDIR
`mosquitto_pub -h "${m_parameters__mqtt}" -t "${topic}" -f "${json}" -q 2`
done < ${TEMP}
fi
#rm ${TEMP}
#rm ${json}

exit 1
