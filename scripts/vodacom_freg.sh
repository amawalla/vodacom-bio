#!/bin/bash
#####
# @Author Oscar Makala
# @Date Tue May 2016
# 1. script checks all numbers that are in registration table, but have no status. sends them to treg
# 2. checks all numbers that have been sent to treg succesfully, but have not been sent to freg or have
# been only submitted
#
####
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo $DIR
. $DIR"/parse_yaml.sh"

pidfile="${DIR}/recon.pid"
LOGDIR="${DIR}/bashlogs/recon.log"
CONFIG="${DIR}/../app/config/parameters.yml"
TEMP="${DIR}/vodacom_recon1.out"

:> ${TEMP}
echo "`date`    starting script at "; >> $LOGDIR;

eval $(parse_yaml ${CONFIG} "m_")


if test -e "$pidfile"
then
        #get the pid value from the file
        pid_id=`cat $pidfile`
        pid_count=`ps -ef|grep " $pid_id "|grep -v 'grep' | wc -l`
        if [ $pid_count -ge 1 ];then
                echo "`date` | another icap resend module is still running ($pid_id)" >> $LOGDIR
                exit 1
        fi
fi

mysql_conn="mysql -u${m_parameters__database_user} -p${m_parameters__database_password} -h${m_parameters__database_host} -ss -e";

echo "$$" > $pidfile;

:> ${TEMP}

#checks all numbers that have been sent to treg succesfully, but have not been sent to freg or have
# been only submitted
$mysql_conn "select  concat(if(temporaryRegDesc is null,'empty',temporaryRegDesc),',',if(fullRegDesc is null,'empty',fullRegDesc),',',registration.registrationid) from RegistrationStatus inner join registration on RegistrationStatus.registrationId = registration.id where temporaryRegStatus in (2,6) and fullRegStatus in (1,0,6)" ${m_parameters__database_name} > ${TEMP}

if [ -s ${TEMP} ];
then
while read content;
do
echo "`date`    ${content}" >> $LOGDIR;
id=$(echo $content|awk -F',' '{print $3}');
curl -i -v "https://vodalive.registersim.com/api/endpoint/rerun_freg/${id}"
sleep 3
done < ${TEMP}
rm ${TEMP}
else
echo "`date` no freg information to send to the server" >> $LOGDIR;
fi
echo "ending script at `date`";

hour=`date +%H`;
minute=`date +%M`;



rm -f $pidfile
exit 1
