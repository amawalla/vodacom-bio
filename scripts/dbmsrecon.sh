#!/usr/bin/env bash


DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo $DIR
. $DIR"/parse_yaml.sh"

pidfile="${DIR}/dbms.pid"
LOGDIR="${DIR}/bashlogs/dbms.log"
CONFIG="${DIR}/../app/config/parameters.yml"
TEMP="${DIR}/dbms.out"

:> ${TEMP}
echo "`date`    starting script at "; >> $LOGDIR;

eval $(parse_yaml ${CONFIG} "m_")


if test -e "$pidfile"
then
        #get the pid value from the file
        pid_id=`cat $pidfile`
        pid_count=`ps -ef|grep " $pid_id "|grep -v 'grep' | wc -l`
        if [ $pid_count -ge 1 ];then
                echo "`date` | another dbms resend module is still running ($pid_id)" >> $LOGDIR
                exit 1
        fi
fi

mysql_conn="mysql -u${m_parameters__database_user} -p${m_parameters__database_password} -h${m_parameters__database_host} -ss -e";

echo "$$" > $pidfile;

#query
#select registrationId,mpesa_state from RegistrationStatus where mpesa_state not in (1,3) and icap_state =6 order
#
#script checks all numbers that are in registration table, but have no status. sends them to treg
#$mysql_conn "select registrationId,fullRegStatus,icap_state from RegistrationStatus where fullRegStatus =6 and temporaryRegStatus in (6,7) and MONTH(createdDate)=3 and YEAR(createdDate)=2017  LIMIT 20 " ${m_parameters__database_name} > ${TEMP}
$mysql_conn "select registrationId,fullRegStatus,icap_state from RegistrationStatus where fullRegStatus =6 and temporaryRegStatus in (6,7) and date(createdDate) >= '2017-01-01' ORDER BY id ASC LIMIT 20 " ${m_parameters__database_name} > ${TEMP}
if [ -s ${TEMP} ];
then
while read info;
do
registration=$(echo $info|awk '{print $1}');
mpesa_state=$(echo $info|awk '{print $2}');
echo "`date` rerunning mepsa registraiton for registration : ${registration} with state ${mpesa_state}" >> $LOGDIR;
php /var/www/html/vodacomEKYCLive/app/console rest_api:vodacom_processor_command "dbms" "${registration}" &
((count++))
if [ $count -eq 100 ];then
count=0
sleep 1
fi
done < ${TEMP}
fi

rm -f $pidfile
exit 1

