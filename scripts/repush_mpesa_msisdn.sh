#!/usr/bin/env bash

batch_size=1000
COUNTER=0

numbers=""

while read mobile;
do

    if test "x${numbers}" != "x"; then
        numbers=${mobile}
    fi

    ${numbers}=${numbers}","${mobile}
    
    if ${COUNTER} -eq ${batch_size} ; then
        echo ${numbers}
    fi

    ((++COUNTER))

        id=`echo $mobile|awk -F',' '{print $1}'`
        if test "x$id" != "x";
        then
                update_temp_status "$id"
                fcontent=`echo $mobile|sed -e 's/:\/\//:|/g'|sed -e 's/\//|/g'`
                echo $fconent;
                echo "`date` | Start Executing php file for $id" >>$LOG
                /usr/bin/php /var/websites/vodacom/index.php dbms cli_execute "$fcontent" >> $PLOG &
                echo "`date` | End Executing php file" >>$LOGI
                if [ $COUNTER -eq 10 ];then
                       #sleep 3
                       COUNTER=0
                fi
                ((++COUNTER))
        fi
done < $1