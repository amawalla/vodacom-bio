#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
LOGDIR="${DIR}/bashlogs/report_generator.log"

queryString=$1
email=$2
title=$3


echo "${queryString}-${email} -${title}" >> $LOGDIR

sleep 10