#!/bin/bash

#####################################################
#                                                   #
# @author Sam Samson <samdev06@gmail.com>           #
# @date Mon August 2017                             #
#                                                   #
#####################################################


SCRIPT_DIRECTORY="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo ${SCRIPT_DIRECTORY}
. ${SCRIPT_DIRECTORY}"/parse_yaml.sh"


EKYC_DIRECTORY=${SCRIPT_DIRECTORY}/..
CONFIG="${EKYC_DIRECTORY}/app/config/parameters.yml"

eval $(parse_yaml ${CONFIG} "m_")

lastXminutes=`date --date='4 minutes ago' +%Y-%m-%d:%H:%M`

mysql_conn="mysql -u${m_parameters__database_user} -p${m_parameters__database_password} -h${m_parameters__database_host} -ss -e";

#$mysql_conn "UPDATE RegistrationStatus set verifyState = 0 WHERE verifyState IN(-1) AND TIMESTAMPDIFF(MINUTE, verifyLock , NOW()) > 5;" ${m_parameters__database_name}
#$mysql_conn "UPDATE RegistrationStatus set verifyState = 0 WHERE verifyState IN(-2) AND TIMESTAMPDIFF(MINUTE, verifyLock , NOW()) > 5;" ${m_parameters__database_name}


$mysql_conn "UPDATE RegistrationStatus set verifyState = 0 WHERE verifyDate <= (DATE_SUB(now(), INTERVAL 3 MINUTE)) AND verifyState IN(-1) AND createdDate >= subdate(current_date, 3);" ${m_parameters__database_name}

$mysql_conn "UPDATE RegistrationStatus set verifyState = 2 WHERE verifyDate <= (DATE_SUB(now(), INTERVAL 3 MINUTE)) AND verifyState IN(-2) AND createdDate >= subdate(current_date, 3);" ${m_parameters__database_name}

$mysql_conn "UPDATE RegistrationStatus set auditState = 0 WHERE auditDate <= (DATE_SUB(now(), INTERVAL 60 MINUTE)) AND auditState IN(-1) AND createdDate >= subdate(current_date, 3);" ${m_parameters__database_name}

exit 1
