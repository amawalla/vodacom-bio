#!/usr/bin/env bash


DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo $DIR
. $DIR"/parse_yaml.sh"

pidfile="${DIR}/mpesa.pid"
LOGDIR="${DIR}/bashlogs/mpesa.log"
CONFIG="${DIR}/../app/config/parameters.yml"
TEMP="${DIR}/mpesa/mpesa_mobile.out"
num_files=5
echo "`date`    starting script at "; >> $LOGDIR;

eval $(parse_yaml ${CONFIG} "m_")


if test -e "$pidfile"
then
        #get the pid value from the file
        pid_id=`cat $pidfile`
        pid_count=`ps -ef|grep " $pid_id "|grep -v 'grep' | wc -l`
        if [ $pid_count -ge 1 ];then
                echo "`date` | another icap resend module is still running ($pid_id)" >> $LOGDIR
                exit 1
        fi
fi


countR=$(ps -ef|grep 'rest_api:vodacom_processor_command mpesa'|wc -l)
if [ $countR -gt 1 ];
then
                echo "`date` | Still running another batch of files. to avoid retry" >> $LOGDIR
                exit 1
fi

mysql_conn="mysql -u${m_parameters__database_user} -p${m_parameters__database_password} -h${m_parameters__database_host} -ss -e";

echo "$$" > $pidfile;

:> ${TEMP}

#query
#select registrationId,mpesa_state from RegistrationStatus where mpesa_state not in (1,3) and icap_state =6 order
#
#script checks all numbers that are in registration table, but have no status. sends them to treg

numbers=`cat $1`

$mysql_conn "select id from registration where msisdn in(${numbers})" ${m_parameters__database_name} > ${TEMP}

count=0

if [ -s ${TEMP} ];
then
total_lines=$(wc -l <${TEMP})
((lines_per_file = (total_lines + num_files - 1) / num_files))
split --lines=${lines_per_file} ${TEMP} mpesarecon.
echo "Total lines     = ${total_lines}"
echo "Lines  per file = ${lines_per_file}"

for f in mpesarecon.*;
do
#id=$(less ${f} | awk -vORS=, '{ print $1 }' | sed 's/,$/\n/')

id=$(tr '\n' ',' < ${f})

php /var/www/html/vodacomEKYCLive/app/console rest_api:vodacom_processor_command "mpesa" "${id}" &

sleep 2
done
fi


rm -f $pidfile
exit 1

