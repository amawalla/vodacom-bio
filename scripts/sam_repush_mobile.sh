#!/usr/bin/env bash

COUNTER=0

while read registration; 
do

  php /var/www/html/vodacomEKYCLive/app/console rest_api:vodacom_processor_command "mpesa" "${registration}" &
 
  if [ $COUNTER -eq 20 ];then
	COUNTER=0
  	sleep 2;
  fi

  ((++COUNTER)) 

done < $1

exit;
