#!/bin/bash
#####
# @Author Oscar Makala
# @Date Tue May 816
# 1. script checks all numbers that are in registration table, but have no status. sends them to treg
# 2. checks all numbers that have been sent to treg succesfully, but have not been sent to freg or have
# been only submitted
#
####
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo $DIR
. $DIR"/parse_yaml.sh"

pidfile="${DIR}/recon.pid"
LOGDIR="${DIR}/bashlogs/recon.log"
CONFIG="${DIR}/../app/config/parameters.yml"
TEMP="${DIR}/vodacom_recon1.out"
num_files=10
today=$(date +"%Y-%m-%d")

mysql_database_host="eregdatabase-replica.c1menozddi24.us-east-1.rds.amazonaws.com"

echo "`date`    starting script at "; >> $LOGDIR;

eval $(parse_yaml ${CONFIG} "m_")


if test -e "$pidfile"
then
        #get the pid value from the file
        pid_id=`cat $pidfile`
        pid_count=`ps -ef|grep " $pid_id "|grep -v 'grep' | wc -l`
        if [ $pid_count -ge 1 ];then
                echo "`date` | another icap resend module is still running ($pid_id)" >> $LOGDIR
                exit 1
        fi
fi

tregCount=$(ps -ef|grep 'rest_api:vodacom_processor_command treg'|wc -l)
if [ $tregCount -gt 1 ];
then
                echo "`date` | Still running another batch of files. to avoid retry" >> $LOGDIR
                exit 1
fi




:> ${TEMP}
mysql_conn="mysql -u${m_parameters__database_user} -p${m_parameters__database_password} -h${m_parameters__database_host} -ss -e";

echo "$$" > $pidfile;
#$mysql_conn "  select registrationid From registration where id not in (select registrationId from RegistrationStatus where registrationId is NOT NULL and left(createdDate,10) = current_date) and left(createdDate,10) = current_date  order  by id desc limit 1000" ${m_parameters__database_name} > ${TEMP}
#if [ -s ${TEMP} ];
#then
#total_lines=$(wc -l <${TEMP})
#((lines_per_file = (total_lines + num_files - 1) / num_files))
#split --lines=${lines_per_file} ${TEMP} xyzzy.
#echo "Total lines     = ${total_lines}"
#echo "Lines  per file = ${lines_per_file}"

#for f in xyzzy.*;
#do
#id=$(less ${f} | awk -vORS=, '{ print $1 }' | sed 's/,$/\n/')
#php /var/www/html/vodacomEKYCLive/app/console rest_api:vodacom_processor_command "treg" "${id}" &
#done
#fi

#:> ${TEMP}

#sleep 5

count=0


$mysql_conn " UPDATE registration SET identificationType = CASE WHEN identificationType = 'National ID' THEN 3 WHEN identificationType = 'WEO/VEO/REF' THEN 14 WHEN identificationType = 'Voters ID' THEN 1 WHEN identificationType = 'Health Insurance ID' THEN 9 WHEN identificationType = 'Student ID' THEN 13 WHEN identificationType = 'Employer ID' THEN 10 WHEN identificationType = 'Drivers License' THEN 2 WHEN identificationType = 'Passport' THEN 4 WHEN identificationType = 'VAT Certificate' THEN 7 WHEN identificationType = 'Business Licence' THEN 5 WHEN identificationType IN('NSSF Pension Card', 'Pension Fund ID') THEN 11 WHEN identificationType = 'Certificate of Incorporation' THEN 8 ELSE identificationType END WHERE length(identificationType) > 2 and date(createdDate) >= '${today}'; "  ${m_parameters__database_name}


$mysql_conn "select registrationId from RegistrationStatus where temporaryRegStatus = 0 and verifyState = 1 and icap_state = 0 and icap_counter = 0 and date(createdDate) >= '2017-07-01' order by id desc limit 1000; "  ${m_parameters__database_name} > ${TEMP}

if [ -s ${TEMP} ];
then
total_lines=$(wc -l <${TEMP})
((lines_per_file = (total_lines + num_files - 1) / num_files))
#lines_per_file=5
split --lines=${lines_per_file} ${TEMP} xyzzy.
echo "Total lines     = ${total_lines}"
echo "Lines  per file = ${lines_per_file}"

for f in xyzzy.*;
do
id=$(less ${f} | awk -vORS=, '{ print $1 }' | sed 's/,$/\n/')
php /var/www/html/vodacomEKYCLive/app/console --env=prod rest_api:vodacom_processor_command "treg" "${id}" &
rm $f
#sleep 1
done

fi

sleep 5
#empty the file
:> ${TEMP}


rm -f $pidfile
exit 1
