#!/bin/bash
#####
# @Author Oscar Makala
# @Date Tue May 816
# 1. script checks all numbers that are in registration table, but have no status. sends them to treg
# 2. checks all numbers that have been sent to treg succesfully, but have not been sent to freg or have
# been only submitted
#
####
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo $DIR
. $DIR"/parse_yaml.sh"

pidfile="${DIR}/recon.pid"
LOGDIR="${DIR}/bashlogs/recon.log"
CONFIG="${DIR}/../app/config/parameters.yml"
TEMP="${DIR}/vodacom_recon1.out"
num_files=20
echo "`date`    starting script at "; >> $LOGDIR;

eval $(parse_yaml ${CONFIG} "m_")


if test -e "$pidfile"
then
        #get the pid value from the file
        pid_id=`cat $pidfile`
        pid_count=`ps -ef|grep " $pid_id "|grep -v 'grep' | wc -l`
        if [ $pid_count -ge 1 ];then
                echo "`date` | another icap resend module is still running ($pid_id)" >> $LOGDIR
                exit 1
        fi
fi
tregCount=$(ps -ef|grep 'rest_api:vodacom_processor_command treg'|wc -l)
if [ $tregCount -gt 1 ];
then
                echo "`date` | Still running another batch of files. to avoid retry" >> $LOGDIR
                exit 1
fi


:> ${TEMP}
mysql_conn="mysql -u${m_parameters__database_user} -p${m_parameters__database_password} -h${m_parameters__database_host} -ss -e";

echo "$$" > $pidfile;
#script checks all numbers that are in registration table, but have no status. sends them to treg
$mysql_conn "select registrationid From registration where id not in (select registrationId from RegistrationStatus where registrationId is NOT NULL and left(createdDate,10) = current_date) and left(createdDate,10) = current_date  order  by id desc" ${m_parameters__database_name} > ${TEMP}
if [ -s ${TEMP} ];
then
total_lines=$(wc -l <${TEMP})
((lines_per_file = (total_lines + num_files - 1) / num_files))
split --lines=${lines_per_file} ${TEMP} xyzzy.
echo "Total lines     = ${total_lines}"
echo "Lines  per file = ${lines_per_file}"

for f in xyzzy.*;
do
id=$(less ${f} | awk -vORS=, '{ print $1 }' | sed 's/,$/\n/')
php /var/www/html/vodacomEKYCLive/app/console rest_api:vodacom_processor_command "treg" "${id}" &
rm $f
done
fi
#empty the file
:> ${TEMP}
#temporaryRegStatus

#sleep 5

count=0
$mysql_conn "select r.registrationid from registration r inner join RegistrationStatus s on s.registrationId=r.id  where s.temporaryRegStatus in (1,0)   and left(r.createdDate,10)=current_date order  by r.id desc " ${m_parameters__database_name} > ${TEMP}
if [ -s ${TEMP} ];
then
total_lines=$(wc -l <${TEMP})
((lines_per_file = (total_lines + num_files - 1) / num_files))
split --lines=${lines_per_file} ${TEMP} xyzzy.
echo "Total lines     = ${total_lines}"
echo "Lines  per file = ${lines_per_file}"

for f in xyzzy.*;
do
id=$(less ${f} | awk -vORS=, '{ print $1 }' | sed 's/,$/\n/')
php /var/www/html/vodacomEKYCLive/app/console rest_api:vodacom_processor_command "treg" "${id}" &
rm $f
done
fi

sleep 2
#empty the file
:> ${TEMP}


rm -f $pidfile
exit 1
