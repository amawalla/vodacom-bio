#!/usr/bin/env bash

logfile=$1;
if [ "x$logfile" != "x" ];then
    less $logfile|while read line;
    do
        topic=$(echo $line |awk -F'|' '{print $2}');
        message=$(echo $line |awk -F'|' '{print $3}');
        mosquitto_pub -h "messaging.registersim.com" -t "${topic}" -m "${message}"
    done
fi