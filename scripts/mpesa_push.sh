#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo ${DIR}
. ${DIR}"/parse_yaml.sh"

pidfile="${DIR}/mpesa.pid"
LOGDIR="${DIR}/bashlogs/mpesa.log"
CONFIG="${DIR}/../app/config/parameters.yml"
TEMP="${DIR}/mpesa.out"
num_files=5

lastXminutes=`date --date='4 minutes ago' +%Y-%m-%d:%H:%M`
todaysDate=`date +%Y-%m-%d`

echo "`date`    starting script at "; >> ${LOGDIR};

eval $(parse_yaml ${CONFIG} "m_")


if test -e "$pidfile"
then
#get the pid value from the file
pid_id=`cat ${pidfile}`
pid_count=`ps -ef|grep " $pid_id "|grep -v 'grep' | wc -l`
if [ ${pid_count} -ge 1 ];then
echo "`date` | another icap resend module is still running ($pid_id)" >> ${LOGDIR}
exit 1
fi
fi

countR=$(ps -ef|grep 'rest_api:vodacom_processor_command mpesa'|wc -l)
if [ ${countR} -gt 1 ];
then
echo "`date` | Still running another batch of files. to avoid retry" >> ${LOGDIR}
exit 1
fi

mysql_conn="mysql -u${m_parameters__database_user} -h${m_parameters__database_host} -ss -e";
echo "$$" > ${pidfile};

:> ${TEMP}

#query
#select registrationId,mpesa_state from RegistrationStatus where mpesa_state not in (1,3) and icap_state =6 order
#
#script checks all numbers that are in registration table, but have no status. sends them to treg
${mysql_conn} "UPDATE RegistrationStatus SET mpesa_state = 0 WHERE mpesa_state = -1 AND verifyState = 1 AND createdDate >= subdate(current_date, 1) AND createdDate <= '${lastXminutes}';" ${m_parameters__database_name}

currentDate=`date +%Y%m%d`

#${mysql_conn} "select e.registrationId from (select date_number_createdDate, registrationId from RegistrationStatus where verifyState = 1 AND mpesa_state in (0,2) AND icap_state = 6 and fullRegStatus  in(6,7) order by id desc limit 4000) as e where e.date_number_createdDate >= subdate(current_date, 1) order by e.registrationId asc limit 400" ${m_parameters__database_name} > ${TEMP}

${mysql_conn} "select registrationId from RegistrationStatus where verifyState = 1 AND mpesa_state in (0,2) AND icap_state = 6 and createdDate >= subdate(current_date, 1) limit 1000;" ${m_parameters__database_name} > ${TEMP}

count=1

if [ -s ${TEMP} ];
then
total_lines=$(wc -l <${TEMP})
((lines_per_file = (total_lines + num_files - 1) / num_files))
lines_per_file=3
split --lines=${lines_per_file} ${TEMP} mgxyzzy.
echo "Total lines     = ${total_lines}"
echo "Lines  per file = ${lines_per_file}"

for f in mgxyzzy.*;
do
id=$(less ${f} | awk -vORS=, '{ print $1 }' | sed 's/,$/\n/')

${mysql_conn} "UPDATE RegistrationStatus SET mpesa_state = -1, mpesaDate = now() WHERE RegistrationStatus.registrationId IN (${id}) " ${m_parameters__database_name}
echo "Count is ${count}";

php /var/www/html/environments/simreg/dev/app/console rest_api:vodacom_processor_command "mpesa" "${id}" &

if [ ${count} -ge 2 ]; then
    count=0;
    sleep 1
fi

count=$(( $count + 1 ))

rm ${f}
#sleep 1
done
fi

rm -f ${pidfile}
exit 1

