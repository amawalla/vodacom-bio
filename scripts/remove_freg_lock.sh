#!/bin/bash

#####################################################
#                                                   #
# @author LG          				    #
# @date Tue September 2017                          #
# Unlock registrations 				    #
# if 4 minutes passed without callback              #
#####################################################


SCRIPT_DIRECTORY="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo ${SCRIPT_DIRECTORY}
. ${SCRIPT_DIRECTORY}"/parse_yaml.sh"


EKYC_DIRECTORY=${SCRIPT_DIRECTORY}/..
CONFIG="${EKYC_DIRECTORY}/app/config/parameters.yml"

eval $(parse_yaml ${CONFIG} "m_")

lastXminutes=`date --date='4 minutes ago' +%Y-%m-%d:%H:%M`

mysql_conn="mysql -u${m_parameters__database_user} -p${m_parameters__database_password} -h${m_parameters__database_host} -ss -e";

$mysql_conn "UPDATE RegistrationStatus set fullRegStatus = 0 WHERE date (createdDate) >= subdate(current_date, 1) and date(fregDate) <= '${lastXminutes}' AND fullRegStatus IN(-1) AND verifyState = 1;" ${m_parameters__database_name} 

exit 1

