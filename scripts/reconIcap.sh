#!/usr/bin/env bash


DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo $DIR
. $DIR"/parse_yaml.sh"

CONFIG="${DIR}/../app/config/parameters.yml"
TEMP="${DIR}/reconicap.out"
num_files=10

eval $(parse_yaml ${CONFIG} "m_")
mysql_conn="mysql -u${m_parameters__database_user} -p${m_parameters__database_password} -h${m_parameters__database_host} -ss -e";

$mysql_conn "select firstName,lastName,msisdn,identification,left(registration.createdDate,10),registration.id from RegistrationStatus inner join registration on RegistrationStatus.registrationId =registration.id where RegistrationStatus.temporaryRegStatus =4 and temporaryRegDesc ='Fully Registered iCAP' order by registration.id desc " ${m_parameters__database_name} > ${TEMP}


if [ -s ${TEMP} ];
then
less "${TEMP}"|while read line;
do
firstName=$(echo $line|awk '{print $1}')
lastName=$(echo $line|awk '{print $2}')
msisdn=$(echo $line|awk '{print $3}')
identification=$(echo $line|awk '{print $4}')
createdDate=$(echo $line|awk '{print $5}')
id=$(echo $line|awk '{print $6}')
result=`curl -X POST -H "Authorization: Basic ZWt5YzI6ZWt5Y1Jlc3RmdWxsVXNlcjEyM0AzMjEj" -H "Accept: application/json" -H "Cache-Control: no-cache" -H "Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW" -F "mobileNumber=255${msisdn}" "http://middleware1.registersim.com:8080/VodaGateway/rest/services/vodaGateway/verifyICAP"`
if [ "x$result" != "x" ];then
xfirstName=$(echo $result|grep -P -o '(?<=firstName":")[a-zA-Z]*(?=")')
xlastName=$(echo $result|grep -P -o '(?<=lastName":")[a-zA-Z]*(?=")')
xxmsisdn=$(echo $result|grep -P -o '(?<=MSISDN":")[0-9]*(?=")')
xmsisdn=${xxmsisdn:(-9)}
xidentification=$(echo $result|grep -P -o '(?<=ID_NUMBER":")[a-zA-Z0-9]*(?=")')
xxcreatedDate=$(echo $result|grep -P -o '(?<=registrationDate":")[0-9-\s:]*(?=")')
xcreatedDate=${xxcreatedDate:0:10}
if [ "${msisdn}" == "${xmsisdn}" ];then
if [ "${firstName}" == "${xfirstName}" ];then
if [ "${lastName}" == "${xlastName}" ];then
if [ "${identification}" == "${xidentification}" ];then
if [ "${createdDate}" == "${xcreatedDate}" ];then
echo "${id}" >> "${DIR}/reconicap.final"
fi
fi
fi
fi
fi
fi
done
fi

