#!/usr/bin/env bash


DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo $DIR
. $DIR"/parse_yaml.sh"

pidfile="${DIR}/mpesa.pid"
LOGDIR="${DIR}/bashlogs/mpesa.log"
CONFIG="${DIR}/../app/config/parameters.yml"
TEMP="${DIR}/retrympesa.out"
num_files=10
echo "`date`    starting script at "; >> $LOGDIR;
days=$1
eval $(parse_yaml ${CONFIG} "m_")

:> ${TEMP}

#query
#select registrationId,mpesa_state from RegistrationStatus where mpesa_state not in (1,3) and icap_state =6 order
#i

mysql_conn="mysql -u${m_parameters__database_user} -p${m_parameters__database_password} -h${m_parameters__database_host} -ss -e";
#script checks all numbers that are in registration table, but have no status. sends them to treg
$mysql_conn "select registrationId from RegistrationStatus where mpesa_state in (0) and icap_state =6  and left(createdDate,10)=subdate(current_date,${days})" ${m_parameters__database_name} > ${TEMP}
count=0

if [ -s ${TEMP} ];
then
total_lines=$(wc -l <${TEMP})
((lines_per_file = (total_lines + num_files - 1) / num_files))
split --lines=${lines_per_file} ${TEMP} mgxyzzy.
echo "Total lines     = ${total_lines}"
echo "Lines  per file = ${lines_per_file}"

for f in mgxyzzy.*;
do
id=$(less ${f} | awk -vORS=, '{ print $1 }' | sed 's/,$/\n/')
php /var/www/html/vodacomEKYCLive/app/console rest_api:vodacom_processor_command "retrympesa" "${id}" &
rm $f
sleep 5
done
fi


exit 1


