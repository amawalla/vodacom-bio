<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 12/14/15
 * Time: 3:08 PM
 */

namespace aimgroup\RestApiBundle\Event;

use aimgroup\RestApiBundle\Entity\RegImages;
use Symfony\Component\EventDispatcher\Event;


class ImagesEvent extends Event
{

    protected $regImages;

    public function __construct(RegImages $regImages)
    {
        $this->regImages = $regImages;
    }

    public function getRegImages()
    {
        return $this->regImages;
    }
}