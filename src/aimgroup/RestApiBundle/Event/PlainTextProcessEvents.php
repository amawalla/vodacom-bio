<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 12/14/15
 * Time: 2:55 PM
 */

namespace aimgroup\RestApiBundle\Event;


final class PlainTextProcessEvents
{
    const PROCESS_PLAINTEXT = "process.plaintext";

}