<?php 
/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\RestApiBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use aimgroup\RestApiBundle\Entity\User;

class UserEvent extends Event{

	private $user;

	public function __construct(User $user){
             $this->user = $user;
	}

	public function getUser(){
		return $this->user;
	}
}
