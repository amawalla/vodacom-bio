<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace aimgroup\RestApiBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use aimgroup\RestApiBundle\Entity\Role;
use aimgroup\RestApiBundle\Entity\User;
/**
 * Description of Group
 *
 * @author Michael Tarimo
 */

/**
 * @ORM\Table(name="ereg_group")
 * @ORM\Entity()
 */
class Group implements \FOS\UserBundle\Model\GroupInterface {
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /** @ORM\Column(name="group_name", type="string", length=30) */
    private $groupName;
    /** @ORM\Column(name="group_code", type="string", length=20, unique=true) */
    private $groupCode;
    /**
    * @ORM\ManyToMany(targetEntity="Role", inversedBy="groups")
    */
    private $roles;
    
    /**
    * @ORM\ManyToMany(targetEntity="User", mappedBy="groups")
    */
    private $users;
    
    public function __construct(){
        $this->roles = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    public function getId() {
        return $this->id;
    }

    public function getGroupName() {
        return $this->groupName;
    }
    public function getName() {
        return $this->groupName;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setName($name) {
        $this->groupName = $name;
    }

    public function setUsers($users) {
        $this->users = $users;
    }

    public function getGroupCode() {
        return $this->groupCode;
    }
    
    public function setGroupCode($group){
        $this->groupCode = $group;
        return $this;
    }

    public function hasRole($role){
        
     return false;
    }
    public function addRole($role){
        
     return $this;
    }

    public function setRoles(array $roles){
        $this->roles = $roles;
    }

    public function getRoles(){
        return $this->roles->toArray();
    }

    public function removeRole($role){
      return $this->roles->removeElement($role);  
    }
    
    public function getUsers(){
        return $this->users;
    }
    
    public function getGroupCanonical(){
        return array(
            'groupId'=>  $this->getId(),
            'groupName'=> $this->getName(),
            'groupCode'=> $this->getGroupCode()
             
        );
    }
    
    public function getRolesCanonical(){
        $roles = array();
        foreach ($this->roles as $role){
            array_push($roles,$role->toCanonical());
        }
        
        return $roles;
    }
    
}
