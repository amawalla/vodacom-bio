<?php
/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 17/10/2016
 * Time: 10:53
 */

namespace aimgroup\RestApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Configuration
 * @package aimgroup\RestApiBundle\Entity
 * @ORM\Entity
 */
class OperatorConfiguration
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     *
     */
    private $id;


    /**
     * @ORM\Column(name="name", type="string",length= 40)
     */
    private $name;
    /**
     * @ORM\Column(name="prefix", type="string",length= 40)
     */
    private $prefix;

    /**
     * @ORM\Column(name="configUrl", type="string")
     */
    private $configUrl;


    /**
     * @ORM\Column(name="tokenAuthUrl", type="string")
     */
    private $tokenAuthUrl;

    /**
     * @ORM\Column(name="passwordSetUrl", type="string")
     */
    private $passwordSetUrl;

    /**
     * @ORM\Column(name="signatureSaveUrl", type="string")
     */
    private $signatureSaveUrl;
    
    
    /**
     * @ORM\Column(name="commandStatusUrl", type="string")
     */
    private $commandStatusUrl;


    /**
     * @ORM\Column(name="reportUrl", type="string")
     */
    private $reportUrl;

    /**
     * @ORM\Column(name="icon", type="string")
     */
    private $icon;

    /**
     * @ORM\Column(name="promptRegType", type="string")
     */
    private $promptRegType;

    /**
     * @ORM\Column(name="simSerialPrefix", type="string")
     */
    private $simSerialPrefix;

    /**
     * @ORM\Column(name="color", type="string")
     */
    private $color;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * @param mixed $prefix
     */
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;
    }

    /**
     * @return mixed
     */
    public function getConfigUrl()
    {
        return $this->configUrl;
    }

    /**
     * @param mixed $configUrl
     */
    public function setConfigUrl($configUrl)
    {
        $this->configUrl = $configUrl;
    }

    /**
     * @return mixed
     */
    public function getTokenAuthUrl()
    {
        return $this->tokenAuthUrl;
    }

    /**
     * @param mixed $tokenAuthUrl
     */
    public function setTokenAuthUrl($tokenAuthUrl)
    {
        $this->tokenAuthUrl = $tokenAuthUrl;
    }

    /**
     * @return mixed
     */
    public function getPasswordSetUrl()
    {
        return $this->passwordSetUrl;
    }

    /**
     * @param mixed $passwordSetUrl
     */
    public function setPasswordSetUrl($passwordSetUrl)
    {
        $this->passwordSetUrl = $passwordSetUrl;
    }

    /**
     * @return mixed
     */
    public function getSignatureSaveUrl()
    {
        return $this->signatureSaveUrl;
    }

    /**
     * @param mixed $signatureSaveUrl
     */
    public function setSignatureSaveUrl($signatureSaveUrl)
    {
        $this->signatureSaveUrl = $signatureSaveUrl;
    }

    /**
     * @param mixed $commandStatusUrl
     */
    public function setCommandStatusUrl($commandStatusUrl)
    {
        $this->commandStatusUrl = $commandStatusUrl;
    }

    /**
     * @return mixed
     */
    public function getReportUrl()
    {
        return $this->reportUrl;
    }

    /**
     * @param mixed $reportUrl
     */
    public function setReportUrl($reportUrl)
    {
        $this->reportUrl = $reportUrl;
    }

    /**
     * @return mixed
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param mixed $icon
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
    }

    /**
     * @return mixed
     */
    public function getPromptRegType()
    {
        return $this->promptRegType;
    }

    /**
     * @param mixed $promptRegType
     */
    public function setPromptRegType($promptRegType)
    {
        $this->promptRegType = $promptRegType;
    }

    /**
     * @return mixed
     */
    public function getSimSerialPrefix()
    {
        return $this->simSerialPrefix;
    }

    /**
     * @param mixed $simSerialPrefix
     */
    public function setSimSerialPrefix($simSerialPrefix)
    {
        $this->simSerialPrefix = $simSerialPrefix;
    }

    /**
     * @return mixed
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param mixed $color
     */
    public function setColor($color)
    {
        $this->color = $color;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }




}