<?php
/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\RestApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * Description of Country
 *
 * @author robert
 */

/**
 * @ORM\Table(name="country")
 * @ORM\Entity
 */

class Country {
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="country_name", type="string", nullable=false)
     */
    private $countryName;
    
    /**
     * @ORM\Column(name="area_code", type="string", nullable=false)
     */
    private $areaCode;
    
    /**
     * @ORM\Column(name="signature", type="string",length = 5, nullable=false)
     */
    private $signature;
    
    /**
     * @ORM\Column(name="country_phone_char_length", type="integer", nullable=false)
     */
    private $countryPhoneCharLength;
    
    /**
     * @ORM\Column(name="status", type="smallint", nullable=false)
     */
    private $status;
    
    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;
    
    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;
    
     public function setId($id){
        $this->id = $id;
    }
    
    public function getId(){
        return $this->id;
    }
    
    public function setCountryName($name){
        $this->countryName = $name;
        return $this;
    }
    
    public function getCountryName(){
        return $this->countryName;
    }
    
    public function setAreaCode($code){
        $this->areaCode = $code;
        return $this;
    }
    
    public function getAreaCode(){
        return $this->areaCode;
    }
    
    public function setSignature($signature){
        $this->signature = $signature;
        return $this;
    }
    
    public function getSignature(){
        return $this->signature;
    }
    
    public function setCountryPhoneCharLength($length){
        $this->countryPhoneCharLength = $length;
        return $this;
    }
    
    public function getCountryPhoneCharLength(){
        return $this->countryPhoneCharLength;
    }
    
    public function setStatus($status){
        $this->status = $status;
        return $this;
    }
    
    public function getStatus(){
        return $this->status;
    }
    
    public function setCreatedAt(\DateTime $datetime){
        $this->createdAt = $datetime;
        return $this;
    }
    
    public function getCreatedAt(){
        return $this->createdAt;
    }
    
    public function setUpdatedAt(\DateTime $datetime){
        $this->updatedAt = $datetime;
        return $this;
    }
    
    public function getUpdatedAt(){
        return $this->updatedAt;
    }
}
