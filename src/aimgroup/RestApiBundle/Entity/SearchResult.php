<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\RestApiBundle\Entity;

/**
 * Description of SearchResult
 *
 * @author Michael Tarimo
 */
class SearchResult {
    
    private $products;
    private $artists;
    
    
    function getProducts() {
        return $this->products;
    }

    function getArtists() {
        return $this->artists;
    }

    function setProducts($products) {
        $this->products = $products;
    }

    function setArtists($artists) {
        $this->artists = $artists;
    }
}