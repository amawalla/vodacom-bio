<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\RestApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Description of CountryIPRange
 *
 * @author robert
 */

/**
 * @ORM\Table(name="country_ip_ranges")
 * @ORM\Entity
 */
class CountryIPRange {
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="lower_range", type="string",length= 40, nullable=false)
     */
    private $lowerRange;
    
     /**
     * @ORM\Column(name="higher_range", type="string",length= 40, nullable=false)
     */
    private $higherRange;
    
     /**
     * @ORM\Column(name="latitude", type="string",length= 10, nullable=false)
     */
    private $latitude;
    
     /**
     * @ORM\Column(name="longitude", type="string",length= 10, nullable=false)
     */
    private $longitude;
    
     /**
     * @ORM\Column(name="country_signature", type="string",length= 3, nullable=false)
     */
    private $countrySignature;
    
     /**
     * @ORM\Column(name="country_name", type="string",length= 50, nullable=false)
     */
    private $countryName;
    
     /**
     * @ORM\Column(name="status", type="smallint", nullable=false)
     */
    private $status;
    
    /**
     * @ORM\Column(name="timestamp", type="datetime", nullable=false)
     */
    private $timestamp;
    
    /**
     * 
     * @return integer
     */
    public function getId(){
        return $this->id;
    }
    
    /**
     * 
     * @return string
     */
    public function getLowerRange(){
        return $this->lowerRange;
    }
    
    /**
     * 
     * @return string
     */
    public function getHigherRange(){
        return $this->higherRange;
    }
    
    /**
     * 
     * @return string
     */
    public function getLatitude(){
        return $this->latitude;
    }
    
    /**
     * 
     * @return string
     */
    public function getLongitude(){
        return $this->longitude;
    }
    
    /**
     * 
     * @return string
     */
    public function getCountrySignature(){
        return $this->countrySignature;
    }
    
    /**
     * 
     * @return string
     */
    public function getCountryName(){
        return $this->countryName;
    }
    
    /**
     * 
     * @return integer
     */
    public function getStatus(){
        return $this->status;
    }
    
    /**
     * 
     * @return \DateTime
     */
    public function getTimestamp(){
        return $this->timestamp;
    }
}
