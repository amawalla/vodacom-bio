<?php

namespace aimgroup\RestApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RegistrationStatus
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="aimgroup\RestApiBundle\Repository\RegistrationStatusRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class RegistrationStatus {

    const TREG = 2;
    const FREG = 1;
    const FAILED_DUE_VALIDATION = 99;
    const FAILED = 4;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     *
     * @ORM\OneToOne(targetEntity="aimgroup\RestApiBundle\Entity\Registration")
     * @ORM\JoinColumn(name="registrationId", referencedColumnName="id")
     */
    private $registrationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="temporaryRegStatus", type="smallint")
     */
    private $temporaryRegStatus = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="fullRegStatus", type="smallint")
     */
    private $fullRegStatus = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="temporaryRegDesc", type="string", length=255, nullable=true)
     */
    private $temporaryRegDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="fullRegDesc", type="string", length=255,nullable=true)
     */
    private $fullRegDesc;

    /**
     * @var integer
     *
     * @ORM\Column(name="icap_state", type="smallint",nullable=true)
     */
    private $icapState = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="icap_counter", type="integer",nullable=true)
     */
    private $icapCounter = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="mpesa_state", type="smallint",nullable=true)
     */
    private $mpesaState = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="dbms_state", type="smallint",nullable=true)
     */
    private $dbmsState = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="reg_type", type="smallint",nullable=true)
     */
    private $regType;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $createdDate;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $mpesaDate;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $tregDate;


    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $dbmsDate;
    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $fregDate;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $allimageDate;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $verifyDate;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $originalVerifyDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="verifyState", type="smallint",nullable=true)
     */
    private $verifyState = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="verifyDescr", type="string", length=255, nullable=true)
     */
    private $verifyDescr;

    /**
     * @var string
     *
     * @ORM\Column(name="originalVerifyDescr", type="string", length=255, nullable=true)
     */
    private $originalVerifyDescr;

    /**
     *
     * @ORM\ManyToOne(targetEntity="aimgroup\RestApiBundle\Entity\User")
     * @ORM\JoinColumn(name="verifyBy", referencedColumnName="id")
     */
    private $verifyBy;

    /**
     *
     * @ORM\ManyToOne(targetEntity="aimgroup\RestApiBundle\Entity\User")
     * @ORM\JoinColumn(name="originalVerifyBy", referencedColumnName="id")
     */
    private $originalVerifyBy;


    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $auditDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="auditState", type="smallint",nullable=true)
     */
    private $auditState = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="auditDescr", type="string", length=255, nullable=true)
     */
    private $auditDescr;

    /**
     * @var string
     *
     * @ORM\Column(name="verifyLock", type="string", length=255, nullable=true)
     */
    private $verifyLock;

    /**
     *
     * @ORM\ManyToOne(targetEntity="aimgroup\RestApiBundle\Entity\User")
     * @ORM\JoinColumn(name="auditBy", referencedColumnName="id")
     */
    private $auditBy;


    /**
     * @var string
     *
     * @ORM\Column(name="registry_type", type="smallint")
     */
    private $registryType;


    public function __construct() {
        $this->createdDate = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set registrationId
     *
     * @param integer $registrationId
     * @return RegistrationStatus
     */
    public function setRegistrationId($registrationId) {
        $this->registrationId = $registrationId;

        return $this;
    }

    /**
     * Get registrationId
     *
     * @return mixed
     */
    public function getRegistrationId() {
        return $this->registrationId;
    }

    /**
     * Set temporaryRegStatus
     *
     * @param integer $temporaryRegStatus
     * @return RegistrationStatus
     */
    public function setTemporaryRegStatus($temporaryRegStatus) {
        $this->temporaryRegStatus = $temporaryRegStatus;

        return $this;
    }

    /**
     * Get temporaryRegStatus
     *
     * @return integer
     */
    public function getTemporaryRegStatus() {
        return $this->temporaryRegStatus;
    }

    /**
     * Set fullRegStatus
     *
     * @param integer $fullRegStatus
     * @return RegistrationStatus
     */
    public function setFullRegStatus($fullRegStatus) {
        $this->fullRegStatus = $fullRegStatus;

        return $this;
    }

    /**
     * Get fullRegStatus
     *
     * @return integer
     */
    public function getFullRegStatus() {
        return $this->fullRegStatus;
    }

    /**
     * Set temporaryRegDesc
     *
     * @param string $temporaryRegDesc
     * @return RegistrationStatus
     */
    public function setTemporaryRegDesc($temporaryRegDesc) {
        $this->temporaryRegDesc = $temporaryRegDesc;

        return $this;
    }

    /**
     * Get temporaryRegDesc
     *
     * @return string
     */
    public function getTemporaryRegDesc() {
        return $this->temporaryRegDesc;
    }

    /**
     * Set fullRegDesc
     *
     * @param string $fullRegDesc
     * @return RegistrationStatus
     */
    public function setFullRegDesc($fullRegDesc) {
        $this->fullRegDesc = $fullRegDesc;

        return $this;
    }

    /**
     * Get fullRegDesc
     *
     * @return string
     */
    public function getFullRegDesc() {
        return $this->fullRegDesc;
    }

    /**
     * Set icapState
     *
     * @param integer $icapState
     * @return RegistrationStatuses
     */
    public function setIcapState($icapState) {
        $this->icapState = $icapState;

        return $this;
    }

    /**
     * Get icapState
     *
     * @return integer
     */
    public function getIcapState() {
        return $this->icapState;
    }

    /**
     * Set mpesaState
     *
     * @param integer $mpesaState
     * @return RegistrationStatuses
     */
    public function setMpesaState($mpesaState) {
        $this->mpesaState = $mpesaState;

        return $this;
    }

    /**
     * Get mpesaState
     *
     * @return integer
     */
    public function getMpesaState() {
        return $this->mpesaState;
    }

    /**
     * Set dbmsState
     *
     * @param integer $dbmsState
     * @return RegistrationStatuses
     */
    public function setDbmsState($dbmsState) {
        $this->dbmsState = $dbmsState;

        return $this;
    }

    /**
     * Get dbmsState
     *
     * @return integer
     */
    public function getDbmsState() {
        return $this->dbmsState;
    }

    /**
     * Set regType
     *
     * @param integer $regType
     * @return RegistrationStatuses
     */
    public function setRegType($regType) {
        $this->regType = $regType;

        return $this;
    }

    /**
     * Get regType
     *
     * @return integer
     */
    public function getRegType() {
        return $this->regType;
    }

    /**
     * @return mixed
     */
    public function getCreatedDate() {
        return $this->createdDate;
    }

    /**
     * @ORM\PrePersist
     * @param \Datetime $createdDate
     * @return Task
     */
    public function setCreatedDate() {
        if (!isset($this->createdDate)) {
            $this->createdDate = new \DateTime;
        }
    }

    /**
     * @return mixed
     */
    public function getMpesaDate() {
        return $this->mpesaDate;
    }

    /**
     * @ORM\PrePersist
     * @param \Datetime $mpesaDate
     * @return Task
     */
    public function setMpesaDate() {
        if (!isset($this->mpesaDate)) {
            $this->mpesaDate = new \DateTime;
        }
    }


    /**
     * @return mixed
     */
    public function getTregDate() {
        return $this->tregDate;
    }

    /**
     * Set tregDate
     *
     * @param integer $tregDate
     * @return RegistrationStatuses
     */
    public function setTregDate($tregDate) {
        $this->tregDate = $tregDate;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getFregDate() {
        return $this->fregDate;
    }

    /**
     * Set fregDate
     *
     * @param integer $fregDate
     * @return RegistrationStatuses
     */
    public function setFregDate($fregDate) {
        $this->tregDate = $fregDate;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getAllimageDate() {
        return $this->allimageDate;
    }

    /**
     * Set allimageDate
     *
     * @param integer $allimageDate
     * @return RegistrationStatuses
     */
    public function setAllimageDate($allimageDate) {
        $this->allimageDate = $allimageDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getVerifyDate() {
        return $this->verifyDate;
    }

    /**
     * Set verifyDate
     *
     * @param integer $verifyDate
     * @return RegistrationStatuses
     */
    public function setVerifyDate($verifyDate) {
        $this->verifyDate = $verifyDate;

        return $this;
    }

    /**
     * Set verifyState
     *
     * @param integer $verifyState
     * @return RegistrationStatuses
     */
    public function setVerifyState($verifyState) {
        $this->verifyState = $verifyState;

        return $this;
    }

    /**
     * Get verifyState
     *
     * @return integer
     */
    public function getVerifyState() {
        return $this->verifyState;
    }

    /**
     * Set verifyDescr
     *
     * @param string $verifyDescr
     * @return RegistrationStatus
     */
    public function setVerifyDescr($verifyDescr) {
        $this->verifyDescr = $verifyDescr;

        return $this;
    }

    /**
     * Get verifyDescr
     *
     * @return string
     */
    public function getVerifyDescr() {
        return $this->verifyDescr;
    }

    /**
     *
     * @return User
     */
    public function getVerifyBy() {
        return $this->verifyBy;
    }

    /**
     *
     * @param $verifyBy
     */
    public function setVerifyBy($verifyBy) {
        $this->verifyBy = $verifyBy;
    }


    /**
     * @return mixed
     */
    public function getAuditDate() {
        return $this->auditDate;
    }

    /**
     * Set auditDate
     *
     * @param integer $auditDate
     * @return RegistrationStatuses
     */
    public function setAuditDate($auditDate) {
        $this->auditDate = $auditDate;

        return $this;
    }

    /**
     * Set auditState
     *
     * @param integer $auditState
     * @return RegistrationStatuses
     */
    public function setAuditState($auditState) {
        $this->auditState = $auditState;

        return $this;
    }

    /**
     * Get auditState
     *
     * @return integer
     */
    public function getAuditState() {
        return $this->auditState;
    }

    /**
     * Set auditDescr
     *
     * @param string $auditDescr
     * @return RegistrationStatus
     */
    public function setAuditDescr($auditDescr) {
        $this->auditDescr = $auditDescr;

        return $this;
    }

    /**
     * Get auditDescr
     *
     * @return string
     */
    public function getAuditDescr() {
        return $this->auditDescr;
    }

    /**
     *
     * @return User
     */
    public function getAuditBy() {
        return $this->auditBy;
    }

    /**
     * @return mixed
     */
    public function getDbmsDate() {
        return $this->dbmsDate;
    }

    /**
     * @param mixed $dbmsDate
     */
    public function setDbmsDate($dbmsDate) {
        $this->dbmsDate = $dbmsDate;
    }

    /**
     *
     * @param User $auditBy
     */
    public function setAuditBy(User $auditBy) {
        $this->auditBy = $auditBy;
    }

    function getOriginalVerifyDate() {
        return $this->originalVerifyDate;
    }

    function getOriginalVerifyDescr() {
        return $this->originalVerifyDescr;
    }

    function getOriginalVerifyBy() {
        return $this->originalVerifyBy;
    }

    function setOriginalVerifyDate($originalVerifyDate) {
        $this->originalVerifyDate = $originalVerifyDate;
    }

    function setOriginalVerifyDescr($originalVerifyDescr) {
        $this->originalVerifyDescr = $originalVerifyDescr;
    }

    function setOriginalVerifyBy($originalVerifyBy) {
        $this->originalVerifyBy = $originalVerifyBy;
    }

    /**
     * @return int
     */
    public function getIcapCounter() {
        return $this->icapCounter;
    }

    /**
     * @param int $icapCounter
     */
    public function setIcapCounter($icapCounter) {
        $this->icapCounter = $icapCounter;
    }


    public function setRegistryType($type) {

        $this->registryType = $type;
    }


    public function getRegistryType() {

        return $this->registryType;
    }

}
