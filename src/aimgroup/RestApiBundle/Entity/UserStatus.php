<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\RestApiBundle\Entity;

/**
 * Description of UserStatus
 *
 * @author Michael Tarimo
 */
final class UserStatus {

    const ACTIVE = 'active';
    const INACTIVE = 'inactive';
}