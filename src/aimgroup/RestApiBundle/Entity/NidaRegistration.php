<?php

namespace aimgroup\RestApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NidaRegistration
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class NidaRegistration
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdDate", type="datetime")
     */
    private $createdDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deviceSentTime", type="datetime")
     */
    private $deviceSentTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="registeredTime", type="datetime")
     */
    private $registeredTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="msisdn", type="integer")
     */
    private $msisdn;

    /**
     * @var integer
     *
     * @ORM\Column(name="iccid", type="string", length=255)
     */
    private $iccid;

    /**
     * @var integer
     *
     * @ORM\Column(name="nin", type="string", length=255)
     */
    private $nin;

    /**
     * @var string
     *
     * @ORM\Column(name="fingerPrintCode", type="string", length=255)
     */
    private $fingerPrintCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="data", type="string", length=255)
     */
    private $data = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="registrationId", type="string", length=255)
     */
    private $registrationId;

    /**
     * @ORM\Column(type="string",nullable=true)
     */
    protected $deviceId;
    
    /**
     * @ORM\Column(type="string",nullable=true)
     */
    protected $appVersion;

    /**
     *
     * @ORM\ManyToOne(targetEntity="aimgroup\RestApiBundle\Entity\User")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $languageCode;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdDate
     *
     * @ORM\PrePersist
     * @param \DateTime $createdDate
     *
     * @return NidaRegistration
     */
    public function setCreatedDate()
    {
        if(!isset($this->createdDate)){
            $this->createdDate = new \DateTime();
        }

        return $this;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }


    /**
     * @return \DateTime
     */
    public function getDeviceSentTime()
    {
        return $this->deviceSentTime;
    }

    /**
     * @param \DateTime $deviceSentTime
     */
    public function setDeviceSentTime($deviceSentTime)
    {
        $this->deviceSentTime = $deviceSentTime;
    }

    /**
     * @return \DateTime
     */
    public function getRegisteredTime()
    {
        return $this->registeredTime;
    }

    /**
     * @param \DateTime $registeredTime
     */
    public function setRegisteredTime($registeredTime)
    {
        $this->registeredTime = $registeredTime;
    }

    /**
     * Set msisdn
     *
     * @param integer $msisdn
     *
     * @return NidaRegistration
     */
    public function setMsisdn($msisdn)
    {
        $this->msisdn = $msisdn;

        return $this;
    }

    /**
     * Get msisdn
     *
     * @return integer
     */
    public function getMsisdn()
    {
        return $this->msisdn;
    }

    /**
     * Set iccid
     *
     * @param integer $iccid
     *
     * @return NidaRegistration
     */
    public function setIccid($iccid)
    {
        $this->iccid = $iccid;

        return $this;
    }

    /**
     * Get iccid
     *
     * @return integer
     */
    public function getIccid()
    {
        return $this->iccid;
    }

    /**
     * Set nin
     *
     * @param integer $nin
     *
     * @return NidaRegistration
     */
    public function setNin($nin)
    {
        $this->nin = $nin;

        return $this;
    }

    /**
     * Get nin
     *
     * @return integer
     */
    public function getNin()
    {
        return $this->nin;
    }

    /**
     * Set fingerPrintCode
     *
     * @param string $fingerPrintCode
     *
     * @return NidaRegistration
     */
    public function setFingerPrintCode($fingerPrintCode)
    {
        $this->fingerPrintCode = $fingerPrintCode;

        return $this;
    }

    /**
     * Get fingerPrintCode
     *
     * @return string
     */
    public function getFingerPrintCode()
    {
        return $this->fingerPrintCode;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return NidaRegistration
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set data
     *
     * @param string $data
     *
     * @return NidaRegistration
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set registrationId
     *
     * @param string $registrationId
     *
     * @return NidaRegistration
     */
    public function setRegistrationId($registrationId)
    {
        $this->registrationId = $registrationId;

        return $this;
    }

    /**
     * Get registrationId
     *
     * @return string
     */
    public function getRegistrationId()
    {
        return $this->registrationId;
    }
    
    
    /**
     * @return mixed
     */
    public function getAppVersion()
    {
        return $this->appVersion;
    }

    /**
     * @param mixed $appVersion
     */
    public function setAppVersion($appVersion)
    {
        $this->appVersion = $appVersion;
    }

    /**
     * @return mixed
     */
    public function getDeviceId()
    {
        return $this->deviceId;
    }

    /**
     * @param mixed $deviceId
     */
    public function setDeviceId($deviceId)
    {
        $this->deviceId = $deviceId;
    }

    /**
     *
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     *
     * @param User $owner
     */
    public function setOwner(User $owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return string
     */
    public function getLanguageCode()
    {
        return $this->languageCode;
    }

    /**
     * @param string $languageCode
     */
    public function setLanguageCode($languageCode)
    {
        $this->languageCode = $languageCode;
    }

}