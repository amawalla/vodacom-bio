<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\RestApiBundle\Entity;

/**
 * Description of UserType
 *
 * @author Michael Tarimo
 */
final class UserType extends EnumType  {

    const AGENT_USER = 100;
    const ADMIN = 200;
}

