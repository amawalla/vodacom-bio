<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\RestApiBundle\Entity;

/**
 * Description of AdminUser
 *
 * @author Michael Tarimo
 */
class AdminUser extends User {
    
    protected $roles;
}
