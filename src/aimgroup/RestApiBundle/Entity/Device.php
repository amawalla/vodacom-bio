<?php
/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 8/6/15
 * Time: 11:06
 */

namespace aimgroup\RestApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\NamedQueries;
use Doctrine\ORM\Mapping\NamedQuery;

/**
 *
 * Class Device
 * @package UserBundle\Entity
 * @ORM\Entity(repositoryClass="aimgroup\RestApiBundle\Repository\DeviceRepository")
 *
 * @ORM\Table(name="device", uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"msisdn","imei"})
 *     })
 * @ORM\HasLifecycleCallbacks()
 * @NamedQueries(
 *     {
 *     @NamedQuery(name="deviceWithDeviceId",query="SELECT d.id,d.msisdn,d.deviceId FROM __CLASS__ d where d.deviceId is not null and d.status=1"),
 *     @NamedQuery(name="countDevices",query="SELECT count(d) FROM __CLASS__ d")
 *     },
 * )
 */
class Device implements \Serializable
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     */
    private $accessToken;

    /**
     * @ORM\Column(type="string")
     */
    private $msisdn;

    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private $imei;


    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private $public_key;


    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private $private_key;


    /**
     * @ORM\Column(type="string",nullable=true)
     */
    protected $appVersion;


    /**
     *
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    private $status = 1;
    /**
     * @ORM\Column(type="datetime")
     */
    private $createdOn;
    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    private $activeOn;

    
    
    
      /**
     * @var string
     * @ORM\Column(name="last_check",type="integer", nullable=true)
     */
    protected $lastCheck = 0;
    
    
    
    
    
    /**
     * @var
     * @ORM\Column(type="boolean")
     */
    private $isTiedToDevice = true;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="aimgroup\RestApiBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinColumn(name="user_id",referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\Column(type="string")
     */
    private $deviceId = "";

    /**
     * @ORM\Column(type="integer")
     */
    private $userPermission = 0;


    /**
     * @var integer
     * @ORM\Column(type="smallint", options={"default": 0},nullable=true))
     */
    private $isBlocked;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @param mixed $accessToken
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @return mixed
     */
    public function getMsisdn()
    {
        return $this->msisdn;
    }

    /**
     * @param mixed $msisdn
     */
    public function setMsisdn($msisdn)
    {
        $this->msisdn = $msisdn;
    }


    /**
     * @return mixed
     */
    public function getAppVersion()
    {
        return $this->appVersion;
    }

    /**
     * @param mixed $appVersion
     */
    public function setAppVersion($appVersion)
    {
        $this->appVersion = $appVersion;
    }

    /**
     * @return mixed
     */
    public function getImei()
    {
        return $this->imei;
    }

    /**
     * @param mixed $imei
     */
    public function setImei($imei)
    {
        $this->imei = $imei;
    }


    /**
     * @return mixed
     */
    public function getPrivateKey()
    {
        return $this->private_key;
    }

    /**
     * @param mixed $private_key
     */
    public function setPrivateKey($private_key)
    {
        $this->private_key = $private_key;
    }


    /**
     * @return mixed
     */
    public function getPublicKey()
    {
        return $this->public_key;
    }

    /**
     * @param mixed $public_key
     */
    public function setPublicKey($public_key)
    {
        $this->public_key = $public_key;
    }


    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @return mixed
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * @param mixed $createdOn
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;
    }

    /**
     * @return mixed
     */
    public function getActiveOn()
    {
        return $this->activeOn;
    }

    /**
     * @param mixed $activeOn
     */
    public function setActiveOn($activeOn)
    {
        $this->activeOn = $activeOn;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }


    /**
     * @ORM\PrePersist()
     */
    public function prePersistDevice()
    {
        $this->setCreatedOn(new \DateTime(date('Y-m-d H:i:s')));
        $this->setIsActive(false);
    }


    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     */
    public function serialize()
    {
        return serialize(
            array(
                $this->id,
            )
        );
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            ) = unserialize($serialized);
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getIsTiedToDevice()
    {
        return $this->isTiedToDevice;
    }

    /**
     * @param mixed $isTiedToDevice
     */
    public function setIsTiedToDevice($isTiedToDevice)
    {
        $this->isTiedToDevice = $isTiedToDevice;
    }

    /**
     * @return mixed
     */
    public function getDeviceId()
    {
        return $this->deviceId;
    }

    /**
     * @param mixed $deviceId
     */
    public function setDeviceId($deviceId)
    {
        $this->deviceId = $deviceId;
    }

    /**
     * @return mixed
     */
    public function getUserPermission()
    {
        return $this->userPermission;
    }

    /**
     * @param mixed $userPermission
     */
    public function setUserPermission($userPermission)
    {
        $this->userPermission = $userPermission;
    }


    /**
     * @return mixed
     */
    public function getIsBlocked()
    {
        return $this->isBlocked;
    }

    /**
     * @param mixed $isBlocked
     */
    public function setIsBlocked($isBlocked)
    {
        $this->isBlocked = $isBlocked;
    }


    
    
    
    
    
    
    
    
    
    
    
        
     /**
     * @return mixed
     */
    public function getLastCheck() {
        return $this->lastCheck;
    }

    /**
     * @param mixed $lastCheck
     */
    public function setLastCheck($lastCheck) {
        $this->lastCheck = $lastCheck;
    }
    
    
    
    
    

}