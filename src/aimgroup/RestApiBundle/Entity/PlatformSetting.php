<?php
/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\RestApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * Description of PlatformSettings
 *
 * @author robert
 */

/**
 * @ORM\Table(name="platform_setting")
 * @ORM\Entity
 */

class PlatformSetting {
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="key", type="string", nullable=false)
     */
    private $key;
    
    /**
     * @ORM\Column(name="value", type="string",length=1000, nullable=false)
     */
    private $value;
    
    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;
    
    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;
    
    public function getId(){
        return $this->id;
    }
    
    public function setKey($key){
        $this->key = $key;
        return $this;
    }
    
    public function getKey(){
        return $this->key;
    }
    
    public function setValue($value){
        $this->value = $value;
        return $this;
    }
    
    public function getValue(){
        return $this->value;
    }
    
    public function setCreatedAt(\DateTime $datetime){
        $this->createdAt = $datetime;
        return $this;
    }
    
    public function getCreatedAt(){
        return $this->createdAt;
    }
    
    public function setUpdatedAt(\DateTime $datetime){
        $this->updatedAt = $datetime;
        return $this;
    }
    
    public function getUpdatedAt(){
        return $this->updatedAt;
    }
}
