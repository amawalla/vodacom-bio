<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\RestApiBundle\Entity;

use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use \FOS\UserBundle\Model\GroupInterface;
use aimgroup\RestApiBundle\Entity\Group;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * Description of User
 * @ORM\Entity(repositoryClass="aimgroup\RestApiBundle\Repository\UserRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="ereg_user_type", type="string")
 * @ORM\Table(name="user")
 * @UniqueEntity(fields="usernameCanonical",errorPath="username", message="fos_user.username.already_used")
 * @ORM\AttributeOverrides({
 *     @ORM\AttributeOverride(
 *     name="email",
 *     column=@ORM\Column(type="string", name="email", length=255, unique=false, nullable=true)
 * ),@ORM\AttributeOverride(
 *     name="emailCanonical",
 *     column=@ORM\Column(type="string", name="email_canonical", length=255, unique=false, nullable=true)
 * )
 *})
 * @ORM\HasLifecycleCallbacks()
 *
 *
 *
 * @author Michael Tarimo
 * @author Michael Tarimo
 *
 *
 */
class User extends BaseUser
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     *
     * @ORM\Column(name="dob", type="datetime", nullable=true)
     */
    protected $dob;

    /**
     * @ORM\Column(name="type", type="integer",nullable=true)
     */
    protected $type;

    /**
     * @ORM\Column(name="status", type="smallint", nullable=true)
     */
    protected $status;

    /** One-to-many, unidirectional
     *
     * @ORM\ManyToMany(targetEntity="Group")
     * @ORM\JoinTable(name="user_group",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id",
     *                           referencedColumnName="id", unique=false)}
     *      )
     */
    protected $groups;

    /**
     * @var string
     * @ORM\Column(name="first_name",type="string", length=60)
     */
    protected $firstName = "";

    /**
     * @ORM\Column(name="middle_name" ,type="string", length=60,nullable=true)
     */
    protected $middleName = "";


    /**
     * @ORM\Column(name="last_name",type="string", length=60)
     */
    protected $lastName = "";


    /**
     * @ORM\Column(name="admin_role",type="string", length=80)
     */
    protected $adminRole = "";

    /**
     * @ORM\Column(name="admin_department",type="string", length=80)
     */
    protected $adminDepartment = "";


    /**
     * @ORM\ManyToOne(targetEntity="aimgroup\DashboardBundle\Entity\Idtype")
     * @ORM\JoinColumn(name="identification_type", referencedColumnName="id", nullable=true)
     */
    private $idType = "";

    /**
     * @var string
     * @ORM\Column(name="id_number",type="string", length=60, nullable=true)
     */
    protected $idNumber = "";

    /**
     *
     * @ORM\Column( name="gender", type="string", length=1 )
     */
    protected $gender = "";


    /**
     * @ORM\Column(name="mobile_number",type="string", length=15)
     */
    protected $mobileNumber = "";


    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     *
     * @ORM\Column( name="number_devices", type="integer", length=11 )
     */
    protected $number_devices = 1;

    /**
     *
     * @ORM\Column( name="agent_exclusivity", type="integer", length=11 )
     */
    protected $agent_exclusivity = 1;


    /**
     *
     * @ORM\ManyToOne(targetEntity="aimgroup\RestApiBundle\Entity\User")
     */
    private $parent_id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="aimgroup\RestApiBundle\Entity\User")
     */
    private $vodashop_id;

    /**
     * @var
     * @ORM\Column( name="agent_code", type="string", length=20, nullable=true )
     */
    protected $agentCode;

    
    /**
     * @var
     * @ORM\Column( name="sales_code", type="string", length=20, nullable=true )
     */
    protected $salesCode;
    
    /**
     * @var string
     * @ORM\Column(name="last_check",type="integer", nullable=true)
     */
    protected $lastCheck = 0;
    
    
    /**
     * @ORM\ManyToOne(targetEntity="aimgroup\DashboardBundle\Entity\Region")
     * @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     */
    private $region;


    /**
     * @ORM\ManyToOne(targetEntity="aimgroup\DashboardBundle\Entity\Territory")
     * @ORM\JoinColumn(name="territory_id", referencedColumnName="id")
     */
    private $territory;

     /**
     * @var boolean
     *
     * @ORM\Column(name="is_enrolled_to_test",type="boolean")
     */
    protected $isEnrolledToTest = false;


    public function __construct()
    {
        parent::__construct();
        $this->isActive = true;
        // may not be needed, see section on salt below
        // $this->salt = md5(uniqid(null, true));
        $this->groups = new ArrayCollection();
    }


    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    public function setSalt($salt)
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        $this->salt = $salt;
        return $this->salt;
    }


    /**
     *
     * @return User
     */
    public function getParentId()
    {
        if (!$this->parent_id) {
            return "" ;
        }
        return $this->parent_id;
    }

    /**
     *
     * @param User $parent_id
     */
    public function setParentId(User $parent_id)
    {
        $this->parent_id = $parent_id;
    }

    /**
     *
     * @return User
     */
    public function getVodashopId()
    {
        return $this->vodashop_id;
    }

    /**
     *
     * @param User $vodashop_id
     */
    public function setVodashopId(User $vodashop_id)
    {
        $this->vodashop_id = $vodashop_id;
    }


    public function setGroups($groups = array())
    {
        $this->groups = $groups;

        //to allow for chaining
        return $this;
    }

    public function getGroups()
    {

        return $this->groups->toArray();
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getIdNumber()
    {
        return $this->idNumber;
    }

    /**
     * @param string $idNumber
     */
    public function setIdNumber($idNumber)
    {
        $this->idNumber = $idNumber;
    }


    public function getGroupsCanonical()
    {
        $groups = array();
        foreach ($this->groups as $group) {
            $groups[] = array(
                'groupId' => $group->getId(),
                'groupCode' => $group->getGroupCode(),
                'groupName' => $group->getGroupName()
            );
        }
        return $groups;
    }

    public function getIdentity()
    {
        $groups = $this->getGroupsCanonical();
        $flags = $this->groupFlags();
        $identity = array(
            'id' => $this->getId(),
            'username' => $this->getUsername(),
            'firstName' => $this->getFirstName(),
            'lastName' => $this->getLastName(),
            'mobileNumber' => $this->getMobileNumber(),
            'email' => $this->getEmail(),
            //'uid' => $this->getUid(),
            'gender' => $this->getGender(),
            'groups' => $groups,
            'flags' => $flags
        );

        return $identity;
    }

    public function groupFlags()
    {
        $groups = $this->getGroups();
        $flags = array(
            'isAdmin' => false,
            'isCustomer' => false,
            'isArtist' => false,
            'isArtistManager' => false,
            'isAdvertiser' => false,
            'isBlogger' => false,
            'isRecordLabel' => false,
        );
        foreach ($groups as $group) {
            switch ($group->getGroupCode()) {
                case GroupType::ADMIN:
                    $flags['isAdmin'] = true;
                    continue;
                case GroupType::ARTIST:
                    $flags['isArtist'] = true;
                    continue;
                case GroupType::CUSTOMER:
                    $flags['isCustomer'] = true;
                    continue;
                case GroupType::ADVERTISER:
                    $flags['isAdvertiser'] = true;
                    continue;
                case GroupType::BLOGGER:
                    $flags['isBlogger'] = true;
                    continue;
                case GroupType::ARTIST_MANAGER:
                    $flags['isArtistManager'] = true;
                    continue;
                case GroupType::RECORD_LABEL:
                    $flags['isRecordLabel'] = true;
                    continue;
                default :
                    continue;
            }
        }

        return $flags;
    }

    public function eraseCredentials()
    {

    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }

    function getStatus()
    {
        return $this->status;
    }

    function setStatus($status)
    {
        $this->status = $status;
    }


    /**
     * @return mixed
     */
    public function getNumberDevices()
    {
        return $this->number_devices;
    }

    /**
     * @param mixed $number_devices
     */
    public function setNumberDevices($number_devices)
    {
        $this->number_devices = $number_devices;
    }

    /**
     * @return mixed
     */
    public function getAgentExclusivity()
    {
        return $this->agent_exclusivity;
    }

    /**
     * @param mixed $agent_exclusivity
     */
    public function setAgentExclusivity($agent_exclusivity)
    {
        $this->agent_exclusivity = $agent_exclusivity;
    }


    /**
     * Set adminRole
     *
     * @param string $adminRole
     * @return User
     */
    public function setAdminRole($adminRole)
    {
        $this->adminRole = $adminRole;
        return $this;
    }

    /**
     * Get adminRole
     *
     * @return string
     */
    public function getAdminRole()
    {
        return $this->adminRole;
    }

    /**
     * Set adminDepartment
     *
     * @param string $adminDepartment
     * @return User
     */
    public function setAdminDepartment($adminDepartment)
    {
        $this->adminDepartment = $adminDepartment;
        return $this;
    }

    /**
     * Get adminDepartment
     *
     * @return string
     */
    public function getAdminDepartment()
    {
        return $this->adminDepartment;
    }


    /**
     * Set firstName
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set middleName
     *
     * @param string $middleName
     * @return User
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;

        return $this;
    }

    /**
     * Get middleName
     *
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set dob
     *
     * @param date $dob
     * @return User
     */
    public function setDob($dob)
    {
        $this->dob = $dob;

        return $this;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function setGender($gender)
    {
        $this->gender = $gender;
        return $this;
    }

    public function setUserType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function getUserType()
    {
        return $this->type;
    }

    /**
     * Get dob
     *
     * @return \DateTime
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * Set mobileNumber
     *
     * @param string $mobileNumber
     * @return User
     */
    public function setMobileNumber($mobileNumber)
    {
        $this->mobileNumber = $mobileNumber;

        return $this;
    }

    /**
     * Get mobileNumber
     *
     * @return string
     */
    public function getMobileNumber()
    {
        return $this->mobileNumber;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersistDevice()
    {
        $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add groups
     *
     * @param \aimgroup\RestApiBundle\Entity\Group $group
     * @return User
     */
    public function addGroup(GroupInterface $group)
    {
        $this->groups->add($group);

        return $this;
    }

    /**
     * Remove roles
     *
     * @param \aimgroup\RestApiBundle\Entity\Group $group
     */
    public function removeGroup(GroupInterface $group)
    {
        return $this->groups->removeElement($group);
    }

    public function getLanguage()
    {
        return "en";
    }

    public function setCreatedAt(\DateTime $datetime)
    {
        $this->createdAt = $datetime;
        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setUpdatedAt(\DateTime $datetime)
    {
        $this->updatedAt = $datetime;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return mixed
     */
    public function getAgentCode()
    {
        return $this->agentCode;
    }

    /**
     * @param mixed $agentCode
     */
    public function setAgentCode($agentCode)
    {
        $this->agentCode = $agentCode;
    }
    
         /**
     * @return mixed
     */
    public function getSalesCode()
    {
        return $this->salesCode;
    }

    /**
     * @param mixed $salesCode
     */
    public function setSalesCode($salesCode)
    {
        $this->salesCode = $salesCode;
    }

    /**
     * @return mixed
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param mixed $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * @return mixed
     */
    public function getTerritory()
    {
        return $this->territory;
    }

    /**
     * @param mixed $territory
     */
    public function setTerritory($territory)
    {
        $this->territory = $territory;
    }

    /**
     * @return mixed
     */
    public function getIdType() {
        return $this->idType;
    }

    /**
     * @param mixed $idType
     */
    public function setIdType($idType) {
        $this->idType = $idType;
    }
    
    
    
    
    
    
    
    
    
    
    
    
     /**
     * @return mixed
     */
    public function getLastCheck() {
        return $this->lastCheck;
    }

    /**
     * @param mixed $lastCheck
     */
    public function setLastCheck($lastCheck) {
        $this->lastCheck = $lastCheck;
    }

    public function getExpireDate() {
        return $this->expiresAt;
    }


     public function setExpireDate(\DateTime $datetime) {
        $this->expiresAt = $datetime;
        return $this;
    }

     /**
     * @return bool
     */
    public function getIsEnrolledToTest() {

        return $this->isEnrolledToTest;
    }

    /**
     * @param $check
     */
    public function setIsEnrolledToTest($check) {

        $this->isEnrolledToTest = (bool) $check;
    }
}
