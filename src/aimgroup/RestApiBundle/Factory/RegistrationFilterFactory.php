<?php
/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 10/06/2016
 * Time: 11:28 AM
 */

namespace aimgroup\RestApiBundle\Factory;



use aimgroup\RestApiBundle\ServiceImpl\Filter\VodacomRegistrationFilter;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RegistrationFilterFactory
{
    /**
     * Static factory method for creating endpoints
     * @param $operator
     * @param ContainerInterface $container
     * @return AirtelRegistrationFilter|TigoRegistrationFilter|VodacomRegistrationFilter|null
     * @throws \Exception
     */
    public static function createFilter($operator, ContainerInterface $container)
    {
        $instance = null;
        switch ($operator) {
            case "vodacom":
                $instance = new VodacomRegistrationFilter($container);
                break;
            default:
                throw new \Exception("Invalid Registration Filter");
        }

        return $instance;
    }

}