<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 12/14/15
 * Time: 1:18 PM
 */

namespace aimgroup\RestApiBundle\Factory;


use aimgroup\RestApiBundle\ServiceImpl\AirtelProcessor;
use aimgroup\RestApiBundle\ServiceImpl\TigoProcessor;
use aimgroup\RestApiBundle\ServiceImpl\VodacomProcessor;
use Symfony\Component\DependencyInjection\ContainerInterface;

class EndpointFactory
{


    /**
     * Static factory method for creating endpoints
     * @param $operator
     * @param ContainerInterface $container
     * @return AirtelProcessor|TigoProcessor|VodacomProcessor|null
     * @throws \Exception
     */
    public static function createEnpoint($operator, ContainerInterface $container)
    {
        $instance = null;
        switch ($operator) {
            case "vodacom":
                $instance = new VodacomProcessor($container);
                break;
            case "airtel":
                $instance = new AirtelProcessor($container);
                break;
            case "tigo":
                $instance = new TigoProcessor($container);
                break;

            default:
                throw new \Exception("Invalid processor");
        }

        return $instance;
    }

}