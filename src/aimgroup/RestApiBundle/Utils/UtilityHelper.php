<?php

/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 10/06/2016
 * Time: 12:04 PM
 */

namespace aimgroup\RestApiBundle\Utils;

use FOS\ElasticaBundle\Manager\RepositoryManager;
use FOS\ElasticaBundle\Repository;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UtilityHelper {

    const MINIMUM_NAME_LENGTH = 4;
    const NAME_REGEX = '/(.)\1{3}/';
    const MINIMUM_CUSTOMER_AGE = 18;
    const SAME_ID_TYPE_SAME_NUMBER_THRESHOLD = 5;
    const SAME_NUMBER_THRESHOLD = 5;

    protected $container;
    protected $manager;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
        $this->manager = $this->container->get("fos_elastica.manager");
    }

    public function isValidPersonName($firstName, $lastName) {
        if (is_null($firstName) || is_null($lastName)) {
            return false;
        }
        $name = $firstName . " " . $lastName;
        return (strlen($name) > UtilityHelper::MINIMUM_NAME_LENGTH && preg_match(UtilityHelper::NAME_REGEX, $name) == 0);
    }

    public function isValidDateOfBirth($dob) {
	   //return true;
        if (is_null($dob)) {
            return false;
        }
        try {
            $difference = (new \DateTime())->diff(new \DateTime(date('Y-m-d', strtotime($dob))));
            return ($difference->y >= UtilityHelper::MINIMUM_CUSTOMER_AGE && $difference->y < 100);
        } catch (\Exception $e) {
            return false;
        }
    }

    public function checkIdNumberUsageLimit($idnumber) {
        $count = $this->manager->getRepository("RestApiBundle:Registration")->findCountOfIdNumber($idnumber);
        return ($count < UtilityHelper::SAME_NUMBER_THRESHOLD);
    }

    public function checkIdUsageLimit($idType, $idNo) {
        $count = $this->manager->getRepository("RestApiBundle:Registration")->findCountOfIdTypeAndIdNumber($idType, $idNo);
        return ($count < UtilityHelper::SAME_ID_TYPE_SAME_NUMBER_THRESHOLD);
    }

    public function findByMsisdnAndSerial($msisdn, $serial) {
        return $this->manager->getRepository("RestApiBundle:Registration")->findByMsisdnAndSerial($msisdn, $serial);
    }

}
