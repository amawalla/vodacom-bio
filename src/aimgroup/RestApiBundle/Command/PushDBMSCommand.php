<?php

namespace aimgroup\RestApiBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PushDBMSCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('rest_api:push_dbmscommand')
            ->addArgument("id")
            ->setDescription('Push Records to DBMS');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $apiHelper = $this->getContainer()->get("api.helper");
        $em = $this->getContainer()->get('doctrine')->getManager();

        $registrationId = $input->getArgument('id');
        try {
            $registration = $em->getRepository("RestApiBundle:Registration")->find($registrationId);
            if ($registration) {
                $url = $this->getContainer()->getParameter("vodacom_freg_endpoint");
                $signature = $em->getRepository("RestApiBundle:AgentSignatures")->findOneBy(array("msisdn" => $registration->getAgentMsisdn()));
                /** @var  $user User */
                $user = $registration->getOwner();
                $idType = $em->getRepository("DashboardBundle:Idtype")->find($registration->getIdentificationType());
                $region = $em->getRepository("DashboardBundle:Region")->find($registration->getRegion());
                $territory = $em->getRepository("DashboardBundle:Territory")->find($registration->getTerritory());

                $data = array(
                    "REG_TYPE" => "FREG",
                    "RECORD_ID" => $registration->getId(),
                    "MSISDN" => "255" . $registration->getMsisdn(),
                    "FIRST_NAME" => $registration->getFirstName(),
                    "LAST_NAME" => $registration->getLastName(),
                    "MIDDLE_NAME" => $registration->getMiddleName(),
                    "DOB" => date('d-m-Y', strtotime($registration->getDob())),
                    "ID_TYPE" => $idType->getName() == "NSSF Pension Card" ? "Pension Fund ID" : $idType->getName(),
                    "ID_NUMBER" => $registration->getIdentification(),
                    "AGENT_NAME" => $user->getFirstName() . " " . $user->getLastName(),
                    "AGEN_CODE" => "0" . $registration->getMsisdn(),
                    "location" => $territory,
                    "region" => $region
                );


                $image_key = array(
                    'signature' => 'customer_signature_image',
                    'rear-pic' => 'id_back_image',
                    'front-pic' => 'id_front_image',
                    'potrait' => 'picture',
                );


                $images = $em->getRepository("RestApiBundle:RegImages")->findBy(array("registration" => $registration->getRegistrationid()));
                //$em->getRepository("RestApiBundle:AgentSignatures")
                /** @var  $image RegImages */
                foreach ($images as $image) {
                    $data[$image_key[$image->getImageType()]] = $image->getWebPath();
                }
                $data["agent_signature_image"] = $signature->getFullPath();


                $callback = $this->getContainer()->getParameter("callback_dbms_retry");
                $response = $apiHelper->curl_message($url, $data, $callback);
                $apiHelper->logInfo($this->tag, "processImages", array('data' => $data, 'response' => $response));

            }
        } catch (\Exception $e) {

        }


    }
}
