<?php

namespace aimgroup\RestApiBundle\Command;

use aimgroup\RestApiBundle\Entity\NidaRegistration;
use aimgroup\RestApiBundle\Entity\RegImages;
use aimgroup\RestApiBundle\Entity\Registration;
use aimgroup\RestApiBundle\Entity\RegistrationStatus;
use Doctrine\ORM\EntityManager;
use EightPoints\Bundle\GuzzleBundle\GuzzleBundle;
use Elastica\Transport\Guzzle;
use GuzzleHttp\Client;
use Carbon\Carbon;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;
use Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class NidaRegistrationCommand extends ContainerAwareCommand {

    /**
     * {@inheritdoc}
     */
    protected function configure() {
        $this
            ->setName('biometric:registration:verify')
            ->setDescription('Nida Registration Verification Command');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output) {

        $output->writeln('..Processing pending biometric verification... .. .');

        $this->processPendingVerification();
    }

    private function processPendingVerification() {

        $container = $this->getContainer();
        $logger = $container->get('monolog.logger.api');


        $entityManager = $this->getContainer()->get('doctrine')->getManager();

        /**
         * Get the registration with regType 2 that has not been verified
         */

        try {

            // $registrations = $entityManager->get

            $query = $entityManager->getRepository('RestApiBundle:RegistrationStatus')
                ->createQueryBuilder('s')
                ->leftJoin('s.registrationId', 'p')
                ->where('s.registryType = :registryType')
                ->andWhere('s.createdDate >= :createdDate')
                ->andWhere('s.verifyState = :verifyState')
                ->andWhere('s.temporaryRegStatus = :regStatus')
                ->setParameter('createdDate', date('Y-m-d H:i:s', mktime(0, 0, 0)))
                ->setParameter('registryType', 2)// For Biometric
                ->setParameter('verifyState', 0)// Not verified
                ->setParameter('regStatus', 0)// Not Sent
                ->getQuery();

            $items =  (array) $query->getResult();


            echo count($items);

            /**
             * @var $registration Registration
             */

            if (count($items) > 0) {

                $logger->info('Found  ( ' . count($items) . ' )  unverified biometric registrations');

                /**
                 * $item  = Registration Status instance
                 *
                 * @var RegistrationStatus $item
                 */
                foreach ($items as $item) {

                    $registration = $entityManager->getRepository('RestApiBundle:Registration')->find($item->getRegistrationId());

                    if ($registration) {

                        $status = $this->verifyBiometricDetails($registration, $item);
                        
                        if ($status) {

                            $responseObject = json_decode($status->getBody()->getContents());

                            switch ($status->getStatusCode()) {

                                case 200:
                                    echo 'Response 200:'; 
                                    $logger->info(json_encode(json_decode(json_encode($responseObject), true)));

                                    if ($responseObject->code == 200 && in_array($responseObject->responseObject->BiometricResult, array( 0, 1, 2, 3 ))) {
			
                                        $this->updateCustomer($registration, $item);
                                    }
                                    break;
                                
                                case 406:
			          
                                    $logger->error('Received a bad response ' . $responseObject->message . '  :::: ');
                                    // Decline this registration based on the response status
                                    $this->declineRegistration($item, $responseObject);
                                    break;

                                case 500:

                                    $logger->error('Not so good response.. ' . $responseObject->message . '  :::: ');
                                    break;

                                default :

                                    $logger->error('NIDA Response Error', [
                                        'status'  => $status->getStatusCode(),
                                        'message' => $status->getBody()->getContents(),
                                        'registration' => $registration
                                    ]);

				 $msg = $container->get('api.helper')->getTranslatedLanguage('validation.nida.busy', [] ,
                    					$registration->getLanguage());

                                    $this->markAsFailedRegistration($registration, $item, $msg);

                                    break;
                            }
                        }
                    }

                }

            } else {

                $logger->error('No Biometric registrations to send ...');

            }

        } catch (\ErrorException $e) {
              echo $e->getMessage(); 
            $logger->error($e->getMessage());
        }

    }


    /**
     * Verify registration
     *
     * @param Registration $registration
     * @param RegistrationStatus $registrationStatus
     * @return bool
     */
    private function verifyBiometricDetails($registration,  $registrationStatus = null) {

        $identityCardNumber = $registration->getIdentification();

        $fingerPrintCode = $registration->getFingerPrintCode();

        $registrationId = $registration->getRegistrationid();

        echo $identityCardNumber . ' - ' . $fingerPrintCode . ' - ' . $registrationId;

        $container = $this->getContainer();
        $logger = $container->get('monolog.logger.api');

        $logger->error('verifyBiometricDetails', [ 'id' => $identityCardNumber, 'fingerPrintCode' => $fingerPrintCode ]);

        $entityManager = $this->getContainer()->get('doctrine')->getManager();


        $logger->error('registration:' . $registrationId . '    imageType: finger-print');

        /** @var RegImages $fingerPrintImage */
        $fingerPrintImage = $entityManager->getRepository('RestApiBundle:RegImages')->findOneBy([
            'registration' => $registrationId,
            'imageType'    => 'finger-print'
        ]);

        $query = $entityManager->createQueryBuilder()
            ->select("r from RestApiBundle:RegImages r where r.registration = '" . $registrationId . "' and r.imageType = 'finger-print'")
            ->getQuery();

        $fingerPrintImage = $query->getOneOrNullResult();

        if (!is_null($fingerPrintImage)) {
            
            $client = $this->getContainer()->get('guzzle.client.api_idgateway');

            $logger->error(' 1.strt');
            $logger->error(' ::: identityNumber:' . $identityCardNumber . '  fingerCode:' . $fingerPrintCode . ' fingerprintPath:' . $fingerPrintImage->getWebPath());

            $response = $client->post('IDGateway/rest/services/idGateway/queryBiometricIdentity', array(
                'multipart' => array(
                    array(
                        'name'     => 'identityNumber',
                        'contents' => $identityCardNumber
                    ),
                    array(
                        'name'     => 'fingerCode',
                        'contents' => $fingerPrintCode
                    ),
                    array(
                        'name'     => 'fingerprintPath',
                        'contents' => $fingerPrintImage->getWebPath()
                    )
                ),

            ));

            return $response;

        } else {

            $logger->error('verifyBiometricDetails missing finger print image..');

	   $msg = $container
                ->get('api.helper')
                ->getTranslatedLanguage('validation.nida.decline.incorrect', [] ,
                    $registrationStatus->getRegistrationId()->getLanguage()
                );
            
 	    $this->markAsFailedRegistration($registration, $registrationStatus, $msg);

            return False;
        }

    }

    private function updateCustomer(Registration $nidaRegistration, RegistrationStatus $registrationStatus) {

        $container = $this->getContainer();

        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine')->getManager();

        /**
         * @var $client Client
         */

        $client = $this->getContainer()->get('guzzle.client.api_idgateway');

        $response = $client->post('IDGateway/rest/services/idGateway/queryIdentity', array(
            'multipart' => [ 
		[
                'name'     => 'identityNumber',
                'contents' => $nidaRegistration->getIdentification()
                ]
	     ]
        ));
	
        if ($response->getStatusCode() == 200) {

            $nidaResponseObject = \GuzzleHttp\json_decode($response->getBody()->getContents());

            if ($nidaResponseObject && array_key_exists('code', $nidaResponseObject) && $nidaResponseObject->code == 200) {

                $nidaResponse = $nidaResponseObject->responseObject;

                if ($nidaResponse) {

                    $registration = $nidaRegistration;

                    $registration->setAddress($nidaResponse->ResidentialAddressStreet);
                    $registration->setContact($container->get("api.helper")->formatMsisdn($nidaRegistration->getMsisdn()));
                    $registration->setDob(date('d-m-Y', strtotime($nidaResponse->DateofBirth)));
                    $registration->setEmail("");
                    $registration->setFirstName($nidaResponse->FirstName);
                    $registration->setLastName($nidaResponse->LastName);
                    $registration->setMiddleName($nidaResponse->MiddleName);
                    $registration->setGender(strtolower($nidaResponse->Sex) == 'male' ? 'M' : 'F');
                    $registration->setWards($nidaResponse->ResidentialAddressWard);
                    $registration->setTerritory($registration->getOwner()->getTerritory()->getId());
                    $registration->setRegion($registration->getOwner()->getRegion()->getCode());
                    $registration->setLocationLatLon("");
                    $registration->setDeviceModel("");
                    $registration->setAgentImei("");
		    $registration->setNationality($nidaResponse->Nationality);	
                    $registration->setRegistrationTime(new \DateTime());
                    // $registration->setDeviceSentTime(new \DateTime());

                    // $registration->setOwner($nidaRegistration->getOwner());

                    $entityManager->persist($registration);
                  //  $entityManager->flush();
			
	          // Update registration status
                    $registrationStatus->setFullRegStatus(0);
                    $registrationStatus->setIcapState(0);
                    $registrationStatus->setVerifyState(1);
		    $registrationStatus->setVerifyDate(new \DateTime());
                  

                    $webPath = $this->uploadNidaImage($nidaRegistration, $nidaResponse->Photo);
                    
                    if ($webPath != null) {

                        $fileName = "{$registration->getMsisdn()}_portait.jpeg";

                        $imageObj = new RegImages();
                        $imageObj->setFullPath("");
                        $imageObj->setRegistration($registration->getRegistrationid());
                        $imageObj->setName($fileName);
                        $imageObj->setImageType('potrait');
                        $imageObj->setWebPath($webPath);

                        $registration->setImageCount(1);
                        $registrationStatus->setAllimageDate(new \DateTime());
                       // $registrationStatus->setVerifyState(1);

                        $entityManager->persist($imageObj);


                    } else {
                           
                        // There was a missing image : Left for
                    }

                    $entityManager->persist($registration);
                    $entityManager->persist($registrationStatus);

                    $entityManager->flush();

                }
            }

        }

    }

    /**
     * Decline Registration based on status
     *
     * @param RegistrationStatus $registrationStatus
     * @param $status
     */
    private function declineRegistration(RegistrationStatus $registrationStatus, $status) {
	 
        if ($status && isset($status->code) && in_array($status->code, [ 500 ])) {
	    	     
            $container = $this->getContainer();

            $entityManager = $container->get('doctrine')->getManager();

	   $msg = $container
                ->get('api.helper')
                ->getTranslatedLanguage('validation.nida.decline.incorrect',
                    [] ,
                $registrationStatus->getRegistrationId()->getLanguage()
            );

            $registrationStatus->setVerifyState(4);
            $registrationStatus->setVerifyDate(new \DateTime());
            $registrationStatus->setVerifyDescr($msg);

      //      $registrationStatus->setTemporaryRegStatus(4);
        //    $registrationStatus->setTemporaryRegDesc($msg);
  
            // Return the response back to the device;

	    $registration = $registrationStatus->getRegistrationId();
	    $registration->setSimSerial(substr($registration->getSimSerial(), 0, 19). '_'. date('YmdHis'));

            $entityManager->persist($registrationStatus);
            $entityManager->persist($registration);	
	
            $entityManager->flush();

            $container->get('api.helper')->notifyRegStatus($registrationStatus->getRegistrationId(), 3, $msg);

        }

    }

  
     /**
     * Mark Registration As Failed
     *
     * @param RegistrationStatus $registrationStatus
     * @param $registration Registration
     * @param $message
     */
    private function markAsFailedRegistration(Registration $registration, RegistrationStatus $registrationStatus, $message = 'Verification Failed') {

//    $this->getContainer()->get('monolog.logger.api')->error('Verification Failed', Carbon::parse($registration->getCreatedDate()) );
       

        if ($registration->getCreatedDate()) {

           $entityManager = $this->getContainer()->get('doctrine')->getManager();

          try {
		
             $registrationDate = Carbon::parse($registration->getCreatedDate()->format('Y-m-d H:i:s'));

            if ($registrationDate->diffInMinutes() > 5) {

                $registrationStatus->setVerifyState(4);
                $registrationStatus->setVerifyDate(new \DateTime());
                $registrationStatus->setVerifyDescr($message);

                //$registrationStatus->setTemporaryRegStatus(4);
                //$registrationStatus->setTemporaryRegDesc($message);

                // Return the response back to the device;

		 $registration = $registrationStatus->getRegistrationId();
                 $registration->setSimSerial(substr($registration->getSimSerial(), 0, 19). '_'. date('YmdHis'));

                 $entityManager->persist($registrationStatus);
                 $entityManager->persist($registration);


                $entityManager->flush();

                $this->getContainer()->get('api.helper')->notifyRegStatus($registrationStatus->getRegistrationId(), 3, $message);

            }
	 } catch (\Exception $e) {
               
                $this->getContainer()
                    ->get('monolog.logger.api')
                    ->error('Error Verification Failed :'.$e->getMessage(), $e->getTrace());
            }


        }
    }



    /**
     * Upload NIDA image
     *
     * @param Registration $registration
     * @param $encodedData
     * @throws \Exception
     */
    private function uploadNidaImage(Registration $registration, $encodedData) {

        $container = $this->getContainer();

        $entityManager = $container->get('doctrine')->getManager();

        $filesystem = $container->get('s3_filesystem');


        $fileName = "{$registration->getMsisdn()}_portait.jpeg";

        $remoteFilePath = date('Ymd') . DIRECTORY_SEPARATOR . $fileName;

        $webPath = "https://s3.amazonaws.com/{$container->getParameter('s3_bucket')}/{$container->getParameter('s3_bucket_prefix')}{$remoteFilePath}";
                    
        if (!$filesystem->has($remoteFilePath)) {

            $uploaded = $filesystem->write($remoteFilePath, base64_decode($encodedData));

             if ($uploaded) {
                //Allow the file to be accessible publicly
                $filesystem->setVisibility($remoteFilePath, 'public');

                return $webPath;

            } else {
                echo 'Failed Uploading image';
                return null;
            }
        }
    }

    /**
     * @param string $url
     * @param array $parameters
     * @param array $headers
     *
     * @return array
     * @throws \Exception
     */
    private function sendPostHttpRequest($url = null, $parameters = array(), $headers = array( 'Accept' => 'application/json' )) {

        if (is_null($url) || empty($parameters)) {
            throw new \InvalidArgumentException('AbstractRequestProcessor::sendPostHttpRequest() expects $url and array $parameters ');
        }

        try {

            $requestParameters = array();

            foreach ($parameters as $name => $value) {
                $requestParameters[] = array(
                    'name'     => $name,
                    'contents' => $value
                );
            }

            $response = (new Client())->request('POST', $url, array(
                'multipart' => $requestParameters,
                'headers'   => $headers,
                'auth'      => array( 'ekyc2', 'ekycRestfullUser123@321#' )
            ));

            return array(
                'statusCode'      => $response->getStatusCode(),
                'responseContent' => $response->getBody()->getContents()
            );

        } catch (RequestException $requestException) {
            throw $requestException;
        }

    }


}
