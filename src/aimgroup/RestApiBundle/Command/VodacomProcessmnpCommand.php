<?php

namespace aimgroup\RestApiBundle\Command;

use aimgroup\RestApiBundle\Entity\RegImages;
use aimgroup\RestApiBundle\Entity\Registration;
use aimgroup\RestApiBundle\Entity\User;
use aimgroup\RestApiBundle\Event\PlainTextEvent;
use aimgroup\RestApiBundle\Event\PlainTextProcessEvents;
use aimgroup\RestApiBundle\EventListener\PlainTextEventListener;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class VodacomProcessmnpCommand extends ContainerAwareCommand {

    protected $TAG = "VodacomProcessorCommandsCommand";
    protected $idArray = array();
    protected $regionArray = array();
    protected $territoryArray = array(
        "89" => "mwanza",
        "90" => "kagera",
        "91" => "pwani",
        "92" => "morogoro",
        "93" => "njombe",
        "94" => "zanzibar",
        "95" => "kigoma",
        "96" => "mtwara",
        "97" => "ruvuma",
        "98" => "pemba",
        "99" => "singida",
        "100" => "shinyanga",
        "101" => "arusha",
        "102" => "manyara",
        "103" => "mara",
        "104" => "mbeya",
        "105" => "rukwa",
        "106" => "dar-es-salaam",
        "107" => "dodoma",
        "108" => "tabora",
        "109" => "lindi",
        "110" => "Geita",
        "111" => "kilimanjaro",
        "112" => "tanga",
        "113" => "kahama",
        "114" => "iringa",
        "115" => "simiyu",
    );

    /**
     * {@inheritdoc}
     */
    protected function configure() {
        $this
                ->setName('rest_api:vodacom_mnp_command')
                ->addArgument("type")
                ->addArgument("registrationId")
                ->setDescription('This command ');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        $type = $input->getArgument('type');
        $registrationId = $input->getArgument('registrationId');

        $output->writeln('type: ' . $type);
        $output->writeln('registrationId: ' . $registrationId);

        $logger = $this->getContainer()->get('monolog.logger.api');

        $logger->info("registrationid : " . $registrationId . " " . $type);

        if ($registrationId) {
            if ($type == "mnp")
                $this->doMnp($registrationId);
            else if ($type == "checkMnp")
                $this->checkMnp($registrationId);


        }
    }

    private function doMnp($ids) {
                $apiHelper = $this->getContainer()->get('api.helper');

        $em = $this->getContainer()->get('doctrine')->getManager();
        $registrationIds = explode(",", $ids);
        
        foreach ($registrationIds as $registrationId) {
            
            $logger = $this->getContainer()->get('monolog.logger.api')->error("doMnp: processing " . $registrationId);
           
            
            
            
            $registration = $em->getRepository("RestApiBundle:Registration")->findOneBy(
                    array("registrationid" => $registrationId)
            );

            
            $registration->setSimSerial($apiHelper->iccdCheckValidate($registration->getSimSerial()));
           
            
              
            $logger = $this->getContainer()->get('monolog.logger.api')->error("freg: processing 11" . $registrationId."Sim Serial".$registration->getSimSerial());
            
            if ($registration) {
                
                
                $logger = $this->getContainer()->get('monolog.logger.api')->error("registration ffound 22 " . $registrationId);
                
                
                $receivedImages = $em->getRepository("RestApiBundle:RegImages")->findBy(
                        array("registration" => $registration->getRegistrationid())
                );
                $this->getContainer()->get("api.helper")->logInfo("rerunFregEndpointAction", "rerunFregEndpointAction", array("imagecount" => count($receivedImages)));

                $imageCountConfig = $this->getContainer()->getParameter("image_count_for_freg");

                $fregVerificationThreshold = $this->getContainer()->getParameter("freg_score_threshold");

                $activeLiveVerification = false; //$this->getContainer()->getParameter("live_verification_active");

                if ($activeLiveVerification) {
                    
                    $logger = $this->getContainer()->get('monolog.logger.api')->error("if activeLiveVerification 33 " . $registrationId);
                    
                    $verifyStateArray = array(1);
                    if ($fregVerificationThreshold == 3) {
                        $verifyStateArray = array(1, 2, 3);
                    } else if ($fregVerificationThreshold == 2) {
                        $verifyStateArray = array(1, 2);
                    }

                    $registrationStatus = $em->getRepository("RestApiBundle:RegistrationStatus")->findOneBy(
                            array("registrationId" => $registration->getId(), "verifyState" => $verifyStateArray)
                    );

                    $logger = $this->getContainer()->get('monolog.logger.api')->error("freg: processing 11" . $registrationId);

                    if ($registrationStatus && count($receivedImages) >= $imageCountConfig) {
                        $this->getContainer()->get("vodacom.endpoint")->processMnp($receivedImages);
                    }
                } else {
                    
                    $logger = $this->getContainer()->get('monolog.logger.api')->error("ELSE activeLiveVerification 33 " . $registrationId);
                    
                    if (count($receivedImages) >= $imageCountConfig) {
                        $this->getContainer()->get("vodacom.endpoint")->processMnp($receivedImages);
                    }
                }
            }else{
                
                $logger = $this->getContainer()->get('monolog.logger.api')->error("registration NOT ffound 22 " . $registrationId);
                
            }
        }




    }



    private function checkMnp($ids)
    {
        $apiHelper = $this->getContainer()->get('api.helper');
        $em = $this->getContainer()->get('doctrine')->getManager();
        $registrationIds = explode(",", $ids);
         $logger = $this->getContainer()->get('monolog.logger.api')->error("checkMnp: processing " . $registrationId);


        //TODO : will add a different end_point here for vodacom_checkMnpStatus_endpoint
        $url = $this->getContainer()->getParameter('vodacom_checkMnpStatus_endpoint');

        foreach ($registrationIds as $registrationId) {

            $registration = $em->getRepository("RestApiBundle:Registration")->findOneBy(
                array("registrationid" => $registrationId)
            );


            if($registration) {
                $logger = $this->getContainer()->get('monolog.logger.api')->error("checkMnp: processing this id " . $registrationId);
                $data = array("mobileNumber" => "255".$registration->getMsisdn(),);
                $response = $apiHelper->check_mnp_curl_message($url, $data);
                $response  = json_decode($response);


                if($response->responseObject->registrationStateCategory=="Un-registered")
                {

                    $logger = $this->getContainer()->get('monolog.logger.api')->error("checkMnp: this is unreg ");
                    $registration->setIsPorted("4");
                    $em->persist($registration);
                    $em->flush();
                    $this->doTreg($registrationId);

                    $registrationStatus = $em->getRepository("RestApiBundle:RegistrationStatus")->findOneBy(
                            array("registrationId" => $registration->getId())
                    );
                    $registrationStatus->setTemporaryRegStatus("2");
                    $em->persist($registrationStatus);
                    $em->flush();


                    $apiHelper->notifyMnpStatus($registration,1,"");


                     $msg = $apiHelper->getTranslatedLanguage(
                             "register.status.successPort",
                            array('%msisdn%' => $registration->getMsisdn()),
                           $registration->getLanguage()
                        );
                    $message = new Message();
                    $message->setDeviceId($registration->getDeviceId());
                    $message->setMessage($msg);
                    $message->setTopic($this->getContainer()->getParameter("mqtt_notify_msg"));
                    $apiHelper->sendMessage($message);

                    
                    
                }
                $logger = $this->getContainer()->get('monolog.logger.api')->error("checkMnp: registrationStateCategory ".$response->responseObject->registrationStateCategory);

            }
            else
                $logger = $this->getContainer()->get('monolog.logger.api')->error("checkMnp: Registration was not found ");

        }

    }














    private function doTreg($ids) {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $registrationIds = explode(",", $ids);
        foreach ($registrationIds as $registrationId) {

            $registration = $em->getRepository("RestApiBundle:Registration")->findOneBy(
                    array("registrationid" => $registrationId)
            );
            if ($registration) {
                $this->getContainer()->get("vodacom.endpoint")->processText($registration);
            }
        }
    }

    private function resendMpesa($record_ids) {

        $apiHelper = $this->getContainer()->get('api.helper');
        $records = explode(",", $record_ids);
        $em = $this->getContainer()->get("doctrine")->getManager();
        /**
         *
         * moves this to parameters
         */
        $url = $this->getContainer()->getParameter('vodacom_mpesa_endpoint');
        $callback = $this->getContainer()->getParameter("callback_url");
        foreach ($records as $record_id) {
            try {

                $apiHelper->logInfo($this->TAG, "resendMpesaAction-" . $record_id, array());
                /** @var  $registration Registration */
                $registration = $em->getRepository("RestApiBundle:Registration")->findOneBy(array("id" => $record_id));
                if ($registration) {
                    /** @var  $user User */
                    $user = $registration->getOwner();

                    if (in_array($registration->getIdentificationType(), $this->idArray)) {
                        $idType = $this->idArray[$registration->getIdentificationType()];
                    } else {
                        $idType = $em->getRepository("DashboardBundle:Idtype")->find($registration->getIdentificationType());
                        $this->idArray[$registration->getIdentificationType()] = $idType;
                    }

                    if (in_array($registration->getRegion(), $this->regionArray)) {
                        $region = $this->regionArray[$registration->getRegion()];
                    } else {
                        $region = $em->getRepository("DashboardBundle:Region")->findOneBy(array("code" => $registration->getRegion()))->getName();
                        $this->regionArray[$registration->getRegion()] = $region;
                    }


                    if (in_array($registration->getTerritory(), $this->territoryArray)) {
                        $territoryN = $this->territoryArray[$registration->getTerritory()];
                    } else {
                        try {
                            $territoryN = $em->getRepository("DashboardBundle:Territory")->find($registration->getTerritory())->getName();
                            $this->territoryArray[$registration->getTerritory()] = $territoryN;
                        } catch (\Exception $e) {
                            
                        }
                    }

                    if ($territoryN) {

                        $identificationTypeName = $idType->getName();

                        if (in_array($identificationTypeName, array('NSSF Pension Card', 'ZSSF'))) {
                            $identificationTypeName = 'Pension Fund ID';
                        } elseif ($identificationTypeName == 'Zanzibar Resident ID') {
                            $identificationTypeName = 'National ID';
                        } else {
                            $identificationTypeName = str_replace('(ZNZ)', '', $identificationTypeName);
                        }

                        $data = array(
                            "REG_TYPE" => "TREG",
                            "RECORD_ID" => $registration->getId(),
                            "MSISDN" => "255" . $registration->getMsisdn(),
                            "FIRST_NAME" => $registration->getFirstName(),
                            "LAST_NAME" => $registration->getLastName(),
                            "MIDDLE_NAME" => $registration->getMiddleName(),
                            "DOB" => date('d-m-Y', strtotime($registration->getDob())),
                            "ID_TYPE" => $identificationTypeName, //$idType->getName() == "NSSF Pension Card" ? "Pension Fund ID" : $idType->getName(),
                            "ID_NUMBER" => $registration->getIdentification(),
                            "AGENT_NAME" => $user->getFirstName() . " " . $user->getLastName(),
                            "AGEN_CODE" => "0" . $registration->getAgentMsisdn(),
                            "location" => $territoryN,
                            "region" => $region
                        );

                        $response = $apiHelper->curl_message($url, $data, $callback);
                        $apiHelper->logInfo($this->TAG, "resendMpesaAction-" . $record_id, array('data' => $data, 'response' => $response));
                    }
                }
            } catch (\Exception $e) {
                $apiHelper->logE($this->TAG, "resendMpesaAction", $e);
            }
        }
    }

    private function sendDBMS($record_id) {
        $apiHelper = $this->getContainer()->get('api.helper');
        /** move this to parameters yml */
        $url = "http://middleware1.registersim.com:8080/VodaGateway/rest/services/vodaGateway/dbmsRegisterCustomer";
        $apiHelper->logInfo($this->TAG, "apiRetryDBMSAction", array('id' => $record_id));
        $em = $this->getContainer()->get("doctrine")->getManager();
        try {
            /** @var  $registration Registration */
            $registration = $em->getRepository("RestApiBundle:Registration")->findOneBy(array("id" => $record_id));
            if ($registration) {
                //get images
                $images = $em->getRepository("RestApiBundle:RegImages")->findBy(array("registration" => $registration->getRegistrationid()));
                $signature = $em->getRepository("RestApiBundle:AgentSignatures")->findOneBy(array("msisdn" => $registration->getAgentMsisdn()));
                /** @var  $user User */
                $user = $registration->getOwner();
                $idType = $em->getRepository("DashboardBundle:Idtype")->find($registration->getIdentificationType());

                $region = $em->getRepository("DashboardBundle:Region")->findOneBy(array("code" => $registration->getRegion()));

                $territory = $em->getRepository("DashboardBundle:Territory")->find($registration->getTerritory());
                $territoryN = $territory->getName();

                $data = array(
                    "REG_TYPE" => "FREG",
                    "RECORD_ID" => $registration->getId(),
                    "MSISDN" => "255" . $registration->getMsisdn(),
                    "FIRST_NAME" => $registration->getFirstName(),
                    "LAST_NAME" => $registration->getLastName(),
                    "MIDDLE_NAME" => $registration->getMiddleName(),
                    "DOB" => date('d-m-Y', strtotime($registration->getDob())),
                    "ID_TYPE" => $idType->getName() == "NSSF Pension Card" ? "Pension Fund ID" : $idType->getName(),
                    "ID_NUMBER" => $registration->getIdentification(),
                    "AGENT_NAME" => $user->getFirstName() . " " . $user->getLastName(),
                    "AGEN_CODE" => "0" . $registration->getAgentMsisdn(),
                    "location" => $territoryN,
                    "region" => $region->getName()
                );

                $image_key = array(
                    'signature' => 'customer_signature_image',
                    'rear-pic' => 'id_back_image',
                    'camera' => 'id_back_image',
                    'front-pic' => 'id_front_image',
                    'potrait' => 'picture',
                );


                //$em->getRepository("RestApiBundle:AgentSignatures")
                /** @var  $image RegImages */
                foreach ($images as $image) {
                    $data[$image_key[$image->getImageType()]] = $image->getWebPath();
                }

                $data["agent_signature_image"] = $signature->getWebPath();
                if (count($images) != 4) {
                    echo "Less images.." . count($data);
                    exit;
                }
                $callback = $this->getContainer()->getParameter("callback_url");
                $response = $apiHelper->curl_message($url, $data, $callback);
                $apiHelper->logInfo($this->TAG, "processImages", array('data' => $data, 'response' => $response));

                $response_message = new \SimpleXMLElement($response);
                $response_message = json_decode(json_encode($response_message), true);

                if ($response_message["resultCode"] == 200) {
                    $infos = $response_message["resultsParameters"]["entry"];
                    $apiHelper->logInfo($this->TAG, "apiRetryDBMSAction", array('parameters' => $infos));
                    $setString = "";
                    $shouldUpdate = false;
                    foreach ($infos as $info) {
                        if ($info["key"] == "dbmsStatus" && $info["value"] == "Success") {
                            $shouldUpdate = true;
                            $setString = $setString . "u.fregDate = '" . date('Y-m-d H:i:s') . "',";
                            $setString = $setString . "u.dbmsState = '7', ";
                            $setString = $setString . "u.fullRegStatus = '7'";
                        } else if ($info["key"] == "dbmsMessage") {
                            $setString = $setString . " u.fullRegDesc = '" . $info["value"] . "', ";
                        }
                    }





                    if ($shouldUpdate) {
                        $query = "UPDATE RestApiBundle:RegistrationStatus u SET " . $setString . "  WHERE u.registrationId = " . $record_id;
                        $em->createQuery($query)->getArrayResult();
                        $apiHelper->logInfo($this->TAG, "apiRetryDBMSAction", array('query' => $query));
                    }
                }
            }
        } catch (\Exception $e) {
            $apiHelper->logE($this->TAG, "apiRetryDBMSAction", $e);
        }
    }

    private function deRegister($msisdn = null) {
        if (is_null($msisdn)) {
            return false;
        }

        $entityManager = $this->getContainer()->get('doctrine')->getManager();

        $msisdn = substr($msisdn, -9);

        $records = $entityManager->getRepository('RestApiBundle:Registration')->findBy(['msisdn' => $msisdn]);

        foreach ($records as $index => $record) {
            $simSerial = $record->getSimSerial();
            $record->setSimSerial($simSerial . '_' . date('YmdHis'));
            $entityManager->persist($record);
            $entityManager->flush();
        }

        $entityManager->clear();
    }

}
