<?php
/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 18/04/2016
 * Time: 12:17 PM
 */

namespace aimgroup\RestApiBundle\Tests\Controller;


use aimgroup\RestApiBundle\Entity\Registration;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MqttTestController extends KernelTestCase
{

    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    private $container;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        self::bootKernel();
        $this->container = static::$kernel->getContainer();
        $this->em = $this->container->get('doctrine')->getManager();
    }


    public function testGetRoles()
    {
        $query = $this->em->createQuery('SELECT u FROM RestApiBundle:User u WHERE u.roles LIKE :roles')->setParameter('roles', '%"ROLE_SYSTEM"%');
        $users = $query->getSingleResult();
        dump($users);

    }

    public function testNida()
    {

        $registration = $this->em->getRepository("RestApiBundle:Registration")->find(138);
        $this->container->get('vodacom.endpoint')->processText($registration);
    }

    public function testNotifyRegistrationStatus()
    {

        $description = "fullRegDesc";
        $registrationStatus = $this->em->getRepository("RestApiBundle:RegistrationStatus")->find(18);
        if ($registrationStatus) {
            $registration = $registrationStatus->getRegistrationId();
            $apiHelper = $this->container->get('api.helper');
            if ($description == 'fullRegDesc') {
                $apiHelper->notifyRegStatus($registration, 1);
            } else {
                if ($description = 'temporaryRegStatus') {
                    //  $apiHelper->notifyRegStatus($registration, 3);
                }
            }
        }
//        $registration = $this->em->getRepository("RestApiBundle:RegistrationStatus")->find(17);
//        dump($registration->getRegistrationId());
        // $this->container->get('api.helper')->notifyRegStatus($registration,400);
        // dump($registration);
    }

    public function testNNIMQueries()
    {
        $result = $this->em->getRepository("RestApiBundle:ReservedNumbers")->findBy(
            array('msisdn' => '255716506167'),
            array('id' => 'DESC'),
            1
        );
        if ($result) {
            return $result[0];
        } else {
            return;
        }
    }

    public function testParseDBMSResponse()
    {
        $response = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<processResult>
    <resultCode>200</resultCode>
    <resultType>SUCCESS_DBMS</resultType>
    <message>Successfully posted to DBMS</message>
    <resultsParameters>
        <entry>
            <key>subtask</key>
            <value>freg_dbms</value>
        </entry>
        <entry>
            <key>dbmsMessage</key>
            <value>Successfully Fully Registered DBMS</value>
        </entry>
        <entry>
            <key>dbmsStatus</key>
            <value>Success</value>
        </entry>
    </resultsParameters>
    <responseObject>
        <code>0</code>
        <responseObject xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="dbmsCustomer">
            <RECORD_ID>297446</RECORD_ID>
            <FIRST_NAME>Jafari</FIRST_NAME>
            <MIDDLE_NAME>R</MIDDLE_NAME>
            <LAST_NAME>Lugi</LAST_NAME>
            <ID_TYPE>Voters ID</ID_TYPE>
            <ID_NUMBER>100269996646</ID_NUMBER>
            <AGENT_NAME>HAMENYA THADEO</AGENT_NAME>
            <AGEN_CODE>0766654996</AGEN_CODE>
            <agentCode>0766654996</agentCode>
            <agentName>HAMENYA THADEO</agentName>
            <DOB>10-10-1995</DOB>
            <firstName>Jafari</firstName>
            <lastName>Lugi</lastName>
            <MSISDN>255766515124</MSISDN>
            <middleName>R</middleName>
            <id_front_image>https://s3.amazonaws.com/ereg2/vodacom/images/20160721/766515124_20160721103410_front-pic_.jpeg</id_front_image>
            <id_back_image>https://s3.amazonaws.com/ereg2/vodacom/images/20160721/766515124_20160721103421_rear-pic_.jpeg</id_back_image>
            <picture>https://s3.amazonaws.com/ereg2/vodacom/images/20160721/766515124_20160721103353_potrait_.jpeg</picture>
            <customer_signature_image>https://s3.amazonaws.com/ereg2/vodacom/images/20160721/766515124_20160721103435_signature_.jpeg</customer_signature_image>
            <agent_signature_image>https://vodalive.registersim.com/euploads/signatures/20160629/766654996_20160629113057_agent_signature_.jpeg</agent_signature_image>
            <location>KIGOMA</location>
            <region>TANGANYIKA</region>
        </responseObject>
    </responseObject>
</processResult>';
        $response_message = new \SimpleXMLElement($response);
        $response_message = json_decode(json_encode($response_message), true);

        if ($response_message["resultCode"] == 200) {
            $infos = $response_message["resultsParameters"]["entry"];
            $setString = "";
            $record_id = 1;

            foreach ($infos as $info) {
                if ($info["key"] == "dbmsStatus" && $info["value"] == "Success") {
                    $setString = $setString . "u.fregDate = '" . date('Y-m-d H:i:s') . "',";
                    $setString = $setString . "u.dbmsState = '" . 7 . "' ";
                } else if ($info["key"] == "dbmsMessage") {
                    $setString = $setString . " u.fullRegDesc = '" . $info["value"] . "', ";
                }

            }
            $query = "UPDATE RestApiBundle:RegistrationStatus u SET " . $setString . "  WHERE u.registrationId = " . $record_id;
        }
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->em->close();
    }

}