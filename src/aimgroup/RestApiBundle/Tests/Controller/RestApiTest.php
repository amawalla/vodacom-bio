<?php
/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 09/08/2016
 * Time: 6:29 PM
 */

namespace aimgroup\RestApiBundle\Tests\Controller;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;


class RestApiTest extends WebTestCase
{
    public function testFileUpload()
    {
        $client = static::createClient();

        $photo = new UploadedFile(
            '/Users/oscarmakala/Desktop/PM.png',
            'PM.png',
            'image/png',
            filesize('/Users/oscarmakala/Desktop/PM.png')
        );

        $client->insulate();
        $client->request(
            'POST',
            '/api/registration/customers/register.json',
            array('name' => 'Fabien'),
            array('photo' => $photo)
        );

    }

}