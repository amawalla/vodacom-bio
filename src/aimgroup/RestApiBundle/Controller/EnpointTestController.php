<?php

namespace aimgroup\RestApiBundle\Controller;

use aimgroup\RestApiBundle\Entity\RegImages;
use aimgroup\RestApiBundle\Entity\Registration;
use aimgroup\RestApiBundle\Entity\RegistrationStatus;
use aimgroup\RestApiBundle\Event\ImagesEvent;
use aimgroup\RestApiBundle\Event\ImagesEvents;
use aimgroup\RestApiBundle\Event\PlainTextEvent;
use aimgroup\RestApiBundle\Event\PlainTextProcessEvents;
use aimgroup\RestApiBundle\EventListener\ImagesEventListener;
use aimgroup\RestApiBundle\EventListener\PlainTextEventListener;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Prefix;
use Symfony\Component\EventDispatcher\EventDispatcher;

/**
 * Class ConfigurationController
 * @package aimgroup\RestApiBundle\Controller
 * @Route("/endpoint")
 * @Prefix("endpoint")
 */
class EnpointTestController extends AbstractController
{

    /**
     * @Get("/testendpoint", name="testendpoint")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function testEndpointAction()
    {
//        $em=$this->getDoctrine()->getManager();
//        $dispatcher = $this->get('event_dispatcher');
//        $dispatcher->addSubscriber(new ImagesEventListener($em,$this->container));
//
//        $imageObj = new RegImages();
//        $imageObj->setFullPath("/var/www/html/images/1449757707960_rear-pic_0785124577.jpg");
//        $imageObj->setRegistration("1449757707960");
//        $imageObj->setName("1449757707960_rear-pic_0785124577.jpg");
//        $imageObj->setImageType("rear-pic");
//        $imageObj->setWebPath("http://localhost/images/1449757707960_potrait_0785124577.jpg");
//        $imageObj->setId(32);
//
//        $event = new ImagesEvent($imageObj);
//        $dispatcher->dispatch(ImagesEvents::PROCESS_IMAGES, $event);
//

        $em = $this->getDoctrine()->getManager();
        $dispatcher = $this->get('event_dispatcher');

        $dispatcher->addSubscriber(new PlainTextEventListener($em, $this->container));

        $registration = $em->getRepository("RestApiBundle:Registration")->findOneBy(
            array("registrationid" => "1458228441578")
        );
        if ($registration) {

            $event = new PlainTextEvent($registration);
            $dispatcher->dispatch(PlainTextProcessEvents::PROCESS_PLAINTEXT, $event);
        }

        return $this->buildResponse(true);
    }


    /**
     * @Get("/rerun_treg/{registrationID}", name="rerun_treg")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function rerunTregEndpointAction($registrationID)
    {
	exit("Your not allowd to use this end point");

        $em = $this->getDoctrine()->getManager();

        if ($registrationID) {
            $registration = $em->getRepository("RestApiBundle:Registration")->findOneBy(
                array("registrationid" => $registrationID)
            );
            if ($registration) {
                $dispatcher = $this->get('event_dispatcher');
                $dispatcher->addSubscriber(new PlainTextEventListener($em, $this->container));
                $event = new PlainTextEvent($registration);
                $dispatcher->dispatch(PlainTextProcessEvents::PROCESS_PLAINTEXT, $event);
            }
        }

        return $this->buildResponse(false);

    }

    /**
     * @Get("/rerun_freg/{registrationID}", name="rerun_freg")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function rerunFregEndpointAction($registrationID)
    {
        $em = $this->getDoctrine()->getManager();
        $dispatcher = $this->get('event_dispatcher');
        $dispatcher->addSubscriber(new PlainTextEventListener($em, $this->container));


        if ($registrationID) {
            $registration = $em->getRepository("RestApiBundle:Registration")->findOneBy(
                array("registrationid" => $registrationID)
            );
            if ($registration) {
                $receivedImages = $em->getRepository("RestApiBundle:RegImages")->findBy(
                    array("registration" => $registration->getRegistrationid())
                );
                $this->container->get("api.helper")->logInfo("rerunFregEndpointAction", "rerunFregEndpointAction", array("imagecount" => count($receivedImages)));
                if (count($receivedImages) >= 3) {
                    $this->container->get("vodacom.endpoint")->processImages($receivedImages);
                }
            }

        }

        return $this->buildResponse(true);
    }

}
