<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\RestApiBundle\Controller;

use aimgroup\DashboardBundle\Dao\JsonObject;
use aimgroup\DashboardBundle\Entity\ConfigMaster;
use aimgroup\DashboardBundle\Entity\Idtype;
use aimgroup\RestApiBundle\Dao\Message;
use aimgroup\RestApiBundle\Dao\RegStatusEnum;
use aimgroup\RestApiBundle\Entity\AgentSignatures;
use aimgroup\RestApiBundle\Entity\Device;
use aimgroup\RestApiBundle\Entity\NidaRegistration;
use aimgroup\RestApiBundle\Entity\PortedRegistration;
use aimgroup\RestApiBundle\Entity\RegImages;
use aimgroup\RestApiBundle\Entity\Registration;
use aimgroup\DashboardBundle\Entity\Verifications;
use aimgroup\RestApiBundle\Entity\RegistrationStatus;
use aimgroup\RestApiBundle\Event\ImagesEvent;
use aimgroup\RestApiBundle\Event\ImagesEvents;
use aimgroup\RestApiBundle\Event\PlainTextEvent;
use aimgroup\RestApiBundle\Event\PlainTextProcessEvents;
use aimgroup\RestApiBundle\EventListener\ImagesEventListener;
use aimgroup\RestApiBundle\EventListener\PlainTextEventListener;
use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use FOS\RestBundle\Controller\Annotations\Route;
use League\Flysystem\Filesystem;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Prefix;
use aimgroup\RestApiBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\ORMException;
use PDOException;
use Symfony\Component\Stopwatch\Stopwatch;

/**
 * Description of RegistrationController
 *
 * @author Michael Tarimo
 *
 * @Route("/registration")
 * @Prefix("registration")
 */
class RegistrationController extends AbstractController {

    private $TAG = 'RegistrationController';
    private $correctionCountry = array( 'mtanzania', 'mtz', 'tz' );







 
      
    /**
     * reg_information Summary
     *
     * @Post()
     * @param Request $request
     * 
     * * @return JsonResponse
     * 
     */
    public function registrationDetailAction(Request $request) {
        
        $data2 = array('asdasdas' => "23423432");
        $attributes = $request->request->all();
        
        
        $where = " WHERE 1=1 ";
        $em = $this->getDoctrine()->getManager();

       
        
        
        $em = $this->getDoctrine()->getManager();

        $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();

        $result = $queryBuilder->select('u')
                ->from('RestApiBundle:Registration', 'u')
                ->where('u.id = :id ')
                ->setParameter('id', $attributes['reg_id'])
                ->getQuery()
                ->getArrayResult();

        
        echo json_encode($result);
        exit;
        
    }







    /**
     *
     * @Post()
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function registerCustomerAction(Request $request) {
        $attributes = $request->request->all();

        if (array_key_exists('isPorted', $attributes) || (isset($attributes[ 'reRegistrationFlag' ]) && $attributes[ 'reRegistrationFlag' ] == 'mnp')) {
            return $this->doPortedRegistration($attributes, $request->getHost(), 'data');
        }

        if (array_key_exists('fingercode', $attributes) || (isset($attributes[ 'reRegistrationFlag' ]) && $attributes[ 'reRegistrationFlag' ] == 'biometric')) {
            // return $this->doBiometricRegistration($attributes);
        }

        return $this->doRegistration($attributes, $request->getHost(), 'data');
    }


    private function doBiometricRegistration($attributes) {

        $status = false;
        $message = 'SYSTEM ERROR';
        $resultCode = RegStatusEnum::FAIL;
        $apiHelper = $this->container->get('api.helper');


        try {

            $language = $apiHelper->getLanguage(isset($attributes[ 'langCode' ]) ? $attributes[ 'langCode' ] : 'en');

            $entityManager = $this->getDoctrine()->getManager();

            $registeringDevice = $entityManager->getRepository('RestApiBundle:Device')->findOneBy(
                array( 'msisdn' => $apiHelper->formatMsisdn($attributes[ 'username' ]) )
            );


            if ($registeringDevice) {

                //check duplicates

                $registrationId = $attributes[ 'registrationId' ];

                //check if its a duplicate response
                $registration = $entityManager->getRepository('RestApiBundle:NidaRegistration')->findBy(
                    array(
                        'registrationId' => $registrationId,
                    )
                );

                if (!$registration) {

                    $registration = new NidaRegistration();
                    $registration->setMsisdn($attributes[ 'msisdn' ]);
                    $registration->setIccid($attributes[ 'simserial' ]);
                    $registration->setDeviceId($attributes[ 'deviceId' ]);
                    $registration->setNin($attributes[ 'nin' ]);
                    $registration->setFingerPrintCode($attributes[ 'fingercode' ]);
                    $registration->setOwner($registeringDevice->getUser());
                    $registration->setRegistrationId($attributes[ 'registrationId' ]);
                    $registration->setAppVersion($attributes[ 'appVersion' ]);
                    $registration->setDeviceSentTime(new \DateTime($attributes[ 'deviceSentTime' ]));
                    $registration->setRegisteredTime(new \DateTime($attributes[ 'registeredTime' ]));
                    $registration->setLanguageCode($attributes[ 'langCode' ]);

                    $entityManager->persist($registration);
                    $entityManager->flush();

                    $status = true;
                    $message = $this->get('translator')->trans(
                        'register.success', array(), 'messages', $language
                    );

                    $resultCode = RegStatusEnum::SUCCESS;
                } else {
                    $message = $this->get('translator')->trans('register.success', array(), 'messages', $language);
                    $resultCode = RegStatusEnum::SUCCESS;
                }
            } else {

                $message = $this->get('translator')->trans('register.agent.not.exist', array(), 'messages', $language);
                $resultCode = RegStatusEnum::AGENT_NOT_EXIST;
            }
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
        }

        $response = new JsonResponse();

        $response->setData(
            array(
                'status'     => $status,
                'message'    => $message,
                'resultCode' => $resultCode,
            )
        );

        return $response;
    }

    private function doRegistration($attributes, $host, $channel = 'data') {

        $resp = new JsonObject();
        $status = false;
        $message = 'SYSTEM ERROR';
        $resultCode = RegStatusEnum::FAIL;
        $utilityHelper = $this->container->get('utility.helper');
        $apiHelper = $this->container->get('api.helper');

        $apiHelper->logInfo($this->TAG, 'doRegistration', array(
            'result'  => $attributes,
            'host'    => $host,
            'channel' => $channel,
        ));


        //if ID in below list just exit
        $arrayID = array( 1, 2, 3, 4, 17, 16, 15, 21 );

	if (!$this->isBiometric($attributes)) {

        if (!in_array($attributes[ 'id-type' ], $arrayID)) {
            $resp->setResultCode(RegStatusEnum::FAIL);
            $resp->setMessage("INVALID ID");
            $resp->setStatus(false);

            return $this->buildResponse($resp, Response::HTTP_OK);
            exit;
        }

            $attributes[ 'id-type' ] = ($attributes[ 'id-type' ] == 21 ? 4 : $attributes[ 'id-type' ]);

        }

        $lang = $apiHelper->getLanguage(isset($attributes[ 'langCode' ]) ? $attributes[ 'langCode' ] : 'en');

        $resp->setItem(array( 'registrationId' => $attributes[ 'registrationId' ] ));
        try {

            $em = $this->getDoctrine()->getManager();
            $registration = $this->buildRegistrationObject($attributes, null, $channel);

            $registration->setIsPorted(0);


            //Get Identification Id incase we received Identication name instead of expected Id number
            if (!is_numeric($registration->getIdentificationType())) {
                $identificationType = $em->getRepository('DashboardBundle:Idtype')->findOneBy(array( 'name' => $registration->getIdentificationType() ));
                $registration->setIdentificationType($identificationType->getId());
                $identificationTypeId = $identificationType->getId();
            }

            //validate the registration object
            $errors = $this->get('validator')->validate($registration);
            if (count($errors) > 0) {
                $errorsString = (string)$errors;
                $resp->setMessage($errorsString);
            } else {
                /** @var Device $registeringAgent */
                $registeringAgent = $em->getRepository('RestApiBundle:Device')->findOneBy(array( 'msisdn' => $apiHelper->formatMsisdn($attributes[ 'username' ]) ));
                if (!$registeringAgent) {
                    $message = $this->get('translator')->trans('register.agent.not.exist', array(), 'messages', $lang);
                    $resultCode = RegStatusEnum::AGENT_NOT_EXIST;
                } else {
                    if ($registeringAgent->getUser()->isEnabled() == 1 && $registeringAgent->getUser()->getStatus() == 1) {

                        if (null === $registration) {
                            return;
                        }

                        $registration->setOwner($registeringAgent->getUser());
                        //evaluate if there is no registartion with the same simserial/msisdn
                        $previousRegistrations = $em->getRepository('RestApiBundle:Registration')->findBy(array(
                            'msisdn'    => $registration->getMsisdn(),
                            'simSerial' => $registration->getSimSerial(),
                        ));
                        if ($previousRegistrations) {
                            $isTheSame = false;
                            /** @var  $previousRegistration Registration */
                            foreach ($previousRegistrations as $previousRegistration) {
                                $apiHelper->logInfo($this->TAG, 'doRegistration', array(
                                        'previous' => $previousRegistration->getRegistrationid(),
                                        'current'  => $registration->getRegistrationid(),
                                    )
                                );

                                if (strcmp($previousRegistration->getRegistrationid(), $registration->getRegistrationid()) == 0) {
                                    $isTheSame = true;
                                    //request was from sms so update
                                    if ($previousRegistration->getIsSms() == 1) {
                                        $this->buildRegistrationObject($attributes, $previousRegistration, $channel);
                                        $em->flush();
                                    }
                                    break;
                                }
                            }
                            $apiHelper->logInfo($this->TAG, 'doRegistration', array(
                                'same'           => $isTheSame,
                                'registrationid' => $registration->getRegistrationid(),
                                'simserial'      => $registration->getSimSerial(),
                                'msisdn'         => $registration->getMsisdn(),
                            ));
                            if ($isTheSame) {
                                $status = true;
                                $message = $this->get('translator')->trans('register.success', array(), 'messages', $lang);
                                $resultCode = RegStatusEnum::SUCCESS;
                            } else {
                                $resultCode = RegStatusEnum::DUPLICATE;
                                $message = $this->get('translator')->trans('register.failed', array(), 'messages', $lang);
                            }
                        } else {

                            /*
                                        13th of December 2017. It has happened registration are being submitted without nationality
                                        to reduce the impact, we'have assumed Voters ID & Driver's licence are issued to Tanzanians
                                        */
                            if ($registration->getNationality() == '' && in_array($registration->getIdentificationType(), [ 1, 2 ])) {
                                $registration->setNationality('Tanzania');
                            }

                            $em->persist($registration);
                            $em->flush();

                            if ($registration->getAppVersion() != $registeringAgent->getAppVersion()) {
                                $registeringAgent->setAppVersion($registration->getAppVersion());
                                $em->flush();
                                $apiHelper->logInfo($this->TAG, 'registerCustomerAction', array(
                                    'set appversion' => $registeringAgent->getAppVersion(),
                                    $registration->getAgentMsisdn(),
                                ));
                            }
                            $registrationStatus = new RegistrationStatus();
                            $registrationStatus->setTemporaryRegStatus(0);
                            $registrationStatus->setIcapCounter(0);
                            $registrationStatus->setFullRegStatus(0);
                            $registrationStatus->setFullRegDesc('');
                            $registrationStatus->setVerifyDescr('');
                            $registrationStatus->setIcapState(0);
                            $registrationStatus->setRegistrationId($registration);

                            $registrationStatus->setRegistryType($this->isBiometric($attributes) ? 2 : 1);

                            $em->persist($registrationStatus);
                            $em->flush();

                            /** @var Idtype $identificationType */
                            $identificationType = $em->getRepository('DashboardBundle:Idtype')->find($registration->getIdentificationType());

                            /**
                             * Validate the registration Entity
                             * This filter will mark the registrationStatus as bad if we find any errors
                             *
                             */
                            $this->container->get('vodacom.registration.filter')->filter($registration, $identificationType);

                            $status = true;
                            $message = 'SUCCESS';
                            $resultCode = RegStatusEnum::SUCCESS;
                        }
                    } else {
                        $message = $this->get('translator')->trans('register.agent.not.exist', array(), 'messages', $lang);
                        $resultCode = RegStatusEnum::AGENT_NOT_EXIST;
                    }
                }
            }
        } catch (\Exception $e) {
            $apiHelper->logE($this->TAG, 'registerCustomerAcction', $e);
            exit($e->getMessage());
            $message = $this->get('translator')->trans('register.failed', array(), 'messages', $lang);
        }

        $resp->setResultCode($resultCode);
        $resp->setMessage($message);
        $resp->setStatus($status);


        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    private function doPortedRegistration($attributes, $host, $channel = 'data') {

        $resp = new JsonObject();
        $status = false;
        $message = 'SYSTEM ERROR';
        $resultCode = RegStatusEnum::FAIL;
        $utilityHelper = $this->container->get('utility.helper');
        $apiHelper = $this->container->get('api.helper');

        $apiHelper->logInfo($this->TAG, 'doRegistration', array(
            'result'  => $attributes,
            'host'    => $host,
            'channel' => $channel,
        ));
        $lang = $apiHelper->getLanguage(isset($attributes[ 'langCode' ]) ? $attributes[ 'langCode' ] : 'en');
        $resp->setItem(array( 'registrationId' => $attributes[ 'registrationId' ] ));
        try {
            $em = $this->getDoctrine()->getManager();
            $registration = $this->buildRegistrationObject($attributes, null, $channel);


            $registration->setIsPorted('1');
            $registration->setIsSms('2');


            //Get Identification Id incase we received Identication name instead of expected Id number
            if (!is_numeric($registration->getIdentificationType())) {
                $identificationType = $em->getRepository('DashboardBundle:Idtype')->findOneBy(array( 'name' => $registration->getIdentificationType() ));
                $registration->setIdentificationType($identificationType->getId());
            }

            //validate the registration object
            $errors = $this->get('validator')->validate($registration);
            if (count($errors) > 0) {
                $errorsString = (string)$errors;
                $resp->setMessage($errorsString);
            } else {
                /** @var Device $registeringAgent */
                $registeringAgent = $em->getRepository('RestApiBundle:Device')->findOneBy(array( 'msisdn' => $apiHelper->formatMsisdn($attributes[ 'username' ]) ));
                if (!$registeringAgent) {
                    $message = $this->get('translator')->trans('register.agent.not.exist', array(), 'messages', $lang);
                    $resultCode = RegStatusEnum::AGENT_NOT_EXIST;
                } else {
                    if ($registeringAgent->getUser()->isEnabled() == 1 && $registeringAgent->getUser()->getStatus() == 1) {
                        $registration->setOwner($registeringAgent->getUser());
                        //evaluate if there is no registartion with the same simserial/msisdn
                        $previousRegistrations = $em->getRepository('RestApiBundle:Registration')->findBy(array(
                            'msisdn'    => $registration->getMsisdn(),
                            'simSerial' => $registration->getSimSerial(),
                        ));
                        if ($previousRegistrations) {
                            $isTheSame = false;
                            /** @var  $previousRegistration Registration */
                            foreach ($previousRegistrations as $previousRegistration) {
                                $apiHelper->logInfo($this->TAG, 'doRegistration', array(
                                        'previous' => $previousRegistration->getRegistrationid(),
                                        'current'  => $registration->getRegistrationid(),
                                    )
                                );

                                if (strcmp($previousRegistration->getRegistrationid(), $registration->getRegistrationid()) == 0) {
                                    $isTheSame = true;
                                    //request was from sms so update
                                    if ($previousRegistration->getIsSms() == 1) {
                                        $this->buildRegistrationObject($attributes, $previousRegistration, $channel);
                                        $em->flush();
                                    }
                                    break;
                                }
                            }
                            $apiHelper->logInfo($this->TAG, 'doRegistration', array(
                                'same'           => $isTheSame,
                                'registrationid' => $registration->getRegistrationid(),
                                'simserial'      => $registration->getSimSerial(),
                                'msisdn'         => $registration->getMsisdn(),
                            ));
                            if ($isTheSame) {
                                $status = true;
                                $message = $this->get('translator')->trans('register.success', array(), 'messages', $lang);
                                $resultCode = RegStatusEnum::SUCCESS;
                            } else {
                                $resultCode = RegStatusEnum::DUPLICATE;
                                $message = $this->get('translator')->trans('register.failed', array(), 'messages', $lang);
                            }
                        } else {
                            $em->persist($registration);
                            $em->flush();

                            if ($registration->getAppVersion() != $registeringAgent->getAppVersion()) {
                                $registeringAgent->setAppVersion($registration->getAppVersion());
                                $em->flush();
                                $apiHelper->logInfo($this->TAG, 'registerCustomerAction', array(
                                    'set appversion' => $registeringAgent->getAppVersion(),
                                    $registration->getAgentMsisdn(),
                                ));
                            }
                            $registrationStatus = new RegistrationStatus();
                            $registrationStatus->setTemporaryRegStatus(0);
                            $registrationStatus->setIcapCounter(0);
                            $registrationStatus->setFullRegStatus(0);
                            $registrationStatus->setFullRegDesc('');
                            $registrationStatus->setIcapState(0);
                            $registrationStatus->setRegistrationId($registration);

                            //check Identification Type status
                            $identificationType = $em->getRepository('DashboardBundle:Idtype')->find($identificationTypeId);

                            if ($identificationType->getIsActive() == 0) {
                                $registrationStatus->setTemporaryRegStatus(4);
                                $registrationStatus->setTemporaryRegDesc('[Invalid Identification Type Used]');
                                $registrationStatus->setVerifyDate(new \DateTime());
                                $description = '[Invalid Identification Type Used]';
                                $registrationStatus->setVerifyDescr('InternalVerification: ' . $description);
                                $registrationStatus->setVerifyState(3);
                                $registrationStatus->setAuditDescr('AmonitoringValidation');
                                $registrationStatus->setVerifyBy('541');
                            }

                            $em->persist($registrationStatus);
                            $em->flush();


                            $this->container->get('vodacom.registration.filter')->filter($registration);

                            // if($registrationStatus->getTemporaryRegStatus() !== 4) {
                            //     $dispatcher = $this->get('event_dispatcher');
                            //     $dispatcher->addSubscriber(new PlainTextEventListener($em, $this->container));
                            //     $event = new PlainTextEvent($registration);
                            //     $dispatcher->dispatch(PlainTextProcessEvents::PROCESS_PLAINTEXT, $event);
                            // }

                            $status = true;
                            $message = 'SUCCESS';
                            $resultCode = RegStatusEnum::SUCCESS;
                        }
                    } else {
                        $message = $this->get('translator')->trans('register.agent.not.exist', array(), 'messages', $lang);
                        $resultCode = RegStatusEnum::AGENT_NOT_EXIST;
                    }
                }
            }
        } catch (\Exception $e) {
            $apiHelper->logE($this->TAG, 'registerCustomerAcction', $e);
            $message = $this->get('translator')->trans('register.failed', array(), 'messages', $lang);
        }

        $resp->setResultCode($resultCode);
        $resp->setMessage($message);
        $resp->setStatus($status);


        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    private function buildRegistrationObject($attributes, $registration = null, $channel) {
        if (!$registration) {

            $registration = new Registration();

            if (isset($attributes[ 'isPorted' ])) {
                $registration = new PortedRegistration();
            }
        }

        if (strcmp($channel, 'sms') == 0) {
            $registration->setIsSms(1);
        }


        if (isset($attributes[ 'simserial' ])) {
            $registration->setSimSerial(trim($attributes[ 'simserial' ]));
        }

        $registration->setMsisdn($this->container->get('api.helper')->formatMsisdn(@$attributes[ 'msisdn' ]));

        /**
         * If the registration is ported
         */

        if (isset($attributes[ 'isPorted' ])) {
            $registration->setIsPorted($attributes[ 'isPorted' ]);
        }

        if ($this->isBiometric($attributes)) {
            // Biometric registration
            $registration->setRegistrationType(2);
            $registration->setFingerPrintCode($attributes['fingercode']);
            $registration->setIdentificationType(3);
	   $registration->setIdentification(trim(@$attributes[ 'nin' ]));	

        } else {

            $registration->setRegistrationType(1);
            // Normal Registration
            $registration->setAddress($attributes[ 'address' ]);
           
            $registration->setDob(@$attributes[ 'dateofbirth' ]);
            $registration->setEmail(@$attributes[ 'email' ]);
            $registration->setEsn(@$attributes[ 'esn' ]);

            $registration->setFirstName(@$attributes[ 'firstname' ]);
            $registration->setLastName(@$attributes[ 'surname' ]);
            $registration->setMiddleName(@$attributes[ 'middlename' ]);
            $registration->setGender(@$attributes[ 'gender' ]);
            if (in_array($attributes[ 'nationality' ], $this->correctionCountry)) {
                $attributes[ 'nationality' ] = 'Tanzania';
            }

            $registration->setNationality(@$attributes[ 'nationality' ]);
            $registration->setWards(@$attributes[ 'ward' ]);
            $registration->setTerritory(@$attributes[ 'territory' ]);
            $registration->setRegion(@$attributes[ 'region' ]);
	    $registration->setIdentificationType($attributes[ 'id-type' ]);
	    $registration->setIdentification(trim(@$attributes[ 'id-number' ]));
			
        }

	 if(isset($attributes['alternative-contact'])) {
            $registration->setContact(
                $this->container->get('api.helper')->formatMsisdn(@$attributes[ 'alternative-contact' ])
            );
        }

        $registration->setRegistrationid($attributes[ 'registrationId' ]);
        $registration->setAgentMsisdn($attributes[ 'username' ]);
        $registration->setAppVersion($attributes[ 'appVersion' ]);
        $registration->setDeviceId($attributes[ 'deviceId' ]);
        $registration->setLocationLatLon(isset($attributes[ 'loc' ]) ? $attributes[ 'loc' ] : '');
        $registration->setDeviceModel(isset($attributes[ 'deviceModel' ]) ? $attributes[ 'deviceModel' ] : '');
        $registration->setAgentImei($attributes[ 'imei' ]);
        $registration->setRegistrationTime(new \DateTime($attributes[ 'registeredTime' ]));
        $registration->setDeviceSentTime(new \DateTime($attributes[ 'deviceSentTime' ]));
        $registration->setLanguage(isset($attributes[ 'langCode' ]) ? $attributes[ 'langCode' ] : 'en');


        return $registration;
    }

    /**
     * @View()
     * @Post()
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function registerCustomerSmsAction(Request $request) {
        $json = $_POST;
        $this->container->get('api.helper')->logInfo(
            $this->TAG, 'registerCustomerSmsAction', array( 'request' => $json )
        );

        if ($json) {
            $attributes = array();
            $segments = explode(',', $json[ 'message' ]);
            foreach ($segments as $segment) {
                $keyValue = explode(':', $segment);
                $this->container->get('api.helper')->logInfo(
                    $this->TAG, 'registerCustomerSmsAction', array( 'keyvalue' => $keyValue )
                );
                if ($keyValue && count($keyValue) == 2) {
                    $attributes[ $this->getSmsFields()[ $keyValue[ 0 ] ] ] = $keyValue[ 1 ];
                }
            }
            $this->container->get('api.helper')->logInfo(
                $this->TAG, 'registerCustomerSmsAction', array( 'attributes' => $attributes )
            );
            if (count($attributes) > 0) {
                return $this->doRegistration($attributes, $request->getHost(), 'sms');
            }
        }

        return $this->buildResponse(false);
    }

    private function getSmsFields() {
        $sms_fields = array(
            1  => 'firstname',
            2  => 'middlename',
            3  => 'surname',
            4  => 'nationality',
            5  => 'dateofbirth',
            6  => 'id-type',
            7  => 'id-number',
            8  => 'msisdn',
            9  => 'simserial',
            10 => 'registrationId',
            11 => 'username',
            12 => 'territory',
            13 => 'region',
        );

        return $sms_fields;
    }

    /**
     * Is Reg Type biometric
     *
     * @param $data
     * @return bool
     */
    protected function isBiometric($data) {

       return isset($data['fingercode']) && isset($data['nin']);
    }

    /**
     * @View()
     * @Post()
     * @param Request $request
     * @return JsonResponse
     */
    public function uploadSignatureAction(Request $request) {
        $apiHelper = $this->container->get("api.helper");
        $resp = new JsonObject();
        $status = false;

        $message = "";
        $username = "";
        try {

            /** @var  $file UploadedFile */
            $file = $request->files->get('file');
            $fileName = $file->getClientOriginalName();
            $imagesDir = $this->container->getParameter("resource_path") . DIRECTORY_SEPARATOR . "signatures" . DIRECTORY_SEPARATOR . date("Ymd");

            // Move the file to the directory where brochures are stored
            $file->move($imagesDir, $fileName);

            $remoteFilePath = date('Ymd') . DIRECTORY_SEPARATOR . $fileName;

            $localImageFilePath = "{$imagesDir}/{$fileName}";

            //Upload the file to s3
            /** @var  $filesystem Filesystem */
            $filesystem = $this->container->get('s3_filesystem');

            $webPath = '';
            $shouldProceed = true;
            if (!$filesystem->has($remoteFilePath)) {
                $uploaded = $filesystem->write($remoteFilePath, file_get_contents($localImageFilePath));
                if ($uploaded) {
                    //Allow the file to be accessible publicly
                    $filesystem->setVisibility($remoteFilePath, 'public');
                    $webPath = "https://s3.amazonaws.com/ereg2/vodacom/images/{$remoteFilePath}";
                } else {
                    $shouldProceed = false;
                }
            } else {
                $webPath = "https://s3.amazonaws.com/ereg2/vodacom/images/{$remoteFilePath}";
            }
            if ($shouldProceed) {
                $attributes = $request->request->all();
                $username = $apiHelper->formatMsisdn($attributes[ 'username' ]);
                $imageType = $attributes[ 'image_type' ];

                $this->get("api.helper")->log("uploadSingatureAction", array( "image:" . $imageType . ",file:" . $fileName, $username ), true);
                // Generate a unique name for the file before saving it
                $em = $this->getDoctrine()->getManager();


                $imageObj = new AgentSignatures();
                $imageObj->setName($fileName);
                $imageObj->setFullPath($localImageFilePath);
                $imageObj->setMsisdn($username);
                $imageObj->setWebPath($webPath);
                $sign = $em->getRepository("RestApiBundle:AgentSignatures")->findOneBy(array( "msisdn" => $username ));
                if (!$sign) {
                    $em->persist($imageObj);
                } else {
                    $sign->setFullPath($imageObj->getFullPath());
                    $sign->setWebPath($webPath);
                }
                try {
                    $em->flush();
                } catch (\Exception $e) {
                    $this->get('api.helper')->logE("uploadSingatureAction", "uploadSingatureAction", $e);
                }

                $status = true;
                /** @var  $configuration ConfigMaster */
                $configuration = $em->getRepository("DashboardBundle:ConfigMaster")->findOneBy(
                    array( "configType" => "formConfig" )
                );
                if ($configuration) {
                    $responseObj[ "formConfig" ][ "form" ] = json_decode($configuration->getConfig(), true);
                    $responseObj[ "formConfig" ][ "version" ] = $configuration->getVersion();
                    $resp->setItem($responseObj);
                }

                $message = "SUCCESS";
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }
        $resp->setMessage($message);
        $resp->setStatus($status);
        $this->get("api.helper")->logInfo("uploadSingatureAction", "uploadSingatureAction", array( "massage" => $resp->getMessage(), "username" => $username ));

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @View()
     * @Post()
     * @param Request $request
     * @return JsonResponse
     */
    public function uploadImagesAction(Request $request) {
        $attributes = $request->request->all();
        $resp = new JsonObject();
        $status = false;
        $message = "";
        try {

            /** @var  $file UploadedFile */
            $file = $request->files->get('file');

            $fileName = $attributes[ 'username' ] . "_" . $file->getClientOriginalName();

            $remoteFilePath = date('Ymd') . DIRECTORY_SEPARATOR . $fileName;

            $localImageFilePath = $file->getRealPath();

            $webPath = '';
            $this->get("api.helper")->logInfo("uploadImagesAction", "uploadImagesAction0", array(
                    "local"  => $localImageFilePath,
                    "remote" => $remoteFilePath
                )
            );
            //Upload the file to s3
            /** @var  $filesystem Filesystem */
            $filesystem = $this->container->get('s3_filesystem');
            $shouldProceed = true;
            if (!$filesystem->has($remoteFilePath)) {

                $uploaded = $filesystem->write($remoteFilePath, file_get_contents($localImageFilePath), array( 'ServerSideEncryption' => 'AES256' ));

                if ($uploaded) {
                    //Allow the file to be accessible publicly
                    //$filesystem->setVisibility($remoteFilePath, 'private');
                    $filesystem->setVisibility($remoteFilePath, 'public');
                    $webPath = "https://s3.amazonaws.com/ereg2/vodacom/images/{$remoteFilePath}";
                    // remove the file in the local directory
                    //unlink($localImageFullPath);
                } else {
                    $shouldProceed = false;
                }
            } else {
                $webPath = "https://s3.amazonaws.com/ereg2/vodacom/images/{$remoteFilePath}";
            }
            if ($shouldProceed) {

                $imageType = $attributes[ 'image_type' ];

                $this->get("api.helper")->logInfo($this->TAG, "uploadImagesAction", array(
                        "type"           => $imageType,
                        "file:"          => $fileName,
                        "registrationId" => $attributes[ "registrationid" ] )
                );
                // Generate a unique name for the file before saving it
                $em = $this->getDoctrine()->getManager();

                $imageObj = new RegImages();
                $imageObj->setFullPath($localImageFilePath);
                $imageObj->setRegistration($attributes[ "registrationid" ]);
                $imageObj->setName($fileName);
                $imageObj->setImageType($imageType);
                $imageObj->setWebPath($webPath);
                $em->persist($imageObj);

                try {
                    $em->flush();
                } catch (\Exception $e) {
                    #   $this->get('api.helper')->logE("uploadImagesAction", "uploadImagesAction", $e);
                }

                $dispatcher = $this->get('event_dispatcher');
                $dispatcher->addSubscriber(new ImagesEventListener($em, $this->container));

                $event = new ImagesEvent($imageObj);
                $dispatcher->dispatch(ImagesEvents::PROCESS_IMAGES, $event);


                $status = true;
                $queryBuilder = $em->createQueryBuilder();
                $count = $queryBuilder->select('count(r.id)')
                    ->from('RestApiBundle:RegImages', 'r')
                    ->where('r.registration=:registration')
                    ->setParameter('registration', $attributes[ "registrationid" ])
                    ->getQuery()->getSingleScalarResult();


                $resp->setItem($count);

                if ($count >= 4) {
                    $registration = $em->getRepository("RestApiBundle:Registration")->findOneBy(array( "registrationid" => $attributes[ "registrationid" ] ));
                    if ($registration) {
                        $query1 = "UPDATE RegistrationStatus SET allimageDate = '" . date('Y-m-d H:i:s') . "' WHERE registrationid = " . $registration->getId() . " AND allimageDate IS NULL;";
                        $em->getConnection()->exec($query1);
                        $this->get("api.helper")->logInfo("uploadImagesAction", "uploadImagesActionQuery", array( "query" => $query1 ));
                    }
                }

                $query1 = "UPDATE registration SET image_count =" . $count . " WHERE registrationid like '" . $attributes[ "registrationid" ] . "'";
                $em->getConnection()->exec($query1);
                $this->get("api.helper")->logInfo("uploadImagesAction", "uploadImagesActionQuery", array( "query" => $query1 ));
                $message = "SUCCESS";
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $this->get('api.helper')->logE("uploadImagesAction", "uploadImagesAction-exception", $e);
        }
        $resp->setMessage($message);
        $resp->setStatus($status);
        $this->get("api.helper")->logInfo("uploadImagesAction", "uploadImagesAction", array( "message" => $resp->getMessage() ));


        $this->get("api.helper")->logInfo("uploadImagesAction ANTOO", "uploadImagesAction", array(
            "registrationid" => $attributes[ "registrationid" ],
            "ccount"         => $count,
            "appversion"     => $attributes[ 'appversion' ],
            "type"           => $imageType,
            "file:"          => $fileName,
            "username"       => $attributes[ 'username' ],
            "message"        => $resp->getMessage() ));

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    public function retriveSummaryAction(Request $request) {
        // daily, weekly, monthly
    }

    /**
     * @View()
     * @Post()
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function checkStatusAction(Request $request) {


        $resp = new JsonObject();
        $status = false;
        $resObj = json_decode($request->getContent(), true);
        $lang = 'sw';

        $this->get('api.helper')->logInfo($this->TAG, 'checkStatusAction :::::: -- ', $resObj);

        $this->get('api.helper')->logInfo($this->TAG, 'checkStatusAction 222:::::: -- ', array(
            'loc'    => @$resObj[ 'loc' ],
            'REGIDD' => @$resObj[ 'registrationId' ],
        ));


        $em = $this->container->get('doctrine')->getManager();


        $query1 = " SELECT deviceId, last_check FROM `device` WHERE deviceId = '" . @$resObj[ 'deviceId' ] . "' LIMIT 1";

        $connection = $em->getConnection();
        $statement = $connection->prepare($query1);
        $statement->execute();
        $data2 = $statement->fetchAll();


        if (count($data2) == 1) {

            //echo " :  ".$data2[0]['last_check']."    : ";
            $diffff = (strtotime(date('Y-m-d H:i:s')) - strtotime($data2[ 0 ][ 'last_check' ])) / 1000 * 60;

            if ($diffff < 5) {
                echo "less than 5 mins ago";
                exit;
            }
        } else {
            exit;
        }


        $query1 = " UPDATE `device` set last_check = " . date('YmdHis') . " WHERE deviceId = '" . @$resObj[ 'deviceId' ] . "' LIMIT 1";

        $connection = $em->getConnection();
        $statement = $connection->prepare($query1);
        $statement->execute();


        try {
            $temp = array();

            $queryBuilder = $em->createQueryBuilder();
            $count = $queryBuilder->select('count(r.id)')
                ->from('RestApiBundle:RegImages', 'r')
                ->where('r.registration=:registration')
                ->setParameter('registration', @$resObj[ 'registrationId' ])
                ->getQuery()->getSingleScalarResult();


            if ($count >= 4) {

                $result = $this->container->get('fos_elastica.manager')->getRepository('RestApiBundle:RegistrationStatus')->getRegStatusByRegNoAndMsisdn(@$resObj[ 'registrationId' ], @$resObj[ 'msisdn' ]);


                // $result = $em->createQuery("SELECT v from RestApiBundle:RegistrationStatus v left join v.registrationId r "
                //            . "WHERE r.registrationid = '" . @$resObj["registrationId"] . "' ")->getResult();

                if ($result) {
                    /** @var  $regStatus RegistrationStatus */
                    $regStatus = $result[ 0 ];
                    /** @var  $registration Registration */
                    $registration = $regStatus->getRegistrationId();
                    if (in_array($regStatus->getIcapState(), array( 6 ))) {
                        $successMsg = $this->container->get('translator')->trans(
                            'register.status.successful', array( '%msisdn%' => $resObj[ 'msisdn' ] ), 'messages', ($registration->getLanguage() == null) ? $lang : $registration->getLanguage()
                        );

                        /*
                          if ($regStatus->getFullRegStatus() == 6 || $regStatus->getFullRegStatus() == 7) {
                          $temp["message"] = $successMsg;
                          $temp["status"] = 1;
                          } else if ($regStatus->getTemporaryRegStatus() == 6) {
                          $temp["message"] = $successMsg;
                          $temp["status"] = 1;
                          }
                         */


                        if ($regStatus->getFullRegStatus() == 6 || $regStatus->getFullRegStatus() == 7) {
                            $temp[ 'message' ] = $successMsg;
                            $temp[ 'status' ] = 1;
                        }
                    } else {

                        $status = $regStatus->getFullRegStatus() == 7 ? 1 : 3;
                        // treg fail
                        if ($regStatus->getTemporaryRegStatus() == '4') {
                            $failedMsg = $this->container->get('translator')->trans(
                                'register.status.failed', array(
                                '%msisdn%' => $resObj[ 'msisdn' ],
                                '%desc%'   => $regStatus->getFullRegDesc(),
                            ), 'messages', ($registration->getLanguage() == null) ? $lang : $registration->getLanguage()
                            );

                            $status = 3;
                        }

                        // ver fail
                        if ($regStatus->getVerifyState() == '3') {
                            $failedMsg = $this->container->get("api.helper")->getTranslatedLanguage("register.verify.status.fregdeclined", array( '%msisdn%' => $resObj[ 'msisdn' ], "%errordescription%" => "Failed Verification" ), "sw");
                            $temp[ 'status' ] = 3;
                        }

                        //freg fail
                        if ($regStatus->getFullRegStatus() == '4') {
                            $failedMsg = $this->container->get('translator')->trans(
                                'register.status.failed', array(
                                '%msisdn%' => $resObj[ 'msisdn' ],
                                '%desc%'   => $regStatus->getFullRegDesc(),
                            ), 'messages', ($registration->getLanguage() == null) ? $lang : $registration->getLanguage()
                            );

                            $status = 3;
                            $temp[ 'message' ] = $failedMsg;
                        }


                        $temp[ 'status' ] = $status;
                        #$temp['message'] = $failedMsg;
                    }
                } else {

                }
            } else {

            }
            #$status = true;
            $resp->setItem($temp);
            $message = 'success';
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }
        $resp->setMessage($message);
        $resp->setStatus($status);


        $this->get('api.helper')->logInfo($this->TAG, 'checkStatusAction 3333:::::: -- ', array(
            'msisdn'   => @$resObj[ 'msisdn' ],
            'status'   => $status,
            'username' => @$resObj[ 'username' ],
            'message'  => $message,
        ));

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * Makes a Post to intergration box
     * @Get("/nida_api/{id}", name="/nida_api")
     */
    public function nidaApiAction($id) {

        $url = $this->container->getParameter('nida_api');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('RestApiBundle:Registration')->find($id);

        //list all id types to formaster key array now that we aint entity relating
        $query = $this->getDoctrine()->getManager()
            ->createQuery('SELECT t FROM DashboardBundle:Idtype t');

        $idtype = $query->getArrayResult();

        foreach ($idtype as $key => $val) {
            $idKeyArray[ $val[ 'id' ] ] = $val[ 'name' ];
        }

        //get those configured to be verified
        $result = $em->createQuery(
            'SELECT p '
            . ' from DashboardBundle:ConfigMaster p '
            . " WHERE p.name = 'id_verification_list' "
        )
            ->getOneOrNullResult();


        if ($result) {

            $idsToBeVerified = json_decode($result->getConfig());

            //echo $entity->getIdentificationType() . " | " .json_encode($idKeyArray) . " | " . json_encode($idsToBeVerified); exit;

            if (in_array($idKeyArray[ $entity->getIdentificationType() ], $idsToBeVerified)) {
                //clal API

                $data = array(
                    'idNumber' => $entity->getIdentification(),
                );

                $response = $this->curl_message($url, $data);


                $response = json_decode($response, true);

                if ($response[ 'json' ][ 'message' ] == 'success') {
                    echo 'OPT 1111';

                    $customer_compare_name = $firstname = $response[ 'json' ][ 'item' ][ 'firstName' ] . ' ' . $lastname = $response[ 'json' ][ 'item' ][ 'lastName' ];

                    //do 85% match
                    $likehood = $this->likehood_percent(
                        $entity->getFirstName() . ' ' . $entity->getLastName(), $customer_compare_name
                    );
                    $likehood_reverse = $this->likehood_percent(
                        $entity->getLastName() . ' ' . $entity->getFirstName(), $customer_compare_name
                    );

                    if ($likehood >= 85 || $likehood_reverse >= 85) {

                        $verification = new Verifications();
                        $verification->setIdType('NIDA');
                        $verification->setResultType('MATCH');
                        $em->persist($verification);
                        $em->flush();
                    } else {
                        echo 'INNOPT 2ttt';
                        //decline on mismatch basis
                        $em->createQuery(
                            "UPDATE RestApiBundle:RegistrationStatus u SET u.temporaryRegStatus = 3, u.temporaryRegDesc = 'Validation Box :: Name mismatch' "
                            . ' WHERE u.registrationId = ' . $entity->getRegistrationid()
                        )
                            ->getArrayResult();

                        $verification = new Verifications();
                        $verification->setIdType('NIDA');
                        $verification->setResultType('MISMATCH');
                        $em->persist($verification);
                        $em->flush();
                    }
                } else {
                    echo 'OPT 222';

                    //Decline invalid ID type

                    $em->createQuery(
                        "UPDATE RestApiBundle:RegistrationStatus u SET u.temporaryRegStatus = 13, u.temporaryRegDesc = 'Validation Box :: Invalid ID type' "
                        . ' WHERE u.registrationId = ' . $id
                    )
                        ->getArrayResult();


                    $verification = new Verifications();
                    $verification->setIdType('NIDA');
                    $verification->setResultType('INVALIDID');
                    $em->persist($verification);
                    $em->flush();
                }
            }
        }

        exit;
    }

//    /**
//     * https://vodademo.registersim.com/api/registration/api_callback/222894/treg/retry/
//     */
//    /**
//     * Makes a Post to intergration box
//     * @Get("/api_callback/{record_id}/{reg_type}/{status}/", name="api_callback_retry")
//     * @Template("DashboardBundle:Admin:agent_details.html.twig")
//     * @View()
//     */
//    public function apiCallbackRetryAction($record_id, $reg_type, $status)
//    {
//        if (strcasecmp($reg_type, "treg") == 0 && strcasecmp($status, "retry") == 0) {
//            $em = $this->container->get('doctrine')->getManager();
//            $registration = $em->getRepository("RestApiBundle:Registration")->find($record_id);
//            if ($registration) {
//                $dispatcher = $this->get('event_dispatcher');
//                $dispatcher->addSubscriber(new PlainTextEventListener($em, $this->container));
//                $event = new PlainTextEvent($registration);
//                $dispatcher->dispatch(PlainTextProcessEvents::PROCESS_PLAINTEXT, $event);
//            }
//        }
//        exit;
//    }


    function curl_message($url, $data) {
        $data_string = json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
            $ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string),
            )
        );

        return curl_exec($ch);
    }

    /**
     * Makes a Post to intergration box
     * @Get("/api_post_mpesa/{record_id}", name="api_post_mpesa")
     * @View()
     */
    public function resendMpesaAction($record_id) {
        $apiHelper = $this->container->get('api.helper');
        try {
            /**
             *
             * moves this to parameters
             */
            $url = 'http://middleware1.registerim.com:8080/VodaGateway/rest/services/vodaGateway/mpesaRegisterCustomerG2';
            $em = $this->container->get('doctrine')->getManager();

            /** @var  $registration Registration */
            $registration = $em->getRepository('RestApiBundle:Registration')->findOneBy(array( 'id' => $record_id ));
            if ($registration) {
                /** @var  $user User */
                $user = $registration->getOwner();
                $idType = $em->getRepository('DashboardBundle:Idtype')->find($registration->getIdentificationType());
                $region = $em->getRepository('DashboardBundle:Region')->findOneBy(array( 'code' => $registration->getRegion() ));
                $territory = $em->getRepository('DashboardBundle:Territory')->find($registration->getTerritory());
                $territoryN = $territory->getName();

                $callback = $this->container->getParameter('callback_url');
                $data = array(
                    'REG_TYPE'    => 'TREG',
                    'RECORD_ID'   => $registration->getId(),
                    'MSISDN'      => '255' . $registration->getMsisdn(),
                    'FIRST_NAME'  => $registration->getFirstName(),
                    'LAST_NAME'   => $registration->getLastName(),
                    'MIDDLE_NAME' => $registration->getMiddleName(),
                    'DOB'         => date('d-m-Y', strtotime($registration->getDob())),
                    'ID_TYPE'     => $idType->getName() == 'NSSF Pension Card' ? 'Pension Fund ID' : $idType->getName(),
                    'ID_NUMBER'   => $registration->getIdentification(),
                    'AGENT_NAME'  => $user->getFirstName() . ' ' . $user->getLastName(),
                    'AGEN_CODE'   => '0' . $registration->getAgentMsisdn(),
                    'location'    => $territoryN,
                    'region'      => $region->getName(),
                );

                $response = $apiHelper->curl_message($url, $data, $callback);
                $apiHelper->logInfo($this->TAG, 'resendMpesaAction', array( 'data' => $data, 'response' => $response ));
            }
        } catch (\Exception $e) {
            $apiHelper->logE($this->TAG, 'resendMpesaAction', $e);
        }

        return $this->buildResponse(array(), Response::HTTP_OK);
    }

    /**
     * Makes a Post to intergration box
     * @Get("/api_post_dbms/{record_id}", name="api_post_dbms_")
     * @View()
     */
    public function apiRetryDBMSAction($record_id) {
        $apiHelper = $this->container->get('api.helper');
        /** move this to parameters yml */
        $url = 'http://middleware1.registersim.com:8080/VodaGateway/rest/services/vodaGateway/dbmsRegisterCustomer';
        $apiHelper->logInfo($this->TAG, 'apiRetryDBMSAction', array( 'id' => $record_id ));
        $em = $this->container->get('doctrine')->getManager();
        try {
            /** @var  $registration Registration */
            $registration = $em->getRepository('RestApiBundle:Registration')->findOneBy(array( 'id' => $record_id ));
            if ($registration) {
                //get images
                $images = $em->getRepository('RestApiBundle:RegImages')->findBy(array( 'registration' => $registration->getRegistrationid() ));
                $signature = $em->getRepository('RestApiBundle:AgentSignatures')->findOneBy(array( 'msisdn' => $registration->getAgentMsisdn() ));
                /** @var  $user User */
                $user = $registration->getOwner();
                $idType = $em->getRepository('DashboardBundle:Idtype')->find($registration->getIdentificationType());

                $region = $em->getRepository('DashboardBundle:Region')->findOneBy(array( 'code' => $registration->getRegion() ));
                $territory = $em->getRepository('DashboardBundle:Territory')->find($registration->getTerritory());
                $territoryN = $territory->getName();

                $data = array(
                    'REG_TYPE'    => 'FREG',
                    'RECORD_ID'   => $registration->getId(),
                    'MSISDN'      => '255' . $registration->getMsisdn(),
                    'FIRST_NAME'  => $registration->getFirstName(),
                    'LAST_NAME'   => $registration->getLastName(),
                    'MIDDLE_NAME' => $registration->getMiddleName(),
                    'DOB'         => date('d-m-Y', strtotime($registration->getDob())),
                    'ID_TYPE'     => $idType->getName() == 'NSSF Pension Card' ? 'Pension Fund ID' : $idType->getName(),
                    'ID_NUMBER'   => $registration->getIdentification(),
                    'AGENT_NAME'  => $user->getFirstName() . ' ' . $user->getLastName(),
                    'AGEN_CODE'   => '0' . $registration->getAgentMsisdn(),
                    'location'    => $territoryN,
                    'region'      => $region->getName(),
                );


                $image_key = array(
                    'signature' => 'customer_signature_image',
                    'rear-pic'  => 'id_back_image',
                    'camera'    => 'id_back_image',
                    'front-pic' => 'id_front_image',
                    'potrait'   => 'picture',
                );


                //$em->getRepository("RestApiBundle:AgentSignatures")
                /** @var  $image RegImages */
                foreach ($images as $image) {
                    $data[ $image_key[ $image->getImageType() ] ] = $image->getWebPath();
                }

                $data[ 'agent_signature_image' ] = $signature->getWebPath();

                $apiHelper->logInfo($this->TAG, 'apiRetryDBMSAction', $data);

                if (count($images) != 4) {
                    exit;
                }

                $apiHelper->logInfo($this->TAG, 'apiRetryDBMSAction', $data);

                $callback = $this->container->getParameter('callback_url');
                $response = $apiHelper->curl_message($url, $data, $callback);
                $apiHelper->logInfo($this->TAG, 'apiRetryDBMSAction:processImages', array(
                    'data'     => $data,
                    'response' => $response,
                ));

                $response_message = new \SimpleXMLElement($response);
                $response_message = json_decode(json_encode($response_message), true);

                if ($response_message[ 'resultCode' ] == 200) {
                    $infos = $response_message[ 'resultsParameters' ][ 'entry' ];
                    $this->get('api.helper')->logInfo($this->TAG, 'apiRetryDBMSAction', array( 'parameters' => $infos ));
                    $setString = '';
                    $shouldUpdate = false;
                    foreach ($infos as $info) {
                        if ($info[ 'key' ] == 'dbmsStatus' && $info[ 'value' ] == 'Success') {
                            $shouldUpdate = true;
                            $setString = $setString . ", u.fregDate = '" . date('Y-m-d H:i:s') . "', ";
                            $setString = $setString . "u.dbmsState = '7', ";
                            $setString = $setString . "u.fullRegStatus = '7'";
                        } else {
                            if ($info[ 'key' ] == 'dbmsMessage') {
                                $setString = $setString . " u.fullRegDesc = '" . $info[ 'value' ] . "' ";
                            }
                        }
                    }


                    if ($shouldUpdate) {
                        $query = 'UPDATE RestApiBundle:RegistrationStatus u SET ' . $setString . '  WHERE u.registrationId = ' . $record_id;
                        $this->get('api.helper')->logInfo($this->TAG, 'apiRetryDBMSAction', array( 'query' => $query ));
                        $em->createQuery($query)->getArrayResult();
                        $this->get('api.helper')->logInfo($this->TAG, 'apiRetryDBMSAction', array( 'query' => $query ));
                    }
                }
            }
        } catch (\Exception $e) {
            $apiHelper->logE($this->TAG, 'apiRetryDBMSAction', $e);
        }

        return $this->buildResponse(array(), Response::HTTP_OK);
    }

    /**
     * Makes a Post to intergration box
     * @Get("/api_callback_dbms_retry/{record_id}/{reg_type}/{status}/{message}", name="api_callback_dbms_retry")
     * @Template("DashboardBundle:Admin:agent_details.html.twig")
     * @View()
     */
    public function apiCallbackDBMSAction($record_id, $reg_type, $status, $message) {
        $this->get('api.helper')->logInfo(
            $this->TAG, 'apiCallbackAction', array(
                'id'      => $record_id,
                'type'    => $reg_type,
                'status'  => $status,
                'message' => $message,
            )
        );

        return $this->buildResponse(array(), Response::HTTP_OK);
    }

    /**
     * Makes a Post to intergration box
     * @POST("/mpesaCallback/", name="mpesaCallback")
     * @View()
     */
    public function apiMpesaCallback(Request $request) {
        $attributes = $request->request->all();
        $this->get('api.helper')->logInfo($this->TAG, 'apiMpesaCallback', $attributes);
    }

    /**
     * Makes a Post to intergration box
     * @POST("/api_post_callback/", name="api_post_callback_one")
     * @POST("/api_post_callback", name="api_post_callback_two")
     * @POST("/api_callback/", name="api_callback_one")
     * @POST("/api_callback", name="api_callback_two")
     * @View()
     */
    public function apiPostCallbackAction(Request $request) {
        $attributes = $request->request->all();
        $this->get('api.helper')->logInfo($this->TAG, "apiPostCallbackAction", $attributes);

        if ($attributes[ 'timetogorandom' ] && $attributes[ 'timetogorandom' ] == "400523L59fgjarkfjdkdjf30391dffmvcRbvmbgghl" && $attributes[ 'evenmorerandom' ] && $attributes[ 'evenmorerandom' ] == "3fflddHGwewPerenl5590azzmbkerebphhkam34lgm"
        ) {
            if (isset($attributes[ "task" ])) {
                $task = strtoupper($attributes[ "task" ]);
                if (strtolower($task) == "freg") {
                    $this->processFregCallback(
                        $attributes[ 'recordId' ], $attributes[ 'task' ], $attributes[ 'subtask' ], $attributes[ 'status' ], $attributes[ 'message' ]
                    );
                } else if (strtolower($task) == "treg") {

                    if (@$attributes[ 'g2StatusCode' ] == 'IC13200') {
                        $attributes[ 'createMPESAStatus' ] = 'registered';
                    }

                    $this->processTregCallback(
                        @$attributes[ 'recordId' ], $attributes[ 'task' ], @$attributes[ 'subtask' ], @$attributes[ 'status' ], @$attributes[ 'createMPESAStatus' ], @$attributes[ 'mpesaStatus' ], @$attributes[ 'newRegistration' ], @$attributes[ "tregMessage" ], @$attributes[ "message" ], @$attributes[ "responseCode" ]);
                }
            }
        }
        exit;
    }

    private function processTregCallback($record_id, $reg_type, $reg_sub_type, $status, $create_mpesa_status, $mpesa_status, $isRereg, $tregMessage, $message, $responseCode) {
        $this->get('api.helper')->logInfo($this->TAG, "processTregCallback", array(
                'id'            => $record_id,
                'type'          => $reg_type,
                'sub_type'      => $reg_sub_type,
                'status'        => $status,
                'message'       => $message,
                'response_code' => $responseCode,
                'treg_message'  => $tregMessage,
                "re_reg"        => $isRereg
            )
        );


        $this->get('api.helper')->logInfo($this->TAG, " ANTO processTregCallback", array(
                'id'            => $record_id,
                'type'          => $reg_type,
                'sub_type'      => $reg_sub_type,
                'status'        => $status,
                'message'       => $message,
                'response_code' => $responseCode,
                'treg_message'  => $tregMessage,
                "re_reg"        => $isRereg
            )
        );


//   if(strcmp($status,"fail")==0)
        //          return 0;
        // A fix for corba api connection problem
        if ($message == 'Unable to establish connection with corba api' && $status == 'fail') {
            $this->get('api.helper')->logInfo($this->TAG, "processTregCallback", array(
                    'message' => 'Exiting the request due to corba api connection issue' )
            );

            return;
        }

        $where = null;

        if ($create_mpesa_status || $reg_sub_type == "MPESACreateCustomer") {

            if ($responseCode === '13200') {
                $create_mpesa_status = 'registered';
            }

            $mpesaStatus = $this->getMpesaState()[ @$create_mpesa_status ];

            $this->get('api.helper')->logInfo($this->TAG, "processTregCallback", array( 'message' => $record_id . ' HereAtSaveMpesa 111 : mpesaStatus:' . $mpesaStatus . " date " . date('Y-m-d H:i:s') ));

            $setString = "u.mpesa_state = '" . $mpesaStatus . "', ";
            $setString = $setString . " u.mpesaDate = '" . date('Y-m-d H:i:s') . "' ";
        } else {
            $description = strlen($tregMessage) > 0 ? $tregMessage : $message;
            $setString = "u.temporaryRegDesc = '" . $description . "', u.auditDescr = 'Amonitor!',";
            if ($reg_type) {
                $status = $this->getStatusKeys()[ $status ];
                #$regType = ($isRereg == true || $isRereg == null) ? 1 : 0;
                $regType = $isRereg ? 1 : 0;
                $setString = $setString . "u.temporaryRegStatus = '" . $status . "' ";
                if (strtolower($reg_type) == "treg") { // treg
                    $setString = $setString . ", u.tregDate = '" . date('Y-m-d H:i:s') . "', ";
                    $setString = $setString . "u.reg_type = '" . $regType . "' ,";
                    $setString = $setString . "u.icap_state = '" . $status . "' ";
                }
                $where = "u.icap_state !=6";
            }
        }

        $this->updateRegistration($setString, $record_id, $status, $message, $where);
        if ($record_id) {
            $em = $this->getDoctrine()->getManager();
            /** @var  $registration Registration */
            $registration = $em->getRepository("RestApiBundle:Registration")->find($record_id);
            if ($registration) {
                $this->get('api.helper')->logInfo($this->TAG, "tregNotification", array( 'status' => $status, 'msisdn' => $registration->getMsisdn() ));
                if (in_array($status, array( 6 ))) {
                    $msg = $this->container
                        ->get("api.helper")
                        ->getTranslatedLanguage("register.status.tregsuccess", array( '%msisdn%' => $registration->getMsisdn() ), $registration->getLanguage());
                    $message = new Message();
                    $message->setDeviceId($registration->getDeviceId());
                    $message->setMessage($msg);
                    $message->setTopic($this->container->getParameter("mqtt_notify_msg"));
                    $this->container->get('api.helper')->sendMessage($message);
                } else if ($status == 4 && $description == "Fully Registered iCAP") {
                    ##############$this->container->get('api.helper')->notifyRegStatus($registration, $status, $message);
                }
            }
        }
    }

    private function ANTOOLDprocessTregCallback(
        $record_id, $reg_type, $reg_sub_type, $status, $create_mpesa_status, $mpesa_status, $isRereg, $tregMessage, $message
    ) {
        $this->get('api.helper')->logInfo($this->TAG, 'processTregCallback', array(
                'id'           => $record_id,
                'type'         => $reg_type,
                'sub_type'     => $reg_sub_type,
                'status'       => $status,
                'message'      => $message,
                'treg_message' => $tregMessage,
                're_reg'       => $isRereg,
            )
        );


        $this->get('api.helper')->logInfo($this->TAG, ' ANTO processTregCallback', array(
                'id'           => $record_id,
                'type'         => $reg_type,
                'sub_type'     => $reg_sub_type,
                'status'       => $status,
                'message'      => $message,
                'treg_message' => $tregMessage,
                're_reg'       => $isRereg,
            )
        );


//   if(strcmp($status,"fail")==0)
        //          return 0;

        $where = null;

        if ($create_mpesa_status || $reg_sub_type == 'MPESACreateCustomer') {

            $mpesaStatus = $this->getMpesaState()[ @$create_mpesa_status ];

            $this->get('api.helper')->logInfo($this->TAG, 'processTregCallback', array( 'message' => $record_id . ' HereAtSaveMpesa 111 : mpesaStatus:' . $mpesaStatus . ' date ' . date('Y-m-d H:i:s') ));

            $setString = "u.mpesa_state = '" . $mpesaStatus . "', ";
            $setString = $setString . " u.mpesaDate = '" . date('Y-m-d H:i:s') . "' ";
        } else {
            $description = strlen($tregMessage) > 0 ? $tregMessage : $message;
            $setString = "u.temporaryRegDesc = '" . $description . "', u.auditDescr = 'Amonitor!',";
            if ($reg_type) {
                $status = $this->getStatusKeys()[ $status ];
                #$regType = ($isRereg == true || $isRereg == null) ? 1 : 0;
                $regType = $isRereg ? 1 : 0;
                $setString = $setString . "u.temporaryRegStatus = '" . $status . "' ";
                if (strtolower($reg_type) == 'treg') { // treg
                    $setString = $setString . ", u.tregDate = '" . date('Y-m-d H:i:s') . "', ";
                    $setString = $setString . "u.reg_type = '" . $regType . "' ,";
                    $setString = $setString . "u.icap_state = '" . $status . "' ";
                }
                $where = 'u.icap_state !=6';
            }
        }

        $this->updateRegistration($setString, $record_id, $status, $message, $where);
        if ($record_id) {
            $em = $this->getDoctrine()->getManager();
            /** @var  $registration Registration */
            $registration = $em->getRepository('RestApiBundle:Registration')->find($record_id);
            if ($registration) {
                $this->get('api.helper')->logInfo($this->TAG, 'tregNotification', array(
                    'status' => $status,
                    'msisdn' => $registration->getMsisdn(),
                ));
                if (in_array($status, array( 6 ))) {
                    $msg = $this->container
                        ->get('api.helper')
                        ->getTranslatedLanguage('register.status.tregsuccess', array( '%msisdn%' => $registration->getMsisdn() ), $registration->getLanguage());
                    $message = new Message();
                    $message->setDeviceId($registration->getDeviceId());
                    $message->setMessage($msg);
                    $message->setTopic($this->container->getParameter('mqtt_notify_msg'));
                    $this->container->get('api.helper')->sendMessage($message);
                } else {
                    if ($status == 4 && $description == 'Fully Registered iCAP') {
                        $this->container->get('api.helper')->notifyRegStatus($registration, $status, $message);
                    }
                }
            }
        }
    }

    private function processFregCallback($record_id, $reg_type, $reg_sub_type, $status, $message) {

        $apiHelper = $this->container->get('api.helper');
        $em = $this->getDoctrine()->getManager();
        $this->get('api.helper')->logInfo(
            $this->TAG, "apiCallbackAction", array( 'id'       => $record_id,
                                                    'type'     => $reg_type,
                                                    'sub_type' => $reg_sub_type,
                                                    'status'   => $status,
                                                    'message'  => $message )
        );

        $dbms_failure = "Failed-to-post-to-DBMS-message-Premature-end-of-file-status-Failed";


        //"status":"success_dbms"
        $message = $this->clean($message);

        if (strcmp($dbms_failure, $message) == 0)
            return 0;


        if (strcmp($status, "fail") == 0)
            return 0;


        $setString = "u.fullRegDesc = '" . $message . "', ";

        if ($reg_sub_type) {
            $status = $this->getStatusKeys()[ $status ];
            $setString = $setString . "u.fullRegStatus = '" . $status . "',";
            $setString = $setString . "u.temporaryRegStatus = '" . $status . "',";

            if ($reg_sub_type == "freg_dbms") { // dbms
                $setString = $setString . "u.fregDate = '" . date('Y-m-d H:i:s') . "',";
                $setString = $setString . "u.dbms_state = '" . $status . "' ";
            } else if ($reg_sub_type == "freg_icap") { //freg icap
                $setString = $setString . "u.dbmsDate = '" . date('Y-m-d H:i:s') . "',";
                $setString = $setString . "u.icap_state = '" . $status . "',";
            }
        }


        if ($record_id) {
            /** @var  $registration Registration */
            $registration = $em->getRepository("RestApiBundle:Registration")->find($record_id);
            if ($registration) {
                $this->get('api.helper')->logInfo(
                    $this->TAG, "apiCallbackAction", array( 'status' => $status, 'msisdn' => $registration->getMsisdn() )
                );
                if (in_array($status, array( 6 ))) {
                    $apiHelper->notifyRegStatus($registration, $status, $message);
                }
            }
        }

        $this->updateRegistration($setString, $record_id, $status, $message);
    }

    private function ANTOOLDprocessFregCallback($record_id, $reg_type, $reg_sub_type, $status, $message) {
//exit('Here..');
        $apiHelper = $this->container->get('api.helper');
        $em = $this->getDoctrine()->getManager();
        $this->get('api.helper')->logInfo(
            $this->TAG, "apiCallbackAction", array( 'id'       => $record_id,
                                                    'type'     => $reg_type,
                                                    'sub_type' => $reg_sub_type,
                                                    'status'   => $status,
                                                    'message'  => $message )
        );

        $dbms_failure = "Failed-to-post-to-DBMS-message-Premature-end-of-file-status-Failed";


        //"status":"success_dbms"
        $message = $this->clean($message);

        if (strcmp($dbms_failure, $message) == 0)
            return 0;


        if (strcmp($status, "fail") == 0)
            return 0;


        $setString = "u.fullRegDesc = '" . $message . "', ";

        if ($reg_sub_type) {
            $status = $this->getStatusKeys()[ $status ];
            $setString = $setString . "u.fullRegStatus = '" . $status . "',";

            if ($reg_sub_type == "freg_dbms") { // dbms
                $setString = $setString . "u.fregDate = '" . date('Y-m-d H:i:s') . "',";
                $setString = $setString . "u.dbms_state = '" . $status . "' ";
            } else if ($reg_sub_type == "freg_icap") { //freg icap
                $setString = $setString . "u.dbmsDate = '" . date('Y-m-d H:i:s') . "',";
                $setString = $setString . "u.icap_state = '" . $status . "',";
            }
        }


        if ($record_id) {
            /** @var  $registration Registration */
            $registration = $em->getRepository("RestApiBundle:Registration")->find($record_id);
            if ($registration) {
                $this->get('api.helper')->logInfo(
                    $this->TAG, "apiCallbackAction", array( 'status' => $status, 'msisdn' => $registration->getMsisdn() )
                );
                if (in_array($status, array( 6 ))) {
                    $apiHelper->notifyRegStatus($registration, $status, $message);
                }
            }
        }

        $this->updateRegistration($setString, $record_id, $status, $message);
    }

    /* private function processFregCallback($record_id, $reg_type, $reg_sub_type, $status, $message)
      {

      $apiHelper = $this->container->get('api.helper');
      $em = $this->getDoctrine()->getManager();
      $this->get('api.helper')->logInfo(
      $this->TAG, 'apiCallbackAction', array(
      'id'       => $record_id,
      'type'     => $reg_type,
      'sub_type' => $reg_sub_type,
      'status'   => $status,
      'message'  => $message,
      )
      );

      $dbms_failure = 'Failed-to-post-to-DBMS-message-Premature-end-of-file-status-Failed';


      //"status":"success_dbms"
      $message = $this->clean($message);

      if (strcmp($dbms_failure, $message) == 0) {
      return 0;
      }


      if (strcmp($status, 'fail') == 0) {
      return 0;
      }


      $setString = "u.fullRegDesc = '".$message."', ";

      if ($reg_sub_type) {
      $status = $this->getStatusKeys()[$status];
      $setString = $setString."u.fullRegStatus = '".$status."',";

      if ($reg_sub_type == 'freg_dbms') { // dbms
      $setString = $setString."u.fregDate = '".date('Y-m-d H:i:s')."',";
      $setString = $setString."u.dbms_state = '".$status."' ";
      } else {
      if ($reg_sub_type == 'freg_icap') { //freg icap
      $setString = $setString."u.dbmsDate = '".date('Y-m-d H:i:s')."',";
      $setString = $setString."u.icap_state = '".$status."',";
      }
      }
      }


      if ($record_id) {
      $registration = $em->getRepository('RestApiBundle:Registration')->find($record_id);
      if ($registration) {
      $this->get('api.helper')->logInfo(
      $this->TAG, 'apiCallbackAction', array('status' => $status, 'msisdn' => $registration->getMsisdn())
      );
      if (in_array($status, array(6))) {
      #############$apiHelper->notifyRegStatus($registration, $status, $message);
      }
      }
      }

      $this->updateRegistration($setString, $record_id, $status, $message);
      } */

    private function updateRegistration($setString, $record_id, $status, $message, $where = null) {
        $setString = substr($setString, 0, -1);
        $em = $this->getDoctrine()->getManager();
        $apiHelper = $this->container->get('api.helper');

        /* $query = "UPDATE RestApiBundle:RegistrationStatus u SET " . $setString . "  WHERE u.registrationId = " . $record_id;
          if (!is_null($where)) {
          $query = $query . " and " . $where;
          }
          $em->createQuery($query)->getArrayResult();
          $em->flush(); */


        $query = "UPDATE RegistrationStatus u SET " . $setString . " WHERE u.registrationId = " . $record_id;
        if (!is_null($where)) {
            $query = $query . " and " . $where;
        }
        $em->getConnection()->exec($query);

        $this->get('api.helper')->logInfo($this->TAG, "updateRegistration", array( 'query' => $query ));
    }

    private function getStatusKeys() {
        $status_keys = array(
            'success'      => 2,
            'declined'     => 3,
            'fail'         => 4,
            'retry'        => 5,
            'success_cap'  => 6,
            'success_dbms' => 7,
        );

        return $status_keys;
    }

    private function getMpesaState() {
        $mpesa_states = array(
            'success'    => 1,
            'failed'     => 2,
            'fail'       => 2,
            'registered' => 3,
        );

        return $mpesa_states;
    }

    private function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    /**
     * https://vodademo.registersim.com/api/registration/api_callback/125/treg/fail/Successful%20Temporary%20Registered%20iCAP
     */

    /**
     * Makes a Post to intergration box
     * @Get("/api_callback/{record_id}/{reg_type}/{status}/{message}", name="api_callback")
     * @Template("DashboardBundle:Admin:agent_details.html.twig")
     * @View()
     */
    public function apiCallbackAction($record_id, $reg_type, $status, $message) {
        /*
          /api_callback/{record_id}/{reg_type}/{status}/{message}

          record_id —>
          reg_type —> treg | freg
          status —> success | fail | retry | success_cap | success_dbms
          message —> url_encode_descriptive_message
         */

        //
        $this->get('api.helper')->logInfo(
            $this->TAG, 'apiCallbackAction', array(
                'id'      => $record_id,
                'type'    => $reg_type,
                'status'  => $status,
                'message' => $message,
            )
        );

        $reg_type_key = array(
            'treg' => 'temporaryRegStatus',
            'freg' => 'fullRegStatus',
        );
        $registration_type = $reg_type_key[ $reg_type ];
        $status_keys = array(
            'success'      => 2,
            'declined'     => 3,
            'fail'         => 4,
            'retry'        => 5,
            'success_cap'  => 6,
            'success_dbms' => 7,
        );

        $message_key = array(
            'treg_desc' => 'temporaryRegDesc',
            'freg_desc' => 'fullRegDesc',
        );

        $description = $message_key[ $reg_type . '_desc' ];

        $status = $status_keys[ $status ];

        $em = $this->getDoctrine()->getManager();

        /**
         * This is a temporary solution to mark the ones with name mismatch as declined.
         * Later will evalute on the gateway to mark as declined.
         */
        if ($status == '4' && strtolower($message) == 'mismatched') {
            $status = 3;
        }

        $call_back_datefield = 'fregDate';
        if ($reg_type == 'treg') {
            $call_back_datefield = 'tregDate';
        }

        $em->createQuery(
            'UPDATE RestApiBundle:RegistrationStatus u SET u.' . $registration_type . " = '" . $status . "', u." . $call_back_datefield . " = '" . date('Y-m-d H:i:s') . "', u." . $description . " = '" . $message . "' WHERE u.registrationId = " . $record_id
        )
            ->getArrayResult();


        if ($record_id) {
            /** @var  $registration Registration */
            $registration = $em->getRepository('RestApiBundle:Registration')->find($record_id);
            $this->get('api.helper')->logInfo(
                $this->TAG, 'apiCallbackAction', array( 'status' => $status, 'msisdn' => $registration->getMsisdn() )
            );
        }

        if (in_array($status, array( 7, 6 ))) {
            $apiHelper = $this->container->get('api.helper');
            $apiHelper->notifyRegStatus($registration, $status, $message);
        }


        exit;
    }

    /**
     * Makes a Post to intergration box
     * @Get("/tigo_api/{id}", name="/tigo_api")
     */
    public function tigoApiAction($id) {

        $url = 'http://196.41.61.247:8080/Nida_API/api/nida/getfulldemographicdata';


        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('RestApiBundle:Registration')->find($id);

        //list all id types to formaster key array now that we aint entity relating
        $query = $this->getDoctrine()->getManager()
            ->createQuery('SELECT t FROM DashboardBundle:Idtype t');

        $idtype = $query->getArrayResult();

        foreach ($idtype as $key => $val) {
            $idKeyArray[ $val[ 'id' ] ] = $val[ 'name' ];
        }

        //get those configured to be verified
        $result = $em->createQuery(
            'SELECT p '
            . ' from DashboardBundle:ConfigMaster p '
            . " WHERE p.name = 'id_verification_list' "
        )
            ->getOneOrNullResult();


        if ($result) {

            $idsToBeVerified = json_decode($result->getConfig());

            //echo $entity->getIdentificationType() . " | " .json_encode($idKeyArray) . " | " . json_encode($idsToBeVerified); exit;

            if (in_array($idKeyArray[ $entity->getIdentificationType() ], $idsToBeVerified)) {
                //clal API

                $data = array(
                    'idNumber' => $entity->getIdentification(),
                );

                echo $response = $this->curl_message($url, $data);
            }
        }

        $response = json_decode($response, true);

        $em = $this->getDoctrine()->getManager();
        if ($response[ 'json' ][ 'message' ] == 'success') {

            $customer_compare_name = $firstname = $response[ 'json' ][ 'item' ][ 'firstName' ] . ' ' . $lastname = $response[ 'json' ][ 'item' ][ 'lastName' ];

            //do 85% match
            $likehood = $this->likehood_percent(
                $entity->getFirstName() . ' ' . $entity->getLastName(), $customer_compare_name
            );
            $likehood_reverse = $this->likehood_percent(
                $entity->getLastName() . ' ' . $entity->getFirstName(), $customer_compare_name
            );

            if ($likehood >= 85 || $likehood_reverse >= 85) {
                //let it be
            } else {
                //decline on mismatch basis
                $em->createQuery(
                    "UPDATE RestApiBundle:RegistrationStatus u SET u.temporaryRegStatus = 3, u.temporaryRegDesc = 'Validation Box :: Name mismatch' "
                    . ' WHERE u.registrationId = ' . $entity->getRegistrationid()
                )
                    ->getArrayResult();
            }
        } elseif ($response[ 'json' ][ 'message' ] == 'Invalid id number') {

            //Decline invalid ID type
            $em->createQuery(
                "UPDATE RestApiBundle:RegistrationStatus u SET u.temporaryRegStatus = 3, u.temporaryRegDesc = 'Invalid ID type' "
                . ' WHERE u.registrationId = ' . $entity->getRegistrationid()
            )
                ->getArrayResult();
        }

        exit;
    }

    function likehood_percent($str1, $str2) {
        similar_text($str1, $str2, $percentMatch);

        return round($percentMatch);
    }

    /**
     * hourly_summary Summary
     *
     * @Route("/ccount_summary", name="ccount_summary")
     * @Method("GET")
     * @Template()
     */
    public function ccount_summaryAction() {

        $end_date = date_format(date_sub(date_create(date('Y-m-d')), date_interval_create_from_date_string('10 days')), 'Y-m-d');

        $em = $this->getDoctrine()->getManager();

        $where = " WHERE 1=1 AND date(x.createdDate) <= '" . $end_date . "' ";

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');


        $treg = $freg = $fail = $pending = 0;

        $count_tfreg = $em->createQuery('SELECT count(r) as reg_count, x.image_count, r.temporaryRegStatus as treg, r.fullRegStatus as freg FROM RestApiBundle:RegistrationStatus r'
            . ' JOIN r.registrationId x ' . $where
            . ' GROUP BY x.image_count, r.temporaryRegStatus, r.fullRegStatus'
        )->getArrayResult();


        $data[ 'freg' ] = $data[ 'receivedinfo' ] = $data[ 'fail' ] = $data[ 'treg' ] = $data[ 'pending_freg' ] = $data[ 'pendingtreg' ] = $data[ 'pending' ] = $data[ 'total' ] = 0;
        foreach ($count_tfreg as $key => $val) {
            $data[ 'total' ] = $data[ 'total' ] + $val[ 'reg_count' ];
            if ($val[ 'image_count' ] < 3) {
                $data[ 'pending' ] = $data[ 'pending' ] + $val[ 'reg_count' ];
            } else {
                $data[ 'receivedinfo' ] = $data[ 'receivedinfo' ] + $val[ 'reg_count' ];
            }

            //available states 0,1,2,3,4,5,6,7
            if (in_array($val[ 'treg' ], array( 1, 2, 6, 7 ))) {
                //TREG
                $data[ 'treg' ] = $data[ 'treg' ] + $val[ 'reg_count' ];
            } elseif (in_array($val[ 'treg' ], array( 4, 5, 3 ))) {
                //FAIL
                $data[ 'fail' ] = $data[ 'fail' ] + $val[ 'reg_count' ];
            } else {
                //NOT SENT YET
                $data[ 'pendingtreg' ] = $data[ 'pendingtreg' ] + $val[ 'reg_count' ];
            }

            if (in_array($val[ 'treg' ], array( 1, 2, 6, 7 )) && in_array($val[ 'freg' ], array( 1, 2, 6, 7, 4 ))) {
                //FREG
                $data[ 'freg' ] = $data[ 'freg' ] + $val[ 'reg_count' ];
            }
            if (in_array($val[ 'treg' ], array( 1, 2, 6, 7 )) && $val[ 'freg' ] == 0) {
                //PENDING FREG
                $data[ 'pending_freg' ] = $data[ 'pending_freg' ] + $val[ 'reg_count' ];
            }
            //--
        }

        $summary = $em->createQuery('SELECT s '
            . 'from DashboardBundle:Summary s '
            . " where s.dataType = 'top_count'")
            ->getOneOrNullResult();


        if ($summary) {
            $query1 = "UPDATE Summary SET dataString = '" . json_encode($data) . "' WHERE dataType = 'top_count'";
            $em->getConnection()->exec($query1);
        } else {
            $query1 = "INSERT INTO `Summary` (`createdDate`, `dataType`, `dataString`, `dataStringTwo`) VALUES ('" . date('Y-m-d H:i:s') . "', 'top_count', '" . json_encode($data) . "', '');";
            $em->getConnection()->exec($query1);
        }

        exit;
    }

}
