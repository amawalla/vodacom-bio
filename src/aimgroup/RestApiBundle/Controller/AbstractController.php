<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\RestApiBundle\Controller;

use \FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\HttpFoundation\Response;
use aimgroup\RestApiBundle\Dao\LogsDao;

/**
 * This abstract controller encapsulates all functions which are common to all controllers.
 *
 * @author Michael Tarimo
 */
class AbstractController extends FOSRestController {

    /**
     *
     * @param type $dataObject The data you wish to put in the response object
     * @param type|int $responseStatus The status of the response
     * @param type|string $format This can be 'json' or 'xml'. The daufault is 'json'
     * @return JsonResponse
     */
    protected function buildResponse($dataObject, $responseStatus = Response::HTTP_OK, $format = 'json') {
        $response = new Response();
        
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new GetSetMethodNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        
        $serializedProduct = $serializer->serialize($dataObject, $format);
        
        return $response->create($serializedProduct, $responseStatus);
    }
    
    public function getAccountingManipulator() {
        if (!$this->container->has('aimgroup_accounting.manipulator')) {
            throw new \LogicException('The accounting manipulator is not registered in your application.');
        }

        return $this->container->get('aimgroup_accounting.manipulator');
    }
    
    
    
    /**
     * This method includes common data to be merged with desired data to compose response
     * 
     * @param array $data
     * @return type merged array
     */
    protected function prepareResponse(array $data) {
        $user = $this->getUser();
        $roles = $user->getRoles();

        $result = array_merge(array('date_today' => date('Y-m-d'),
                                    'user' => $user,
                                    'roles' => $roles, 'isAdministrator' => $this->isRoleAdministrator($roles)), $data);
        
        return $result;
    }
    
    private function isRoleAdministrator($roles) {
        if($roles != null) {
            return in_array('ROLE_ADMINISTRATOR', $roles);
        }
        return false;
    }
    
    public function getUser() {
        return $this->get('security.token_storage')->getToken()->getUser();
    }
    
    public function logUserEvent($action, $description, $metadata = array()) {
        $logsDao = new LogsDao();
        
        $logsDao->addUserLogs($this->getUser(), $action, $description, $metadata);
    }
    
    public function logSystemEvent($tag, $method, $content, $isError = FALSE) {
        $apiHelper = $this->container->get('api.helper');
        if($isError) {
           $apiHelper->logE($tag, $method, $content);
        }
        $apiHelper->logInfo($tag . ":" . $method, $content);
    }
}