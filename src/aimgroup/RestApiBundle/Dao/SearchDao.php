<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\RestApiBundle\Dao;

use Elastica\Query;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Description of SearchDao
 *
 * @author robert
 */
class SearchDao {
    
    private  $_seachManager;
    private $_container;
    
    public function __construct(ContainerInterface $container){
        $this->_seachManager = $container->get('fos_elastica.manager');
        $this->_container = $container;
       
        
    }
    
    public function search($keyword){
        $finder = $this->_container->get('fos_elastica.finder.eReg');

        $boolQuery = new Query\BoolQuery();

        $queryString = new Query\QueryString();
        $queryString->setDefaultField('_all');
        $queryString->setQuery($keyword);
        $boolQuery->addMust($queryString);

        $query = new Query($boolQuery);

        $query->setHighlight(array(
            "fields" => array("*" => new \stdClass)
        ));

        // Returns a mixed array of any objects mapped + highlights
        //$results = $finder->findHybrid($query);
        return $finder->find($query);
    }
    
    public function searchRegistration($keyword){
      
        $finder = $this->_container->get('fos_elastica.finder.eReg.registration');
        
        $queryString = new Query\QueryString();
        $queryString->setDefaultField('_all');
        $queryString->setQuery($keyword);
        
        $query = new Query($queryString);
        $query->setHighLight(array(
            'fields'=>array('*'=>new \stdClass())
        ));

        return $finder->find($query);
    }

    public function searchRegistrationAutoComplete($keyword){

        $finder = $this->_container->get('fos_elastica.finder.eReg.registration');

        $queryString = new Query\QueryString();
        $queryString->setDefaultField('_all');
        $queryString->setQuery($keyword);

        $query = new Query($queryString);
        $query->setHighLight(array(
            'fields'=>array('*'=>new \stdClass())
        ));

        return $finder->find($query);
    }
}
