<?php
/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 14/01/2016
 * Time: 3:44 PM
 */

namespace aimgroup\RestApiBundle\Dao;


namespace aimgroup\RestApiBundle\Dao;

/**
 *
 *
 * Errors below 100 are for retrying. apart from that fail
 * Class RegStatusEnum
 * @package aimgroup\RestApiBundle\Dao
 *
 */
abstract class RegStatusEnum
{
    const SUCCESS = 0;
    const FAIL = 2;
    const AGENT_NOT_EXIST = 100;
    const DUPLICATE = 101;


}