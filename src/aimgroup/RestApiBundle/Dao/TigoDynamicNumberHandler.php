<?php
/**
 * Created by PhpStorm.
 * User: samdev
 * Date: 12/16/15
 * Time: 11:42 AM
 */

namespace aimgroup\RestApiBundle\Dao;


use Symfony\Component\DependencyInjection\Container;

class TigoDynamicNumberHandler
{

    private $container;
    private $apiEndPointUrl = null;
    private $tigoApiUsername = null;
    private $tigoApiPassword = null;
    private $tigoApiConsumerID = null;

    public function __construct(Container $container )
    {

        $this->container = $container;

        $this->apiEndPointUrl = $this->container->getParameter('TIGO_NNIM_API_END_POINT');
        $this->tigoApiUsername = $this->container->getParameter('TIGO_NNIM_USERNAME');
        $this->tigoApiPassword = $this->container->getParameter('TIGO_NNIM_PASSWORD');
        $this->tigoApiConsumerID = $this->container->getParameter('TIGO_NNIM_CNNSUMER_ID');
    }

    /**
     * @param string $msisdn
     * @param int $quantity
     * @param bool $sequential
     * @param int $validityWindow
     * @return array|bool
     */
    public function ReserveNetworkNumbersTemporarily( $msisdn = null, $quantity = 3, $sequential = false, $validityWindow = null )
    {

        if( is_null($msisdn) ) {
            return false;
        }

        if( is_null($validityWindow) ) {
            $validityWindow = $this->container->getParameter('DEFAULT_NNIM_VALIDITY_WINDOW');
        }

        $tigoResponseArray = $this->SendDataRemotely( 'reservenetworknumbers', array(
            'v1:ReserveNetworkNumbersTemporarilyRequest' => array(
                'v3:RequestHeader' => array(
                    'v3:GeneralConsumerInformation' => array(
                        'v3:consumerID' => $this->tigoApiConsumerID,
                        'v3:transactionID' => 'TRX_'.substr($msisdn, -9),
                        'v3:country' => 'TZA',
                        'v3:correlationID' => 'COR_'.substr($msisdn, -9)
                    )
                ),
                'v1:RequestBody' => array(
                    'v1:networkNumberSubTypeId' => 56,
                    'v1:quantitytobereserved' => $quantity,
                    'v1:validityWindowMinutes' => $validityWindow,
                    'v1:sequential' => ($sequential ? 'true' : 'false'),
                    'v1:networkNumberPskFilter' => $msisdn,
                    'v1:additionalParameters' => array(
                        'v2:ParameterType' => array(
                            'v2:parameterName' => '?',
                            'v2:parameterValue' => '?'
                        )
                    )
                )
            )
        ));

        if( $tigoResponseArray['ErrorCode'] != 0 || $tigoResponseArray['HttpStatusCode'] != 200 ){
            return false;
        }

        $xmlResponseObject = simplexml_load_string( $tigoResponseArray['ResponseContent']);

        $xmlResponseObject->registerXPathNamespace('soapenv', 'http://schemas.xmlsoap.org/soap/envelope/');
        $xmlResponseObject->registerXPathNamespace('v31', 'http://xmlns.tigo.com/ResponseHeader/V3');
        $xmlResponseObject->registerXPathNamespace('wsse', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd');

        $numberReservationCode = (string)$xmlResponseObject->children('soapenv', true )->Body->children('v11', true)->ReserveNetworkNumbersTemporarilyResponse->responseBody->reservationCode;

        $reservedMobileNumbers = array();

        foreach( $xmlResponseObject->xpath('//v31:ResponseHeader') as $header ) {
            foreach( $header->xpath('//v11:psk') as $value ) {
                $reservedMobileNumbers[] = (string)$value[0];
            }
        }

        return array(
            'ReservationCode' => $numberReservationCode,
            'ReservedNumbers' => $reservedMobileNumbers
        );

    }


    /**
     * FindNetworkDevice - Find IMSI number for the provided ICCID number
     *
     * @param string $iccid
     * @param string $msisdn
     * @return bool|string
     */
    public function FindNetworkDevice( $msisdn = null, $iccid = null, $recordID = null ) {

        if( is_null($iccid) || is_null($msisdn) || is_null($recordID) ) {
            return false;
        }

        $tigoResponseArray = $this->SendDataRemotely( 'findnetworkdevice', array(
            'v1:FindNetworkDeviceRequest' => array(
                'v3:RequestHeader' => array(
                    'v3:GeneralConsumerInformation' => array(
                        'v3:consumerID' => $this->tigoApiConsumerID,
                        'v3:transactionID' => 'TRX_'.$recordID,
                        'v3:country' => 'TZA',
                        'v3:correlationID' => 'COR_'.$recordID
                    ),
                    'v1:RequestBody' => array(
                        'v1:iccid' => $iccid,
                        'v1:searchCriteria' => 'usk',
                        'v1:additionalParameters' => array(
                            'v2:ParameterType' => array(
                                'v2:parameterName' => '?',
                                'v2:parameterValue' => '?'
                            )
                        )
                    )
                )
            )
        ));

        if( $tigoResponseArray['ErrorCode'] != 0 || $tigoResponseArray['HttpStatusCode'] != 200 ){
            return false;
        }

        $imsi = $this->FindStringBetween( $tigoResponseArray['ResponseContent'], '<v1:psk>', '</v1:psk>' );

        return ( strlen($imsi) > 0 ? $imsi : false );

    }


    /**
     * ConfirmNetworkNumberReservation
     *
     * @param null $selectedMsisdn
     * @param null $reservationCode
     * @param null $recordID
     * @return bool
     */
    public function ConfirmNetworkNumberReservation($selectedMsisdn = null, $reservationCode = null, $recordID = null ) {

        if( is_null($selectedMsisdn) || is_null($reservationCode) || is_null($recordID) ) {
            return false;
        }

        $tigoResponseArray = $this->SendDataRemotely( 'confirmnetworknumberrerserve', array(
            'v1:ConfirmNetworkNumberReservationRequest' => array(
                'v3:RequestHeader' => array(
                    'v3:GeneralConsumerInformation' => array(
                        'v3:consumerID' => $this->tigoApiConsumerID,
                        'v3:transactionID' => 'TRX_'.$recordID,
                        'v3:country' => 'TZA',
                        'v3:correlationID' => 'COR_'.$recordID
                    )
                ),
                'v1:RequestBody' => array(
                        'v1:selectedMsisdn' => $selectedMsisdn,
                        'v1:reservationCode' => $reservationCode,
                        'v1:additionalParameters' => array(
                            'v2:ParameterType' => array(
                                'v2:parameterName' => '?',
                                'v2:parameterValue' => '?'
                            )
                        )
                    )
                )
        ));

        if( $tigoResponseArray['ErrorCode'] != 0 || $tigoResponseArray['HttpStatusCode'] != 200 ) {
            return false;
        }

        return $this->FindStringBetween( $tigoResponseArray['ResponseContent'], '<v31:status>', '</v31:status>' );

    }


    /**
     * @param string $msisdn
     * @param string $imsi
     * @param string $recordID
     * @param array $customerDetails
     * @return array|bool
     */
    public function CreateSubscriber($msisdn = null, $imsi = null, $recordID = null, $customerDetails = array() ) {

        if( is_null($msisdn) || is_null($imsi) || is_null($recordID) || empty($customerDetails) ) {
            return false;
        }

        //send requests
        $tigoResponseArray = $this->SendDataRemotely( 'createsubscriber', array(
            'v1:CreateSubscriberRequest' => array(
                'v3:RequestHeader' => array(
                    'v3:GeneralConsumerInformation' => array(
                        'v3:consumerID' => $this->tigoApiConsumerID,
                        'v3:transactionID' => 'TRX_'.$recordID,
                        'v3:country' => 'TZA',
                        'v3:correlationID' => 'COR_'.$recordID
                    )
                ),
                'v1:requestBody' => array(
                    'v1:msisdn' => $msisdn,
                    'v1:imsi' => $imsi,
                    'v1:KI' => 12343678,
                    'v1:customer' => array(
                            'v11:name' => $customerDetails['name'],
                            'v11:birthDate' => $customerDetails['dob'],
                            'v11:gender' => $customerDetails['gender'],
                            'v11:address'.$customerDetails['address'],
                            'v11:clientGrade' => 'Default'
                    ),
                    'v1:subscriber' => array(
                        'v12:language' => 'Swahili',
                        'v12:customerType' => 'Prepaid',
                        'v12:mainProductID' => 3000048,
                    ),
                    'account' => array(
                        'v13:customerName' => $customerDetails['name'],
						'v13:address' => $customerDetails['address'],
						'v13:title' => 'Mr',
						'v13:billcycleType' => 1,
						'v13:additionalProperties' => array(
							'v2:ParameterType' => array(
								'v2:parameterName' => 'EmailBillAddr',
								'v2:parameterValue' => 'ekyc@aimgroup.co.tz'
                            ),
							'v2:ParameterType' => array(
								'v2:parameterName' => 'SMSBillAddr',
								'v2:parameterValue' => 255713123454
                            ),
                            'v2:ParameterType' => array(
								'v2:parameterName' => 'BillAddr1',
								'v2:parameterValue' => 'NA'
                            ),
                            'v2:ParameterType' => array(
								'v2:parameterName' => 'FirstName',
								'v2:parameterValue' => 'NA'
                            ),
                            'v1:orderBMList' => array(
								'v1:orderBM' => array(
									'v1:billMediumId' => 2,
									'v1:effDate' => 20140408000000
                                )
                            ),
							'v1:products' => ''
                        )
                    )
                )
            )
        ));

        if( $tigoResponseArray['ErrorCode'] != 0 || $tigoResponseArray['HttpStatusCode'] != 200 ) {
            return false;
        }

        //$xml = simplexml_load_string( $response );

        $status = $this->FindStringBetween( $tigoResponseArray['ResponseContent'], '<v31:status>', '</v31:status>' );

        return array(
            'status' => $status,
            'message' => $this->FindStringBetween( $tigoResponseArray['ResponseContent'], '<cmn:description>', '</cmn:description>')
        );
    }

    /**
     * @param string $msisdn
     * @param string $imsi
     * @return string
     */
    public function LinkNumberToNetworkDevice($msisdn = null, $imsi = null, $recordID = null ) {

        if( is_null($msisdn) || is_null($imsi) || is_null($recordID) ) {
            return false;
        }

        $tigoResponseArray = $this->SendDataRemotely( 'linknumbers', array(
            'LinkNumbertoNetworkDeviceRequest' => array(
                'v3:RequestHeader' => array(
                    'v3:GeneralConsumerInformation' => array(
                        'v3:consumerID' => $this->tigoApiConsumerID,
                        'v3:transactionID' => 'TRX_'.$recordID,
                        'v3:country' => 'TZA',
                        'v3:correlationID' => 'COR_'.$recordID
                    )
                ),
                'v1:requestBody' => array(
                    'v1:msisdn' => $msisdn,
                    'v1:IMSI' => $imsi,
                    'v1:externalReference' => 'REGISTRATION',
                    'v1:authCode' => '',
                    'v1:additionalParameters' => array(
                        'v2:ParameterType' => array(
                            'v2:parameterName' => '',
                            'v2:parameterValue' => ''
                        )
                    )
                )
            )
        ));

        if( $tigoResponseArray['ErrorCode'] != 0 || $tigoResponseArray['HttpStatusCode'] != 200 ) {
            return false;
        }

        return $this->FindStringBetween( $tigoResponseArray['ResponseContent'], '<v31:status>', '</v31:status>');

    }

    public function RemoveNetworkNumberDeviceLink() {}

    /**
     * @param string $msisdn
     * @param int $recordID
     * @return bool
     */
    public function CancelNetworkNumberReservation( $msisdn = null, $recordID = null ) {

        if( is_null($msisdn) || is_null($recordID) ) {
            return false;
        }

        $tigoResponseArray = $this->SendDataRemotely( 'cancelnetworknumberreserve', array(
            'v1:CancelNetworkNumberReservationRequest' => array(
                'v3:RequestHeader' => array(
                    'v3:GeneralConsumerInformation' => array(
                        'v3:consumerID' => $this->tigoApiConsumerID,
                        'v3:transactionID' => 'TRX_'.$recordID,
                        'v3:country' => 'TZA',
                        'v3:correlationID' => 'COR_'.$recordID
                    )
                ),
                'v1:requestBody' => array(
                    'v1:msisdn' => $msisdn,
                    'v1:additionalParameters' => array(
                        'v2:ParameterType' => array(
                            'v2:parameterName' => '',
                            'v2:parameterValue' => ''
                        )
                    )
                )
            )
        ));

        if( $tigoResponseArray['ErrorCode'] != 0 || $tigoResponseArray['HttpStatusCode'] != 200 ) {
            return false;
        }

        return ( $tigoResponseArray['HttpStatusCode'] == 200 ? true : false );

    }


    /**
     * @param string $uriString
     * @param array $dataArray
     * @return array|bool
     */
    private function SendDataRemotely( $uriString = null, $dataArray = array()  ) {

        if( is_null( $uriString ) || empty($dataArray) ) {
            return false;
        }

        //construct api url
        $resourceUrlEndpoint = "{$this->apiEndPointUrl}/{$uriString}";

        //build request based on parsed data
        $requestBodyString = $this->BuildRequestXML();

        $xmlRequestString = $this->RequestHeader()."\n{$requestBodyString}\n".$this->RequestFooter();

        return $this->Curl( $resourceUrlEndpoint, array(
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $xmlRequestString,
            CURLOPT_HEADER => 0,
            CURLOPT_HTTPHEADER => array(
                "Content-type: text/xml;charset=\"utf-8\"",
                "Accept: text/xml",
                "Cache-Control: no-cache",
                "Pragma: no-cache",
                "SOAPAction: \"\",",
                "Content-length: ".strlen($xmlRequestString)
            )
        ));
    }

    /**
     * @param array $dataArray
     * @param string $baseNode
     * @param string $requestOutputXML
     * @return bool|string
     */
    private function BuildRequestXML( $dataArray = array(), $baseNode = null, $requestOutputXML = "" ) {

        if( empty( $dataArray ) || !is_array($dataArray) ) {
            return false;
        }

        $requestOutputXML .= ( !is_null($baseNode) ? "<{$baseNode}>" : '' );
        foreach ( $dataArray as $nodeIndex => $nodeValue ) {
            if( is_array( $nodeValue )) {
                return $this->BuildRequestXML( $nodeValue, $nodeIndex, $requestOutputXML );
            } else {
                $requestOutputXML .= "<{$nodeIndex}>{$nodeValue}</{$nodeIndex}>";
            }
        }

        $requestOutputXML .= ( !is_null($baseNode) ? "</{$baseNode}>" : '' );

        return $requestOutputXML;

    }

    /**
     * Soap Request Header
     * @return string
     */
    private function RequestHeader() {

        return '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v1="http://xmlns.tigo.com/ReserveNetworkNumbersTemporarilyRequest/V1" xmlns:v3="http://xmlns.tigo.com/RequestHeader/V3" xmlns:v2="http://xmlns.tigo.com/ParameterType/V2">
                       <soapenv:Header xmlns:cor="http://soa.mic.co.af/coredata_1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
                       <cor:debugFlag>true</cor:debugFlag>
                          <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
                             <wsse:UsernameToken>
                                <wsse:Username>'.$this->tigoApiUsername.'</wsse:Username>
                                <wsse:Password>'.$this->tigoApiPassword.'</wsse:Password>
                             </wsse:UsernameToken>
                          </wsse:Security>
                       </soapenv:Header>
                       <soapenv:Body>';
    }

    /**
     * Soap Request Footer
     *
     * @return string
     */
    private function RequestFooter() {
        return '</soapenv:Body>
              </soapenv:Envelope>';
    }

    /**
     * FindStringBetween
     *
     * @param $string
     * @param $start
     * @param $end
     * @return string
     */
    private function FindStringBetween( $string, $start, $end ) {

        $string = " ".$string;
        $ini = strpos( $string, $start );

        if ( $ini == 0 ) {
            return "";
        }

        $ini += strlen($start);

        $len = strpos( $string, $end, $ini ) - $ini;

        return substr( $string, $ini, $len );

    }


    /**
     * Curl utility helper
     *
     * @param string $urlEndPoint
     * @param array $customCurlOptions
     * @return array|bool
     */
    private function Curl( $urlEndPoint = null, $customCurlOptions = array() ) {

        if( is_null( $urlEndPoint ) ) {
            return false;
        }

        $CurlOptions = array (
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HEADER => FALSE,
            CURLOPT_FOLLOWLOCATION => TRUE,
            CURLOPT_AUTOREFERER => TRUE
        );

        if( !empty( $customCurlOptions ) ) {
            foreach ($customCurlOptions as $key => $curlOption ) {
                $CurlOptions[$key] = $curlOption;
            }
        }

        //Initiate curl request and set options
        $channel = curl_init ( $urlEndPoint );
        curl_setopt_array ( $channel, $CurlOptions );
        $responseContent = curl_exec ( $channel );
        $errorCode = curl_errno ( $channel );
        $errorMessage = curl_error ( $channel );
        $responseHeaders = curl_getinfo ( $channel );
        $httpStatusCode = curl_getinfo ( $channel, CURLINFO_HTTP_CODE );
        curl_close ( $channel );

        return array(
            'ErrorCode' => $errorCode,
            'HttpStatusCode' => $httpStatusCode,
            'ResponseHeaders' => $responseHeaders,
            'ErrorMessage' => $errorMessage,
            'ResponseContent' => $responseContent
        );

    }

}