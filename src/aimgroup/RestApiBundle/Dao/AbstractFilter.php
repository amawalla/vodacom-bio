<?php
/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 10/06/2016
 * Time: 11:40 AM
 */

namespace aimgroup\RestApiBundle\Dao;


use aimgroup\RestApiBundle\Entity\Registration;
use aimgroup\RestApiBundle\Services\RegistrationFilter;
use aimgroup\RestApiBundle\Services\RegistrationFilterInterface;
use aimgroup\RestApiBundle\Utils\UtilityHelper;
use FOS\ElasticaBundle\Repository;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class AbstractFilter implements RegistrationFilterInterface
{

    protected $container;
    /** @var  UtilityHelper */
    protected $utilityHelper;
    protected $reasonArray = array();
    

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->utilityHelper = $this->container->get('utility.helper');

    }

    public function filter(Registration $registration)
    {
        //get the system tuser
        $status = true;
	try{
        if (!$this->utilityHelper->isValidPersonName($registration->getFirstName(), $registration->getLastName())) {
            $status = false;
            array_push($this->reasonArray, $this->container->get('translator')->trans('validation.invalid.name'));
        }

        if (!$this->utilityHelper->isValidDateOfBirth($registration->getDob())) {
            $status = false;
            array_push($this->reasonArray, $this->container->get('translator')->trans('validation.invalid.dob'));
        }


        if (!$this->utilityHelper->checkIdUsageLimit($registration->getIdentificationType(), $registration->getIdentification())) {
            $status = false;
            array_push($this->reasonArray, $this->container->get('translator')->trans('validation.invalid.maximum_usage_reached_id_number_and_type'));
        }


        if (!$this->utilityHelper->checkIdNumberUsageLimit($registration->getIdentification())) {
            $status = false;
            array_push($this->reasonArray, $this->container->get('translator')->trans('validation.invalid.maximum_usage_same_id_number'));
        }
	} catch (\Exception $e) {
            $this->container->get('api.helper')->logE("AbstractFilter", "filter", $e);
        }


        return $status;

    }
}
