<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\RestApiBundle\Listener;

use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use aimgroup\RestApiBundle\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Description of LoginListener
 *
 * @author Michael Tarimo
 */
class LoginListener {
    
    protected $userManager;
    
    public function __construct(UserManagerInterface $userManager) {
        $this->userManager = $userManager;
    }
    
    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event ) {
        $user = $event->getAuthenticationToken()->getUser();
        $expireDate = $user->getExpireDate();
        $today = new \DateTime();
        if($expireDate == null || ($expireDate != null && $today->diff($expireDate)->days < 5)) {
        //if($user->isExpired()) {
            $response = new RedirectResponse($event->getRequest()->getUriForPath('/admin/change_password'));
           
            $response->send();
        } else {
            // set last login
        }
    }
}
