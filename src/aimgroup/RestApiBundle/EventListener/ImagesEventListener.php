<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 12/14/15
 * Time: 3:10 PM
 */

namespace aimgroup\RestApiBundle\EventListener;


use aimgroup\RestApiBundle\Dao\GkycRequest;
use aimgroup\RestApiBundle\Event\ImagesEvent;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ImagesEventListener implements EventSubscriberInterface
{

    private $TAG="ImagesEventListener";
    private $entityManager;
    private $container;

    public function __construct(EntityManager $entityManager, ContainerInterface $container)
    {
        $this->entityManager = $entityManager;
        $this->container = $container;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2'))
     *
     * @return array The event names to listen to
     *
     * @api
     */
    public static function getSubscribedEvents()
    {
        return array(
            'process.images' => array('onReceiveImages', 0),
        );
    }

    public function onReceiveImages(ImagesEvent $imagesEvent)
    {
        $apiHelper = $this->container->get('api.helper');
        if (!isset($imagesEvent)) {
            return;
        }


        try {
            $registrationImage = $imagesEvent->getRegImages();
            $receivedImages = $this->entityManager->getRepository("RestApiBundle:RegImages")->findBy(array("registration" => $registrationImage->getRegistration()));

            $imageCountConfig = $this->container->getParameter("image_count_for_freg");
            $activeLiveVerification = $this->container->getParameter("live_verification_active");
            if (count($receivedImages) >= $imageCountConfig && !$activeLiveVerification) {
                $this->container->get("vodacom.endpoint")->processImages($receivedImages);
            };

        } catch (\Exception $e) {
            $e->getMessage();
            $apiHelper->logE($this->TAG, "onReceiveImages", $e);
        }


    }
}