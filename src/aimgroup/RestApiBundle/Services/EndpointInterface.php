<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 12/14/15
 * Time: 1:11 PM
 */

namespace aimgroup\RestApiBundle\Services;


interface EndpointInterface
{

    public function processText($registration);

    public function processImages($registration);

}