<?php


namespace aimgroup\RestApiBundle\Services\Filter;


use GuzzleHttp\Client;

class MobileNumberFilter extends AbstractFilter
{

    const MESSAGE_FULLY_REGISTERED = 'validation.invalid.number_not_exits';

    const MESSAGE_INVALID_NUMBER = 'validation.invalid.mobile_number';

    const MESSAGE_DOES_NOT_EXIST = 'validation.invalid.number_not_exits';

    /**
     * @var
     */
    private $mobileNumber;
    /**
     * @var Client
     */
    private $client;

    /**
     * MobileNumberFilter constructor.
     *
     * @param Client $client
     * @param $mobileNumber
     */
    public function __construct(Client $client, $mobileNumber)
    {
        $this->mobileNumber = $mobileNumber;
        $this->client = $client;
    }


    /**
     * {@inheritdoc}
     * @throws \RuntimeException
     */
    public function validate()
    {

        if( 0 === preg_match('/[6-7][0-9]{1}[0-9]{1}[0-9]{6}/', $this->mobileNumber)) {
            $this->setError(self::MESSAGE_INVALID_NUMBER);
            return false;
        }

        $response = $this->client->post('/VodaGateway/rest/services/vodaGateway/verifyICAP',
            [
                'multipart' => [
                    [
                        'name'     => 'mobileNumber',
                        'contents' => "255{$this->mobileNumber}",
                    ],
                ],
            ]
        );

        $result = \GuzzleHttp\json_decode($response->getBody()->getContents());

        if($result->code === '200') {
            $responseObject = $result->responseObject;

            if($responseObject->registrationStateCategory === 'Fully registered') {

                $this->setError(self::MESSAGE_FULLY_REGISTERED);

                return false;
            }
        }

        if ($result->code === '500' && null !== $result->corbaCode && $result->corbaCode === '20509') {
            $this->setError(self::MESSAGE_DOES_NOT_EXIST);

            return false;
        }

        return true;

    }
}
