<?php


namespace aimgroup\RestApiBundle\Services\Filter;

class DateOfBirthFilter extends AbstractFilter
{

    const MINIMUM_AGE = 18;

    const MESSAGE = 'validation.invalid.dob';

    /**
     * @var \DateTime
     */
    private $dateOfBirth;

    /**
     * DateOfBirthFilter constructor.
     *
     * @param \DateTime $dateOfBirth
     */
    public function __construct(\DateTime $dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;
    }

    /**
     * @return bool
     */
    public function validate()
    {

        $customerAge = $this->dateOfBirth->diff(new \DateTime())->y;

        if ($customerAge < self::MINIMUM_AGE) {
            $this->setError(self::MESSAGE);

            return false;
        }

        return true;
    }
}