<?php
/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 10/06/2016
 * Time: 11:26 AM
 */

namespace aimgroup\RestApiBundle\Services;


use aimgroup\DashboardBundle\Entity\Idtype;
use aimgroup\RestApiBundle\Entity\Registration;

interface RegistrationFilterInterface
{
    public function filter(Registration $registration, Idtype $idtype);

}