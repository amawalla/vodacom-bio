<?php
/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 10/06/2016
 * Time: 11:40 AM
 */

namespace aimgroup\RestApiBundle\Services;


use aimgroup\DashboardBundle\Entity\Idtype;
use aimgroup\RestApiBundle\Entity\Registration;
use aimgroup\RestApiBundle\Services\Filter\DateOfBirthFilter;
use aimgroup\RestApiBundle\Services\Filter\FilterProcessor;
use aimgroup\RestApiBundle\Services\Filter\IdentityCardFilter;
use aimgroup\RestApiBundle\Services\Filter\IDUsageFilter;
use aimgroup\RestApiBundle\Services\Filter\MobileNumberFilter;
use aimgroup\RestApiBundle\Services\Filter\PersonNameFilter;
use aimgroup\RestApiBundle\Utils\UtilityHelper;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class AbstractFilter implements RegistrationFilterInterface
{


    const INVALID_NAME = "firstname,surname,middlename";
    const INVALID_DOB = "dateofbirth";

    protected $tag = "AbstractFilter";
    protected $container;
    /** @var  UtilityHelper */
    protected $utilityHelper;
    protected $reasonArray = array();
    protected $reasonCode = array();


    protected $allowedIds = array(1, 2, 3, 4);


    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->utilityHelper = $this->container->get('utility.helper');

    }

    public function filter(Registration $registration, Idtype $idtype)
    {

        $em = $this->container->get('doctrine')->getManager();

        $processor = new FilterProcessor();

       // $dateOfBirth = \DateTime::createFromFormat('d-m-Y', $registration->getDob());

        /** @var Client $client */
        $client = $this->container->get('guzzle.client.vodacom_gateway');
  
        $errors = $processor->process([
            //new DateOfBirthFilter($dateOfBirth),
            
            new IdentityCardFilter($idtype, $registration->getIdentification()),
            new IDUsageFilter($em, $registration),

         //   new MobileNumberFilter($client, $registration->getMsisdn()),

            // Validate name only if the registration is not NIDA
          !$registration->isNidaRegistration() ? new PersonNameFilter($registration) : null,
        ]);

        if (count($errors) > 0 ) {
            $this->reasonArray = $errors;

            return false;
        }

        return true;

    }
}
