<?php

/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 10/06/2016
 * Time: 11:45 AM
 */
namespace aimgroup\RestApiBundle\ServiceImpl\Filters;

use aimgroup\RestApiBundle\Dao\AbstractFilter;
use aimgroup\RestApiBundle\Entity\Registration;
use aimgroup\RestApiBundle\Entity\RegistrationStatus;

class VodacomRegistrationFilter extends AbstractFilter
{


    /**
     * @param Registration $registration
     * @return bool
     */
    public function filter(Registration $registration)
    {
        $status = parent::filter($registration);
        if (!$status) {
            $this->container->get('doctrine')
                ->getRepository("RestApiBundle:RegistrationStatus")
                ->updateDueToFilter($registration, json_encode($this->reasonArray));

        }
	 $this->container->get('api.helper')->logInfo($this->tag, "processText", array('data' => json_encode($this->reasonArray)));
        return $status;

    }

}
