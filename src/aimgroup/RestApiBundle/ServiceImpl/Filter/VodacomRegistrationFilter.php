<?php

/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 10/06/2016
 * Time: 11:45 AM
 */
namespace aimgroup\RestApiBundle\ServiceImpl\Filter;


use aimgroup\DashboardBundle\Entity\Idtype;
use aimgroup\RestApiBundle\Dao\Message;
use aimgroup\RestApiBundle\Entity\Registration;
use aimgroup\RestApiBundle\Entity\RegistrationStatus;
use aimgroup\RestApiBundle\Services\AbstractFilter;


class VodacomRegistrationFilter extends AbstractFilter
{


    /**
     * @param Registration $registration
     * @param Idtype $idtype
     *
     * @return bool
     */
    public function filter(Registration $registration, Idtype $idtype)
    {
        $status = parent::filter($registration, $idtype);
	
        $reasons = $descriptions = array();

		
        foreach ($this->reasonArray as $index => $item) {
            $reasons[] = $this->container->get('api.helper')->getTranslatedLanguage( $item, array(), $registration->getLanguage());
            $descriptions[] = $this->container->get('api.helper')->getTranslatedLanguage( $item, array(), 'en');
        }
	
        if (!$status) {
            $this->container->get('doctrine')->getRepository("RestApiBundle:RegistrationStatus")->updateDueToFilter($registration, json_encode($descriptions));
            $message = new Message();
            $message->setDeviceId($registration->getDeviceId());
            $message->setMessage(implode(',', $reasons));
            /*if (count($this->reasonCode)) {
                $msg = array(
                    "status" => 3,
                    "registrationId" => $registration->getRegistrationid()
                    //"editFields" => implode(',', $this->reasonCode)
                );
            } else {
                $msg = array(
                    "status" => 3,
                    "registrationId" => $registration->getRegistrationid()
                );
            }*/
            
            $msg = array(
                "status" => 3,
                "registrationId" => $registration->getRegistrationid()
            );
            $message->setData($msg);
            $message->setTopic($this->container->getParameter("mqtt_registration_status"));
            $this->container->get('api.helper')->sendMessage($message);
        }
        return $status;

    }

}
