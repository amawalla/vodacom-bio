<?php

/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 12/14/15
 * Time: 1:14 PM
 */

namespace aimgroup\RestApiBundle\ServiceImpl;

use aimgroup\DashboardBundle\Entity\Idtype;
use aimgroup\DashboardBundle\Entity\Verifications;
use aimgroup\RestApiBundle\Dao\AbstractEndpoint;
use aimgroup\RestApiBundle\Dao\Message;
use aimgroup\RestApiBundle\Entity\Registration;
use aimgroup\RestApiBundle\Entity\RegistrationStatus;
use aimgroup\RestApiBundle\Entity\User;
use aimgroup\RestApiBundle\Services\EndpointInterface;
use aimgroup\RestApiBundle\Entity\AgentSignatures;
use aimgroup\RestApiBundle\Entity\RegImages;

use League\Flysystem\Adapter\AwsS3;
use League\Flysystem\Filesystem;
use aimgroup\DashboardBundle\Util\RegistrationUtil;

class VodacomProcessor extends AbstractEndpoint implements EndpointInterface
{

    private $tag = "VodacomProcessor";


    private $bagArray = array(
        "89" => "mwanza",
        "90" => "kagera",
        "91" => "pwani",
        "92" => "morogoro",
        "93" => "njombe",
        "94" => "zanzibar",
        "95" => "kigoma",
        "96" => "mtwara",
        "97" => "ruvuma",
        "98" => "pemba",
        "99" => "singida",
        "100" => "shinyanga",
        "101" => "arusha",
        "102" => "manyara",
        "103" => "mara",
        "104" => "mbeya",
        "105" => "rukwa",
        "106" => "dar-es-salaam",
        "107" => "dodoma",
        "108" => "tabora",
        "109" => "lindi",
        "110" => "Geita",
        "111" => "kilimanjaro",
        "112" => "tanga",
        "113" => "kahama",
        "114" => "iringa",
        "115" => "simiyu",
    );

    /**
     * @param Registration $registration
     */
    public function processText($registration)
    {
        exit("Sorry, we no longer use this");

        $apiHelper = $this->container->get('api.helper');
        try {
            $em = $this->container->get('doctrine')->getEntityManager();
            $isPassAllFilters = $this->container->get("vodacom.registration.filter")->filter($registration);

            if ($isPassAllFilters) {
                $url = $this->container->getParameter("vodacom_treg_endpoint");
                /** @var  $user User */
                $user = $registration->getOwner();
                $idType = $em->getRepository("DashboardBundle:Idtype")->find($registration->getIdentificationType());
                $region = $em->getRepository("DashboardBundle:Region")->findOneBy(array("code" => $registration->getRegion()));
                $territory = $em->getRepository("DashboardBundle:Territory")->find($registration->getTerritory());


                if (!$territory) {
                    $territoryN = $this->bagArray[$registration->getTerritory()];
                } else {
                    $territoryN = $territory->getName();
                }

                $veriCheck = FALSE;
                if ($this->nidaApiAction($registration) && "tanzania") {
                    $veriCheck = TRUE;
                }

                $callback = $this->container->getParameter("callback_url");


                $identificationTypeName = $idType->getName();

                if(in_array($identificationTypeName, array('NSSF Pension Card', 'ZSSF'))) {
                    $identificationTypeName = 'Pension Fund ID';
                } elseif ($identificationTypeName == 'Zanzibar Resident ID') {
                    $identificationTypeName = 'National ID';
                }else {
                    $identificationTypeName = str_replace('(ZNZ)', '', $identificationTypeName);
                }

                if ($veriCheck) {
                    $data = array(
                        "REG_TYPE" => "TREG",
                        "RECORD_ID" => $registration->getId(),
                        "MSISDN" => "255" . $registration->getMsisdn(),
                        "FIRST_NAME" => $registration->getFirstName(),
                        "LAST_NAME" => $registration->getLastName(),
                        "MIDDLE_NAME" => $registration->getMiddleName(),
                        "DOB" => date('d-m-Y', strtotime($registration->getDob())),
                        "ID_TYPE" => $identificationTypeName, //in_array($idType->getName(), array('NSSF Pension Card', 'ZSSF')) ? 'Pension Fund ID' : str_replace(' (ZNZ)', '', $idType->getName()),
                        "ID_NUMBER" => $this->formatIdentification($registration->getIdentification()),
                        "AGENT_NAME" => $user->getFirstName() . " " . $user->getLastName(),
                        "AGEN_CODE" => "0" . $registration->getAgentMsisdn(),
                        "location" => $territoryN,
                        "region" => $region->getName()
                    );


                    $response = $apiHelper->curl_message($url, $data, $callback);
                    $apiHelper->logInfo($this->tag, "processText", array('data' => $data, 'response' => $response));
                    $response_message = new \SimpleXMLElement($response);
                    $response_message = json_decode(json_encode($response_message));

                    if ($response_message->code == 200) {
                        $apiHelper->logInfo($this->tag, "updateREgSTatus", $data);
                        $em->getRepository("RestApiBundle:RegistrationStatus")->updateStatus(
                            $registration, $response_message->message, RegistrationStatus::TREG
                        );
                    }
                    unset($data);
                }
            }
        } catch (\Exception $e) {
            $apiHelper->logE($this->tag, "processText", $e);
        }
    }

    public function processImages($images)
    {


        $em = $this->container->get("doctrine")->getManager();
        $apiHelper = $this->container->get('api.helper');
        try {

            /** @var  $registration Registration */
            $registration = $em->getRepository("RestApiBundle:Registration")->findOneBy(
                array("registrationid" => $images[0]->getRegistration())
            );

            if ($registration) {
                /** @var  $registrationStatus RegistrationStatus */
                $registrationStatus = $em->getRepository("RestApiBundle:RegistrationStatus")->findOneBy(array("registrationId" => $registration->getId()));

                $idType = $em->getRepository('DashboardBundle:Idtype')->find($registration->getIdentificationType());

                if (null === $idType ) {
                    $apiHelper->logInfo($this->tag, 'processImages', array('Message' => 'Could not find ID Type registration ID --> '. $registration->getId()));
                    return false;
                }
  
	
                $filtered = $this->container->get('vodacom.registration.filter')->filter($registration, $idType);

                $apiHelper->logInfo($this->tag, 'processImages', array('Filter' => $filtered, 'Registration Status' => $registrationStatus, 'Filter & Reg' => $registrationStatus && $filtered, 'Msisdn' => $registration->getMsisdn()));
                if ($registrationStatus && $filtered) {


                    $query1 = "UPDATE RegistrationStatus SET fullRegStatus = -1, fregDate = NOW() WHERE RegistrationStatus.id = ". $registrationStatus->getId();
                    $em->getConnection()->exec($query1);



                    $url = $this->container->getParameter("vodacom_freg_endpoint");
                    $signature = $em->getRepository("RestApiBundle:AgentSignatures")->findOneBy(array("msisdn" => $registration->getAgentMsisdn()));
                    /** @var  $user User */
                    $user = $registration->getOwner();
                    $region = $em->getRepository("DashboardBundle:Region")->findOneBy(array("code" => $registration->getRegion()));
                    $territory = $em->getRepository("DashboardBundle:Territory")->find($registration->getTerritory());

                    if (!$territory) {
                        $territoryN = $this->bagArray[$registration->getTerritory()];
                        $registration->setTerritory($registration->getTerritory());
                    } else {
                        $territoryN = $territory->getName();
                    }

                    $identificationTypeName = $idType->getName();

                    if(in_array($identificationTypeName, array('NSSF Pension Card', 'ZSSF'))) {
                        $identificationTypeName = 'Pension Fund ID';
                    } else if ($identificationTypeName == 'Zanzibar Resident ID') {
                        $identificationTypeName = 'National ID';
                    }else {
                        $identificationTypeName = str_replace('(ZNZ)', '', $identificationTypeName);
                    }

                    $data = array(
                        "REG_TYPE" => "FREG",
                        "RECORD_ID" => $registration->getId(),
                        "MSISDN" => "255" . $registration->getMsisdn(),
                        "FIRST_NAME" => $registration->getFirstName(),
                        "LAST_NAME" => $registration->getLastName(),
                        "MIDDLE_NAME" => $registration->getMiddleName(),
                        "DOB" => date('d-m-Y', strtotime($registration->getDob())),
                        //"ID_TYPE" => in_array($idType->getName(), array('NSSF Pension Card', 'ZSSF') ? "Pension Fund ID" : $idType->getName(),
                        "ID_TYPE" => trim($identificationTypeName), //in_array($idType->getName(), array('NSSF Pension Card', 'ZSSF')) ? 'Pension Fund ID' : str_replace('(ZNZ)', '', $idType->getName()),
                        "ID_NUMBER" => $registration->getIdentification(),
                        "AGENT_NAME" => $user->getFirstName() . " " . $user->getLastName(),
                        "AGEN_CODE" => "0" . $registration->getAgentMsisdn(),
                        "location" => $territoryN,
                        "region" => $region->getName()
                    );

                    $image_key = array(
                        'signature' => 'customer_signature_image',
                        'rear-pic' => 'id_back_image',
                        'camera' => 'id_back_image',
                        'front-pic' => 'id_front_image',
                        'potrait' => 'picture',
                    );

                    $webPathPrefix = $this->container->getParameter('filesystem_root_image_path').
                        $this->container->getParameter('s3_bucket').
                        '/'.$this->container->getParameter('s3_bucket_prefix');

                    $imageCount = count($images);
                    //$em->getRepository("RestApiBundle:AgentSignatures")
                    /** @var  $image RegImages */
                    foreach ($images as $image) {

                      if (in_array($image->getImageType(), array_keys($image_key))) {
                            $data[ $image_key[ $image->getImageType() ] ] = $image->getWebPath();
                    }

                    // If its a biometric registration, get the default webPath  images
                    if ($registration->isNidaRegistration()) {

                        $placeholder = $this->container->getParameter('biometric_placeholder_image');

                        $data['customer_signature_image'] = $placeholder;
                        $data['id_back_image'] = $placeholder;
                        $data['id_front_image'] = $placeholder;

                    }
                    }
                    if (!$signature) {
                        $dateTime = (new \DateTime())->format('YmdHis');
                        $signature = new AgentSignatures();
                        $signature->setName($registration->getAgentMsisdn() . "_" . $dateTime . "_agent_signature. jpeg");
                        $signature->setFullPath("753758085_20160603135317_agent_signature_.jpeg");
                        $signature->setWebPath("https://vodalive.registersim.com/euploads/signatures/754596059_20160316200557_agent_signature_1585777471.jpeg");
                        $signature->setMsisdn($registration->getAgentMsisdn());
                        $em->persist($signature);
                    }
                    $data["agent_signature_image"] = $signature->getWebPath();


                    $callback = $this->container->getParameter("callback_url");
  

                    $response = $apiHelper->curl_message($url, $data, $callback);
                    $apiHelper->logInfo($this->tag, "processImages", array('data' => $data, 'response' => $response));              
                   echo var_dump($response); 
                    $response_message = new \SimpleXMLElement($response);
                    $response_message = json_decode(json_encode($response_message));

                    if ($response_message->code == 200) {
                        $registrationStatus->setFullRegDesc($response_message->message);
                        $registrationStatus->setFullRegStatus(RegistrationStatus::FREG);
                        $registrationStatus->setAllimageDate(new \DateTime());
                        $em->flush();
                    }
                    if ($registration->getImage_count() != $imageCount) {
                        $registration->setImage_count($imageCount);
                        $em->flush();
                    }
                }
            }
        } catch (\Exception $e) {
            exit($e->getMessage());
            $apiHelper->logE($this->tag, "processText", $e);
        }
    }


    public function nidaApiAction(Registration $entity)
    {
        return true;

        if (!in_array(strtolower($entity->getNationality()), array('tanzania', 'mtanzania'))) {
            return true;
        }

        $this->container->get('api.helper')->logInfo($this->tag, "nidaApiAction", array('start' => 'entered for:' . $entity->getMsisdn()));
        $url = $this->container->getParameter('nida_api');
        $em = $this->container->get("doctrine")->getManager();

        //list all id types to formaster key array now that we aint entity relating
        $idtypes = $em->getRepository("DashboardBundle:Idtype")->findAll();
        /** @var  $idtype Idtype */
        foreach ($idtypes as $idtype) {
            $idKeyArray[$idtype->getId()] = $idtype->getName();
        }

        $result = $em->getRepository("DashboardBundle:ConfigMaster")->findOneBy(array("name" => "id_verification_list"));
        if ($result) {
            $idsToBeVerified = json_decode($result->getConfig());
            if (in_array($idKeyArray[$entity->getIdentificationType()], $idsToBeVerified)) {
                $this->container->get('api.helper')->logInfo($this->tag, "nidaApiAction", array('msg' => 'id valid for verification:' . $entity->getMsisdn()));
                //clal API
                $data = array(
                    "idNumber" => $entity->getIdentification(),
                );

                $time_start = strtotime(date('Y-m-d H:i:s'));
                $response = $this->curl_message($url, $data);
                $time_end = strtotime(date('Y-m-d H:i:s'));

                $response = json_decode($response, true);
                $this->container->get('api.helper')->logInfo(
                    $this->tag, "nidaApiAction",
                    array(
                        'msg' => $entity->getMsisdn(),
                        "nida response" => $response['json']['message'])
                );
                if ($response['json']['message'] == "success") {

                    $customer_compare_name = strtolower($response['json']['item']['firstName'] . ' ' . $response['json']['item']['lastName']);
                    $database_customer_name = strtolower($entity->getFirstName() . ' ' . $entity->getLastName());
                    $database_customer_name_reversed = strtolower($entity->getLastName() . ' ' . $entity->getFirstName());

                    //do 85% match
                    $likehood = $this->likehood_percent($database_customer_name, $customer_compare_name);
                    $likehood_reverse = $this->likehood_percent($database_customer_name_reversed, $customer_compare_name);

                    unset($response['json']['item']['photo']);

                    $verification = new Verifications();
                    $verification->setIdType('NIDA');
                    $verification->setNidaResponseString(json_encode($response));
                    $verification->setCustomerMsisdn($entity->getMsisdn());
                    $verification->setCustomerRecordNames($entity->getLastName() . ' ' . $entity->getFirstName());
                    $verification->setTimeTaken($time_end - $time_start);
                    $verification->setCardDistrict($response['json']['item']['district']);
                    $verification->setRegion($entity->getRegion());

                    $nidaResults = array($response['json']['item']['firstName'], $response['json']['item']['middleName'], $response['json']['item']['lastName']);

                    $this->container->get('api.helper')->logInfo($this->tag, "nidaApiAction", array('msg' => 'likehood:' . $likehood . ' reverse-likehood:' . $likehood_reverse . ' for : ' . $entity->getMsisdn()));
                    //if ($likehood >= 85 || $likehood_reverse >= 85) {
                    if ($this->compareStrings($entity->getFirstName(), $nidaResults)['matched'] /*&& $this->compareStrings($entity->getMiddleName(), $nidaResults)['matched']*/ && $this->compareStrings($entity->getLastName(), $nidaResults)['matched']) {
                        $verification->setResultType('MATCH');
                        $em->persist($verification);
                        $em->flush();
                        RETURN TRUE;
                    } else {
                        //decline on mismatch basis
                        $em->getRepository("RestApiBundle:RegistrationStatus")->updateTemporaryRegStatus(3, 'Validation Box :: Name mismatch', $entity);
                        $verification->setResultType('MISMATCH');
                        $em->persist($verification);
                        $em->flush();


                        $message = new Message();
                        $message->setDeviceId($entity->getDeviceId());
                        $message->setTopic($this->container->getParameter("mqtt_registration_status"));
                        $message->setMessage($this->container->get('api.helper')->getTranslatedLanguage(
                            "register.status.failed_nida",
                            array(),
                            $entity->getLanguage())
                        );
                        $message->setData(
                            array(
                                "status" => 3,
                                "registrationId" => $entity->getRegistrationid()
                            )
                        );
                        $this->container->get('api.helper')->sendMessage($message);

                        //send a messave
                        RETURN FALSE;
                    }
                } else if ($response['json']['message'] == "Invalid id number" || $response['json']['message']=="Request failed with result Code :2" ) {
                    $em->getRepository("RestApiBundle:RegistrationStatus")->updateTemporaryRegStatus(3, 'Validation Box :: ' . $response['json']['message'], $entity);

                    $message = new Message();
                    $message->setDeviceId($entity->getDeviceId());
                    $message->setTopic($this->container->getParameter("mqtt_registration_status"));
                    $message->setMessage($this->container->get('api.helper')->getTranslatedLanguage(
                        "register.status.failed_nida",
                        array(),
                        $entity->getLanguage())
                    );
                    $message->setData(
                        array(
                            "status" => 3,
                            "registrationId" => $entity->getRegistrationid()
                        )
                    );
                    $this->container->get('api.helper')->sendMessage($message);

                    //send a messave
                    RETURN FALSE;
                }else {
                    $this->container->get('api.helper')->logInfo($this->tag, "nidaApiAction", array('msg' => 'Failed to get valid response from nida:' . $entity->getMsisdn()));
                    RETURN TRUE;
                }
            } else {
                $this->container->get('api.helper')->logInfo($this->tag, "nidaApiAction", array('msg' => 'id in-valid for verification:' . $entity->getMsisdn()));
            }
        }


        RETURN TRUE;
    }


    function curl_message($url, $data)
    {
        $data_string = json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
            $ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string),
            )
        );

        return curl_exec($ch);
    }

    function likehood_percent($str1, $str2)
    {
        similar_text($str1, $str2, $percentMatch);
        return round($percentMatch);
    }

    private function formatIdentification($identification)
    {
        $count = strlen($identification);
        if ($count < 4) {
            $identification = (str_repeat('0', 4 - strlen($identification)) . $identification);
        }
        return $identification;
    }

    /**
     * @param $string
     * @param array $possibilities
     * @param integer $threshold
     * @param bool $ignore_case
     * @return array
     */
    public function compareStrings( $string = null, $possibilities = array(), $threshold = 85, $ignore_case = true) {

        $string = trim($ignore_case ? strtolower($string) : $string);

        if (is_null($string) || strlen($string) == 0) {
            return [
                'matched' => true,
                'compared' => '',
                'percentage' => 100
            ];
        }

        $results = ['matched' => false, 'original' => $string, 'percentage' => 0];

        foreach ($possibilities as $index => $possibility) {

            $possibility = ($ignore_case ? strtolower($possibility) : $possibility);

            //Do the name sounds the same
            if( soundex($string) == soundex($possibility) ) {
                $results = [
                    'matched' => true,
                    'compared' => $possibility,
                    'percentage' => 100
                ];
                break;
            }
            else {

                //do percentage matching
                similar_text($string, $possibility, $percentage);

                if ($percentage >= $threshold) {

                    $results = [
                        'matched' => true,
                        'compared' => $possibility,
                        'percentage' => $percentage
                    ];

                    break;
                }
            }
        }

        return $results;
    }

}

