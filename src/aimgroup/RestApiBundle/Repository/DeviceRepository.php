<?php
/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 8/6/15
 * Time: 11:12
 */

namespace aimgroup\RestApiBundle\Repository;


use aimgroup\RestApiBundle\Entity\Device;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

class DeviceRepository extends EntityRepository
{

    public function save(Device $device)
    {
        try {
            $this->_em->persist($device);
            $this->_em->flush();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
        return $device;
    }

    public function findByTokenAndMsisdn($accessToken, $msisdn)
    {
        $device = null;
        if (isset($accessToken) && isset($msisdn)) {
            $device = $this->_em->getRepository("RestApiBundle:Device")->findOneBy(
                array("accessToken" => $accessToken, "msisdn" => $msisdn)
            );
        }
        if (!$device) {
            throw new NoResultException("ACCESS TOKEN/MSISDN NOT FOUND");
        }
        return $device;
    }

    public function findByMsisdnAndImei($msisdn, $imei)
    {
        $q = $this->createQueryBuilder('d')
            ->leftJoin("d.user", "u")
            ->where('d.imei=:imei')
            ->andWhere("d.msisdn = :msisdn")
            ->setParameter('imei', $imei)
            ->setParameter('msisdn', $msisdn)
            ->select('d.id', 'u.id')
            ->getQuery();
        $device = $q->setMaxResults(1)->getOneOrNullResult();
        if (!$device) {
            throw new NoResultException();
        }
        return $device;
    }

    public function countDevices($msisdn){
        $q = $this->createQueryBuilder('d')
            ->select('count(d.id)')
            ->where("d.msisdn = :msisdn")
            ->setParameter("msisdn",$msisdn)
            ->getQuery();
        return $q->getSingleScalarResult();
    }


}
