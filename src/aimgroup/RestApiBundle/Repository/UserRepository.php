<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace aimgroup\RestApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use aimgroup\RestApiBundle\Entity\User;
use aimgroup\RestApiBundle\Provider\UserInterface;

/**
 * Description of UserRepository
 *
 * @author Oscar Makala
 *
 */
class UserRepository extends EntityRepository
{

    public function save(User $user)
    {
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function findByMsisdnAndImei($msisdn, $imei)
    {
        $user = $this->findOneBy(array(
            'msisdn' => $msisdn,
            'imei' => $imei
        ));
        if (!$user) {
            throw new NoResultException();
        }
        return $user;
    }

    public function findByMsisdn($msisdn)
    {
        $user = $this->findOneBy(array('mobileNumber' => $msisdn));
        if (!$user) {
            throw new NoResultException();
        }
        return $user;
    }

    public function isMobileExists($mobile)
    {
        $isExists = false;
        $users = $this->findBy(array('mobileNumber' => $mobile));
        if ($users) {
            $isExists = true;
        }
        return $isExists;
    }
    

    public function isUsernameExists($mobile)
    {
        $isExists = false;
        $users = $this->findBy(array('username' => $mobile));
        if ($users) {
            $isExists = true;
        }
        return $isExists;
    }
}