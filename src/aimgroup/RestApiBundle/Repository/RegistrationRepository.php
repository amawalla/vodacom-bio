<?php
/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 09/03/2016
 * Time: 3:53 PM
 */

namespace aimgroup\RestApiBundle\Repository;


use Doctrine\ORM\EntityRepository;

class RegistrationRepository extends EntityRepository
{
    /**
     * Get a count of all the registrations by registration id
     * @param $registrationid
     * @return mixed
     */
    public function countRegistrationsById($registrationid)
    {
        return $this->getEntityManager()
            ->createQuery(
                ' SELECT count(r.id) FROM RestApiBundle:Registration r WHERE r.registrationid like (:registrationid)'
            )
            ->setParameter('registrationid', $registrationid)
            ->getSingleScalarResult();
    }

    public function findByTReg($status, $id)
    {
        return $this->getEntityManager()->createQuery(
            "SELECT d.id, d.msisdn, d.firstName, d.lastName, d.identification, d.identificationType, u.username, d.dob, CONCAT( u.firstName, ' ', u.lastName ) AS agent_name, d.treg, d.freg  "
            ."FROM RestApiBundle:Registration d"
            ." JOIN d.owner u"
            ." WHERE d.treg = :status and d.id = :id"
        )
            ->setParameter('id', $id)
            ->setParameter('status', $status)
            ->getResult();
    }

    public function findByTRegOnly()
    {
        return $this->findBy(array('treg'=>0));
    }
}
