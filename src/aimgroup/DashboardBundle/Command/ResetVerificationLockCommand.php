<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\DashboardBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

/**
 * Description of ResetVerificationLock
 *
 * @author Michael Tarimo
 */
class ResetVerificationLockCommand extends ContainerAwareCommand {
    
    protected $TAG = "ResetVerificationLockCommand";
   
    /**
     * {@inheritdoc}
     */
    protected function configure() {
        $this
            ->setName('dashboard:reset_verification_lock')
            ->addOption("for", null, InputOption::VALUE_REQUIRED)
            ->setDescription('Resets verification locks')
            ->setHelp(<<<EOT
                The <info>%command.name%</info> command resets verification locks:
                <info>php %command.full_name%</info>
                You can also optionally specify the date:
                <info>php %command.full_name% --connection=default</info>
EOT
                );
    }
    
    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        //$logger = $this->getContainer()->get('monolog.formatter.api');
        $lock_expire_time = $this->getContainer()->getParameter("verification_expire_lock_time_in_minutes");
        
        $for = $input->getOption('for');
        $param = null;
        if($for) {
            $param = $this->translateParameter($for);
        }

        $output->writeln([
            'RESET Verification Locks',
            '========================',
            'Executing at: ' .  date('d-m-Y H:i:s'),
        ]);
        $output->writeln('Resetting...');
        $error = false;
        try {
            $em = $this->getContainer()->get('doctrine')->getManager();
  
            $query1 = "UPDATE RegistrationStatus SET verifyState = (verifyState * -1) WHERE verifyState < -1 AND verifyLock IS NOT NULL AND verifyDate > (CURDATE() - INTERVAL 1 DAY) AND verifyDate < (CURRENT_TIMESTAMP - INTERVAL " . $lock_expire_time . " MINUTE)";
            $query2 = "UPDATE RegistrationStatus SET verifyState = 0 WHERE verifyState = -1 AND verifyLock IS NOT NULL AND verifyDate > (CURDATE() - INTERVAL 1 DAY) AND verifyDate < (CURRENT_TIMESTAMP - INTERVAL " . $lock_expire_time . " MINUTE)";
            
            if($param) {
                if(is_string($param)) { 
                    if(strcasecmp ($param, 'all') == 0) {
                        $query1 = "UPDATE RegistrationStatus SET verifyState = (verifyState * -1) WHERE verifyState < -1 AND verifyLock IS NOT NULL AND verifyDate < CURRENT_TIMESTAMP";
                        $query2 = "UPDATE RegistrationStatus SET verifyState = 0 WHERE verifyState = -1 AND verifyLock IS NOT NULL AND verifyDate < CURRENT_TIMESTAMP";
                    } else if(strcasecmp ($param, 'today') == 0) {
                        $query1 = "UPDATE RegistrationStatus SET verifyState = (verifyState * -1) WHERE verifyState < -1 AND verifyLock IS NOT NULL AND DATE(verifyDate) = CURDATE()";
                        $query2 = "UPDATE RegistrationStatus SET verifyState = 0 WHERE verifyState = -1 AND verifyLock IS NOT NULL AND DATE(verifyDate) = CURDATE()";
                    }
                } else { // is date
                    $query1 = "UPDATE RegistrationStatus SET verifyState = (verifyState * -1) WHERE verifyState < -1 AND verifyLock IS NOT NULL AND DATE_FORMAT(verifyDate, '%d-%m-%Y') = '" . $param->format('d-m-Y') . "'";
                    $query2 = "UPDATE RegistrationStatus SET verifyState = 0 WHERE verifyState = -1 AND verifyLock IS NOT NULL AND DATE_FORMAT(verifyDate, '%d-%m-%Y') = '" . $param->format('d-m-Y') . "'";
                }
            } 
            $output->writeln($query1);
            $output->writeln($em->getConnection()->exec($query1));
            $output->writeln($query2);
            $output->writeln($em->getConnection()->exec($query2));
            
            $query1 = "select count(*), image_count , temporaryRegStatus , verifyState , allimageDate from RegistrationStatus  left join registration on registration.id = RegistrationStatus.registrationId  where date(registration.createdDate ) >= '2016-11-04' and allimageDate IS NULL and image_count >= 3 group by temporaryRegStatus , verifyState order by verifyState asc";
            $em->getConnection()->exec($query1);
            
        } catch (\Exception $e) {
            $output->writeln(sprintf('<error>Could not reset locked registrations</error>'));
            $output->writeln(sprintf('<error>%s</error>', $e->getMessage()));
            $error = true;
        }
        $output->writeln('Completed at: ' .  date('d-m-Y H:i:s'));
        
        return $error ? 1 : 0;
    }
    
    private function translateParameter($valueString) {
        if (\DateTime::createFromFormat('Y-m-d', $valueString) !== FALSE) {
            return \DateTime::createFromFormat('Y-m-d', $valueString);
        } else if (\DateTime::createFromFormat('d-m-Y', $valueString) !== FALSE) {
            return \DateTime::createFromFormat('d-m-Y', $valueString);
        } else if (\DateTime::createFromFormat('d/m/Y', $valueString) !== FALSE) {
            return \DateTime::createFromFormat('d/m/Y', $valueString);
        } else if (strcasecmp ($valueString, "all") == 0 || strcasecmp ($valueString, "today") == 0) {
            return $valueString;
        }
        return null;
    }
} 
