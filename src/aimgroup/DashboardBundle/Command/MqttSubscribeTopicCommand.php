<?php

namespace aimgroup\DashboardBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MqttSubscribeTopicCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('dashboard:mqtt_subscribe_topic_command')
            ->setDescription('Hello PhpStorm');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $apiHelper = $this->getContainer()->get("api.helper");
//        $topic = "";
//        $deviceId = "";
//        $apiHelper->subscribe($topic, $deviceId);
        if ("2.0.7" > "2.0.8") {
            echo "greater";
        } else {
            echo "little";
        }
    }
}
