<?php

namespace aimgroup\DashboardBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MqttGeneralPublishCommand extends ContainerAwareCommand
{

    protected $TAG = "MqttGeneralPublishCommand";

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('dashboard:mqtt_general_publish')
            ->addArgument("type")
            ->addArgument("msisdn")
            ->setDescription('General Mqtt Publish Command');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $type = $input->getArgument('type');
        $msisdn = $input->getArgument('msisdn');

        $output->writeln('type: ' . $type);
        $output->writeln('msisdn: ' . $msisdn);

        $apiHelper = $this->getContainer()->get('api.helper');

        $em = $this->getContainer()->get('doctrine')->getManager();

        $apiHelper->logInfo($this->TAG, "execute", array('type' => $type, 'msisdn' => $msisdn));

        if ($msisdn) {
            if ($type == "form") {
                $this->doPublishForm($msisdn, $apiHelper, $em);
            }
            else if ($type == "app") {
                $this->doPublishApp($msisdn, $apiHelper, $em);
            }
        }
    }

    private function doPublishForm($topics, $apiHelper, $em)
    {

        $topics = explode(",", $topics);
        $service = $em->getRepository("DashboardBundle:Service")->find(1);

        if ($service) {

            foreach ($topics as $topic) {
                $apiHelper->logInfo($this->TAG, "doPublishForm:Start", array('topic' => $topic));

                $device = $em->getRepository('RestApiBundle:Device')->findOneBy(array("msisdn" => $topic));


                if ($device) {

                    $deviceId = $device->getDeviceId();
                    if ($deviceId) {

                        $configVAlue["form"] = json_decode($service->getFormJson(), true);
                        $configVAlue["version"] = 13;
                        $configVAlue["ordering"] = $service->getOrdering();
                        $configVAlue["name"] = $service->getName();
                        $configVAlue["type"] = $service->getServiceCode();
                        $configVAlue["formType"] = $service->getName();
                        $configVAlue["operator"] = $this->getContainer()->getParameter("operator_id");

                        $response = $apiHelper->publish(
                            $deviceId,
                            $this->getContainer()->getParameter("mqtt_formupdate_msg"),
                            json_encode($configVAlue)
                        );

                        $apiHelper->logInfo($this->TAG, "doPublishForm:End", array('topic' => $topic, "Response" => $response));

                    }else{
                        $apiHelper->logInfo($this->TAG, "doPublishForm", array('topic' => $topic, "Message" => "Device Id Not found"));
                    }
                } else {
                    $apiHelper->logInfo($this->TAG, "doPublishForm", array('topic' => $topic, "Message" => "Device Not found"));
                }
            }
        } else {
            $apiHelper->logInfo($this->TAG, "doPublishForm", array("Message" => "Service Not found"));
        }
    }

    private function doPublishApp($topics, $apiHelper, $em)
    {

        $topics = explode(",", $topics);

        $updateId = 19;

        $applicationUpdate = $em->getRepository("DashboardBundle:AppUpload")->findOneBy(array("id" => $updateId));


        if ($applicationUpdate) {

            $obj = array();

            $obj["version"] = $applicationUpdate->getVersionName();
            $explode = pathinfo($applicationUpdate->getFileLocation());
            $httpPath = str_replace($this->getContainer()->getParameter("resource_path"), $this->getContainer()->getParameter("hostname") . DIRECTORY_SEPARATOR . "euploads", $explode["dirname"]);
            $obj["filePath"] = $httpPath . "/" . $explode["basename"];
            $obj["shouldBlock"] = $applicationUpdate->isShouldBlock();

            $apiHelper->logInfo($this->TAG, "doPublishApp:Object", $obj);

            foreach ($topics as $topic) {

                $apiHelper->logInfo($this->TAG, "doPublishApp:Start", array('topic' => $topic));

                $device = $em->getRepository('RestApiBundle:Device')->findOneBy(array("msisdn" => $topic));

                if ($device) {

                    $deviceId = $device->getDeviceId();

                    if ($deviceId) {

                        $response = $apiHelper->publish($deviceId,
                            $this->getContainer()->getParameter("mqtt_apkupdate_msg"),
                            json_encode($obj));

                        $apiHelper->logInfo($this->TAG, "doPublishApp:End", array('topic' => $topic, "Response" => $response));

                    }else{
                        $apiHelper->logInfo($this->TAG, "doPublishApp", array('topic' => $topic, "Message" => "Device Id Not found"));
                    }
                } else {
                    $apiHelper->logInfo($this->TAG, "doPublishApp", array('topic' => $topic, "Message" => "Device Not found"));
                }
            }
        } else {
            $apiHelper->logInfo($this->TAG, "doPublishApp", array("Message" => "Application Not found"));
        }
    }
}


