<?php

namespace aimgroup\DashboardBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BulkDeleteCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('dashboard:bulk_delete_command')
            ->addArgument("file_path")
            ->setDescription('Removing agents from the system');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $apiHelper = $this->getContainer()->get("api.helper");
        $userHelper = $this->getContainer()->get("users.helper");

        $logger = $this->getContainer()->get('monolog.logger.api');
        $filePath = $input->getArgument('file_path');

        $logger->info("executing agent bulk delete command: " . $filePath);
        $data = $apiHelper->parseCSV($filePath);

        foreach ($data as $row) {
            $attributes = array();
            $attributes["msisdn"] = $row["msisdn"];
            if (strlen($attributes["msisdn"])) {
                $status = $userHelper->deleteUsers($attributes, true);
                $logger->info("user was deleted with  status (" . $status . ") details: " . json_encode($attributes));
            }

        }

    }
}
