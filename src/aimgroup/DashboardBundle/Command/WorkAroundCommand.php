<?php

namespace aimgroup\DashboardBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\ProcessBuilder;

class WorkAroundCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('dashboard:report_workaround_command')
            ->addArgument("email")
            ->addArgument("dddate")
            ->addArgument("filename")
            ->addArgument("title")
            ->setDescription('Report Generator');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        echo " here 2.ARRIVED ";
        
        $email = $input->getArgument('email');
        $title = $input->getArgument('title');
        $filename = $input->getArgument('filename');
        $dddate = $input->getArgument('dddate');
        //execute bash script
        $output->writeln('starting');
        $script = $this->getContainer()->getParameter("base_path") . "/scripts/Sendmail.sh";
        $output->writeln("path of script-" . $script);

        echo " here 2.PRESEND to: " . $email . " title: " . $title;
        
        //exec($script  . "\" \"" . $email . "\" \"" . $title . "\" > &");
        exec($script . " \"" . $email . "\" \"" . $title . "\" \"" . $filename . "\" \"" . $heading . "\" > /dev/null &");

        echo " here 2.SENT ";

    }
}