<?php

namespace aimgroup\DashboardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Operator
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="aimgroup\DashboardBundle\Repository\OperatorRepository")
 */
class Operator
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="configUrl", type="string", length=150)
     */
    private $configUrl;
    /**
     * @var string
     *
     * @ORM\Column(name="gpsStatus", type="boolean")
     */
    private $gpsStatus;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150)
     */
    private $name;
    /**
     * @var string
     *
     * @ORM\Column(name="passwordSetUrl", type="string", length=150)
     */
    private $passwordSetUrl;
    /**
     * @var string
     *
     * @ORM\Column(name="reportUrl", type="string", length=150)
     */
    private $reportUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="signatureSaveUrl", type="string", length=150)
     */
    private $signatureSaveUrl;
    /**
     * @var string
     *
     * @ORM\Column(name="tokenAuthUrl", type="string", length=150)
     */
    private $tokenAuthUrl;


    /**
     * @var string
     *
     * @ORM\Column(name="prefix", type="string", length=150)
     */
    private $prefix;

    /**
     * @var string
     *
     * @ORM\Column(name="simSerialPrefix", type="string", length=150)
     */
    private $simSerialPrefix;
    /**
     * @return string
     */
    public function getConfigUrl()
    {
        return $this->configUrl;
    }

    /**
     * @param string $configUrl
     */
    public function setConfigUrl($configUrl)
    {
        $this->configUrl = $configUrl;
    }

    /**
     * @return string
     */
    public function getGpsStatus()
    {
        return $this->gpsStatus;
    }

    /**
     * @param string $gpsStatus
     */
    public function setGpsStatus($gpsStatus)
    {
        $this->gpsStatus = $gpsStatus;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getPasswordSetUrl()
    {
        return $this->passwordSetUrl;
    }

    /**
     * @param string $passwordSetUrl
     */
    public function setPasswordSetUrl($passwordSetUrl)
    {
        $this->passwordSetUrl = $passwordSetUrl;
    }

    /**
     * @return string
     */
    public function getReportUrl()
    {
        return $this->reportUrl;
    }

    /**
     * @param string $reportUrl
     */
    public function setReportUrl($reportUrl)
    {
        $this->reportUrl = $reportUrl;
    }

    /**
     * @return string
     */
    public function getSignatureSaveUrl()
    {
        return $this->signatureSaveUrl;
    }

    /**
     * @param string $signatureSaveUrl
     */
    public function setSignatureSaveUrl($signatureSaveUrl)
    {
        $this->signatureSaveUrl = $signatureSaveUrl;
    }

    /**
     * @return string
     */
    public function getTokenAuthUrl()
    {
        return $this->tokenAuthUrl;
    }

    /**
     * @param string $tokenAuthUrl
     */
    public function setTokenAuthUrl($tokenAuthUrl)
    {
        $this->tokenAuthUrl = $tokenAuthUrl;
    }

    /**
     * @return string
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * @param string $prefix
     */
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSimSerialPrefix()
    {
        return $this->simSerialPrefix;
    }

    /**
     * @param string $simSerialPrefix
     */
    public function setSimSerialPrefix($simSerialPrefix)
    {
        $this->simSerialPrefix = $simSerialPrefix;
    }


}
