<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\DashboardBundle\Entity;

/**
 * Description of SearchResult
 *
 * @author Michael Tarimo
 */
class SearchResult {
    
    private $products;
    private $artists;
    
    
    public function getProducts() {
        return $this->products;
    }

    public function getArtists() {
        return $this->artists;
    }

    public function setProducts($products) {
         $data = [];
            foreach($products as $product){
                $data[] = ['id'=>$product->getId(),'title'=>$product->getTitle(),'uri'=>$product->getPathToSong()];
            }
        $this->products = $data;
    }

    public function setArtists($artists) {
        $data = [];
            foreach($artists as $artist){
                $data[] = ['id'=>$artist->getId(),'name'=>$artist->getAkaName(),'bio'=>$artist->getBio()];
            }
        $this->artists = $data;
    }
}