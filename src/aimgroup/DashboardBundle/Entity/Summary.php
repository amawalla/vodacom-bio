<?php

namespace aimgroup\DashboardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Summary
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Summary
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdDate", type="datetime")
     */
    private $createdDate;

    /**
     * @var string
     *
     * @ORM\Column(name="dataType", type="string", length=255)
     */
    private $dataType;

    /**
     * @var string
     *
     * @ORM\Column(name="dataString", type="text")
     */
    private $dataString;

    /**
     * @var string
     *
     * @ORM\Column(name="dataStringTwo", type="text")
     */
    private $dataStringTwo;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\PrePersist
     * @param \Datetime $createdDate
     * @return Summary
     */
    public function setCreatedDate() {
        if (!isset($this->createdDate)) {
            $this->createdDate = new \DateTime;
        }
    }
    

    /**
     * Get createdDate
     *
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * Set dataType
     *
     * @param string $dataType
     *
     * @return Summary
     */
    public function setDataType($dataType)
    {
        $this->dataType = $dataType;

        return $this;
    }

    /**
     * Get dataType
     *
     * @return string
     */
    public function getDataType()
    {
        return $this->dataType;
    }

    /**
     * Set dataString
     *
     * @param string $dataString
     *
     * @return Summary
     */
    public function setDataString($dataString)
    {
        $this->dataString = $dataString;

        return $this;
    }

    /**
     * Get dataString
     *
     * @return string
     */
    public function getDataString()
    {
        return $this->dataString;
    }

    /**
     * Set dataStringTwo
     *
     * @param string $dataStringTwo
     *
     * @return Summary
     */
    public function setDataStringTwo($dataStringTwo)
    {
        $this->dataStringTwo = $dataStringTwo;

        return $this;
    }

    /**
     * Get dataStringTwo
     *
     * @return string
     */
    public function getDataStringTwo()
    {
        return $this->dataStringTwo;
    }
}

