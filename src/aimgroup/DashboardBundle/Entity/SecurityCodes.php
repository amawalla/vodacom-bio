<?php

namespace aimgroup\DashboardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SecurityCodes
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="aimgroup\DashboardBundle\Repository\SecurityCodesRepository")
 */
class SecurityCodes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="action_name", type="string", length=255)
     */
    private $actionName;

    /**
     * @var string
     *
     * @ORM\Column(name="action_code", type="string", length=255)
     */
    private $actionCode;

    /**
     * @var string
     *
     * @ORM\Column(name="email_address", type="string", length=255)
     */
    private $emailAddress;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="action_status", type="string", length=255)
     */
    private $actionStatus;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set actionName
     *
     * @param string $actionName
     * @return SecurityCodes
     */
    public function setActionName($actionName)
    {
        $this->actionName = $actionName;

        return $this;
    }

    /**
     * Get actionName
     *
     * @return string 
     */
    public function getActionName()
    {
        return $this->actionName;
    }

    /**
     * Set actionCode
     *
     * @param string $actionCode
     * @return SecurityCodes
     */
    public function setActionCode($actionCode)
    {
        $this->actionCode = $actionCode;

        return $this;
    }

    /**
     * Get actionCode
     *
     * @return string 
     */
    public function getActionCode()
    {
        return $this->actionCode;
    }

    /**
     * Set emailAddress
     *
     * @param string $emailAddress
     * @return SecurityCodes
     */
    public function setEmailAddress($emailAddress)
    {
        $this->emailAddress = $emailAddress;

        return $this;
    }

    /**
     * Get emailAddress
     *
     * @return string 
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return SecurityCodes
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set actionStatus
     *
     * @param string $actionStatus
     * @return SecurityCodes
     */
    public function setActionStatus($actionStatus)
    {
        $this->actionStatus = $actionStatus;

        return $this;
    }

    /**
     * Get actionStatus
     *
     * @return string 
     */
    public function getActionStatus()
    {
        return $this->actionStatus;
    }
}
