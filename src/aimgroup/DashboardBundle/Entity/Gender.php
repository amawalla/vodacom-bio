<?php

namespace aimgroup\DashboardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Gender
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="aimgroup\DashboardBundle\Repository\GenderRepository")
 */
class Gender
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=10)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="nameAlt", type="string", length=10)
     */
    private $nameAlt;


    /**
     * @var string
     *
     * @ORM\Column(name="shorthand", type="string", length=10)
     */
    private $shortHand;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Gender
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nameAlt
     *
     * @param string $nameAlt
     * @return Gender
     */
    public function setNameAlt($nameAlt)
    {
        $this->nameAlt = $nameAlt;

        return $this;
    }

    /**
     * Get nameAlt
     *
     * @return string 
     */
    public function getNameAlt()
    {
        return $this->nameAlt;
    }

    /**
     * @return string
     */
    public function getShortHand()
    {
        return $this->shortHand;
    }

    /**
     * @param string $shortHand
     */
    public function setShortHand($shortHand)
    {
        $this->shortHand = $shortHand;
    }


}
