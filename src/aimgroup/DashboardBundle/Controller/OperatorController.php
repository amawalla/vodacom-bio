<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\DashboardBundle\Controller;

use aimgroup\DashboardBundle\Dao\JsonObject;
use aimgroup\DashboardBundle\Entity\Operator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use aimgroup\DashboardBundle\Entity\UserLog;


/**
 * Class OperatorController
 * @Route("admin/operators")
 * @package aimgroup\DashboardBundle\Controller
 */
class OperatorController extends AbstractController
{
    var $session;

    public function __construct()
    {
        $this->session = new Session();
    }


    /**
     *
     * @Route("/", name="admin/operators")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {

        if (!in_array(46, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title' => "Manage Operator",
            'mqttMessageTopic' => $this->container->getParameter("mqtt_notify_msg"),
            'operator' => $this->container->getParameter("operator_name"),
        );

        return $this->prepareResponse($data);
    }

    /**
     * @param Request $request
     * @Method("GET")
     * @Route("/list",name="list_operator")
     */
    public function listOperator(Request $request)
    {
        $resp = new JsonObject();
        $status = false;
        try {
            $em = $this->getDoctrine()->getManager();
            $operator = $em->getRepository("DashboardBundle:Operator")->find(1);
            $resp->setItem($operator);
            $status = true;
            $resp->setMessage("success");
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
        }
        $resp->setStatus($status);

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @Method("POST")
     * @Route("/save",name="save_operator")
     */
    public function saveOperator(Request $request)
    {
        $resp = new JsonObject();
        $status = false;
        $attributes = $request->getContent();
        try {
            $json = json_decode($attributes, true);
            $em = $this->getDoctrine()->getManager();

            $operator = $em->getRepository("DashboardBundle:Operator")->find(1);
            if ($operator) {
                $operator->setName($json["name"]);
                $operator->setConfigUrl($json["configUrl"]);
                $operator->setTokenAuthUrl($json["tokenAuthUrl"]);
                $operator->setPasswordSetUrl($json["passwordSetUrl"]);
                $operator->setReportUrl($json["reportUrl"]);
                $operator->setSignatureSaveUrl($json["signatureSaveUrl"]);
                $operator->setGpsStatus($json["gpsStatus"]);
                $operator->setPrefix($json["prefix"]);
                $operator->setSimSerialPrefix($json["simSerialPrefix"]);
            } else {
                $operator = new Operator();
                $operator->setName($json["name"]);
                $operator->setConfigUrl($json["configUrl"]);
                $operator->setTokenAuthUrl($json["tokenAuthUrl"]);
                $operator->setPasswordSetUrl($json["passwordSetUrl"]);
                $operator->setReportUrl($json["reportUrl"]);
                $operator->setSignatureSaveUrl($json["signatureSaveUrl"]);
                $operator->setGpsStatus($json["gpsStatus"]);
                $operator->setPrefix($json["prefix"]);
                $operator->setSimSerialPrefix($json["simSerialPrefix"]);
                $em->persist($operator);
            }

            $em->flush();

            $status = true;
            $resp->setMessage("success");

            $this->logUserEvent(UserLog::CHANGE_CONFIGS, "update operator data",
                array("attributes" => $attributes));

        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
        }
        $resp->setStatus($status);

        return $this->buildResponse($resp, Response::HTTP_OK);
    }


}
