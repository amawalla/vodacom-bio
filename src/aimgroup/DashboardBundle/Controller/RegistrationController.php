<?php

namespace aimgroup\DashboardBundle\Controller;

use aimgroup\DashboardBundle\Dao\JTableResponse;
use aimgroup\RestApiBundle\Entity\Registration;
use aimgroup\RestApiBundle\Entity\RegistrationStatus;
use aimgroup\RestApiBundle\Entity\User;
use Elastica\Query;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class RegistrationController
 * @package aimgroup\DashboardBundle\Controller
 * @Route("admin/registration")
 */
class RegistrationController extends AbstractController {

    protected $session;
    private $superAgents = array();

    public function __construct()
    {
        $this->session = new Session();
    }

    /**
     * View Reports
     *
     * @Route("/", name="admin/registration")
     * @Method({"POST","GET"})
     * @Template("DashboardBundle:Admin:registrations.html.twig")
     * @param Request $request
     *
     * @return type|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function indexAction(Request $request)
    {
        
        $user_roles = json_decode($this->get('session')->get('user_role_perms'), true);

        $this->container->get("api.helper")->logInfo("AdminRegistrationController", "RegistrationController", array("request" => $user_roles, true));

        if (is_null($user_roles) || !in_array(2, $user_roles)) {
            return $this->redirect($this->generateUrl('admin'));
        }




        $em = $this->getDoctrine()->getManager();
        $idtype = $em->getRepository('DashboardBundle:Idtype')->findAll();

        $path = $request->get('path');
        $date = $request->get('date');

        $title = $this->getTitle($path, $date);
        $data = array(
            'title'             => $title . ":",
            'title_descr'       => $title,
            'idtype'            => $idtype,
            'custom'            => $request->get('custom'),
            'path'              => $path,
            'hide_search_class' => ($path != null ? 'hide_search' : '')
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/ported_numbers", name="admin/registration/ported_numbers")
     * @Method({"POST","GET"})
     * @Template("DashboardBundle:Admin:ported_numbers.html.twig")
     * @param Request $request
     *
     * @return type|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function portedNumbersAction(Request $request)
    {
        $user_roles = json_decode($this->get('session')->get('user_role_perms'), true);

        $this->container->get("api.helper")->logInfo("AdminRegistrationController", "RegistrationController", array("request" => $user_roles, true));

        if (is_null($user_roles) || !in_array(2, $user_roles)) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $em = $this->getDoctrine()->getManager();
        $idtype = $em->getRepository('DashboardBundle:Idtype')->findAll();

        //$attributes = json_decode($request->getContent(), true);

        $path = $request->get('path');
        $date = $request->get('date');

        $title = $this->getTitle($path, $date);
        $data = array(
            'title'             => 'Mobile Number Portability',
            'title_descr'       => 'Port In Requests',
            'idtype'            => $idtype,
            'path'              => $path,
            'hide_search_class' => ($path != null ? 'hide_search' : '')
        );

        return $this->prepareResponse($data);
    }

    private function getTitle($path, $date)
    {
        if ($path) {
            if ($path == "treg") {
                return "Temporary Registrations" . ($date ? " (" . $date . ")" : "");
            } else if ($path == "freg") {
                return "Fully Registrations" . ($date ? " (" . $date . ")" : "");
            } else if ($path == "tobesent") {
                return "Waiting to be Sent" . ($date ? " (" . $date . ")" : "");
            } else if ($path == "pending") {
                return "Pending Registrations" . ($date ? " (" . $date . ")" : "");
            } else if ($path == "declined") {
                return "Declined Registrations" . ($date ? " (" . $date . ")" : "");
            }
        }
        return "View Registrations Report:";
    }

    /**
     * View User Role verify_registrations
     *
     * @Route("/verify_registrations", name="admin/verify_registrations")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:verify_registrations.html.twig")
     */
    public function verify_registrationsAction()
    {
        if (!in_array(49, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $em = $this->getDoctrine()->getManager();
        $idtype = $em->getRepository('DashboardBundle:Idtype')->findAll();

        $user = $this->get('security.token_storage')->getToken()->getUser();

        //--
        $configArray = array();
        $numRows = 1;
        $secsCount = 40;
        $query = $em->createQueryBuilder()
            ->select(" u from DashboardBundle:ConfigMaster u WHERE u.name = 'verifierFieldList' ")
            ->getQuery();

        $configMaster = $query->getOneOrNullResult();

        $numRows = 1;
        if ($configMaster) {
            $configArray = explode(":::", $configMaster->getConfig());

            $settingMerged = explode(":::", $configMaster->getConfigType());

            $numRows = @$settingMerged[0];
            $secsCount = @$settingMerged[1];
        }
        //--

        $data = array(
            'title'       => "View Registrations Report:",
            'title_descr' => "View Registrations Report",
            'idtype'      => $idtype,
            'numRows'     => $numRows,
            'secsCount'   => $secsCount,
            'user_id'     => $user->getId()
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/registrationaudit", name="admin/registrationaudit")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:registrationaudit.html.twig")
     */
    public function registrationauditAction() {
        if (!in_array(50, json_decode($this->get('session')->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }


        $em = $this->getDoctrine()->getManager();
        $idtype = $em->getRepository('DashboardBundle:Idtype')->findAll();

        $user = $this->get('security.token_storage')->getToken()->getUser();

        $data = array(
            'title' => "Audit Panel",
            'title_descr' => "Audit Panel",
            'idtype' => $idtype,
            'users' => $this->loadUsers('ROLE_VERIFYER'),
            'user_id' => $user->getId()
        );

        return $this->prepareResponse($data);
    }


    private function getAimRegStatusDesc($tstatus, $fstatus, $imageCount)
    {
        $ret = "Pending";
        if (in_array($tstatus, array('1', '2', '6')) || in_array($fstatus, array('1', '2', '6', '7'))) {
            $ret = "Submitted";
        } else if (in_array($tstatus, array('3', '4')) || in_array($fstatus, array('3', '4'))) {
            $ret = "Declined";
        } else if ($imageCount >= 3) {
            $ret = "Received";
        }

        return $ret; // .$tstatus . $fstatus;
    }

    private function getIcapRegStatusDesc($tstatus, $fstatus)
    {
        $ret = "Not Sent";
        if (in_array($tstatus, array('1', '2', '6')) && $fstatus == 0) {
            $ret = "TREG";
        } else if (in_array($fstatus, array('2', '6', '7'))) {
            $ret = "FREG";
        } else if (in_array($tstatus, array('3', '4')) || in_array($fstatus, array('3', '4'))) {
            $ret = "Declined";
        }
        
        if (in_array($fstatus, array('2', '6', '7'))) {
            $ret = "FREG";
        }

        return $ret; // .$tstatus . $fstatus;
    }

    /**
     * get a list of all the Regions
     *
     * @param Request $request
     * @Method({"GET","POST"})
     * @Route("/list",name="list_registrations_elastic")
     *
     * @return JsonResponse
     */
    public function listRegistrationsAction(Request $request)
    {
        /** @var User $user */
        $loggedInUser = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $loggedInUser->getRoles();


        $resp = new JTableResponse();
        try {
            $attributes = $request->request->all();
            $queryAttrib = $request->query->all();

            //$this->container->get("api.helper")->logInfo("DReg", "DReg", $attributes);
            $results = $this->container->get("fos_elastica.manager")
                ->getRepository("RestApiBundle:RegistrationStatus")->listRegistrationStatus($attributes, $queryAttrib["jtPageSize"], $queryAttrib["jtStartIndex"] + 1);

            $entityManager = $this->getDoctrine()->getManager();

            $territories = $entityManager->getRepository('DashboardBundle:Territory')->findAll();

            $locationBucket = array();
            foreach ($territories as $index => $territory) {
                $locationBucket[$territory->getId()] = array(
                    'territory' => $territory->getName(),
                    'region'    => $territory->getRegion()->getName(),
                );
            }

            $idTypes = $entityManager->getRepository('DashboardBundle:Idtype')->findAll();
            $identificationTypes = array();
            foreach ($idTypes as $index => $idType) {
                $identificationTypes[$idType->getId()] = $idType->getName();
            }

            // $this->container->get("api.helper")->logInfo("DReg", "DReg", $results);
            $registrationsArray = array();
            /** @var  $result RegistrationStatus */
            foreach ($results["results"] as $result) {
                $regOut = array();
                /** @var  $registration Registration */
                $registration = $result->getRegistrationid();
                $user = $registration->getOwner();
                $parentUser = $user->getParentId();

                if (in_array('ROLE_SUPERAGENT', $roles)) {
                    if ($parentUser && $loggedInUser->getId() != $parentUser->getId()) {
                        continue;
                    }
                }

                $regOut["id"] = $registration->getId();
                $regOut["tregStatus"] = $result->getTemporaryRegStatus();
                $regOut["fullRegStatus"] = $result->getFullRegStatus();
                $regOut["customerName"] = $registration->getFirstName() . " " . $registration->getLastName();
                $regOut["image_count"] = $registration->getImage_count();

                if (array_key_exists($registration->getTerritory(), $locationBucket)) {
                    $regOut["region"] = $locationBucket[$registration->getTerritory()]['region'];
                    $regOut["territory"] = $locationBucket[$registration->getTerritory()]['territory'];
                }

                $regOut["created_on"] = $registration->getCreatedDate()->format('Y-m-d H:i:s');
                $regOut["msisdn"] = $registration->getMsisdn();
	        $regOut["type"] = $registration->getRegistrationType() == 1 ? 'Standard' : 'Biometric';
                $regOut["username"] = $registration->getAgentMsisdn();
                $regOut["identificationType"] = $identificationTypes[$registration->getIdentificationType()];
                $regOut["agentName"] = $user->getFirstName() . " " . $user->getLastName();
                $regOut["icapStatus"] = $this->getIcapRegStatusDesc($result->getTemporaryRegStatus(), $result->getFullRegStatus());
                $regOut["aimStatus"] = $this->getAimRegStatusDesc($result->getTemporaryRegStatus(), $result->getFullRegStatus(), $registration->getImageCount());
                $regOut["dbmsStatus"] = (in_array($result->getDbmsState(), array(6, 7))) ? "Y" : "N";
                $regOut["mpesaStatus"] = (in_array($result->getMpesaState(), array(1, 3))) ? "Y" : "N";
                if ($parentUser) {
                    if (strlen($parentUser->getUsername()) > 0) {
                        if (in_array($parentUser->getUsername(), $this->superAgents)) {
                            $parent = $this->superAgents[$parentUser->getUsername()];
                        } else {
                            $query = new \Elastica\Query();
                            $boolean = new \Elastica\Query\Bool();
                            $fieldQuery = new \Elastica\Query\Match();
                            $fieldQuery->setField('parent_id', $parentUser->getUsername());
                            $boolean->addMust($fieldQuery);
                            $query->setQuery($boolean);
                            $query->setSize(1);
                            #$users = $this->container->get("fos_elastica.finder.vodacom.user")->find($query);
                            try{
                                $users = $this->container->get("fos_elastica.manager")->getRepository("RestApiBundle:User")->find($query);
                                /** @var  $parent User */
                                $parent = $users[0];
                                if ($parent) {
                                    $this->superAgents[$parent->getUsername()] = $parent;
                                }
                            }catch(\Exception $e){}
                        }
                        if($parent){
                            $regOut["superAgentName"] = $parent->getFirstName() . " " . $parent->getLastName();
                            $regOut["superAgentPhone"] = $parent->getMobileNumber();
                        }
                    }
                }

                $registrationsArray[] = $regOut;
            }

            $resp->setRecords($registrationsArray);
            if (isset($queryAttrib["jtPageSize"])) {
                $resp->setTotalRecordCount(round($results["count"] / $queryAttrib["jtPageSize"]));
            } else {
                $resp->setTotalRecordCount($results["count"]);
            }

        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    

    /**
     * get a list of all the Regions
     *
     * @param Request $request
     * @Method({"GET","POST"})
     * @Route("/ported_numbers/list",name="ported_numbers/list")
     *
     * @return JsonResponse
     */
    public function ported_numbersAction(Request $request) {

        $resp = new JTableResponse();
        try {

            $em = $this->getDoctrine()->getManager();
            $emConfig = $em->getConfiguration();
            $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
            $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
            $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
            $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');


            $attributes = $request->request->all();
            $where = " WHERE u.isPorted!=0 ";

            /** @var  $user User */
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $roles = $user->getRoles(); //array of roles

            if (in_array("ROLE_SUPERAGENT", $roles)) {
                $where .= " AND a.parent_id = " . $user->getId();
            }
            if (in_array("ROLE_VODASHOP", $roles)) {
                $where .= " AND a.vodashop_id = " . $user->getId();
            }
            if (strlen(@$attributes["msisdn"]) >= 4) {
                $where .= " AND u.msisdn like '%" . $attributes["msisdn"] . "%'";
            }
            if (strlen(@$attributes["first_name"]) >= 2) {//echo "<<<<<<<<<<<<<<";
                $where .= " AND u.firstName like '%" . $attributes["first_name"] . "%'";
            }
            if (strlen(@$attributes["last_name"]) >= 2) {
                $where .= " AND u.lastName like '%" . $attributes["last_name"] . "%'";
            }
            if (@$attributes["identificationType"] >= 1) {
                $where .= " AND u.identificationType = '" . $attributes["identificationType"] . "'";
            }
            if (strlen(@$attributes["id_number"]) >= 4) {
                $where .= " AND u.identification like '%" . $attributes["id_number"] . "%'";
            }
            if (strlen(@$attributes["agent_firstname"]) >= 3) {
                $where .= " AND a.firstName like '%" . $attributes["agent_firstname"] . "%'";
            }
            if (strlen(@$attributes["agent_lastname"]) >= 3) {
                $where .= " AND a.lastName like '%" . $attributes["agent_lastname"] . "%'";
            }
            if (strlen(@$attributes["agent_phone"]) >= 4) {
                $where .= " AND a.username = '" . $attributes["agent_phone"] . "'";
            }
            if (@$attributes["territory"] > 0) {
                $where .= " AND u.territory = '" . $attributes["territory"] . "'";
            }
            if (strlen(@$attributes["from_date"]) > 4) {
                $where .= " AND DATE(u.createdDate) >= '" . date('Y-m-d', strtotime($attributes["from_date"])) . "'";
            }
            if (strlen(@$attributes["to_date"]) > 4) {
                $where .= " AND DATE(u.createdDate) <= '" . date('Y-m-d', strtotime($attributes["to_date"])) . "'";
            }
            if (strlen(@$attributes["icap_status"]) > 2) {
                $where .= $this->getWhereForICAPSatus($attributes["icap_status"]);
            }
            if (strlen(@$attributes["ereg_status"]) > 2) {
                $where .= $this->getWhereForEREGSatus($attributes["ereg_status"]);
            }
            //echo @$attributes["date"];
            if (strlen(@$attributes["date"]) > 4) {
                if (strlen(@$attributes["path"]) > 2 && @$attributes["path"] == "treg") {
                    //sent to TREG NOT sent to FREG
                    $where .= " AND x.temporaryRegStatus IN (1,2,6) AND x.fullRegStatus = 0 AND DATE(u.createdDate) = '" . @$attributes["date"] . "'";
                } else if (strlen(@$attributes["path"]) > 2 && @$attributes["path"] == "tobesent") {
                    //NOT sent to TREG
                    $where .= " AND x.temporaryRegStatus = 0 AND DATE(u.createdDate) = '" . @$attributes["date"] . "'";
                } else if (strlen(@$attributes["path"]) > 2 && @$attributes["path"] == "declined") {
                    //DECLINED or FAILED
                    $where .= " AND x.temporaryRegStatus IN (3,4) AND DATE(u.createdDate) = '" . @$attributes["date"] . "'";
                } else if (strlen(@$attributes["path"]) > 2 && @$attributes["path"] == "freg") {
                    $where .= " AND x.temporaryRegStatus IN (1,2,6) AND x.fullRegStatus IN (1,6,7) AND DATE(u.createdDate) = '" . @$attributes["date"] . "'";
                }

                if (strlen(@$attributes["path"]) > 2 && @$attributes["path"] == "pending") {
                    $where .= " AND u.image_count < 3 AND DATE(u.createdDate) = '" . @$attributes["date"] . "'";
                }
            } else {
                if (strlen(@$attributes["path"]) > 2 && @$attributes["path"] == "treg") {
                    //sent to TREG NOT sent to FREG
                    $where .= " AND x.temporaryRegStatus IN (1,2,6) AND x.fullRegStatus = 0";
                } else if (strlen(@$attributes["path"]) > 2 && @$attributes["path"] == "tobesent") {
                    //NOT sent to TREG
                    $where .= " AND x.temporaryRegStatus = 0";
                } else if (strlen(@$attributes["path"]) > 2 && @$attributes["path"] == "declined") {
                    //DECLINED or FAILED
                    $where .= " AND x.temporaryRegStatus IN (3,4)";
                } else if (strlen(@$attributes["path"]) > 2 && @$attributes["path"] == "freg") {
                    $where .= " AND x.temporaryRegStatus IN (1,2,6) AND x.fullRegStatus IN (1,6,7)";
                }

                if (strlen(@$attributes["path"]) > 2 && @$attributes["path"] == "pending") {
                    $where .= " AND u.image_count < 3";
                }
            }

            $queryAttrib = $request->query->all();

            $queryString = " SELECT u.id,u.msisdn, x.temporaryRegStatus as tregStatus,
             x.fullRegStatus as fregStatus, x.auditState, x.verifyState, x.mpesaState,
             concat(u.firstName, ' ', u.lastName) as customerName, 
             DATE_FORMAT(u.createdDate, '%Y-%m-%d') as created_on, 
             DATE_FORMAT(u.createdDate, '%H:%i:%s') as created_time, 
             u.image_count as image_count, u.identificationType,
              u.identification, a.username, concat(a.firstName,' ', a.lastName) as agentName,
               m.mobileNumber as superAgentPhone,
                concat(m.firstName,' ', m.lastName) as superAgentName, u.region,
                 u.territory,u.isPorted "
                . "FROM RestApiBundle:RegistrationStatus x "
                . "LEFT OUTER JOIN x.registrationId u "
                . "LEFT OUTER JOIN u.owner a "
                . "LEFT OUTER JOIN a.parent_id m  " . $where
                . " ORDER BY u.id desc ";




            $query = $em->createQuery($queryString);




            $query->setMaxResults($queryAttrib["jtPageSize"]);
            $query->setFirstResult($queryAttrib["jtStartIndex"]);
            //echo $query->getSql(); exit;
            //echo $queryString; exit;
            $agents = $query->getResult();

        // echo  json_encode($agents );

          //  exit;

            $entities = $em->getRepository('DashboardBundle:Idtype')->findAll();

            $id_array = array();
            foreach ($entities as $akey => $aval) {
                $id_array[$aval->getId()] = $aval->getName();
            }

            $territory_entiry = $em->getRepository('DashboardBundle:Territory')->findAll();

            $territory_array = array();
            $territory_array['none'] = "";
            foreach ($territory_entiry as $bkey => $bval) {
                $territory_array[$bval->getId()] = $bval->getName();
            }


            $returnData['Result'] = "OK";
            $rowData = array();

            if ($agents) {
                foreach ($agents as $kkey => $vval) {
                    if ($vval["territory"]) {
                        $vval['territory'] = @$territory_array[$vval['territory']];
                    }
                    $vval['temporaryRegStatus'] = $this->getRegStatusDesc($vval['tregStatus']);

                    $vval['aimStatus'] = $this->getAimRegStatusDesc($vval['tregStatus'], $vval['fregStatus'], $vval['image_count']);
                    $vval["dbmsStatus"] = (in_array($vval['mpesaState'], array(6, 7))) ? "Y" : "N";

                    $vval['icapStatus'] = $this->getIcapRegStatusDesc($vval['tregStatus'], $vval['fregStatus']);
                    $vval['isPorted']=$this->getPortStatus($vval['isPorted']);
                    $vval['region'] = $this->get_regionName_fromKey($vval['region']);
                    $vval['identificationType'] = $id_array[$vval['identificationType']];
                    $rowData[] = $vval;
                }
            }
            $returnData['Records'] = $rowData;
            $returnData['Record'] = null;
            $returnData['Message'] = null;
            $returnData['Options'] = null;

            echo json_encode($returnData);
            exit;

            $resp->setRecords($agents);
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }
        return $this->buildResponse($resp, Response::HTTP_OK);
    }



    public function dbRegistrations(Request $request){
        $resp = new JTableResponse();
        try {

            $em = $this->getDoctrine()->getManager();
            $emConfig = $em->getConfiguration();
            $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
            $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
            $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
            $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');


            $attributes = $request->request->all();
            $where = " WHERE u.isPorted!=0 ";

            /** @var  $user User */
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $roles = $user->getRoles(); //array of roles

            if (in_array("ROLE_SUPERAGENT", $roles)) {
                $where .= " AND a.parent_id = " . $user->getId();
            }
            if (in_array("ROLE_VODASHOP", $roles)) {
                $where .= " AND a.vodashop_id = " . $user->getId();
            }
            if (strlen(@$attributes["msisdn"]) >= 4) {
                $where .= " AND u.msisdn like '%" . $attributes["msisdn"] . "%'";
            }
            if (strlen(@$attributes["first_name"]) >= 2) {//echo "<<<<<<<<<<<<<<";
                $where .= " AND u.firstName like '%" . $attributes["first_name"] . "%'";
            }
            if (strlen(@$attributes["last_name"]) >= 2) {
                $where .= " AND u.lastName like '%" . $attributes["last_name"] . "%'";
            }
            if (@$attributes["identificationType"] >= 1) {
                $where .= " AND u.identificationType = '" . $attributes["identificationType"] . "'";
            }
            if (strlen(@$attributes["id_number"]) >= 4) {
                $where .= " AND u.identification like '%" . $attributes["id_number"] . "%'";
            }
            if (strlen(@$attributes["agent_firstname"]) >= 3) {
                $where .= " AND a.firstName like '%" . $attributes["agent_firstname"] . "%'";
            }
            if (strlen(@$attributes["agent_lastname"]) >= 3) {
                $where .= " AND a.lastName like '%" . $attributes["agent_lastname"] . "%'";
            }
            if (strlen(@$attributes["agent_phone"]) >= 4) {
                $where .= " AND a.username = '" . $attributes["agent_phone"] . "'";
            }
            if (@$attributes["territory"] > 0) {
                $where .= " AND u.territory = '" . $attributes["territory"] . "'";
            }
            if (strlen(@$attributes["from_date"]) > 4) {
                $where .= " AND DATE(u.createdDate) >= '" . date('Y-m-d', strtotime($attributes["from_date"])) . "'";
            }
            if (strlen(@$attributes["to_date"]) > 4) {
                $where .= " AND DATE(u.createdDate) <= '" . date('Y-m-d', strtotime($attributes["to_date"])) . "'";
            }
            if (strlen(@$attributes["icap_status"]) > 2) {
                $where .= $this->getWhereForICAPSatus($attributes["icap_status"]);
            }
            if (strlen(@$attributes["ereg_status"]) > 2) {
                $where .= $this->getWhereForEREGSatus($attributes["ereg_status"]);
            }
            //echo @$attributes["date"];
            if (strlen(@$attributes["date"]) > 4) {
                if (strlen(@$attributes["path"]) > 2 && @$attributes["path"] == "treg") {
                    //sent to TREG NOT sent to FREG
                    $where .= " AND x.temporaryRegStatus IN (1,2,6) AND x.fullRegStatus = 0 AND DATE(u.createdDate) = '" . @$attributes["date"] . "'";
                } else if (strlen(@$attributes["path"]) > 2 && @$attributes["path"] == "tobesent") {
                    //NOT sent to TREG
                    $where .= " AND x.temporaryRegStatus = 0 AND DATE(u.createdDate) = '" . @$attributes["date"] . "'";
                } else if (strlen(@$attributes["path"]) > 2 && @$attributes["path"] == "declined") {
                    //DECLINED or FAILED
                    $where .= " AND x.temporaryRegStatus IN (3,4) AND DATE(u.createdDate) = '" . @$attributes["date"] . "'";
                } else if (strlen(@$attributes["path"]) > 2 && @$attributes["path"] == "freg") {
                    $where .= " AND x.temporaryRegStatus IN (1,2,6) AND x.fullRegStatus IN (1,6,7) AND DATE(u.createdDate) = '" . @$attributes["date"] . "'";
                }

                if (strlen(@$attributes["path"]) > 2 && @$attributes["path"] == "pending") {
                    $where .= " AND u.image_count < 3 AND DATE(u.createdDate) = '" . @$attributes["date"] . "'";
                }
            } else {
                if (strlen(@$attributes["path"]) > 2 && @$attributes["path"] == "treg") {
                    //sent to TREG NOT sent to FREG
                    $where .= " AND x.temporaryRegStatus IN (1,2,6) AND x.fullRegStatus = 0";
                } else if (strlen(@$attributes["path"]) > 2 && @$attributes["path"] == "tobesent") {
                    //NOT sent to TREG
                    $where .= " AND x.temporaryRegStatus = 0";
                } else if (strlen(@$attributes["path"]) > 2 && @$attributes["path"] == "declined") {
                    //DECLINED or FAILED
                    $where .= " AND x.temporaryRegStatus IN (3,4)";
                } else if (strlen(@$attributes["path"]) > 2 && @$attributes["path"] == "freg") {
                    $where .= " AND x.temporaryRegStatus IN (1,2,6) AND x.fullRegStatus IN (1,6,7)";
                }

                if (strlen(@$attributes["path"]) > 2 && @$attributes["path"] == "pending") {
                    $where .= " AND u.image_count < 3";
                }
            }

            $queryAttrib = $request->query->all();

            $queryString = " SELECT u.id,u.msisdn, x.temporaryRegStatus as tregStatus,
             x.fullRegStatus as fregStatus, x.auditState, x.verifyState, 
             concat(u.firstName, ' ', u.lastName) as customerName, 
             DATE_FORMAT(u.createdDate, '%Y-%m-%d') as created_on, 
             DATE_FORMAT(u.createdDate, '%H:%i:%s') as created_time, 
             u.image_count as image_count, u.identificationType,
              u.identification, a.username, concat(a.firstName,' ', a.lastName) as agentName,
               m.mobileNumber as superAgentPhone,
                concat(m.firstName,' ', m.lastName) as superAgentName, u.region,
                 u.territory,u.isPorted "
                . "FROM RestApiBundle:RegistrationStatus x "
                . "LEFT OUTER JOIN x.registrationId u "
                . "LEFT OUTER JOIN u.owner a "
                . "LEFT OUTER JOIN a.parent_id m  " . $where
                . " ORDER BY u.id desc ";



            //  echo $queryString;
            // exit;
            $query = $em->createQuery($queryString);

            $query->setMaxResults($queryAttrib["jtPageSize"]);
            $query->setFirstResult($queryAttrib["jtStartIndex"]);
            //echo $query->getSql(); exit;
            //echo $queryString; exit;
            $agents = $query->getResult();

            $entities = $em->getRepository('DashboardBundle:Idtype')->findAll();

            $id_array = array();
            foreach ($entities as $akey => $aval) {
                $id_array[$aval->getId()] = $aval->getName();
            }

            $territory_entiry = $em->getRepository('DashboardBundle:Territory')->findAll();

            $territory_array = array();
            $territory_array['none'] = "";
            foreach ($territory_entiry as $bkey => $bval) {
                $territory_array[$bval->getId()] = $bval->getName();
            }


            $returnData['Result'] = "OK";
            $rowData = array();

            if ($agents) {
                foreach ($agents as $kkey => $vval) {
                    if ($vval["territory"]) {
                        $vval['territory'] = @$territory_array[$vval['territory']];
                    }
                    $vval['temporaryRegStatus'] = $this->getRegStatusDesc($vval['tregStatus']);

                    $vval['aimStatus'] = $this->getAimRegStatusDesc($vval['tregStatus'], $vval['fregStatus'], $vval['image_count']);

                    $vval['icapStatus'] = $this->getIcapRegStatusDesc($vval['tregStatus'], $vval['fregStatus']);


                    $vval['isPorted']=$this->getPortStatus($vval['isPorted']);
                    $vval['region'] = $this->get_regionName_fromKey($vval['region']);
                    $vval['identificationType'] = $id_array[$vval['identificationType']];
                    $rowData[] = $vval;


                }
            }
            $returnData['Records'] = $rowData;
            $returnData['Record'] = null;
            $returnData['Message'] = null;
            $returnData['Options'] = null;

            echo json_encode($returnData);
            exit;

            $resp->setRecords($agents);
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }
        return $this->buildResponse($resp, Response::HTTP_OK);



    }
    private function  getPortStatus($status){

        if($status==3)
            return "Portin Failed";
        else if($status==2)
            return "Portin Submited";
        else if($status==4)
            return "Porting Successful";
        else{
            return "Port Received";
        }


    }


    /**
     * get a list of all the Regions
     *
     * @param Request $request
     * @Method({"GET","POST"})
     * @Route("/OLDported_numbers/list",name="OLDported_numbers/list")
     *
     * @return JsonResponse
     */
    public function OLDportedNumbersRegistrationAction(Request $request)
    {
        $resp = new JTableResponse();
        try {

            $attributes = $request->request->all();
            $queryAttrib = $request->query->all();

            $attributes['path'] = 'ported_numbers';
            $attributes['date'] = $request->get('date');

            $results = $this->container->get("fos_elastica.manager")
                ->getRepository("RestApiBundle:RegistrationStatus")->listRegistrationStatus($attributes, $queryAttrib["jtPageSize"], $queryAttrib["jtStartIndex"] + 1);

            $registrationsArray = array();
            /** @var  $result RegistrationStatus */
            foreach ($results["results"] as $result) {
                $regOut = array();
                /** @var  $registration Registration */
                $registration = $result->getRegistrationid();
                $user = $registration->getOwner();
                $parentUser = $user->getParentId();
                
                if (in_array('ROLE_SUPERAGENT', $roles)) {
                    if ($parentUser && $loggedInUser->getId() != $parentUser->getId()) {
                        continue;
                    }
                }
                
                $regOut["id"] = $registration->getId();
                $regOut["tregStatus"] = $result->getTemporaryRegStatus();
                $regOut["fullRegStatus"] = $result->getFullRegStatus();
                $regOut["customerName"] = $registration->getFirstName() . " " . $registration->getLastName();
                $regOut["image_count"] = $registration->getImage_count();
              
                if (array_key_exists($registration->getTerritory(), $locationBucket)) {
                    $regOut["region"] = $locationBucket[$registration->getTerritory()]['region'];
                    $regOut["territory"] = $locationBucket[$registration->getTerritory()]['territory'];
                }
                
                $regOut["created_on"] = $registration->getCreatedDate()->format('Y-m-d H:m');
                $regOut["msisdn"] = $registration->getMsisdn();
                $regOut["username"] = $registration->getAgentMsisdn();
                $regOut["identificationType"] = $identificationTypes[$registration->getIdentificationType()];
                $regOut["agentName"] = $user->getFirstName() . " " . $user->getLastName();
                $regOut["icapStatus"] = $this->getIcapRegStatusDesc($result->getTemporaryRegStatus(), $result->getFullRegStatus());
                $regOut["aimStatus"] = $this->getAimRegStatusDesc($result->getTemporaryRegStatus(), $result->getFullRegStatus(), $registration->getImageCount());
                $regOut["dbmsStatus"] = (in_array($result->getDbmsState(), array(6, 7))) ? "Y" : "N";
                $regOut["mpesaStatus"] = (in_array($result->getMpesaState(), array(1, 3))) ? "Y" : "N";
                if ($parentUser) {
                    if (strlen($parentUser->getUsername()) > 0) {
                        if (in_array($parentUser->getUsername(), $this->superAgents)) {
                            $parent = $this->superAgents[$parentUser->getUsername()];
                        } else {
                            $query = new Query();
                            $boolean = new Query\BoolQuery();
                            $fieldQuery = new Query\Match();
                            $fieldQuery->setField('parent_id', $parentUser->getUsername());
                            $boolean->addMust($fieldQuery);
                            $query->setQuery($boolean);
                            $query->setSize(1);
                            #$users = $this->container->get("fos_elastica.finder.vodacom.user")->find($query);
			    try{
                            $users = $this->container->get("fos_elastica.manager")->getRepository("RestApiBundle:User")->find($query);
                            /** @var  $parent User */
                            $parent = $users[0];
                            if ($parent) {
                                $this->superAgents[$parent->getUsername()] = $parent;
                            }
		            }catch(\Exception $e){} 
                        }
			        if($parent){
                        $regOut["superAgentName"] = $parent->getFirstName() . " " . $parent->getLastName();
                        $regOut["superAgentPhone"] = $parent->getMobileNumber();
                	}
		}
                }
                
                $registrationsArray[] = $regOut;
            }

            $resp->setRecords($registrationsArray);
            if (isset($queryAttrib["jtPageSize"])) {
                $resp->setTotalRecordCount(round($results["count"] / $queryAttrib["jtPageSize"]));
            } else {
                $resp->setTotalRecordCount($results["count"]);
            }

        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * View Reports
     *
     * @Route("/multipleregistration", name="admin/multipleregistration")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:multipleregistration.html.twig")
     */
    public function multipleregistrationAction()
    {
        if (!in_array(50, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $em = $this->getDoctrine()->getManager();
        $idtype = $em->getRepository('DashboardBundle:Idtype')->findAll();

        $user = $this->get('security.token_storage')->getToken()->getUser();

        $data = array(
            'title'       => "View Registrations Report:",
            'title_descr' => "View Registrations Report",
            'idtype'      => $idtype,
            'user_id'     => $user->getId()
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/journey", name="registration_journey")
     * @Method({"POST","GET"})
     * @Template("DashboardBundle:Admin:registration_journey.html.twig")
     * @param Request $request
     *
     * @return type|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function registrationJourneyAction(Request $request)
    {
        $user_roles = json_decode($this->get('session')->get('user_role_perms'), true);

        $this->container->get("api.helper")->logInfo("AdminRegistrationController", "RegistrationController", array("request" => $user_roles, true));

        if (is_null($user_roles) || !in_array(2, $user_roles)) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $em = $this->getDoctrine()->getManager();
        $idtype = $em->getRepository('DashboardBundle:Idtype')->findAll();

        //$attributes = json_decode($request->getContent(), true);

        $path = $request->get('path');
        $date = $request->get('date');

        $title = $this->getTitle($path, $date);
        $data = array(
            'title'             => 'Mobile Number Portability',
            'title_descr'       => 'Registration Journey',
            'idtype'            => $idtype,
            'path'              => $path,
            'hide_search_class' => ($path != null ? 'hide_search' : '')
        );

        return $this->prepareResponse($data);
    }

    /**
     * get a list of all the Regions
     *
     * @param Request $request
     * @Method({"GET","POST"})
     * @Route("/journey/list",name="registration_journey_elastic")
     *
     * @return JsonResponse
     */
    public function listRegistrationJourneyAction(Request $request)
    {

        $resp = new JTableResponse();
        try {

            $attributes = $request->request->all();
            $queryAttrib = $request->query->all();

            $attributes['path'] = $request->get('path');
            $attributes['date'] = $request->get('date');

            $results = $this->container->get("fos_elastica.manager")
                ->getRepository("RestApiBundle:RegistrationStatus")->listRegistrationStatus($attributes, $queryAttrib["jtPageSize"], $queryAttrib["jtStartIndex"] + 1);

            $this->container->get("api.helper")->logInfo("DReg", "DReg", $results);
            $registrationsArray = array();
            /** @var  $result RegistrationStatus */
            foreach ($results["results"] as $result) {
                $regOut = array();
                /** @var  $registration Registration */
                $registration = $result->getRegistrationid();
                $user = $registration->getOwner();
                $parentUser = $user->getParentId();
                $regOut["tregStatus"] = $result->getTemporaryRegStatus();
                $regOut["fullRegStatus"] = $result->getFullRegStatus();
                $regOut["customerName"] = $registration->getFirstName() . " " . $registration->getLastName();
                $regOut["image_count"] = $registration->getImage_count();
                $regOut["region"] = $registration->getRegion();
                $regOut["territory"] = $registration->getTerritory();
                $regOut["created_on"] = $registration->getCreatedDate()->format('Y-m-d H:i:s');
                $regOut["msisdn"] = $registration->getMsisdn();
                $regOut["username"] = $registration->getAgentMsisdn();
                $regOut["identificationType"] = $registration->getIdentificationType();
                $regOut["id"] = $registration->getId();
                $regOut["agentName"] = $user->getFirstName() . " " . $user->getLastName();
                $regOut["icapStatus"] = $this->getIcapRegStatusDesc($result->getTemporaryRegStatus(), $result->getFullRegStatus());
                $regOut["aimStatus"] = $this->getAimRegStatusDesc($result->getTemporaryRegStatus(), $result->getFullRegStatus(), $registration->getImageCount());
                $regOut["dbmsStatus"] = (in_array($result->getMpesaState(), array(6, 7))) ? "Y" : "N";
                $regOut["registrationDate"] = (!is_null($registration->getRegistrationTime()) ? $registration->getRegistrationTime()->format('Y-m-d H:i:s'):'');
                $regOut["deviceDate"] = (!is_null($registration->getDeviceSentTime()) ? $registration->getDeviceSentTime()->format('Y-m-d H:i:s'):'');
                $regOut["tregDate"] = (!is_null($result->getTregDate()) ? $result->getTregDate()->format('Y-m-d H:i:s') : '');
                $regOut["fregDate"] = (!is_null($result->getFregDate()) ? $result->getFregDate()->format('Y-m-d H:i:s') : '');
                $regOut["allimageDate"] = (!is_null($result->getAllimageDate()) ? $result->getAllimageDate()->format('Y-m-d H:i:s') : '');

                if ($parentUser) {
                    if (strlen($parentUser->getUsername()) > 0) {
                        if (in_array($parentUser->getUsername(), $this->superAgents)) {
                            $parent = $this->superAgents[$parentUser->getUsername()];
                        } else {
                            $query = new Query();
                            $boolean = new Query\BoolQuery();
                            $fieldQuery = new Query\Match();
                            $fieldQuery->setField('username', strtolower($parentUser->getUsername()));
                            $boolean->addMust($fieldQuery);
                            $query->setQuery($boolean);
                            $query->setSize(1);
                            $users = $this->container->get("fos_elastica.finder.vodacom_mnp_test.user")->find($query);
                            $this->container->get("api.helper")->logInfo("AdminRegistration", "list", $users);
                            /** @var  $parent User */
                            $parent = $users[0];
                            if ($parent) {
                                $this->superAgents[$parent->getUsername()] = $parent;
                            }
                        }
                        $this->container->get("api.helper")->logInfo("AdminRegistration", "list", array("parent" => $parent, "mobile" => $registration->getMsisdn()));
                        if ($parent) {
                            $regOut["superAgentName"] = $parent->getFirstName() . " " . $parent->getLastName();
                            $regOut["superAgentPhone"] = $parent->getMobileNumber();
                        }
                    }
                }
                array_push($registrationsArray, $regOut);
            }

            $resp->setRecords($registrationsArray);
            if (isset($queryAttrib["jtPageSize"])) {
                $resp->setTotalRecordCount(round($results["count"] / $queryAttrib["jtPageSize"]));
            } else {
                $resp->setTotalRecordCount($results["count"]);
            }

        }
        catch (\Exception $e) {
            $this->container->get("api.helper")->logE("AdminRegistration", "list", $e);
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }

        return $this->buildResponse($resp, Response::HTTP_OK);
    }


    public function get_regionName_fromKey($region_key) {
        $regionsArray = array(
            'PN' => 'PEMBA',
            'PS' => 'PEMBA',
            'KR' => 'KAGERA',
            'MW' => 'BUKOBA',
            'MA' => 'BUNDA',
            'GE' => 'GEITA',
            'PW' => 'KARAGWE',
            'MW' => 'MWANZA',
            'MA' => 'TARIME',
            'DO' => 'DODOMA',
            'MO' => 'KILOSA',
            'DO' => 'KONDOA',
            'SI' => 'MANYONI',
            'MO' => 'MOROGORO',
            'PW' => 'MPWAPWA',
            'SD' => 'SINGIDA',
            'TN' => 'KOROGWE',
            'LI' => 'LINDI',
            'MT' => 'MTWARA',
            'TN' => 'TANGA',
            'ZS' => 'ZANZIBAR',
            'ZW' => 'ZANZIBAR',
            'ZN' => 'ZANZIBAR',
            'PW' => 'MKURANGA',
            'PW' => 'BAGAMOYO',
            'DS' => 'DAR ES SALAAM',
            'AS' => 'ARUSHA',
            'MY' => 'BABATI',
            'KL' => 'MOSHI',
            'MB' => 'MBEYA',
            'RK' => 'MPANDA',
            'MB' => 'RUNGWE',
            'RK' => 'SUMBAWANGA',
            'NJ' => 'MBARALI',
            'IR' => 'IRINGA',
            'NJ' => 'NJOMBE',
            'RV' => 'SONGEA',
            'IR' => 'MAKAMBAKO',
            'SH' => 'BARIADI',
            'KA' => 'KAHAMA',
            'KL' => 'KASULU',
            'KM' => 'KIGOMA',
            'TB' => 'NZEGA',
            'SH' => 'SHINYANGA',
            'TB' => 'TABORA',
            'all_regions' => 'EntireCountry'
        );

        $regionsname = "";

        if (isset($regionsArray[$region_key])) {
            $regionsname = $regionsArray[$region_key];
        }

        return $regionsname;
    }
    
        public function getRegStatusDesc($status) {
        $regstatus = array(
            '0' => 'Pending',
            '1' => 'Sent,Waiting Response',
            '2' => 'success',
            '3' => 'declined',
            '4' => 'fail',
            '5' => 'retry',
            '6' => 'success_cap',
            '7' => 'success_dbms'
        );

        return $regstatus[strtolower($status)];
    }

    private function loadUsers($userType) {
        $em = $this->getDoctrine()->getManager();

        $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();

        $users = $queryBuilder->select('u')
                ->from('RestApiBundle:User', 'u')
                ->where($queryBuilder->expr()->like('u.roles', ':userType'))//('u.type = :userType ')
                ->setParameter('userType', '%' . $userType . '%')
                ->getQuery()
                ->getArrayResult();

        return $users;
    }
    
}
