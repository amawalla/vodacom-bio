<?php

namespace aimgroup\DashboardBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\ProcessBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;

/**
 * Class AgentMobileReportController
 * @package aimgroup\DashboardBundle\Controller
 * @Route("/mreport")
 * @Cache(expires="now", smaxage="0", maxage="0")
 *
 */
class AgentMobileReportController extends Controller {

    public function indexAction($name) {
        return $this->render('', array('name' => $name));
    }







     
    /**
     * agent_summarytrend Summary
     *
     * @Route("/agent_summarytrend/{start_date}/{end_date}/{agent_id}/{timestamp}", name="agent_summarytrend")
     * @Method("GET")
     * @Template()
     */
    public function agent_summarytrendAction($start_date = 1, $end_date = 1, $agent_id = 1, $timestamp) {
        $em = $this->container->get("doctrine")->getManager();
        $where = " WHERE 1=1 ";
        /** @var  $user User */
        
        
        $agent_id = (int)$agent_id;
        $start_date = ($start_date == 1 ? 1 : date('Y-m-d', strtotime($start_date)));
        $end_date = ($end_date == 1 ? 1 : date('Y-m-d', strtotime($end_date)));
        
        
        
        if ($start_date != 1) {
            $where .= " AND date(e.createdDate) >= '" . $start_date . "' AND date(e.createdDate) <= '" . $end_date . "' ";
        }else{
            $where .= " AND date(e.createdDate) >= '" . date('Y-m-d') . "'";
        }

        $where .= " AND a.id = " . $agent_id;

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

      $result = $em->createQuery("SELECT e.image_count, p.temporaryRegStatus as treg, p.fullRegStatus as freg, p.verifyState as vstate, DATE(e.createdDate) as ddate, COUNT(p.id) as ccount "
                        . " from RestApiBundle:RegistrationStatus p "
//. "WHERE date(created_date) >= '2015-09-28' AND date(created_date) <= '2015-10-08' "
                        . " JOIN p.registrationId e"
                        . " JOIN e.owner a" . $where
                        . " GROUP BY ddate, e.image_count, treg, freg, vstate "
                        . " ORDER BY ddate desc")
                ->getArrayResult(); //getSingleResult();


        $data = $data2 = array();

        $total = $declined = $approved = $synched = $pending = $approved_real = $pending_real = 0;

        foreach ($result as $key => $value) {
            $data[$value['ddate']]['total']['count'] = @$data[$value['ddate']]['total']['count'] + $value['ccount'];

            $status_keys = array(
                'success' => 2,
                'declined' => 3,
                'fail' => 4,
                'retry' => 5,
                'success_cap' => 6,
                'success_dbms' => 7,
            );

            //images not received so pending
            if ($value['image_count'] < 3) {
                if (!in_array($value['treg'], array(4, 5, 3))) {
                    $data[$value['ddate']][0]['count'] = @$data[$value['ddate']][0]['count'] + $value['ccount'];
                }
            }


            if (in_array($value['freg'], array(2, 6, 7))) {
                //TREG
                $data[$value['ddate']][1]['count'] = @$data[$value['ddate']][1]['count'] + $value['ccount'];
            } elseif (in_array($value['freg'], array(4, 5, 3))) {
                //FAIL
                $data[$value['ddate']][3]['count'] = @$data[$value['ddate']][3]['count'] + $value['ccount'];
            } else {
                //NOT SENT YET
                if ($value['vstate'] == 1) {
                    $data[$value['ddate']][5]['count'] = @$data[$value['ddate']][5]['count'] + $value['ccount'];
                }
            }

            if (in_array($value['treg'], array(2, 6, 7)) && in_array($value['freg'], array(2, 6, 7))) {
                //FREG
                $data[$value['ddate']][2]['count'] = @$data[$value['ddate']][2]['count'] + $value['ccount'];
            }

            if (in_array($value['treg'], array(2, 6, 7)) && !in_array($value['freg'], array(2, 6, 7))) {// && $value['vstate'] == 1
                //PENDING FREG
                $data[$value['ddate']][4]['count'] = @$data[$value['ddate']][4]['count'] + $value['ccount'];
            }
        }

        foreach ($data as $key4 => $value4) {
            //echo "<pre>"; print_r($value4); echo "</pre>";
            $data2[] = array(
                'date' => $key4,
                'total' => (@$value4['total']['count']) > 0 ? "<a href=''>" + @$value4['total']['count'] + "</a>" : @$value4['total']['count'],
                'pending_info' => (@$value4[0]['count']) ? @$value4[0]['count'] : "0",
                'pending_tobesent' => (@$value4[5]['count']) ? @$value4[5]['count'] : "0",
                'treg' => (@$value4[1]['count']) ? @$value4[1]['count'] : "0",
                'freg' => (@$value4[2]['count']) ? @$value4[2]['count'] : "0",
                'pending_freg' => (@$value4[4]['count']) ? @$value4[4]['count'] : "0",
                'declined' => (@$value4[3]['count']) ? @$value4[3]['count'] : "0"
            );
        }

        echo json_encode($data2);
        exit;
    }
    


  /**
     * @Method("GET")
     * @Route("/latestReg/{id}/{limit}", name="latestReg")
     * @return JsonResponse
     */
    public function latestRegAction($id = 0, $limit = 15) {

        $id = (int)$id;
        $limit = (int)$limit;

        $em = $this->getDoctrine()->getManager();


        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $result = $em->createQuery("SELECT p.id as regId, DATE(p.createdDate) as date, concat(p.firstName, ' ', p.lastName) as customer_name, p.msisdn, s.verifyState, s.fullRegStatus, s.temporaryRegStatus "
                        . " FROM RestApiBundle:RegistrationStatus s "
                        . " JOIN s.registrationId p "
                        . " JOIN p.owner u "
                        . " WHERE u.id = " . $id
                        . " ORDER BY p.id DESC")->setMaxResults($limit)
                ->getArrayResult();

        $data = array();

        foreach($result as $key=>$val){
            $data[] = $val;
        }


        echo json_encode($data);
        exit;

        return $data;
    }




    /**
     * Displays a form to edit an existing User entity.
     *
     * @Cache(expires="now", smaxage="0", maxage="0")
     * @Route("/agent/{id}", name="magent")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:agent_details.html.twig")
     */
    public function agentdetailsAction(Request $request, $id) {


        $em = $this->get("doctrine")->getManager();
        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');



        $count_tfreg = $em->createQuery("SELECT x.language as language FROM RestApiBundle:Registration x"
                        . " WHERE x.agentMsisdn='" . $id . "' "
                        . " ORDER BY x.id DESC "
                )->setMaxResults(1)->getArrayResult();

        $language = $count_tfreg[0]['language'];

        $pagetitle = "Ripoti toka Mwanzo";
        $where = "";

        $data = array("pending" => "0", "approved" => "0", "sent" => "0", "declined" => "0", "delete_req" => "0");

        try {

            switch ($request->get('timefilter')) {

                case "day":
                    $pagetitle = $this->container->get('translator')->trans('agent.mreport.pagetitle.today', array(), 'messages', $language) . date('Y-m-d');
                    $where .= " AND DATE(r.createdDate) >= '" . date("Y-m-d") . "' ";
                    break;
                case "week":
                    $pagetitle = $this->container->get('translator')->trans('agent.mreport.pagetitle.thisweek', array(), 'messages', $language) . date("Y-m-d", strtotime("-1 week")) . " - " . date('Y-m-d');
                    $where .= " AND DATE(r.createdDate) >= '" . date("Y-m-d", strtotime("-1 week")) . "' ";

                    break;
                case "month":
                    $pagetitle = $this->container->get('translator')->trans('agent.mreport.pagetitle.thismonth', array(), 'messages', $language) . date('F Y');
                    $where .= " AND DATE(r.createdDate) >= '" . date("Y-m-d", strtotime("-1 month")) . "' ";

                    break;
                case "lastmonth":
                    $month = date("m", strtotime("-1 month"));
                    $month2 = date("m");
                    $year = date('Y');

                    $pagetitle = "'" . $year . "-" . $month . "-01' AND '" . $year . "-" . $month2 . $this->container->get('translator')->trans('agent.mreport.pagetitle.lastmonth', array(), 'messages', $language) . date("F Y", strtotime("-1 month"));
                    $where .= " AND DATE(r.createdDate) >= '" . $year . "-" . $month . "-01' AND DATE(r.createdDate) < '" . $year . "-" . $month2 . "-01' ";

                    break;
                default:
                    $pagetitle = $this->container->get('translator')->trans('agent.mreport.pagetitle.all', array(), 'messages', $language);
                    $where .= " ";
            }

            $count_tfreg = $em->createQuery("SELECT count(r) as reg_count, x.image_count, r.temporaryRegStatus as treg, r.fullRegStatus as freg FROM RestApiBundle:RegistrationStatus r"
                            . " JOIN r.registrationId x JOIN x.owner a WHERE x.agentMsisdn='" . $id . "' " . $where
                            . " GROUP BY x.image_count, r.temporaryRegStatus, r.fullRegStatus"
                    )->getArrayResult();
        } catch (\Exception $e) {
            $result = null;
        }

        $data['total'] = $data['full_info'] = $data['treg_reg'] = $data['pending_info'] = $data['full_reg'] = $data['declined_reg'] = $data['pending_reg'] = 0;
        if ($count_tfreg != null) {

            $data['full_info'] = $data['pending_info'] = $data['full_reg'] = $data['declined_reg'] = $data['pending_reg'] = 0;
            foreach ($count_tfreg as $key => $val) {

                $data['total'] = $data['total'] + 1;
                //pending
                if ($val['image_count'] >= 3) {
                    $data['full_info'] = $data['full_info'] + $val['reg_count'];
                } else {
                    $data['pending_info'] = $data['pending_info'] + $val['reg_count'];
                }


                if (in_array($val['treg'], array(2, 6, 7))) {
                    //treg
                    $data['treg_reg'] = $data['treg_reg'] + $val['reg_count'];
                } elseif (in_array($val['treg'], array(4, 5, 3))) {
                    //declined
                    $data['declined_reg'] = $data['declined_reg'] + $val['reg_count'];
                } else {
                    //NOT SENT YET
                    $data['pending_reg'] = $data['pending_reg'] + $val['reg_count'];
                }

                if (in_array($val['treg'], array(1, 2, 6, 7)) && in_array($val['freg'], array(2, 6, 7))) {
                    //FREG
                    $data['full_reg'] = $data['full_reg'] + $val['reg_count'];
                }
//                if (in_array($val['treg'], array(1, 2, 6, 7)) && $val['freg'] == 0) {
//                    //PENDING FREG
//                    $data[$val['ddate']][4]['count'] = @$data[$val['ddate']][4]['count'] + $val['ccount'];
//                 }
            }

            $data['full_reg'] = @$data['treg_reg'];
        }

        return array(
            'title' => "Create New Userx",
            'id' => $id,
            'title_descr' => "Create New Agent",
            'agent_id' => $id,
            'report_type' => $pagetitle,
            'reg_data' => $data,
            'timefilter' => $request->get('timefilter'),
            'ttitlependinginfo' => $this->container->get('translator')->trans('agent.mreport.title.pendinginfo', array(), 'messages', $language),
            'ttitlereceived' => $this->container->get('translator')->trans('agent.mreport.title.received', array(), 'messages', $language),
            'ttitlependingsent' => $this->container->get('translator')->trans('agent.mreport.title.pendingsent', array(), 'messages', $language),
            'ttitlesent' => $this->container->get('translator')->trans('agent.mreport.title.sent', array(), 'messages', $language),
            'ttitledeclined' => $this->container->get('translator')->trans('agent.mreport.title.declined', array(), 'messages', $language),
            'subtitlependinginfo' => $this->container->get('translator')->trans('agent.mreport.subtitle.pendinginfo', array(), 'messages', $language),
            'subtitlereceived' => $this->container->get('translator')->trans('agent.mreport.subtitle.received', array(), 'messages', $language),
            'subtitlependingsent' => $this->container->get('translator')->trans('agent.mreport.subtitle.pendingsent', array(), 'messages', $language),
            'subtitlesent' => $this->container->get('translator')->trans('agent.mreport.subtitle.sent', array(), 'messages', $language),
            'subtitledeclined' => $this->container->get('translator')->trans('agent.mreport.subtitle.declined', array(), 'messages', $language),
            'menutoday' => $this->container->get('translator')->trans('agent.menu.today', array(), 'messages', $language),
            'menuthisweek' => $this->container->get('translator')->trans('agent.menu.thisweek', array(), 'messages', $language),
            'menuthismonth' => $this->container->get('translator')->trans('agent.menu.thismonth', array(), 'messages', $language),
            'menulastmonth' => $this->container->get('translator')->trans('agent.menu.lastmonth', array(), 'messages', $language),
            'menutotal' => $this->container->get('translator')->trans('agent.menu.all', array(), 'messages', $language),
        );
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/agentReg/{id}", name="agentReg")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:agent_registration_details.html.twig")
     */
    public function agentRegistrationdetailsAction(Request $request, $id) {
        //declined  submitted   waitingSubmission   received   pending

        $pagetitle = "Orodha ya Usajili : ";
        $where = "";

        $data = array("pending" => "0", "approved" => "0", "sent" => "0", "declined" => "0", "delete_req" => "0");
        $em = $this->get("doctrine")->getManager();
        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        try {

            switch ($request->get('statusfilter')) {
                case "declined":
                    $pagetitle .= " Zilizokataliwa ";
                    $where .= " AND r.temporaryRegStatus IN (4,5,3) ";
                    break;
                case "submitted":
                    $pagetitle .= " Zillizotumwa ";
                    $where .= " AND r.temporaryRegStatus IN (1,2,6,7)";
                    break;
                case "waitingSubmission":
                    $pagetitle .= " Bado Hazijatumwa ";
                    $where .= " AND r.temporaryRegStatus IN (0,1) ";
                    break;
                case "received":
                    $pagetitle .= " Zilizopokelewa ";
                    $where .= " AND x.image_count >= 3 ";
                    break;
                default:
                    $pagetitle .= " Zinazosubiri ";
                    $where .= " AND x.image_count < 3 ";
            }


            switch ($request->get('timefilter')) {

                case "day":
                    $where .= " AND DATE(r.createdDate) >= '" . date("Y-m-d") . "' ";
                    break;
                case "week":
                    $where .= " AND DATE(r.createdDate) >= '" . date("Y-m-d", strtotime("-1 week")) . "' ";

                    break;
                case "month":
                    $where .= " AND DATE(r.createdDate) >= '" . date("Y-m-d", strtotime("-1 month")) . "' ";

                    break;
                case "lastmonth":
                    $month = date("m", strtotime("-1 month"));
                    $month2 = date("m");
                    $year = date('Y');

                    $where .= " AND DATE(r.createdDate) >= '" . $year . "-" . $month . "-01' AND DATE(r.createdDate) < '" . $year . "-" . $month2 . "-01' ";

                    break;
                default:
                    $where .= " ";
            }
            ##########$where .= " AND DATE(r.createdDate) >= '" . date("Y-m-d", strtotime("-2 DAY")) . "' ";

            $count_tfreg = $em->createQuery("SELECT x.msisdn, x.image_count, CONCAT(x.firstName, ' ', x.lastName) as customerName, x.identificationType as idtype, x.identification as idnumber, x.nationality, x.registrationid as regid, r.temporaryRegStatus as treg, r.fullRegStatus as freg FROM RestApiBundle:RegistrationStatus r"
                            . " JOIN r.registrationId x JOIN x.owner a WHERE x.agentMsisdn='" . $id . "' " . $where
                            . " ORDER BY r.id DESC "
                    )->getArrayResult();
        } catch (\Exception $e) {
            $result = null;
        }

        if ($count_tfreg != null) {
            foreach ($count_tfreg as $key => $val) {
                //potrait  front-pic  rear-pic signature
                $count_tfreg[$key]['potrait'] = "";
                $count_tfreg[$key]['frontpic'] = "";
                $count_tfreg[$key]['rearpic'] = "";
                $count_tfreg[$key]['signature'] = "";

                $imageReg = $em->createQuery("SELECT i.webPath, i.imageType"
                                . " FROM RestApiBundle:RegImages i"
                                . " WHERE i.registration='" . $val['regid'] . "' "
                                . " ORDER BY i.id DESC "
                        )->getArrayResult();

                if ($imageReg != null) {
                    foreach ($imageReg as $xkey => $xval) {
                        $count_tfreg[$key][str_replace("-", "", $xval['imageType'])] = $xval['webPath'];
                    }
                }
            }
        }

        return array(
            'title' => "Create New Userx",
            'title_descr' => "Create New Agent",
            'agent_id' => $id,
            'report_type' => $pagetitle,
            'entities' => $count_tfreg,
            'isAdministrator' => FALSE,
        );
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/help", name="mHelp")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:agent_details.html.twig")
     */
    public function agentHelpAction() {
        $data = array("pending" => "0", "approved" => "0", "sent" => "0", "declined" => "0", "delete_req" => "0");
        return array(
            'title' => "Create New Userx",
            'title_descr' => "Create New Agent",
            'reg_data' => $data,
        );
    }

    /**
     * Android App download Page
     *
     * @Route("/app_download", name="app_download")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:app_download.html.twig")
     */
    public function app_downloadAction() {


        $em = $this->getDoctrine()->getManager();
        $emConfig = $em->getConfiguration();

        $query = $em->createQueryBuilder()
                ->select("p")
                ->from("RestApiBundle:Role", "p")
                ->getQuery();

        try {
            $results = $query->getArrayResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }



        return array(
            'title' => "View User Roles Listing:",
            'title_descr' => "Listing for user permission roles departments ",
            'user_roles' => $results,
                //'entities' => $entities,
        );
    }

    /**
     * Displays a form to create a new Pass entity.
     *
     * @Route("/set_forgot_password/{code}", name="set_forgot_password")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:set_forgot_password.html.twig")
     */
    public function set_forgot_passwordAction($code) {
        $em = $this->getDoctrine()->getManager();
        $result = $em->createQuery("SELECT p "
                        . "from DashboardBundle:SecurityCodes p "
                        . " where p.actionCode = " . $code)
                ->getOneOrNullResult();

        if (!$result) {
            return $this->redirect($this->generateUrl('admin'));
        }

        return array(
            'title' => "Create New FAQ",
            'code' => $code,
            'title_descr' => "Create New Frequently Asked Questions",
            'error' => 'fucktard'
        );
    }

    /**
     * Creates a new Faq entity.
     *
     * @Route("/set_forgot_password_post/{code}", name="set_forgot_password_post")
     * @Method("POST")
     * @Template("DashboardBundle:Faq:new.html.twig")
     */
    public function set_forgot_password_postAction($code, Request $request) {

        $attributes = json_decode($request->getContent(), true);

        $em = $this->getDoctrine()->getManager();
        $result = $em->createQuery("SELECT p "
                        . "from DashboardBundle:SecurityCodes p "
                        . " where p.actionCode = " . $code)
                ->getOneOrNullResult();

        if (!$result) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $password = $request->request->get('password');
        $user = $em->getRepository('RestApiBundle:User')->find($result->getUserId());

        try {

            if ($user) {
                //if device exists update user
                /** @var  $user User */
                //$user = $this->get('security.token_storage')->getToken()->getUser();
                $encoder = $this->container->get('security.password_encoder');

                $user->setPassword($password);
                $encoder = $this->container->get('security.password_encoder');
                $encoded = $encoder->encodePassword($user, $user->getPassword());
                $user->setPassword($encoded);

                $em->flush();
            } else {
                echo "patee user";
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }

        return $this->redirect($this->generateUrl('admin'));
        exit;

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Faq entity.
     *
     * @Route("/forgot_password", name="forgot_password")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:forgot_password.html.twig")
     */
    public function forgot_passwordAction() {

        return array(
            'title' => "Create New FAQ",
            'title_descr' => "Create New Frequently Asked Questions",
            'error' => 'fucktard'
        );
    }

    /**
     * Displays a form to create a new Faq entity.
     *
     * @Route("/sendforgot_password/{email}/{timestamp}", name="sendforgot_password")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:forgot_password.html.twig")
     */
    public function sendforgot_passwordAction($email, $timestamp) {

        $req['email'] = $email;
        $data['status'] = 1;
        $data['msg'] = "SENDING EMAIL FAIL";

        $code = random_int(123456789012345, 5432109876543210);


        $em = $this->getDoctrine()->getManager();
        $result = $em->createQuery("SELECT p "
                        . "from RestApiBundle:User p "
                        . " where p.email = '" . $email . "'")
                ->getOneOrNullResult();

        if (!$result) {
            $data['msg'] = "User Doesnt Exist. Contact your Admin";
            echo json_encode($data);
            exit;
        }



        $em = $this->getDoctrine()->getManager();
        $query1 = "INSERT INTO `SecurityCodes` (action_name, action_code, email_address,user_id) VALUES ('reset_pass', '" . $code . "', '" . $email . "', '" . $result->getId() . "');";
        $em->getConnection()->exec($query1);

        $this->sendmailAction($email, $code);
        $data['msg'] = "SENDING EMAIL SUCCEEDED";

        echo json_encode($data);
        exit;


        return array(
            'title' => "Create New FAQ",
            'title_descr' => "Create New Frequently Asked Questions",
            'error' => 'fucktard'
        );
    }

    function sendmailAction($to, $code) {
        $url = "http://vodacom.registersim.com/api/anto_send_email";
        $data = array(
            'to' => $to,
            'title' => 'eKYC Password Reset',
            'message' => $this->renderView('DashboardBundle:Admin:resetPassEmail.html.twig', array('ccode' => $code, 'base_url' => $this->get('request')->getSchemeAndHttpHost()))
        );
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($ch);
        curl_close($ch);
    }

    public function curl_to_emailAPI($email, $code) { //echo "Email: ".$email."   Code: " . $code;
        $message = \Swift_Message::newInstance()
                ->setSubject('eKYC Password Reset')
                ->setFrom('eams.notify@gmail.com')
                ->setTo($email)
                ->setBody(
                $this->renderView(
                        // app/Resources/views/Emails/registration.html.twig
                        'DashboardBundle:Admin:resetPassEmail.html.twig', array('ccode' => $code)
                ), 'text/html'
                )
        /*
         * If you also want to include a plaintext version of the message
          ->addPart(
          $this->renderView(
          'Emails/registration.txt.twig',
          array('name' => $name)
          ),
          'text/plain'
          )
         */
        ;
        $this->get('mailer')->send($message);

        return array(
            'title' => "Create New FAQ",
            'title_descr' => "Create New Frequently Asked Questions",
            'error' => 'fucktard'
        );
    }

    /**
     * View resetrecordlock
     *
     * @Route("/newresetrecordlock/{type}/{timestamp}", name="newresetrecordlock")
     * @Method("GET")
     * @Template()
     */
    public function newresetrecordlockAction($type, $timestamp) {
        $em = $this->getDoctrine()->getManager();

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        if ($type == "verify") {

            $query1 = "UPDATE RegistrationStatus SET verifyState = (verifyState * -1) WHERE verifyState < -1 AND verifyLock IS NOT NULL AND verifyDate > (CURDATE() - INTERVAL 1 DAY) AND verifyDate < (CURRENT_TIMESTAMP - INTERVAL 5 MINUTE)";
            $query2 = "UPDATE RegistrationStatus SET verifyState = 0 WHERE verifyState = -1 AND verifyLock IS NOT NULL AND verifyDate > (CURDATE() - INTERVAL 1 DAY) AND verifyDate < (CURRENT_TIMESTAMP - INTERVAL 5 MINUTE)";


            $em->getConnection()->exec($query1);

            $em->getConnection()->exec($query2);

            #$query3 = "update RegistrationStatus left join registration on registration.id = RegistrationStatus.registrationId set allimageDate = '2016-11-16 16:16:16' where date(registration.createdDate ) >= '2016-11-04' and allimageDate IS NULL and image_count >= 4;";
            #$em->getConnection()->exec($query3);

            echo "Done";
        } else {
            $data2 = $em->createQuery("SELECT r.id, r." . $type . "State "
                            . " from RestApiBundle:RegistrationStatus r "
                            . " WHERE DATE(r." . $type . "Date) <= '" . date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s')) - 600) . "' AND r." . $type . "State = '-1'")
                    ->setMaxResults(1000)
                    ->getArrayResult(); //getSingleResult();

            $num = 1;

            echo "<h4>Count:: " . count($data2) . "</h4>";
            foreach ($data2 as $key => $value) {
                echo "<pre> " . $num . ". ";
                print_r($value);
                echo "</pre>";

                if ($type == "audit") {
                    $results = $em->createQuery("UPDATE RestApiBundle:RegistrationStatus u SET u.auditState = 0, u.auditBy = NULL, u.auditDescr = '', u.auditDate = '0000-00-00 00:00:00' WHERE u.id = " . $value['id'] . " ")
                            ->getArrayResult();
                }
                if ($type == "verify") {
                    $results = $em->createQuery("UPDATE RestApiBundle:RegistrationStatus u SET u.verifyState = 0, u.verifyBy = NULL, u.verifyDescr = '', u.verifyDate = '0000-00-00 00:00:00' WHERE u.id = " . $value['id'] . " ")
                            ->getArrayResult();
                }

                $num++;
            }
        }
        exit;

        $data = array(
            'title' => "View Reports for:",
            'title_descr' => "View Reports for",
                //'entities' => $entities,
        );

        return $this->prepareResponse($data);
    }

    /**
     * Displays a form to edit an existing User entity.
     * @Cache(expires="now", smaxage="0", maxage="0")
     * @Route("/killextra/{id}", name="killextra")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:agent_details.html.twig")
     */
    public function killextraAction($id) {
        $start = strtotime(date('Y-m-d H:i:s'));
        $sql = "show full processlist;";
        $end = strtotime(date('Y-m-d H:i:s'));

        echo " TimeTaken: " . $end - $start;

        $em = $this->getDoctrine()->getManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $processToDelete = array();
        $num = 1;
        echo "<table style='border:1px solid #eee; font-size:11px;'>";
        foreach ($result as $key => $val) {
            if ($num == 1) {
                echo "<thead><tr>";
                foreach ($val as $hk => $hv) {
                    echo "<th style='font-weight:bold; background: #eee;'>" . $hk . "</th>";
                }
                echo "</tr></thead><tbody>";
            }
            echo "<tr>";
            foreach ($val as $vk => $vv) {
                echo "<td style='border: 1px solid #eee;'>" . $vv . "</td>";
            }
            echo "</tr>";

            if ($val['Time'] > 50 && 'root' == $val['user']) {
                $processToDelete[] = $val;

                $sql = "kill " . $val['Id'] . ";";
                $stmt = $em->getConnection()->prepare($sql);
                $stmt->execute();
                //$result = $stmt->fetchAll();

                echo $sql;
            }
            $num++;
        }
        echo "</tbody></table>";

        echo "<pre>";
        print_r($processToDelete);
        echo "</pre>";

        exit;
        return array(
            'title' => "Create New FAQ",
            'title_descr' => "Create New Frequently Asked Questions",
            'error' => 'fucktard'
        );
    }

    /**
     * Save configurations
     *
     * @Route("/apipostPushMQQT/{deviceId}", name="apipostPushMQQT")
     * @Method("GET")
     * @Template("DashboardBundle:SystemsConfigurations:partners.html.twig")
     */
    public function apipostPushMQQTAction($deviceId) {


        /*
          {"name":"TIGO","prefix":"[6-7][0-9]{1}[0-9]{1}[0-9]{6}","configUrl":"https://tigolive.registersim.com/api/configuration/getEndpoints.json","tokenAuthUrl":"https://tigolive.registersim.com/api/user/verifyToken.json","passwordSetUrl":"https://tigolive.registersim.com/api/user/saveUserPassword.json","signatureSaveUrl":"https://tigolive.registersim.com/api/registration/signatures/upload.json","iccidCallback":"https://tigolive.registersim.com/api/dynamic/get_numbers/","reportUrl":"https://tigolive.registersim.com/mreport/agent/","simSerialPrefix":"892550204","gpsStatus":"1","promptRegType":"1","deviceId":"1490108501170","type":"new_operator"}
         */
        //migrate all to operatorId


        $requestJson = json_decode('{"name":"TIGO","prefix":"[6-7][0-9]{1}[0-9]{1}[0-9]{6}","configUrl":"https://tigolive.registersim.com/api/configuration/getEndpoints.json","tokenAuthUrl":"https://tigolive.registersim.com/api/user/verifyToken.json","passwordSetUrl":"https://tigolive.registersim.com/api/user/saveUserPassword.json","signatureSaveUrl":"https://tigolive.registersim.com/api/registration/signatures/upload.json","iccidCallback":"https://tigolive.registersim.com/api/dynamic/get_numbers/","reportUrl":"https://tigolive.registersim.com/mreport/agent/","simSerialPrefix":"892550204","gpsStatus":"1","promptRegType":"1","type":"new_operator"}', true);

        $requestJson["operator"] = $this->container->getParameter("operator_id");
        $requestJson["operatorId"] = $this->container->getParameter("operator_id");
        $requestJson["id"] = $this->container->getParameter("operator_id");
        $requestJson["operatorName"] = $this->container->getParameter("operator_name");
        $deviceId = $deviceId;
        $topic = $requestJson["type"];

        unset($requestJson["type"]);

        //$requestJson['prefix'] = "[6-7][0-9]{1}[0-9]{1}[0-9]{6}";

        echo "<pre>";
        print_r($requestJson);
        echo "</pre>";

        $respond = $this->get("api.helper")->publish($deviceId, $topic, json_encode($requestJson));

        echo "Sent message to :: " . $deviceId;

        exit;

        $data = array(
            'title' => "Partners Configurations",
            'title_descr' => "List, create, delete, activate System Admins",
            'partners' => $partners
        );
        return $this->prepareResponse($data);
    }

    /**
     * Save configurations
     *
     * @Route("/apiEmailWorkAround/{email}/{title}/{filename}", name="apiEmailWorkAround")
     * @Method("GET")
     * @Template("DashboardBundle:SystemsConfigurations:partners.html.twig")
     */
    public function apiEmailWorkAroundAction($email = "", $title = "", $filename = "") {

        /* echo " here 1 ";
          $email = "mutisya06@gmail.com";
          $title = "TITLEtoREPORT:: ";
          $heading = "HEADINGtothe REPORT HEADING:: ";
          //execute bash script
          $output->writeln('starting');
          $script = $this->getContainer()->getParameter("base_path") . "/scripts/sendmail.sh";
          $output->writeln("path of script-" . $script);

          echo " here 2 ";

          exec($script  . "\" \"" . $email . "\" \"" . $title. "\" > /dev/null &");

          echo " here 3 ";
         */


        echo "email: " . $email . " title: " . $title;




        try {

            echo " here 1.TRY ";
            $kernel = $this->get('kernel');
            $application = new Application($kernel);
            $application->setAutoExit(false);
            $input = new ArrayInput(array('command' => 'dashboard:report_workaround_command', 'email' => str_replace("__", "@", $email), 'date' => date('Ymd'), 'filename' => $filename, 'title' => 'TESTTITTLE-'));
            $application->run($input, null);
            echo " here 2.SENT! ";
        } catch (Exception $e) {
            echo " here 1.CATCH ";
            echo $e;
            return null;
        }

        exit;

        return array();
    }

    /**
     * trendlatest_summary Summary
     *
     * @Route("/appappsummary/{start_date}/{end_date}/{timestamp}", name="appappsummary")
     * @Method("GET")
     * @Template()
     */
    public function appappsummaryAction($start_date = "", $end_date = "", $timestamp = "") {
        $start_date = date('Ymd') . "000000";
        $end_date = date('Ymd') . "235959";

        $em = $this->container->get("doctrine")->getManager();
        $where = " WHERE 1=1 ";
        /** @var  $user User */
        $query1 = "SELECT r0_.image_count AS image_count, r1_.temporaryRegStatus AS treg, r1_.mpesa_state as mpesaState, r1_.fullRegStatus AS freg, r1_.verifyState AS vstate, DATE(r0_.createdDate) AS ddate, COUNT(r1_.id) AS ccount FROM RegistrationStatus r1_ INNER JOIN registration r0_ ON r1_.registrationId = r0_.id INNER JOIN user u2_ ON r0_.owner_id = u2_.id WHERE 1 = 1 AND r0_.date_generated_createdDate >= '" . $start_date . "' AND r0_.date_generated_createdDate <= '" . $end_date . "' GROUP BY ddate, image_count, treg, mpesaState, freg, vstate ORDER BY ddate DESC;";


        $connection = $em->getConnection();
        $statement = $connection->prepare($query1);
        $statement->execute();
        $result = $statement->fetchAll();

        $data = $data2 = array();


//        foreach ($result as $key => $value) {
//            $data[$value['ddate']][$value['state']]['count'] = @$data[$value['ddate']][$value['state']]['count'] + $value['ccount'];
//            $data[$value['ddate']]['total']['count'] = @$data[$value['ddate']]['total']['count'] + $value['ccount'];
//        }

        $total = $declined = $approved = $synched = $pending = $approved_real = $pending_real = 0;
//assigned_entries|approved_entries|declined_entries|delete_request|synched_entries|tickets_assigned|tickets_actioned|tickets_pending	tickets_closed

        foreach ($result as $key => $value) {
            $data[$value['ddate']]['total']['count'] = @$data[$value['ddate']]['total']['count'] + $value['ccount'];

            $status_keys = array(
                'success' => 2,
                'declined' => 3,
                'fail' => 4,
                'retry' => 5,
                'success_cap' => 6,
                'success_dbms' => 7,
            );

            //images not received so pending
            if ($value['image_count'] < 3) {
                if (!in_array($value['treg'], array(4, 5, 3))) {
                    $data[$value['ddate']][0]['count'] = @$data[$value['ddate']][0]['count'] + $value['ccount'];
                }
            }


            if (in_array($value['freg'], array(2, 6, 7))) {
                //TREG
                $data[$value['ddate']][1]['count'] = @$data[$value['ddate']][1]['count'] + $value['ccount'];
            } elseif (in_array($value['freg'], array(4, 5, 3))) {
                //FAIL
                $data[$value['ddate']][3]['count'] = @$data[$value['ddate']][3]['count'] + $value['ccount'];
            } else {
                //NOT SENT YET
                if ($value['vstate'] == 1) {
                    $data[$value['ddate']][5]['count'] = @$data[$value['ddate']][5]['count'] + $value['ccount'];
                }
            }

            if ($value['vstate'] == 1) {
                if ($value['mpesaState'] == 0 || $value['mpesaState'] == '-1') {
                    $data[$value['ddate']][77]['count'] = @$data[$value['ddate']][77]['count'] + $value['ccount'];
                }
            }

            if (in_array($value['treg'], array(2, 6, 7)) && in_array($value['freg'], array(2, 6, 7))) {
                //FREG
                $data[$value['ddate']][2]['count'] = @$data[$value['ddate']][2]['count'] + $value['ccount'];
            }

            if (in_array($value['treg'], array(2, 6, 7)) && !in_array($value['freg'], array(2, 6, 7))) {// && $value['vstate'] == 1
                //PENDING FREG
                $data[$value['ddate']][4]['count'] = @$data[$value['ddate']][4]['count'] + $value['ccount'];
            }

            //if (in_array($value['treg'], array(0, 2, 6)) && $value['image_count'] >= 4 && in_array($value['vstate'], array(0, 1, 2, 3, '-1', '-2', '-3'))) {
            if (true) {
                if (in_array($value['vstate'], array(1, 2, 3, '-2', '-3'))) {
                    $data[$value['ddate']][30]['count'] = @$data[$value['ddate']][30]['count'] + $value['ccount'];

                    if ($value['vstate'] == 1) {
                        //vgood
                        $data[$value['ddate']][32]['count'] = @$data[$value['ddate']][32]['count'] + $value['ccount'];
                    } elseif ($value['vstate'] == 2 || $value['vstate'] == '-2') {
                        //vfair
                        $data[$value['ddate']][33]['count'] = @$data[$value['ddate']][33]['count'] + $value['ccount'];
                    } elseif ($value['vstate'] == 3 || $value['vstate'] == '-3') {
                        //vbad
                        $data[$value['ddate']][34]['count'] = @$data[$value['ddate']][34]['count'] + $value['ccount'];
                    }
                } else {
                    //notverified
                    // echo "<br />DATE: ".$value['ddate']." TREG: ".$value['treg'] ." IMAGECNT: ".$value['image_count']."VSTATE" . $value['vstate'] . "COUNT: ".$value['ccount']."<br />";
                    if (/* in_array($value['treg'], array(6, 7)) && */ $value['image_count'] >= 4 && in_array($value['vstate'], array(0))) {
                        $data[$value['ddate']][31]['count'] = @$data[$value['ddate']][31]['count'] + $value['ccount'];
                    }
                }
            }
        }




        $icapSql = "select count(id) as ccount,  hour(createdDate ) as hhour, verifyState , verifyDate, fregDate , id, TIMESTAMPDIFF(MINUTE, RegistrationStatus.verifyDate , fregDate) as diff, CASE WHEN TIMESTAMPDIFF(MINUTE, RegistrationStatus.verifyDate , fregDate) <= 5 THEN 'lessThan5' ELSE 'moreThan5' END as diff2, temporaryRegStatus from RegistrationStatus where verifyState = 1 and temporaryRegStatus not in (0,1) and createdDate >= '" . date('Y-m-d') . " 00:00:00' group by hhour, diff2;";
        $mpesaSql = "select count(id) as ccount,  hour(createdDate ) as hhour, verifyState , verifyDate, fregDate , id, TIMESTAMPDIFF(MINUTE, RegistrationStatus.verifyDate , mpesaDate) as diff, CASE WHEN TIMESTAMPDIFF(MINUTE, RegistrationStatus.verifyDate , mpesaDate) <= 5 THEN 'lessThan5' ELSE 'moreThan5' END as diff2, temporaryRegStatus from RegistrationStatus where verifyState = 1 and temporaryRegStatus not in (0,1) and createdDate >= '" . date('Y-m-d') . " 00:00:00' group by hhour, diff2;";


        $connection = $em->getConnection();
        $statement = $connection->prepare($icapSql);
        $statement->execute();
        $icapResult = $statement->fetchAll();
        
        $connection = $em->getConnection();
        $statement = $connection->prepare($mpesaSql);
        $statement->execute();
        $mpesaResult = $statement->fetchAll();

        $ihour = $mhour = array();
        
        
        
        /*
        +--------+-------+-----------+
        | ccount | hhour | diff2     |
        +--------+-------+-----------+
        |      4 |     0 | lessThan5 |
        |      1 |     5 | lessThan5 |
        |     20 |     6 | lessThan5 |
        |    215 |     7 | lessThan5 |
        |    729 |     8 | lessThan5 |
        |      6 |     8 | moreThan5 |
        |   1398 |     9 | lessThan5 |
        |      2 |     9 | moreThan5 |
        |   1950 |    10 | lessThan5 |
        |      4 |    10 | moreThan5 |
        |   2271 |    11 | lessThan5 |
        |     13 |    11 | moreThan5 |
        |   2269 |    12 | lessThan5 |
        |      4 |    12 | moreThan5 |
        |   1983 |    13 | lessThan5 |
        |      4 |    13 | moreThan5 |
        |   1867 |    14 | lessThan5 |
        |      9 |    14 | moreThan5 |
        |    990 |    15 | lessThan5 |
        |     48 |    15 | moreThan5 |


        12Array
        (
            [total] => 2273
            [less5] => 2269
            [more5] => 4
        )
        */
        
        
        foreach ($mpesaResult as $mk => $mv) {
            
            $mhour[$mv['hhour']]['total'] = @$mhour[$mv['hhour']]['total'] + $mv['ccount'];
            if($mv['diff2'] == 'lessThan5'){
                $mhour[$mv['hhour']]['less5'] = @$mhour[$mv['hhour']]['less5'] + $mv['ccount'];
            }else{
                $mhour[$mv['hhour']]['more5'] = @$mhour[$mv['hhour']]['more5'] + $mv['ccount'];
            }   
        }
        
        $mpesa_array = $icap_array = array();
        /*for($i=0;$i<24;$i++){
            $mhour[$i]['perc'] = floor((@$mhour[$i]['less5'] / @$mhour[$i]['total'] ) * 100 );
            
            $mpesa_array[] = $mhour[$i]['perc'];
        }*/
        
        
        $mp_defaul = $mp_val = $mp_key = array();
        foreach($mhour as $ok => $ov){
            $ov['perc'] = floor((@$ov['less5'] / @$ov['total'] ) * 100 );
            
            //echo "<b>" . json_encode($ov) . "</b><br />";
            $mp_defaul[] = 95;
            $mp_key[] = $ok;
            $mp_val[] = floor((@$ov['less5'] / @$ov['total'] ) * 100 );
        }
        
        //echo json_encode($mp_defaul) . "<br />";
        //echo json_encode($mp_val) . "<br />";
        //echo json_encode($mp_key) . "<br />";
        
        
        
        
        
        foreach ($icapResult as $ck => $cv) {
            
            $chour[$cv['hhour']]['total'] = @$chour[$cv['hhour']]['total'] + $cv['ccount'];
            $chour[$cv['hhour']]['hour'] = $cv['hhour'];
            
            if($cv['diff2'] == 'lessThan5'){
                $chour[$cv['hhour']]['less5'] = @$chour[$cv['hhour']]['less5'] + $cv['ccount'];
            }else{
                $chour[$cv['hhour']]['more5'] = @$chour[$cv['hhour']]['more5'] + $cv['ccount'];
            }   
        }
        
        //echo "<pre>";
        //print_r($chour);
        //echo "</pre>";
        
        $ic_defaul = $ic_val = $ic_key = array();
        foreach($chour as $ok => $ov){
            $ov['perc'] = floor((@$ov['less5'] / @$ov['total'] ) * 100 );
            
            //echo "<b>" . json_encode($ov) . "</b><br />";
            $ic_defaul[] = 95;
            $ic_key[] = $ok;
            $ic_val[] = floor((@$ov['less5'] / @$ov['total'] ) * 100 );
        }
        
        //echo json_encode($ic_defaul) . "<br />";
        //echo json_encode($ic_val) . "<br />";
        //echo json_encode($ic_key) . "<br />";
        

        $data2 = array();
        foreach ($data as $key4 => $value4) {
            //echo "<pre>"; print_r($value4); echo "</pre>";
            $data2 = array(
                'Time' => date("H:i:s"),
                'Total Reg' => (@$value4['total']['count']) > 0 ? "<a href=''>" + @$value4['total']['count'] + "</a>" : @$value4['total']['count'],
                'Pending Info' => (@$value4[0]['count']) ? @$value4[0]['count'] : "0",
                'Pending toBe Sent' => (@$value4[5]['count']) ? @$value4[5]['count'] : "0",
                //'treg' => (@$value4[1]['count']) ? @$value4[1]['count'] : "0",
                'MpesaQueue**' => (@$value4[77]['count']) ? @$value4[77]['count'] : "0",
                'Sent ' => (@$value4[2]['count']) ? @$value4[2]['count'] : "0",
                'Verified' => (@$value4[30]['count']) ? @$value4[30]['count'] : "0",
                'Not Verified' => (@$value4[31]['count']) ? @$value4[31]['count'] : "0",
                'Good' => (@$value4[32]['count']) ? @$value4[32]['count'] : "0",
                'Fair' => (@$value4[33]['count']) ? @$value4[33]['count'] : "0",
                'Bad' => (@$value4[34]['count']) ? @$value4[34]['count'] : "0",
                //'pending_freg' => (@$value4[4]['count']) ? @$value4[4]['count'] : "0",
                'Declined' => (@$value4[3]['count']) ? @$value4[3]['count'] : "0",
                
                'mp_defaul' => $mp_defaul,
                'mp_val' => $mp_val,
                'mp_key' => $mp_key,
                
                'ic_defaul' => $ic_defaul,
                'ic_val' => $ic_val,
                'ic_key' => $ic_key,
                
                'ICAP_KPI' => $icap_array,
                'Mpesa_KPI' => $mpesa_array//array(110, 94, 92, 98, 84, 86, 98, 82, 96, 99, 98, 95, 90, 90, 92, 97, 96, 94, 100, 99, 100, 83, 84, 95)
            );
        }

        foreach ($data2 as $kk => $vv) {
            $ress[] = array('itemName' => $kk, 'itemVal' => $vv);
        }
        echo json_encode($ress);
        exit;
    }

}
