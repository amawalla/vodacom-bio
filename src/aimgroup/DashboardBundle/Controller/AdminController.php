<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\DashboardBundle\Controller;

use aimgroup\DashboardBundle\Dao\JsonObject;
use aimgroup\DashboardBundle\Dao\JTableResponse;
use aimgroup\RestApiBundle\Entity\Role;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use aimgroup\RestApiBundle\Entity\User;
use aimgroup\RestApiBundle\Entity\Registration;
use aimgroup\DashboardBundle\Form\UserType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use aimgroup\RestApiBundle\Form\RoleType;
use Symfony\Component\HttpFoundation\Session\Session;
use aimgroup\DashboardBundle\Dao\SearchDao;
use aimgroup\DashboardBundle\Entity\SearchResult;
use aimgroup\DashboardBundle\Entity\UserLog;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use aimgroup\RestApiBundle\Dao\Message;
use aimgroup\DashboardBundle\Util\RegistrationUtil;

//use DashboardBundle\Form\FaqType;

/**
 * AdminController controller.
 *
 * @Route("/admin")
 */
class AdminController extends AbstractController {

    var $session;

    const REREG = 1;
    const NEWREG = 0;
    const DECLINED = 2;
    const MPESAPENDING = 0;
    const MPESASENT = 1;
    const MPESAFAILED = 2;
    const MPESAALREADYREGISTERED = 3;
    const DBMSPENDING = 0;
    const DBMSFAILED = 4;
    const DBMSSENT = 6;
    const DBMSSENT2 = 7;

    public function __construct() {
        $this->session = new Session();
    }

    /**
     * Lists all Faq entities.
     *
     * @Route("/", name="admin")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $this->session->set('user_role_perms', json_encode($this->get_department_perms_arrayAction()));


        /*
          //---- Session Games Start

          $session = new Session();
          //        $session->start();
          // set and get session attributes
          $session->set('name', 'Drak');
          $session->get('name');

          // set flash messages
          $session->getFlashBag()->add('notice', 'Profile updated');

          // retrieve messages
          foreach ($session->getFlashBag()->get('notice', array()) as $message) {
          echo '<div class="flash-notice"> ' . $session->get('name') . ' | ' . $message . '</div>';
          }

          $em = $this->getDoctrine()->getManager();

          $entity = $em->getRepository('RestApiBundle:Role')->find($id);
          $entity_perms = $entity->getDeptPerms();

          //---- Session Games End
         */
        //$entities = $em->getRepository('DashboardBundle:Admin')->findAll();

        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array('ROLE_VERIFYER', $roles)) {
            return $this->redirect($this->generateUrl('admin') . 'newverification');
        }
        if (in_array('ROLE_VERIFYER_SUPERVISOR', $roles)) {
            return $this->redirect($this->generateUrl('verifyer'));
        }
        if (in_array('ROLE_EDITOR_VERIFYER', $roles)) {
            return $this->redirect($this->generateUrl('editnewverification'));
        }

        $data = array(
            'title'       => 'View Summary',
            'title_descr' => 'Dashboard',
            'akuku'       => $this->getIcapRegStatusDesc(4, 0) . ' :: ' . json_encode($user->getRoles()) . ' | ' . $this->session->get('user_role_perms'), //$this->get('request')->getSchemeAndHttpHost() . " | " . $this->container->getParameter("hostname"), //json_encode($user->getRoles()) . " | " . $this->session->get('user_role_perms'),
            'date_today'  => date('Y-m-d'),
            'date_start'  => date_format(date_sub(date_create(date('Y-m-d')), date_interval_create_from_date_string('10 days')), 'Y-m-d')
        );

        return $this->prepareResponse($data);
    }

    /**
     *
     * @Route("/change_language/{changeToLocale}", name="change_language")
     * @Method("GET")
     */
    public function changeLanguageAction($changeToLocale) {

//var_dump($request->request);
//$changeToLocale = $request->request->get('loc');
        $this->get('request')->attributes->set('_locale', null);
        $this->get('session')->set('_locale', $changeToLocale);

        $this->get('request')->setLocale($changeToLocale);

//var_dump($changeToLocale);
//var_dump($this->get('request')->getLocale());

        return $this->redirect($this->generateUrl('admin'));
    }

    /**
     * Registration Summary
     *
     * @Route("/anto_test", name="registration_summary")
     * @Method("GET")
     * @Template()
     */
    public function registration_summaryAction() {
        $em = $this->getDoctrine()->getManager();

        /*
          SELECT count(sreg_dashboard_registrations.id) as count, `state` FROM (`sreg_dashboard_registrations`) GROUP BY `state`
         */

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');

        $result = $em->createQuery('SELECT COUNT(p.id) as ccount, p.state '
            . 'FROM RestApiBundle:Registration p '
            . 'GROUP BY p.state')
            ->getArrayResult();

        $data = array();

//PURGE DATA IS TO BE FETCHED FROM purge table that hosts initially archived data!! nice idea huh ;)
        $purge[ 'approved' ] = $purge[ 'pending' ] = $purge[ 'declined' ] = $purge[ 'total' ] = 0;

        $data[ 'filter' ] = //$this->input->post('filter'); $filter;
        $data[ 'start' ] = '2015-01-01'; //$this->input->post('start'); $start;
        $data[ 'end' ] = '2015-01-12'; //$this->input->post('end'); $end;


        $total = $declined = $approved = $synched = $pending = $approved_real = $pending_real = 0;
//assigned_entries|approved_entries|declined_entries|delete_request|synched_entries|tickets_assigned|tickets_actioned|tickets_pending	tickets_closed
        foreach ($result as $key => $value) {

            $total = $total + $value[ 'ccount' ];
            if ($value[ 'state' ] == 2 || $value[ 'state' ] == 4) {
                $approved = $approved + $value[ 'ccount' ];
            }
            if ($value[ 'state' ] == 0) {
                $pending = $value[ 'ccount' ];
            }
            if ($value[ 'state' ] == 3) {
                $declined = $value[ 'ccount' ];
            }


            if ($value[ 'state' ] == 4) {
                $approved_real = $approved_real + $value[ 'ccount' ];
            }
            if ($value[ 'state' ] == 2) {
                $pending_real = $pending_real + $value[ 'ccount' ];
            }
        }

        $data[] = array(
            'total'         => number_format($total + $purge[ 'total' ]),
            'approved'      => number_format($approved + $purge[ 'approved' ]),
            'approved_real' => number_format($approved_real),
            'pending_real'  => number_format($pending_real),
            'pending'       => number_format($pending + $purge[ 'pending' ]),
            'rejected'      => number_format($declined + $purge[ 'declined' ])
        );

        echo json_encode($data);
        exit;
    }

    /**
     * View User Role Departmenrd
     *
     * @Route("/user_role_department", name="user_role_department")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:user_role_department.html.twig")
     */
    public function view_user_role_departmentsAction() {
        if (!in_array(39, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $em = $this->getDoctrine()->getManager();
        $emConfig = $em->getConfiguration();

        $query = $em->createQueryBuilder()
            ->select('p')
            ->from('RestApiBundle:Role', 'p')
            ->getQuery();

        try {
            $results = $query->getArrayResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }


        $data = array(
            'title'       => 'View User Roles Listing:',
            'title_descr' => 'Listing for user permission roles departments ',
            'user_roles'  => $results,
            //'entities' => $entities,
        );

        return $this->prepareResponse($data);
    }

    /**
     * Finds and displays a Faq entity.
     *
     * @Route("/edit_role_department/{id}", name="edit_role_department")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:edit_role_department.html.twig")
     */
    public function showedit_role_departmentAction($id) {
        if (!in_array(40, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RestApiBundle:Role')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Role entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        $data = array(
            'dept_id'      => $id,
            'title'        => 'Edit User Role Department Permissions',
            'title_descr'  => 'Edit User Role Department Permissions',
            'entity_perms' => $entity->getDeptPerms(),
            'delete_form'  => $deleteForm->createView(),
        );

        return $this->prepareResponse($data);
    }

    /**
     * Edits an existing user dept perm
     *
     * @Route("/edit_role_department_action/{id}", name="edit_role_department_action")
     * @Method("POST")
     * @Template("DashboardBundle:Faq:edit.html.twig")
     */
    public function update_edit_role_department_actionAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $attributes = $request->request->all();

        $results = $em->createQuery("UPDATE RestApiBundle:Role u SET u.dept_perms = '" . str_replace('"', '', json_encode($attributes[ 'perms' ])) . "' WHERE u.id = " . $id . ' ')
            ->getArrayResult();

        $this->logUserEvent(UserLog::EDIT_USER_ROLE, 'edit user role', array( 'permissions' => $attributes[ 'perms' ] ));

//return $this->redirect($this->generateUrl('admin/agents'));
        return $this->redirect($this->generateUrl('admin'));
    }

    /**
     * hour_graph Summary
     *
     * @Route("/department_perms_json", name="department_perms_json")
     * @Method("GET")
     * @Template()
     */
    public function department_perms_jsonAction() {
        $em = $this->getDoctrine()->getManager();

        $where = ' WHERE 1=1 ';
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array('ROLE_SUPERAGENT', $roles)) {
            $where .= ' AND a.parent_id = ' . $user->getId();
        }
        if (in_array('ROLE_VODASHOP', $roles)) {
            $where .= ' AND a.vodashop_id = ' . $user->getId();
        }

        $emConfig = $em->getConfiguration();

        $result = $em->createQuery('SELECT p '
            . ' from DashboardBundle:Department_perms p '
            . ' ORDER BY p.parent asc')
            ->getArrayResult(); //getSingleResult();
        $data = array();

        foreach ($result as $key => $value) {
            if ($value[ 'parent' ] == 0) {
                $data[ $value[ 'id' ] ] = array(
                    'id'       => 1,
                    'text'     => $value[ 'name' ],
                    'children' => array()
                );
            } else {
                $data[ $value[ 'parent' ] ][ 'children' ][] = array( 'text' => $value[ 'name' ], 'perm_checked' => 0, 'perm_id' => $value[ 'id' ] );
            }
        }
        foreach ($data as $ey => $alue) {
            $data_perms[] = $alue;
        }

        echo json_encode($data_perms);
        exit;
    }

    /**
     * hour_graph Summary
     *
     * @Route("/department_perms_array", name="department_perms_array")
     * @Method("GET")
     * @Template()
     */
    public function get_department_perms_arrayAction() {
        $em = $this->getDoctrine()->getManager();
        $pperms = $unique_perms = array();
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user) {
            $roles = $user->getRoles(); //array of roles
            $perms_str = str_replace('"', "'", str_replace(']', '', str_replace('[', '', json_encode($user->getRoles()))));

            $emConfig = $em->getConfiguration();
            $result = $em->createQuery('SELECT p '
                . ' from RestApiBundle:Role p '
                . 'WHERE p.role in (' . $perms_str . ')')
                ->getArrayResult(); //getSingleResult();


            foreach ($result as $key => $value) {
                if ($value && array_key_exists('dept_perms', $value)) {
                    $the_arr = is_array($value[ 'dept_perms' ]) ? $value[ 'dept_perms' ] : json_decode($value[ 'dept_perms' ]);
                    foreach ($the_arr as $kkey => $vvalue) {
                        $pperms[ $vvalue ] = $vvalue;
                    }
                }
            }

            ksort($pperms);
            foreach ($pperms as $kkkkey => $vvvalue) {
                $unique_perms[] = $vvvalue;
            }
        }

        return $this->prepareResponse($unique_perms);
    }

    /**
     * Create New User Role Dept
     *
     * @Route("/newUserDept", name="newUserDept")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:newUserDept.html.twig")
     */
    public function newUserDeptAction() {

        $entity = new Role();
        $form = $this->createCreatedForm($entity);

        $data = array(
            'title'       => 'Create New User Role Department',
            'title_descr' => 'Create New User Role Department',
            'entity'      => $entity,
            'form'        => $form->createView(),
        );

        return $this->prepareResponse($data);
        /*
          $entity = new Faq();

          return array(
          'title' => "Create New User Role Department",
          'title_descr' => "Create New User Role Department",
          'entity' => $entity
          );

         */
    }

    /**
     * Creates a new Faq entity.
     *
     * @Route("/newUserDeptaction", name="newUserDeptaction")
     * @Method("POST")
     * @Template("DashboardBundle:Admin:newUserDept.html.twig")
     */
    public function createnewUserDeptAction(Request $request) {

        $entity = new Role();
        $form = $this->createCreatedForm($entity);
        $form->handleRequest($request);

        if (true || $form->isValid()) { //echo "11111111"; exit;
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('user_role_department'));
        }

        //echo "222222222"; exit;

        $data = array(
            'title'       => 'Create New User Role Department',
            'title_descr' => 'Create New User Role Department',
            'entity'      => $entity,
            'form'        => $form->createView()
        );

        return $this->prepareResponse($data);
    }

    /**
     * Creates a form to create a Role entity.
     *
     * @param Role $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreatedForm(Role $entity) {

        $form = $this->createForm(new RoleType(), $entity, array(
            'action' => $this->generateUrl('admin'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array( 'label' => 'Create' ));

        return $form;
    }

    /**
     * Registration Summary
     *
     * @Route("/regbyday_summary/{start_date}/{end_date}", name="regbyday_summary")
     * @Method("GET")
     * @Template()
     */
    public function regbyday_summaryAction($start_date, $end_date) {
        $em = $this->getDoctrine()->getManager();

        /*
          SELECT DAYNAME(created_date) as day_name, count(id) as day_count
          from sreg_dashboard_registrations
          " . $where . "
          group by day_name
          order by FIELD(day_name, 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY');
         */

        $where = '';
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array('ROLE_SUPERAGENT', $roles)) {
            $where .= ' AND a.parent_id = ' . $user->getId();
        }
        if (in_array('ROLE_VODASHOP', $roles)) {
            $where .= ' AND a.vodashop_id = ' . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(p.createdDate) >= '" . $start_date . "' AND date(p.createdDate) <= '" . $end_date . "' ";
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DAYNAME', 'DoctrineExtensions\Query\Mysql\DayName');


        $result = $em->createQuery('SELECT DAYNAME(p.createdDate) as day_name, count(p) as day_count '
            . 'from RestApiBundle:Registration p '
            . ' JOIN p.owner a'
            . ' where 1=1 ' . $where
            . ' group by day_name')
            ->getArrayResult();


        $response_array = array();
        foreach ($result as $key => $value) {
            $response_array[] = array( 'day_name' => $value[ 'day_name' ], 'day_count' => $value[ 'day_count' ] );
        }

        echo json_encode($response_array);
        exit;
    }

    private function getKeyValueArray($buckets) {
        $array = array();
        foreach ($buckets as $value) {
            $array[ $value[ 'key' ] ] = $value[ 'doc_count' ];
        }

        return $array;
    }

    /**
     * abelareport_summary Summary
     *
     * @Route("/daydata_summary/{ddate}/{timestamp}", name="daydata_summary")
     * @Method("GET")
     * @Template()
     */
    public function daydata_summaryAction($ddate, $timestamp) {
        $em = $this->container->get('doctrine')->getManager();
        $where = ' WHERE 1=1 ';
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array('ROLE_SUPERAGENT', $roles)) {
            $where .= ' AND a.parent_id = ' . $user->getId();
        }
        if (in_array('ROLE_VODASHOP', $roles)) {
            $where .= ' AND a.vodashop_id = ' . $user->getId();
        }
        if ($ddate != 1) {
            $where .= " AND date(e.createdDate) = '" . $ddate . "'";
        }


        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $result = $em->createQuery('SELECT DATE(e.createdDate) as ddate, COUNT(p.id) as ccount, e.image_count, p.regType as regType, p.temporaryRegStatus as treg, p.fullRegStatus as freg, p.mpesaState as mpesaState, p.dbmsState as dbmsState '
            . ' from RestApiBundle:RegistrationStatus p '
            . ' JOIN p.registrationId e'
            . ' JOIN e.owner a' . $where
            . ' GROUP BY e.image_count, regType, treg, freg, mpesaState, dbmsState '
            . ' ORDER BY p.id desc')
            ->getArrayResult(); //getSingleResult();


        $data = $data2 = array();


//        foreach ($result as $key => $value) {
//            $data[$value['ddate']][$value['state']]['count'] = @$data[$value['ddate']][$value['state']]['count'] + $value['ccount'];
//            $data[$value['ddate']]['total']['count'] = @$data[$value['ddate']]['total']['count'] + $value['ccount'];
//        }

        $total = $declined = $approved = $synched = $pending = $approved_real = $pending_real = 0;
//assigned_entries|approved_entries|declined_entries|delete_request|synched_entries|tickets_assigned|tickets_actioned|tickets_pending	tickets_closed

        foreach ($result as $key => $value) {
            $data[ $value[ 'ddate' ] ][ 'total' ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 'total' ][ 'count' ] + $value[ 'ccount' ];

            $status_keys = array(
                'success'      => 2,
                'declined'     => 3,
                'fail'         => 4,
                'retry'        => 5,
                'success_cap'  => 6,
                'success_dbms' => 7,
            );

            //images not received so pending
            if ($value[ 'image_count' ] < 3) {
                $data[ $value[ 'ddate' ] ][ 0 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 0 ][ 'count' ] + $value[ 'ccount' ];
            } else {
                $data[ $value[ 'ddate' ] ][ 6 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 6 ][ 'count' ] + $value[ 'ccount' ];
            }
            /*
              +--------+---------------+------------+----------------------+-----------------+---------------+--------------+
              | sclr_0 | image_count_1 | reg_type_2 | temporaryRegStatus_3 | fullRegStatus_4 | mpesa_state_5 | dbms_state_6 |
              +--------+---------------+------------+----------------------+-----------------+---------------+--------------+
              |      2 |             5 |          0 |                    6 |               6 |             3 |            7 |
              |      8 |             1 |          0 |                    6 |               0 |             3 |            0 |
              |      2 |             8 |          2 |                    4 |               0 |             0 |            0 |
              |      3 |             1 |          0 |                    1 |               0 |             3 |            0 |
              |      1 |             6 |          0 |                    6 |               4 |             1 |            4 |
              |      1 |             4 |          1 |                    2 |               6 |             1 |            7 |
              |      5 |             4 |          0 |                    6 |               1 |             1 |            0 |
             */

            if (in_array($value[ 'treg' ], array( 2, 6, 7 ))) {
                //TREG
                //Regtype... = array("1" => "Re-Reg", "0" => "New", "2" => "Declined");
                if ($value[ 'regType' ] == AdminController::REREG) {
                    $data[ $value[ 'ddate' ] ][ 11 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 11 ][ 'count' ] + $value[ 'ccount' ];
                } elseif ($value[ 'regType' ] == AdminController::NEWREG) {
                    $data[ $value[ 'ddate' ] ][ 10 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 10 ][ 'count' ] + $value[ 'ccount' ];
                } else {
                    $data[ $value[ 'ddate' ] ][ 13 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 13 ][ 'count' ] + $value[ 'ccount' ];
                }
                $data[ $value[ 'ddate' ] ][ 1 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 1 ][ 'count' ] + $value[ 'ccount' ];
            } elseif (in_array($value[ 'treg' ], array( 4, 5, 3 ))) {
                //FAIL
                $data[ $value[ 'ddate' ] ][ 3 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 3 ][ 'count' ] + $value[ 'ccount' ];
            } else {
                //NOT SENT YET
                $data[ $value[ 'ddate' ] ][ 5 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 5 ][ 'count' ] + $value[ 'ccount' ];
            }


            if (in_array($value[ 'treg' ], array( 2, 6, 7 )) && in_array($value[ 'freg' ], array( 2, 6, 7 ))) {
                //FREG
                //Regtype... = array("1" => "Re-Reg", "0" => "New", "2" => "Declined");
                if ($value[ 'regType' ] == AdminController::REREG) {
                    $data[ $value[ 'ddate' ] ][ 21 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 21 ][ 'count' ] + $value[ 'ccount' ];
                } elseif ($value[ 'regType' ] == AdminController::NEWREG) {
                    $data[ $value[ 'ddate' ] ][ 20 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 20 ][ 'count' ] + $value[ 'ccount' ];
                } else {
                    $data[ $value[ 'ddate' ] ][ 23 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 23 ][ 'count' ] + $value[ 'ccount' ];
                }
                $data[ $value[ 'ddate' ] ][ 2 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 2 ][ 'count' ] + $value[ 'ccount' ];
            }
            if (in_array($value[ 'treg' ], array( 2, 6, 7 )) && $value[ 'freg' ] == 0 && $value[ 'treg' ] != 4) {
                //PENDING FREG
                $data[ $value[ 'ddate' ] ][ 4 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 4 ][ 'count' ] + $value[ 'ccount' ];
            }

            /*
              mp$states = array("0" => "Pending", "1" => "Successful", "2" => "Failed", "3" => "Already Registered");
              db$states = array("0" => "Pending", "4" => "Failed", "6" => "Success", "7" => "Success");

             */
            if ($value[ 'regType' ] == AdminController::NEWREG) {
                $data[ $value[ 'ddate' ] ][ 321 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 321 ][ 'count' ] + $value[ 'ccount' ];

                if ($value[ 'mpesaState' ] == AdminController::MPESASENT) {
                    $data[ $value[ 'ddate' ] ][ 171 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 171 ][ 'count' ] + $value[ 'ccount' ];
                } elseif ($value[ 'mpesaState' ] == AdminController::MPESAALREADYREGISTERED) {
                    $data[ $value[ 'ddate' ] ][ 172 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 172 ][ 'count' ] + $value[ 'ccount' ];
                } elseif ($value[ 'mpesaState' ] == AdminController::MPESAFAILED) {
                    $data[ $value[ 'ddate' ] ][ 173 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 173 ][ 'count' ] + $value[ 'ccount' ];
                } else {
                    $data[ $value[ 'ddate' ] ][ 170 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 170 ][ 'count' ] + $value[ 'ccount' ];
                }


                if ($value[ 'dbmsState' ] == AdminController::DBMSSENT || $value[ 'dbmsState' ] == AdminController::DBMSSENT2) {
                    $data[ $value[ 'ddate' ] ][ 181 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 181 ][ 'count' ] + $value[ 'ccount' ];
                } elseif ($value[ 'dbmsState' ] == AdminController::DBMSFAILED) {
                    $data[ $value[ 'ddate' ] ][ 182 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 182 ][ 'count' ] + $value[ 'ccount' ];
                } else {
                    $data[ $value[ 'ddate' ] ][ 180 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 180 ][ 'count' ] + $value[ 'ccount' ];
                }
            }
            if ($value[ 'regType' ] == AdminController::REREG) {
                $data[ $value[ 'ddate' ] ][ 322 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 322 ][ 'count' ] + $value[ 'ccount' ];

                if ($value[ 'mpesaState' ] == AdminController::MPESASENT) {
                    $data[ $value[ 'ddate' ] ][ 271 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 271 ][ 'count' ] + $value[ 'ccount' ];
                } elseif ($value[ 'mpesaState' ] == AdminController::MPESAALREADYREGISTERED) {
                    $data[ $value[ 'ddate' ] ][ 272 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 272 ][ 'count' ] + $value[ 'ccount' ];
                } elseif ($value[ 'mpesaState' ] == AdminController::MPESAFAILED) {
                    $data[ $value[ 'ddate' ] ][ 273 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 273 ][ 'count' ] + $value[ 'ccount' ];
                } else {
                    $data[ $value[ 'ddate' ] ][ 270 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 270 ][ 'count' ] + $value[ 'ccount' ];
                }


                if ($value[ 'dbmsState' ] == AdminController::DBMSSENT || $value[ 'dbmsState' ] == AdminController::DBMSSENT2) {
                    $data[ $value[ 'ddate' ] ][ 281 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 281 ][ 'count' ] + $value[ 'ccount' ];
                } elseif ($value[ 'dbmsState' ] == AdminController::DBMSFAILED) {
                    $data[ $value[ 'ddate' ] ][ 282 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 282 ][ 'count' ] + $value[ 'ccount' ];
                } else {
                    $data[ $value[ 'ddate' ] ][ 280 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 280 ][ 'count' ] + $value[ 'ccount' ];
                }
            }
        }

        foreach ($data as $key4 => $value4) {
            //echo "<pre>"; print_r($value4); echo "</pre>";
            $data2 = array(
                'date'             => $key4,
                'total'            => (@$value4[ 'total' ][ 'count' ]) > 0 ? "<a href=''>" + @$value4[ 'total' ][ 'count' ] + '</a>' : @$value4[ 'total' ][ 'count' ],
                'pending_info'     => (@$value4[ 0 ][ 'count' ]) ? @$value4[ 0 ][ 'count' ] : '0',
                'full_info'        => (@$value4[ 6 ][ 'count' ]) ? @$value4[ 6 ][ 'count' ] : '0',
                'declined'         => (@$value4[ 3 ][ 'count' ]) ? @$value4[ 3 ][ 'count' ] : '0',
                'treg'             => (@$value4[ 1 ][ 'count' ]) ? @$value4[ 1 ][ 'count' ] : '0',
                'tregnewreg'       => (@$value4[ 10 ][ 'count' ]) ? @$value4[ 10 ][ 'count' ] : '0',
                'tregrereg'        => (@$value4[ 11 ][ 'count' ]) ? @$value4[ 11 ][ 'count' ] : '0',
                'tregdeclined'     => (@$value4[ 13 ][ 'count' ]) ? @$value4[ 13 ][ 'count' ] : '0',
                'freg'             => (@$value4[ 2 ][ 'count' ]) ? @$value4[ 2 ][ 'count' ] : '0',
                'fregnewreg'       => (@$value4[ 20 ][ 'count' ]) ? @$value4[ 20 ][ 'count' ] : '0',
                'fregrereg'        => (@$value4[ 21 ][ 'count' ]) ? @$value4[ 21 ][ 'count' ] : '0',
                'fregdeclined'     => (@$value4[ 23 ][ 'count' ]) ? @$value4[ 23 ][ 'count' ] : '0',
                'newreg'           => (@$value4[ 321 ][ 'count' ]) ? @$value4[ 321 ][ 'count' ] : '0',
                'rereg'            => (@$value4[ 322 ][ 'count' ]) ? @$value4[ 322 ][ 'count' ] : '0',
                'tregmpesasent'    => (@$value4[ 171 ][ 'count' ]) ? @$value4[ 171 ][ 'count' ] : '0',
                'tregmpesapending' => (@$value4[ 170 ][ 'count' ]) ? @$value4[ 170 ][ 'count' ] : '0',
                'fregmpesasent'    => (@$value4[ 271 ][ 'count' ]) ? @$value4[ 271 ][ 'count' ] : '0',
                'fregmpesapending' => (@$value4[ 270 ][ 'count' ]) ? @$value4[ 270 ][ 'count' ] : '0',
                'tregdbmssent'     => (@$value4[ 181 ][ 'count' ]) ? @$value4[ 181 ][ 'count' ] : '0',
                'tregdbmspending'  => (@$value4[ 180 ][ 'count' ]) ? @$value4[ 180 ][ 'count' ] : '0',
                'fregdbmssent'     => (@$value4[ 281 ][ 'count' ]) ? @$value4[ 281 ][ 'count' ] : '0',
                'fregdbmspending'  => (@$value4[ 280 ][ 'count' ]) ? @$value4[ 280 ][ 'count' ] : '0',
                ////
                /////
                'pending_tobesent' => (@$value4[ 5 ][ 'count' ]) ? @$value4[ 5 ][ 'count' ] : '0',
                'treg'             => (@$value4[ 1 ][ 'count' ]) ? @$value4[ 1 ][ 'count' ] : '0',
                'freg'             => (@$value4[ 2 ][ 'count' ]) ? @$value4[ 2 ][ 'count' ] : '0',
                'pending_freg'     => (@$value4[ 4 ][ 'count' ]) ? @$value4[ 4 ][ 'count' ] : '0',
            );
        }

//        echo "<pre>";
//        print_r($data2);
//        echo "</pre>";

        echo json_encode($data2);
        exit;
    }

    /**
     * trendlatest_summary Summary
     *
     * @Route("/trendlatest_summary/{start_date}/{end_date}/{timestamp}", name="trendlatest_summary")
     * @Method("GET")
     * @Template()
     */
    public function trendlatest_summaryAction($start_date, $end_date, $timestamp) {
        $em = $this->container->get("doctrine")->getManager();
        $where = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND a.parent_id = " . $user->getId();
        }
        if (in_array("ROLE_VODASHOP", $roles)) {
            $where .= " AND a.vodashop_id = " . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(e.createdDate) >= '" . $start_date . "' AND date(e.createdDate) <= '" . $end_date . "' ";
        }


        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $result = $em->createQuery("SELECT e.image_count, p.temporaryRegStatus as treg, p.fullRegStatus as freg, p.verifyState as vstate, DATE(e.createdDate) as ddate, COUNT(p.id) as ccount "
            . " from RestApiBundle:RegistrationStatus p "
//. "WHERE date(created_date) >= '2015-09-28' AND date(created_date) <= '2015-10-08' "
            . " JOIN p.registrationId e"
            . " JOIN e.owner a" . $where
            . " GROUP BY ddate, e.image_count, treg, freg, vstate "
            . " ORDER BY ddate desc")
            ->getArrayResult(); //getSingleResult();


        $data = $data2 = array();


//        foreach ($result as $key => $value) {
//            $data[$value['ddate']][$value['state']]['count'] = @$data[$value['ddate']][$value['state']]['count'] + $value['ccount'];
//            $data[$value['ddate']]['total']['count'] = @$data[$value['ddate']]['total']['count'] + $value['ccount'];
//        }

        $total = $declined = $approved = $synched = $pending = $approved_real = $pending_real = 0;
//assigned_entries|approved_entries|declined_entries|delete_request|synched_entries|tickets_assigned|tickets_actioned|tickets_pending	tickets_closed

        foreach ($result as $key => $value) {
            $data[ $value[ 'ddate' ] ][ 'total' ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 'total' ][ 'count' ] + $value[ 'ccount' ];

            $status_keys = array(
                'success'      => 2,
                'declined'     => 3,
                'fail'         => 4,
                'retry'        => 5,
                'success_cap'  => 6,
                'success_dbms' => 7,
            );

            //images not received so pending
            if ($value[ 'image_count' ] < 3 && $value[ 'vstate' ] == 0) {
                if (!in_array($value[ 'treg' ], array( 4, 5, 3 ))) {
                    $data[ $value[ 'ddate' ] ][ 0 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 0 ][ 'count' ] + $value[ 'ccount' ];
                }
            }


            if (in_array($value[ 'freg' ], array( 2, 6, 7 ))) {
                //TREG
                $data[ $value[ 'ddate' ] ][ 1 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 1 ][ 'count' ] + $value[ 'ccount' ];
            } elseif (in_array($value[ 'freg' ], array( 4, 5, 3 ))) {
                //FAIL
                $data[ $value[ 'ddate' ] ][ 3 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 3 ][ 'count' ] + $value[ 'ccount' ];
            } else {
                //NOT SENT YET
                if ($value[ 'vstate' ] == 1) {
                    $data[ $value[ 'ddate' ] ][ 5 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 5 ][ 'count' ] + $value[ 'ccount' ];
                }
            }

            if (in_array($value[ 'treg' ], array( 2, 6, 7 )) && in_array($value[ 'freg' ], array( 2, 6, 7 ))) {
                //FREG
                $data[ $value[ 'ddate' ] ][ 2 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 2 ][ 'count' ] + $value[ 'ccount' ];
            }

            if (in_array($value[ 'treg' ], array( 2, 6, 7 )) && !in_array($value[ 'freg' ], array( 2, 6, 7 ))) {// && $value['vstate'] == 1
                //PENDING FREG
                $data[ $value[ 'ddate' ] ][ 4 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 4 ][ 'count' ] + $value[ 'ccount' ];
            }

            //if (in_array($value['treg'], array(0, 2, 6)) && $value['image_count'] >= 4 && in_array($value['vstate'], array(0, 1, 2, 3, '-1', '-2', '-3'))) {
            if (true) {
                if (in_array($value[ 'vstate' ], array( 1, 2, 3, '-2', '-3' ))) {
                    $data[ $value[ 'ddate' ] ][ 30 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 30 ][ 'count' ] + $value[ 'ccount' ];

                    if ($value[ 'vstate' ] == 1) {
                        //vgood
                        $data[ $value[ 'ddate' ] ][ 32 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 32 ][ 'count' ] + $value[ 'ccount' ];
                    } elseif ($value[ 'vstate' ] == 2 || $value[ 'vstate' ] == '-2') {
                        //vfair
                        $data[ $value[ 'ddate' ] ][ 33 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 33 ][ 'count' ] + $value[ 'ccount' ];
                    } elseif ($value[ 'vstate' ] == 3 || $value[ 'vstate' ] == '-3') {
                        //vbad
                        $data[ $value[ 'ddate' ] ][ 34 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 34 ][ 'count' ] + $value[ 'ccount' ];
                    }
                } else {
                    //notverified
                    // echo "<br />DATE: ".$value['ddate']." TREG: ".$value['treg'] ." IMAGECNT: ".$value['image_count']."VSTATE" . $value['vstate'] . "COUNT: ".$value['ccount']."<br />";
                    if (/* in_array($value['treg'], array(6, 7)) && */
                        $value[ 'image_count' ] >= 4 && in_array($value[ 'vstate' ], array( 0 ))
                    ) {
                        $data[ $value[ 'ddate' ] ][ 31 ][ 'count' ] = @$data[ $value[ 'ddate' ] ][ 31 ][ 'count' ] + $value[ 'ccount' ];
                    }
                }
            }
        }

        foreach ($data as $key4 => $value4) {
            //echo "<pre>"; print_r($value4); echo "</pre>";
            $data2[] = array(
                'date'             => $key4,
                'total'            => (@$value4[ 'total' ][ 'count' ]) > 0 ? "<a href=''>" + @$value4[ 'total' ][ 'count' ] + "</a>" : @$value4[ 'total' ][ 'count' ],
                'pending_info'     => (@$value4[ 0 ][ 'count' ]) ? @$value4[ 0 ][ 'count' ] : "0",
                'pending_tobesent' => (@$value4[ 5 ][ 'count' ]) ? @$value4[ 5 ][ 'count' ] : "0",
                'treg'             => (@$value4[ 1 ][ 'count' ]) ? @$value4[ 1 ][ 'count' ] : "0",
                'freg'             => (@$value4[ 2 ][ 'count' ]) ? @$value4[ 2 ][ 'count' ] : "0",
                'verified'         => (@$value4[ 30 ][ 'count' ]) ? @$value4[ 30 ][ 'count' ] : "0",
                'notverified'      => (@$value4[ 31 ][ 'count' ]) ? @$value4[ 31 ][ 'count' ] : "0",
                'vgood'            => (@$value4[ 32 ][ 'count' ]) ? @$value4[ 32 ][ 'count' ] : "0",
                'vfair'            => (@$value4[ 33 ][ 'count' ]) ? @$value4[ 33 ][ 'count' ] : "0",
                'vbad'             => (@$value4[ 34 ][ 'count' ]) ? @$value4[ 34 ][ 'count' ] : "0",
                'pending_freg'     => (@$value4[ 4 ][ 'count' ]) ? @$value4[ 4 ][ 'count' ] : "0",
                'declined'         => (@$value4[ 3 ][ 'count' ]) ? @$value4[ 3 ][ 'count' ] : "0"
            );
        }

        echo json_encode($data2);
        exit;
    }

    /**
     * hourly_summary Summary
     *
     * @Route("/ccount_summary/{start_date}/{end_date}/{timestamp}", name="ccount_summary")
     * @Method("GET")
     * @Template()
     */
    public function ccount_summaryAction($start_date, $end_date, $timestamp) {
        $em = $this->getDoctrine()->getManager();

        $where = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        $leftJOIN = "";
        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND a.parent_id = " . $user->getId();
            $leftJOIN = " JOIN x.owner a ";
        }
        if ($start_date != 1) {
            $where .= " AND date(x.createdDate) >= '" . $start_date . "' AND date(x.createdDate) <= '" . $end_date . "' ";
        } else {
            $end_date = date_format(date_sub(date_create(date('Y-m-d')), date_interval_create_from_date_string("10 days")), "Y-m-d");
            //$where .= " AND date(x.createdDate) >= '" . $end_date . "' ";
            if (true) {
                $where .= " AND date(x.createdDate) >= '" . $end_date . "' AND date(x.createdDate) <= '" . date('Y-m-d H:i:s') . "' ";
            }
        }


        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');


        $treg = $freg = $fail = $pending = 0;

        $count_tfreg = $em->createQuery("SELECT count(r) as reg_count, x.image_count, r.temporaryRegStatus as treg, r.fullRegStatus as freg FROM RestApiBundle:RegistrationStatus r"
            . " JOIN r.registrationId x " . $leftJOIN . $where
            . " GROUP BY x.image_count, r.temporaryRegStatus, r.fullRegStatus"
        )->getArrayResult();


        $data[ 'freg' ] = $data[ 'receivedinfo' ] = $data[ 'fail' ] = $data[ 'treg' ] = $data[ 'pending_freg' ] = $data[ 'pendingtreg' ] = $data[ 'pending' ] = $data[ 'total' ] = 0;
        foreach ($count_tfreg as $key => $val) {
            $data[ 'total' ] = $data[ 'total' ] + $val[ 'reg_count' ];
            if ($val[ 'image_count' ] < 3) {
                $data[ 'pending' ] = $data[ 'pending' ] + $val[ 'reg_count' ];
            } else {
                $data[ 'receivedinfo' ] = $data[ 'receivedinfo' ] + $val[ 'reg_count' ];
            }

            //available states 0,1,2,3,4,5,6,7
            if (in_array($val[ 'treg' ], array( 1, 2, 6, 7 ))) {
                //TREG
                $data[ 'treg' ] = $data[ 'treg' ] + $val[ 'reg_count' ];
            } elseif (in_array($val[ 'treg' ], array( 4, 5, 3 ))) {
                //FAIL
                $data[ 'fail' ] = $data[ 'fail' ] + $val[ 'reg_count' ];
            } else {
                //NOT SENT YET
                $data[ 'pendingtreg' ] = $data[ 'pendingtreg' ] + $val[ 'reg_count' ];
            }

            if (in_array($val[ 'treg' ], array( 1, 2, 6, 7 )) && in_array($val[ 'freg' ], array( 1, 2, 6, 7, 4 ))) {
                //FREG
                $data[ 'freg' ] = $data[ 'freg' ] + $val[ 'reg_count' ];
            }
            if (in_array($val[ 'treg' ], array( 1, 2, 6, 7 )) && $val[ 'freg' ] == 0) {
                //PENDING FREG
                $data[ 'pending_freg' ] = $data[ 'pending_freg' ] + $val[ 'reg_count' ];
            }
            //--
        }


        if ($start_date == 1 && !in_array("ROLE_SUPERAGENT", $roles)) {
            $result = $em->createQuery("SELECT s from DashboardBundle:Summary s "
                . "WHERE s.dataType = 'top_count' ")
                ->getArrayResult();

            if (count($result) >= 1) {
                $result = $result[ 0 ];

                foreach (json_decode($result[ 'dataString' ]) as $key => $val) {
                    $data[ $key ] = $data[ $key ] + $val;
                }
            }
        }

        echo json_encode($data);
        exit;
    }

    /**
     * hourly_summary Summary
     *
     * @Route("/hourly_summary/{start_date}/{end_date}/{timestamp}", name="hourly_summary")
     * @Method("GET")
     * @Template()
     */
    public function hourly_summaryAction($start_date, $end_date, $timestamp) {
        $em = $this->getDoctrine()->getManager();

        $where = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        $join_superagent = "";
        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND u1_.parent_id = " . $user->getId();
            $join_superagent = "LEFT JOIN user u1_ ON r0_.owner_id = u1_.id";
        }


        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('HOUR', 'DoctrineExtensions\Query\Mysql\Hour');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        if ($start_date == 1) {
            $start_date = $end_date = date('Y-m-d');
        }


        $where .= " AND r0_.date_generated_createdDate >= " . date('Ymd', strtotime($end_date)) . "000000 AND r0_.date_generated_createdDate <= " . date('Ymd', strtotime($end_date)) . "235959 ";

        $query1 = "SELECT HOUR(r0_.createdDate) AS hhour, COUNT(r0_.id) AS ccount FROM registration r0_ " . $join_superagent . " " . $where . " GROUP BY hhour ORDER BY hhour DESC";

        $connection = $em->getConnection();
        $statement = $connection->prepare($query1);
        $statement->execute();
        $result = $statement->fetchAll();

        //20170410


        $data = array();

        for ($i = 0; $i <= 23; $i++) {
            $data[ $i ] = 0;
        }

        foreach ($result as $key => $value) {
            //echo "<pre>"; print_r($value); echo "</pre>";

            $data[ $value[ 'hhour' ] ] = $value[ 'ccount' ];
        }


        echo json_encode($data);
        exit;
    }

    /**
     * graphreport_region Summary
     *
     * @Route("/graphreport_region/{start_date}/{end_date}/{territory}", name="graphreport_region")
     * @Method("GET")
     * @Template()
     */
    public function graphreport_regionAction($start_date, $end_date, $territory) {
        $em = $this->getDoctrine()->getManager();

        //since territory [town names ] isnt left joined plus saving time lets pick code from array instead :(
        //first get territories iterate to form array then pick!
        $result = $em->createQuery('SELECT t from DashboardBundle:Territory t ')->getArrayResult(); //getSingleResult();

        foreach ($result as $key => $value) {
            $regionskey[ strtoupper($value[ 'name' ]) ] = $value[ 'id' ];
            $regionsName[ $value[ 'id' ] ] = $value[ 'name' ];
        }

        $regionsName[ 'EntireCountry' ] = $regionskey[ 'EntireCountry' ] = 'EntireCountry';
        $regionskey[ 'all_regions' ] = 'EntireCountry';


        $postedRegions = explode(',', $territory);
        foreach ($postedRegions as $xkey => $xval) {
            $regionscode[] = $regionskey[ $this->get_regionName_fromKey(strtoupper($xval)) ];
        }


        $regionssearch = '';
        foreach ($regionscode as $tkey => $tval) {
            $regionssearch .= $tval . ',';
        }
        $regionssearch = rtrim($regionssearch, ',');


        $group_by = $territory_field = '';
        $territory_field = ", 'EntireCountry' AS  tterritory";
        $territory_list = "'" . str_replace(',', "','", $territory) . "'";
        $where = " WHERE DATE(p.createdDate) >= '" . $start_date . "' AND DATE(p.createdDate) <= '" . $end_date . "' ";
        if ($territory != 'all_regions') {
            $where .= ' AND p.territory in (' . $regionssearch . ') ';
            $group_by = ', tterritory';
            $territory_field = ', p.territory as tterritory';
        }

        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array('ROLE_SUPERAGENT', $roles)) {
            $where .= ' AND a.parent_id = ' . $user->getId();
        }
        if (in_array('ROLE_VODASHOP', $roles)) {
            $where .= ' AND a.vodashop_id = ' . $user->getId();
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        //* DESIRED: SELECT count(*) as ccount, gender, DATE_FORMAT(createdDate, '%Y%m') as mmonth, territory, EntireCountry' AS  tterritory FROM `registration` group by mmonth, gender order by id desc */
        $result = $em->createQuery("SELECT DATE_FORMAT(p.createdDate, '%Y/%m') as mmonth, COUNT(p.id) as ccount, p.gender " . $territory_field
            . ' from RestApiBundle:Registration p '
            // . " JOIN p.owner a "
            // . " JOIN p.region b "
            . $where
            . ' GROUP BY mmonth, p.gender' . $group_by
            . ' ORDER BY mmonth')
            ->getArrayResult(); //getSingleResult();
        //echo $em->getSql();

        $data_gender = array();
        $data2[ 0 ][ 0 ][ 0 ] = '[]';
        $data2[ 0 ][ 0 ][ 1 ] = '[]';
        $data2[ 1 ][ 'male' ] = 0;
        $data2[ 1 ][ 'female' ] = 0;
        $data2[ 2 ] = array();


        //get all months and all dates
        $month_list = $territory_list = array();
        $cnt = 1;
        foreach ($result as $key => $value) {

            $antoo2[] = $regionsName[ $value[ 'tterritory' ] ] . '|' . $value[ 'tterritory' ];
            if (!in_array($value[ 'mmonth' ], $month_list)) {
                $month_list[] = $value[ 'mmonth' ];
            }
            $territory_list[ $regionsName[ $value[ 'tterritory' ] ] ] = $regionsName[ $value[ 'tterritory' ] ];
            $cnt++;
        }

        //create interesting empty array for territory Gender and Registrations
        foreach ($territory_list as $tkey => $tval) {

            foreach ($month_list as $mkey => $mval) {
                $territory_data[ 'data' ][ $tval ][ $mval ] = 0;
                $territory_data[ 'gender' ][ $tval ][ 'male' ] = 0;
                $territory_data[ 'gender' ][ $tval ][ 'female' ] = 0;
            }
        }

        //Abracadabra!! let the magic happen!!
        foreach ($result as $kkey => $vvalue) {

            $antoo[] = $regionsName[ $vvalue[ 'tterritory' ] ] . '|' . $vvalue[ 'tterritory' ];
            $territory_data[ 'data' ][ $regionsName[ $vvalue[ 'tterritory' ] ] ][ $vvalue[ 'mmonth' ] ] = $territory_data[ 'data' ][ $regionsName[ $vvalue[ 'tterritory' ] ] ][ $vvalue[ 'mmonth' ] ] + $vvalue[ 'ccount' ];

            if (strtolower($vvalue[ 'gender' ]) == 'male' || strtolower($vvalue[ 'gender' ]) == 'm') {
                $territory_data[ 'gender' ][ $regionsName[ $vvalue[ 'tterritory' ] ] ][ 'male' ] = $territory_data[ 'gender' ][ $regionsName[ $vvalue[ 'tterritory' ] ] ][ 'male' ] + $vvalue[ 'ccount' ];
            } else {
                $territory_data[ 'gender' ][ $regionsName[ $vvalue[ 'tterritory' ] ] ][ 'female' ] = $territory_data[ 'gender' ][ $regionsName[ $vvalue[ 'tterritory' ] ] ][ 'female' ] + $vvalue[ 'ccount' ];
            }
        }

        $territory_data[ 'month_list' ] = $month_list;
        $territory_data[ 'ccount_elements' ] = count(@$territory_data[ 'data' ]);

        echo json_encode($territory_data);
        exit;
    }

    /**
     * hour_graph Summary
     *
     * @Route("/hour_graph/{start_date}/{end_date}/{territory}", name="hour_graph")
     * @Method("GET")
     * @Template()
     */
    public function hour_graphAction($start_date, $end_date, $territory) {
        $em = $this->getDoctrine()->getManager();


        //since territory [town names ] isnt left joined plus saving time lets pick code from array instead :(
        //first get territories iterate to form array then pick!
        $result = $em->createQuery('SELECT t from DashboardBundle:Territory t ')->getArrayResult(); //getSingleResult();
        ########get_regionName_fromKey
        foreach ($result as $key => $value) {
            $regionsName[ strtoupper($value[ 'name' ]) ] = $value[ 'id' ];
            $regionsid[ $value[ 'id' ] ] = strtoupper($value[ 'name' ]);
        }

        $regionsName[ 'EntireCountry' ] = $regionskey[ 'EntireCountry' ] = 'EntireCountry';
        $regionskey[ 'all_regions' ] = 'EntireCountry';

        $postedRegions = explode(',', $territory);
        foreach ($postedRegions as $xkey => $xval) {
            $regionscode[] = $regionsName[ $this->get_regionName_fromKey(strtoupper($xval)) ];
        }

        $regionssearch = '';
        foreach ($regionscode as $tkey => $tval) {
            $regionssearch .= $tval . ',';
        }
        $regionssearch = rtrim($regionssearch, ',');

        $territory = strtolower($territory);
        $group_by = $territory_field = '';
        $territory_field = ", 'EntireCountry' AS  tterritory";
        //echo '[[["[]","[]"]],{"male":0,"female":0},[]]'; exit;
        $territory_list = "'" . str_replace(',', "','", $territory) . "'";

        if ($start_date == 1) {
            $start_date = $end_date = date('Y-m-d');
        }

        $where = ' WHERE 1=1 '; // DATE(p.createdDate) >= '" . $start_date . "' AND DATE(p.createdDate) <= '" . $end_date . "' ";
        if ($territory != 'all_regions') {
            $where .= ' AND p.territory in (' . $regionssearch . ') ';
            $group_by = ', tterritory';
            $territory_field = ', p.territory as tterritory';
        } else {
            $territory = 'EntireCountry';
        }

        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array('ROLE_SUPERAGENT', $roles)) {
            $where .= ' AND a.parent_id = ' . $user->getId();
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('HOUR', 'DoctrineExtensions\Query\Mysql\Hour');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $result = $em->createQuery('SELECT count(p) as ccount, HOUR(p.createdDate) as hhour ' . $territory_field
            . ' from RestApiBundle:Registration p '
            . ' JOIN p.owner a' . $where
            . ' GROUP BY hhour '
            . ' ORDER BY hhour desc')
            ->getArrayResult(); //getSingleResult(); //getSql

        $data = array();

        $string_hour = '[';
        $string_data = '[';
        $cnt = 1;
        foreach (explode(',', $territory) as $key => $value) {

            for ($i = 0; $i <= 23; $i++) {
                $data[ strtoupper($value) ][ $i ] = 0;
            }
            $cnt++;
        }

        $ccount = 0;
        //get all months and all dates
        $month_list = $territory_list = array();
        foreach ($result as $key => $value) {
            $ccount = $ccount + 1;
            $data[ $this->get_regionKey_fromName($regionsid[ $value[ 'tterritory' ] ]) ][ $value[ 'hhour' ] ] = $value[ 'ccount' ];
        }

        $datareturn[ 'ccount_elements' ] = $ccount;
        $datareturn[ 'ddata' ] = $data;

        echo json_encode($datareturn);
        exit;
    }

    /**
     * hour_graph Summary
     *
     * @Route("/agentregistrations/{start_date}/{end_date}/{territory}", name="agentregistrations")
     * @Method("GET")
     * @Template()
     */
    public function agentregistrations_mapAction($start_date, $end_date, $territory) {
        $em = $this->getDoctrine()->getManager();

        $territory = strtolower($territory);

        //echo '[[["[]","[]"]],{"male":0,"female":0},[]]'; exit;
        $where = " WHERE DATE(p.createdDate) >= '" . $start_date . "' AND DATE(p.createdDate) <= '" . $end_date . "' ";

        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array('ROLE_SUPERAGENT', $roles)) {
            $where .= ' AND a.parent_id = ' . $user->getId();
        }
        if (in_array('ROLE_VODASHOP', $roles)) {
            $where .= ' AND a.vodashop_id = ' . $user->getId();
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('HOUR', 'DoctrineExtensions\Query\Mysql\Hour');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $result = $em->createQuery('SELECT count(p) as ccount, p.territory as territory '
            . ' from RestApiBundle:Registration p ' . $where
            . ' GROUP BY territory ')
            ->getArrayResult(); //getSingleResult(); //getSql

        /*
          {
          "code": "GE",
          "z": 362
          }
         */

        $regionskey = array(
            'mwanza'        => 'MW',
            'kagera'        => 'KR',
            'pwani'         => 'PW',
            'morogoro'      => 'MO',
            'njombe'        => 'NJ',
            'zanzibar'      => 'ZS',
            'zanzibar'      => 'ZW',
            'zanzibar'      => 'ZN',
            'kigoma'        => 'KM',
            'mtwara'        => 'MT',
            'ruvuma'        => 'RV',
            'pemba'         => 'PN',
            'pemba'         => 'PS',
            'singida'       => 'SD',
            'shinyanga'     => 'SH',
            'arusha'        => 'AS',
            'manyara'       => 'MY',
            'mara'          => 'MA',
            'simiyu'        => 'SI',
            'mbeya'         => 'MB',
            'rukwa'         => 'RK',
            'dar-es-salaam' => 'DS',
            'dodoma'        => 'DO',
            'tabora'        => 'TB',
            'lindi'         => 'LI',
            'Geita'         => 'GE',
            'kilimanjaro'   => 'KL',
            'tanga'         => 'TN',
            'kahama'        => 'KA',
            'iringa'        => 'IR'
        );

        $data = array();
        foreach ($regionskey as $key => $value) {
            $data[ $key ] = array( 'code' => $value, 'z' => 0 );
        }

        foreach ($result as $kkey => $vval) {
            $data[ $vval[ 'territory' ] ][ 'z' ] = $vval[ 'ccount' ];
        }


//        echo "<p><pre>";
//        print_r($data);
//        echo "</pre></p><p> --- --- --</p><br />";

        echo json_encode($data);
        exit;
    }

    /**
     * hour_graph Summary
     *
     * @Route("/agentlocation_map/{start_date}/{end_date}/{territory}", name="agentlocation_map")
     * @Method("GET")
     * @Template()
     */
    public function agentlocation_mapAction($start_date, $end_date, $territory) {
        $em = $this->getDoctrine()->getManager();

        $territory = strtolower($territory);

        $where = " WHERE DATE(p.createdDate) >= '" . $start_date . "' AND DATE(p.createdDate) <= '" . $end_date . "' ";

        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array('ROLE_SUPERAGENT', $roles)) {
            $where .= ' AND a.parent_id = ' . $user->getId();
        }
        if (in_array('ROLE_VODASHOP', $roles)) {
            $where .= ' AND a.vodashop_id = ' . $user->getId();
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('HOUR', 'DoctrineExtensions\Query\Mysql\Hour');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $result = $em->createQuery('SELECT count(p) as ccount, p.status, a.name as tterritory '
            . ' from RestApiBundle:User p '
            . ' JOIN p.territory a'
            . ' GROUP BY tterritory ')
            ->getArrayResult(); //getSingleResult(); //getSql

        $regionskey = array(
            'mwanza'        => 'MW',
            'kagera'        => 'KR',
            'pwani'         => 'PW',
            'morogoro'      => 'MO',
            'njombe'        => 'NJ',
            'zanzibar'      => 'ZS',
            'zanzibar'      => 'ZW',
            'zanzibar'      => 'ZN',
            'kigoma'        => 'KM',
            'mtwara'        => 'MT',
            'ruvuma'        => 'RV',
            'pemba'         => 'PN',
            'pemba'         => 'PS',
            'singida'       => 'SD',
            'shinyanga'     => 'SH',
            'arusha'        => 'AS',
            'manyara'       => 'MY',
            'mara'          => 'MA',
            'simiyu'        => 'SI',
            'mbeya'         => 'MB',
            'rukwa'         => 'RK',
            'dar-es-salaam' => 'DS',
            'dodoma'        => 'DO',
            'tabora'        => 'TB',
            'lindi'         => 'LI',
            'Geita'         => 'GE',
            'kilimanjaro'   => 'KL',
            'tanga'         => 'TN',
            'kahama'        => 'KA',
            'iringa'        => 'IR'
        );

        $data = array();
        foreach ($regionskey as $key => $value) {
            $data[ $key ] = array( 'code' => $value, 'z' => 0 );
        }

        foreach ($result as $kkey => $vval) {
            $data[ $vval[ 'tterritory' ] ][ 'z' ] = $vval[ 'ccount' ];
        }

        echo json_encode($data);
        exit;
    }

    /**
     * hour_graph Summary
     *
     * @Route("/agentactiveinactive_map/{start_date}/{end_date}/{territory}", name="agentactiveinactive_map")
     * @Method("GET")
     * @Template()
     */
    public function agentactiveinactive_mapAction($start_date, $end_date, $territory) {
        $em = $this->getDoctrine()->getManager();

        $territory = strtolower($territory);
        $group_by = $territory_field = '';
        $territory_field = ", 'EntireCountry' AS  tterritory";

        $territory_list = "'" . str_replace(',', "','", $territory) . "'";
        $where = ' WHERE 1=1 ';

        if ($territory != 'all_regions') {
            $where .= ' AND a.name in (' . $territory_list . ') ';
            $group_by = ', tterritory';
            $territory_field = ', a.name as tterritory';
        } else {
            $territory = 'EntireCountry';
        }


        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array('ROLE_SUPERAGENT', $roles)) {
            $where .= ' AND a.parent_id = ' . $user->getId();
        }
        if (in_array('ROLE_VODASHOP', $roles)) {
            $where .= ' AND a.vodashop_id = ' . $user->getId();
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('HOUR', 'DoctrineExtensions\Query\Mysql\Hour');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $result = $em->createQuery('SELECT count(p) as ccount, p.status ' . $territory_field
            . ' from RestApiBundle:User p '
            . ' JOIN p.territory a' . $where
            . ' GROUP BY tterritory, p.status ')
            ->getArrayResult(); //getSingleResult(); //getSql

        $territory_array = explode(',', $territory);

        $data[ 'ccount_elements' ] = count($territory_array);
        foreach ($territory_array as $keyy => $vall) {
            $data[ 'data' ][ $vall ] = array( 'inactive' => 0, 'active' => 0 );
        }

        foreach ($result as $kkey => $vval) {

            $sstatus = ($vval[ 'status' ] == 1 ? 'active' : 'inactive');
            $data[ 'data' ][ $vval[ 'tterritory' ] ][ $sstatus ] = $vval[ 'ccount' ];
            $data[ 'data' ][ $vval[ 'tterritory' ] ][ $sstatus ] = $vval[ 'ccount' ];
        }

        echo json_encode($data);
        exit;
    }

    /**
     * graphdate_range Summary
     *
     * @Route("/graphdate_range", name="graphdate_range")
     * @Method("GET")
     * @Template()
     */
    public function graphdate_rangeAction() {
        $em = $this->getDoctrine()->getManager();

        $where = '';
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array('ROLE_SUPERAGENT', $roles)) {
            $where .= ' AND a.parent_id = ' . $user->getId();
        }
        if (in_array('ROLE_VODASHOP', $roles)) {
            $where .= ' AND a.vodashop_id = ' . $user->getId();
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');
        $emConfig->addCustomDatetimeFunction('DateDiff', 'DoctrineExtensions\Query\Mysql\Date');
        $emConfig->addCustomDatetimeFunction('Now', 'DoctrineExtensions\Query\Mysql\Date');

//$emConfig->addCustomStringFunction($name, 'aimgroup\DashboardBundle\DQL');
        $emConfig->addCustomNumericFunction('FLOOR', 'aimgroup\DashboardBundle\DQL\MysqlFloor');
//$emConfig->addCustomDatetimeFunction($name, 'aimgroup\DashboardBundle\DQL');


        $result = $em->createQuery("SELECT COUNT(p) as ccount, CASE WHEN FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob , '%d-%m-%Y' ) ) /365 ) <= 10 THEN '0-10'
		WHEN FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob , '%d-%m-%Y' ) ) /365 ) >= 10 AND FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob 
, '%d-%m-%Y' ) ) /365 ) <= 20 THEN '11-20'
		WHEN FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob , '%d-%m-%Y' ) ) /365 ) >=21 AND FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob 
, '%d-%m-%Y' ) ) /365 ) <=30 THEN '21-30'
		WHEN FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob , '%d-%m-%Y' ) ) /365 ) >=31 AND FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob 
, '%d-%m-%Y' ) ) /365 ) <=40 THEN '31-40'
		WHEN FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob , '%d-%m-%Y' ) ) /365 ) >=41 AND FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob 
, '%d-%m-%Y' ) ) /365 ) <= 50 THEN '31-40'
		WHEN FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob , '%d-%m-%Y' ) ) /365 ) >=51 AND FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob 
, '%d-%m-%Y' ) ) /365 ) <=60 THEN '51-60'
		
		WHEN FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob , '%d-%m-%Y' ) ) /365 ) >=61 AND FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob 
, '%d-%m-%Y' ) ) /365 ) <=70 THEN '61-70'
		WHEN FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob , '%d-%m-%Y' ) ) /365 ) >=71 AND FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob 
, '%d-%m-%Y' ) ) /365 ) <=80 THEN '71-80'
        WHEN FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob , '%d-%m-%Y' ) ) /365 ) >=81 THEN '81+'
	  END AS ageband  from RestApiBundle:Registration p GROUP BY ageband  ORDER BY ageband desc")
            ->getArrayResult(); //getSingleResult();

        echo json_encode($result);
        exit;
    }

    /**
     * @param Request $request
     * @Route("/passwordChange", name="passwordChange")
     * @Method("POST")
     * @return JsonResponse
     */
    public function passwordChangeAction(Request $request) {

        $em = $this->getDoctrine()->getManager();

        $resp = new JsonObject();
        $status = false;
        $message = "REQUEST FAILED";
        $attributes = json_decode($request->getContent(), true);

        // $this->get("api.helper")->log("saveUserPasswordAction", $requestString);
        try {

            //echo json_encode($attributes);
            //$username = $attributes['username'];
            $old_password = $attributes[ "toptop" ];
            $password = $attributes[ "password" ];

            $user = $this->get('security.token_storage')->getToken()->getUser();

            /* $em = $this->getDoctrine()->getManager();
              $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();
              $user = $queryBuilder->select('u')
              ->from('RestApiBundle:User', 'u')
              ->where('u.username = :username')
              ->setParameter('username', $username)
              ->getQuery()->getOneOrNullResult(); */

            if ($user) {
                //if device exists update user
                /** @var  $user User */
                //$user = $this->get('security.token_storage')->getToken()->getUser();
                $encoder = $this->container->get('security.password_encoder');
                if ($encoder->isPasswordValid($user, $old_password)) {
                    $user->setPassword($password);


                    $date = new \DateTime();
                    $date->modify('+90 day');

                    $user->setExpireDate($date);

                    $encoder = $this->container->get('security.password_encoder');
                    $encoded = $encoder->encodePassword($user, $user->getPassword());
                    $user->setPassword($encoded);

                    $em->flush();
                    $status = true;
                    $message = "SUCCESS";
                } else {
                    $status = false;
                    $message = "FAILED: Mismatching | Incorrect PAssword ";
                }
            } else {
                echo "patee user";
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }
        $resp->setMessage($message);
        $resp->setStatus($status);

        //$this->get("api.helper")->log("saveUserPasswordAction", array($resp->getMessage(), $username), true);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * top_superagents Summary
     *
     * @Route("/top_superagents/{start_date}/{end_date}/{timestamp}", name="top_superagents")
     * @Method("GET")
     * @Template()
     */
    public function top_superagentsAction($start_date, $end_date, $timestamp) {
        $em = $this->getDoctrine()->getManager();

        $limit = 10;
        $where = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND u.parent_id = " . $user->getId();
        }
        if (in_array("ROLE_VODASHOP", $roles)) {
            $where .= " AND u.vodashop_id = " . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(r.createdDate) >= '" . $start_date . "' AND date(r.createdDate) <= '" . $end_date . "' ";
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

//$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "
        $query = $em->createQueryBuilder()
            ->select("u.username, CONCAT(s.firstName , ' ', s.lastName) as agent_supernames, COUNT(r.id) as registrations_count"
                . " from RestApiBundle:Registration r"
                . " JOIN r.owner u "
                . " JOIN u.parent_id s " . $where
                . " GROUP BY u.parent_id ORDER BY registrations_count DESC ");
        if (strtolower($limit) != "all") {
            $query->setMaxResults($limit);
        }
        $data2 = $query->getQuery()->getResult();


        $data = array();
        foreach ($data2 as $key => $value) {
            $data[] = array( 'name' => $value[ 'agent_supernames' ], 'count' => $value[ 'registrations_count' ] );
        }

        echo json_encode($data);
        exit;
    }

    /**
     * top_agents Summary
     *
     * @Route("/top_agents/{start_date}/{end_date}/{timestamp}", name="top_agents")
     * @Method("GET")
     * @Template()
     */
    public function top_agentsAction($start_date, $end_date, $timestamp) {
        $em = $this->getDoctrine()->getManager();

        $limit = 10;
        $where = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND u0_.parent_id = " . $user->getId();
        }
        if (in_array("ROLE_VODASHOP", $roles)) {
            $where .= " AND u0_.vodashop_id = " . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(r1_.createdDate) >= '" . date('Y-m-d', strtotime($start_date)) . "' AND date(r1_.createdDate) <= '" . date('Y-m-d', strtotime($end_date)) . "' ";
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $limit = (strtolower($limit) == "all" ? " " : " LIMIT 10");
        $query1 = "SELECT u0_.username AS username, CONCAT(u0_.first_name, ' ', u0_.last_name) AS agent_names, COUNT(r1_.id) AS registrations_count "
            . " FROM registration r1_ INNER JOIN user u0_ ON r1_.owner_id = u0_.id "
            . " {$where} "
            . " GROUP BY r1_.owner_id "
            . " ORDER BY registrations_count DESC " . $limit;

        $connection = $em->getConnection();
        $statement = $connection->prepare($query1);
        $statement->execute();
        $data2 = $statement->fetchAll();


        $data = array();
        foreach ($data2 as $key => $value) {
            $data[] = array( 'name' => $value[ 'agent_names' ], 'count' => number_format($value[ 'registrations_count' ], 0) );
        }

        echo json_encode($data);
        exit;
    }

    /**
     * View Users
     *
     * @Route("/view_agents", name="view_agents")
     * @Method("GET")
     * @Template()
     */
    public function view_agentsAction() {
        $em = $this->getDoctrine()->getManager();

//$entities = $em->getRepository('DashboardBundle:Admin')->findAll();
        $entities_user = $em->getRepository('DashboardBundle:User')->findAll();

        $data = array(
            'title'         => 'View Reports for:',
            'title_descr'   => 'View Reports for',
            'entities_user' => $entities_user,
            //'entities' => $entities,
        );

        return $this->prepareResponse($data);
    }

    /**
     * View rectifyImageCount
     *
     * @Route("/rectifyImageCount", name="rectifyImageCount")
     * @Method("GET")
     * @Template()
     */
    public function rectifyImageCountAction() {
        $em = $this->getDoctrine()->getManager();

        $data2 = $em->createQuery('SELECT count(u.id) as ccount, u.registration '
            . ' from RestApiBundle:RegImages u '
            . ' GROUP BY u.registration ')
            ->getArrayResult(); //getSingleResult();

        foreach ($data2 as $key => $value) {

            $results = $em->createQuery("UPDATE RestApiBundle:Registration u SET u.image_count = '" . $value[ 'ccount' ] . "' WHERE u.registrationid = '" . $value[ 'registration' ] . "' ")
                ->getArrayResult();
        }


        /*
          $data4 = $em->createQuery("SELECT u.id as Iid, DATE_FORMAT(s.createdDate, '%Y-%m-%d %H:%i:%s') as sDate "
          . " from RestApiBundle:RegistrationStatus u "
          . " JOIN u.registrationId s ")
          ->getArrayResult(); //getSingleResult();

          foreach ($data4 as $kkey => $vval) {
          echo "<pre>";
          print_r($vval);
          echo "</pre>";

          $newsDate = strtotime($vval['sDate']) + rand(17, 30);
          echo "|" . $newsDate = date('Y-m-d H:i:s', $newsDate) . "<br />";


          $newsDate2 = strtotime($vval['sDate']) + rand(30, 50);
          echo "|" . $newsDate2 = date('Y-m-d H:i:s', $newsDate2) . "<br />";


          $results = $em->createQuery("UPDATE RestApiBundle:RegistrationStatus u SET u.allimageDate = '" . $newsDate . "', u.fregDate = '" . $newsDate2 . "' WHERE u.id = " . $vval['Iid'] . " ")
          ->getArrayResult();
          }
         */
        exit;

        $data = array(
            'title'       => 'View Reports for:',
            'title_descr' => 'View Reports for',
            //'entities' => $entities,
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/web_registration", name="web_registration")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:web_registration.html.twig")
     */
    public function web_registrationAction() {

        if (!in_array(23, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title'       => 'Web Panel Registration',
            'title_descr' => 'Register New Customer',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/getweb_registration", name="getweb_registration")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:getweb_registration.html.twig")
     */
    public function getweb_registrationAction() {

        $em = $this->getDoctrine()->getManager();

        // Get form fields
        $fieldnamesresult = $em->createQuery('SELECT f.id, f.name '
            . ' from DashboardBundle:FieldNames f ')->getArrayResult();
        $fieldnamesArray = array();
        foreach ($fieldnamesresult as $key => $val) {
            $fieldnamesArray[ $val[ 'id' ] ] = $val[ 'name' ];
        }

        // Get form fields
        $fieldtyperesult = $em->createQuery('SELECT c.id, c.name '
            . ' from DashboardBundle:FieldTypes c ')->getArrayResult();
        $fieldtypeArray = array();
        foreach ($fieldtyperesult as $key => $val) {
            $fieldtypeArray[ $val[ 'id' ] ] = $val[ 'name' ];
        }

        $queryString = 'SELECT u.next, s.skey, s.hint, s.type, s.required, s.regex, s.regexError FROM DashboardBundle:StepField s '
            . ' JOIN s.step u '
            . 'WHERE s.step >= 1'
            . 'ORDER BY s.step ASC';
        $query = $this->getDoctrine()->getManager()
            ->createQuery($queryString);
        $resultsteps = $query->getArrayResult();

        $formsteps = array();
        foreach ($resultsteps as $key => $val) {
            if ($val[ 'next' ] == '') {
                $val[ 'next' ] = 'X' . rand(20, 40);
            }
            $val[ 'type' ] = $fieldtypeArray[ $val[ 'type' ] ];
            $val[ 'skey' ] = $fieldnamesArray[ $val[ 'skey' ] ];
            $formsteps[ $val[ 'next' ] ][] = $val;
        }

//        echo "<pre>";
//        print_r($formsteps);
//        echo "</pre>";

        echo json_encode($formsteps);
        exit;

        $data = array(
            'title'       => 'Active & Inactive',
            'title_descr' => 'View Reports for Active Inactive',
            'formsteps'   => $formsteps,
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/active_inactive", name="active_inactive")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:active_inactive.html.twig")
     */
    public function active_inactiveAction() {

        if (!in_array(53, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title'       => 'Active & Inactive',
            'title_descr' => 'View Reports for Active Inactive',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/reset_user_password", name="reset_user_password")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:reset_user_password.html.twig")
     */
    public function reset_user_passwordAction() {

        $data = array(
            'title'       => 'Reset User Password',
            'title_descr' => 'Reset User Password',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/reset_user_password_post/{username}/{password}/{timestamp}", name="reset_user_password_post")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:reset_user_password.html.twig")
     */
    public function reset_user_password_postAction($username, $password, $timestamp) {


        $em = $this->getDoctrine()->getManager();
        $user = $em->createQuery('SELECT p '
            . 'from RestApiBundle:User p '
            . " where p.username = '" . $username . "'")
            ->getOneOrNullResult();

        //echo $agents = $query->getSql(); exit; //Result();

        $data[ 'msg' ] = 'Not successfull !';
        $data[ 'status' ] = 3;


        if ($user) {
            //if device exists update user
            /** @var  $user User */
            //$user = $this->get('security.token_storage')->getToken()->getUser();
            $encoder = $this->container->get('security.password_encoder');

            $user->setPassword($password);
            $encoder = $this->container->get('security.password_encoder');
            $encoded = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($encoded);

            $em->flush();

            $data[ 'msg' ] = 'Password Changing Successful';
            $data[ 'status' ] = 1;
        } else {
            $data[ 'msg' ] = 'User Doesnt Exist. Contact your Admin';
        }

        echo json_encode($data);
        exit;

        return array(
            'title'       => 'Reset User Password',
            'title_descr' => 'Reset User Password',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/audit_reports", name="audit_reports")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:audit_reports.html.twig")
     */
    public function audit_reportsAction() {

        if (!in_array(55, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title'       => 'Audit Report',
            'title_descr' => 'View Reports for Registration Auditing',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/audit_agent_reports", name="audit_agent_reports")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:audit_agent_reports.html.twig")
     */
    public function audit_agent_reportsAction() {

        if (!in_array(23, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title'       => 'Agents Report',
            'title_descr' => 'View Reports for Agents Registration Auditing',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/audit_auditor_reports", name="audit_auditor_reports")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:audit_auditor_reports.html.twig")
     */
    public function audit_auditor_reportsAction() {

        if (!in_array(23, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title'       => 'Auditors Report',
            'title_descr' => 'View Reports for Auditor | Verifier Registration Performance',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/audit_daily_reports", name="audit_daily_reports")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:audit_daily_reports.html.twig")
     */
    public function audit_daily_reportsAction() {

        if (!in_array(23, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title'       => 'Daily Audit Count Report',
            'title_descr' => 'Daily Audit Count Report',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/audit_region_reports", name="audit_region_reports")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:audit_region_reports.html.twig")
     */
    public function audit_region_reportsAction() {

        if (!in_array(23, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title'       => 'Regions Report',
            'title_descr' => 'View Reports for Regions Registration Auditing',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }


    /**
     * hour_graph Summary
     *
     * @Route("/getRegJourneyData/{start_date}/{end_date}", name="getRegJourneyData")
     * @Method("GET")
     * @Template()
     */
    public function getRegJourneyDataAction($start_date, $end_date) {
        $em = $this->getDoctrine()->getManager();

        $query1 = "select avg(timestampdiff(MINUTE,createdDate,allimageDate)) as allimageTime, avg(timestampdiff(MINUTE,allimageDate,verifyDate)) as verifyTime, avg(timestampdiff(minute,verifyDate,fregDate)) as frgeTime, avg(timestampdiff(MINUTE,fregDate,mpesaDate)) as mpesaTime, hour(createdDate) as theHour from RegistrationStatus where verifyState = 1 and createdDate >= '" . date('Y-m-d', strtotime($start_date)) . " 00:00:00' and createdDate <= '" . date('Y-m-d', strtotime($start_date)) . " 23:59:59' group by theHour;";

        $connection = $em->getConnection();
        $statement = $connection->prepare($query1);
        $statement->execute();
        $buckets = $statement->fetchAll();


        $journeyArray = array();

        $allimageTime = array();
        $verifyTime = array();
        $frgeTime = array();
        $mpesaTime = array();


        foreach ($buckets as $key => $bucket) {
            array_push($allimageTime, array( number_format((float)$bucket[ 'theHour' ], 2, '.', ''), ($bucket[ 'allimageTime' ] >= 0 ? number_format((float)$bucket[ 'allimageTime' ], 2, '.', '') : 0) ));
            array_push($verifyTime, array( number_format((float)$bucket[ 'theHour' ], 2, '.', ''), ($bucket[ 'verifyTime' ] >= 0 ? number_format((float)$bucket[ 'verifyTime' ], 2, '.', '') : 0) ));
            array_push($frgeTime, array( number_format((float)$bucket[ 'theHour' ], 2, '.', ''), ($bucket[ 'fregTime' ] >= 0 ? number_format((float)$bucket[ 'fregTime' ], 2, '.', '') : 0) ));
            array_push($mpesaTime, array( number_format((float)$bucket[ 'theHour' ], 2, '.', ''), ($bucket[ 'mpesaTime' ] >= 0 ? number_format((float)$bucket[ 'mpesaTime' ], 2, '.', '') : 0) ));
        }

        array_push($journeyArray, array( 'key' => 'All Images Arrivez', 'values' => $allimageTime ));
        array_push($journeyArray, array( 'key' => 'Verify Time', 'values' => $verifyTime ));
        array_push($journeyArray, array( 'key' => 'ICAP Time', 'values' => $frgeTime ));
        array_push($journeyArray, array( 'key' => 'Mpesa Time', 'values' => $mpesaTime ));

        echo json_encode($journeyArray, JSON_NUMERIC_CHECK);
        exit;

        $data = array(
            'title'          => 'Active & Inactive',
            'title_descr'    => 'View Reports for Active Inactive',
            'message'        => '',
            'histcatexplong' => json_encode($journeyArray, JSON_NUMERIC_CHECK)
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/reg_journey", name="reg_journey")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:reg_journey.html.twig")
     */
    public function reg_journeyAction() {

        if (!in_array(54, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $em = $this->getDoctrine()->getManager();

        //$query = $this->container->get('fos_elastica.manager')->getRepository('RestApiBundle:RegistrationStatus')->getRegistrationJourney();
        //$result = $this->container->get('fos_elastica.index.vodacom.registration_status')->search($query);


        /*
        select avg(timestampdiff(MINUTE,createdDate,allimageDate)) as allimageTime, avg(timestampdiff(MINUTE,createdDate,verifyDate)) as verifyTime, avg(timestampdiff(minute,createdDate,fregDate)) as frgeTime, avg(timestampdiff(MINUTE,createdDate,mpesaDate)) as mpesaTime, avg(timestampdiff(MINUTE,createdDate,dbmsDate)) as dbmsTime, hour(createdDate) as theHour from RegistrationStatus where verifyState = 1 and createdDate >= '2017-11-16 00:00:00' and createdDate <= '2017-11-16 23:59:59' group by theHour;
        +--------------+------------+----------+-----------+----------+---------+
        | allimageTime | verifyTime | frgeTime | mpesaTime | dbmsTime | theHour |
        +--------------+------------+----------+-----------+----------+---------+
        |      44.1333 |    41.0667 |  44.2667 |   46.2000 |  44.2000 |       0 |
        |       9.0000 |     8.0000 |   9.0000 |   11.0000 |   9.0000 |       1 |
        |       5.0000 |     3.0000 |   5.0000 |    7.0000 |   5.0000 |       4 |
        |       3.0000 |     1.0000 |   3.0000 |    5.0000 |   3.0000 |       5 |

        {
            "key" : "Consumer Discretionary" ,
            "values" : [ [ 01 , 27.38478809681] , [ 02 , 27.371377218208] , [ 03 , 26.309915460827] , [ 04 , 26.425199957521] , [ 05 , 26.823411519395] , [ 06 , 23.850443591584] , [ 07 , 23.158355444054] , [ 08 , 22.998689393694]]
        } ,
        {
            "key" : "Consumer Staples" ,
            "values" : [ [ 01 , 7.2800122043237] , [ 02 , 7.1187787503354] , [ 03 , 8.351887016482] , [ 04 , 8.4156698763993] , [ 05 , 8.1673298604231] , [ 06 , 5.5132447126042] , [ 07 , 6.1152537710599] , [ 08 , 6.076765091942]]
        }
        */


        $query1 = "select avg(timestampdiff(MINUTE,createdDate,allimageDate)) as allimageTime, avg(timestampdiff(MINUTE,allimageDate,verifyDate)) as verifyTime, avg(timestampdiff(minute,verifyDate,fregDate)) as frgeTime, avg(timestampdiff(MINUTE,fregDate,mpesaDate)) as mpesaTime, hour(createdDate) as theHour from RegistrationStatus where verifyState = 1 and createdDate >= '" . date('Y-m-d') . " 00:00:00' and createdDate <= '" . date('Y-m-d') . " 23:59:59' group by theHour;";

        $connection = $em->getConnection();
        $statement = $connection->prepare($query1);
        $statement->execute();
        $buckets = $statement->fetchAll();


        $journeyArray = array();

        $allimageTime = array();
        $verifyTime = array();
        $frgeTime = array();
        $mpesaTime = array();


        foreach ($buckets as $key => $bucket) {
            array_push($allimageTime, array( number_format((float)$bucket[ 'theHour' ], 2, '.', ''), ($bucket[ 'allimageTime' ] >= 0 ? number_format((float)$bucket[ 'allimageTime' ], 2, '.', '') : 0) ));
            array_push($verifyTime, array( number_format((float)$bucket[ 'theHour' ], 2, '.', ''), ($bucket[ 'verifyTime' ] >= 0 ? number_format((float)$bucket[ 'verifyTime' ], 2, '.', '') : 0) ));
            array_push($frgeTime, array( number_format((float)$bucket[ 'theHour' ], 2, '.', ''), ($bucket[ 'fregTime' ] >= 0 ? number_format((float)$bucket[ 'fregTime' ], 2, '.', '') : 0) ));
            array_push($mpesaTime, array( number_format((float)$bucket[ 'theHour' ], 2, '.', ''), ($bucket[ 'mpesaTime' ] >= 0 ? number_format((float)$bucket[ 'mpesaTime' ], 2, '.', '') : 0) ));
        }

        array_push($journeyArray, array( 'key' => 'All Images Arrivez', 'values' => $allimageTime ));
        array_push($journeyArray, array( 'key' => 'Verify Time', 'values' => $verifyTime ));
        array_push($journeyArray, array( 'key' => 'ICAP Time', 'values' => $frgeTime ));
        array_push($journeyArray, array( 'key' => 'Mpesa Time', 'values' => $mpesaTime ));


        $data = array(
            'title'          => 'Active & Inactive',
            'title_descr'    => 'View Reports for Active Inactive',
            'message'        => '',
            'datepick'       => date('d/m/Y'), //10/24/1984
            'histcatexplong' => json_encode($journeyArray, JSON_NUMERIC_CHECK)
        );

        return $this->prepareResponse($data);
    }


    /**
     * View Reports
     *
     * @Route("/session_page/{timestamp}", name="session_page")
     * @Method("GET")
     */
    public function sessionPageAction($timestamp = "345") {
        $data[ 'active' ] = 1;
        echo json_encode($data);
        exit;
        $data = array(
            'title'       => 'Active & Inactive',
            'title_descr' => 'View Reports for Active Inactive',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * active_agents Summary
     *
     * @Route("/active_agents/{start_date}/{end_date}", name="active_agents")
     * @Method("GET")
     * @Template()
     */
    public function active_agentsAction($start_date, $end_date) {
        $em = $this->getDoctrine()->getManager();

        /*
         * SELECT `phone`, `users`.`first_name` as first_name, `users`.`last_name` as last_name, count(sreg_dashboard_registrations.id) as count
         * FROM (`sreg_dashboard_registrations`)
         * LEFT JOIN `users` ON `sreg_dashboard_registrations`.`created_by` = `users`.`id`
         * GROUP BY `sreg_dashboard_registrations`.`created_by`
         * ORDER BY `count` desc
         * LIMIT 10
         */

        $where = $where2 = ' WHERE 1=1 ';
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array('ROLE_SUPERAGENT', $roles)) {
            $where .= ' AND u.parent_id = ' . $user->getId();
            $where2 .= ' AND u.parent_id = ' . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(r.createdDate) >= '" . $start_date . "' AND date(r.createdDate) <= '" . $end_date . "' ";
        }

        $where .= " AND u.roles LIKE '%ROLE_AGENT%' ";
        $where2 .= " AND u.roles LIKE '%ROLE_AGENT%' ";

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

//$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "

        $query = $em->createQueryBuilder()
            ->select("COUNT(r.id) as sum_regs, CONCAT(u.firstName , ' - ', u.lastName) as agent_names, u.username, p.mobileNumber as mobileNumber, CONCAT(p.firstName , ' ', p.lastName) as SuperAgentNames, d.name as region_name, t.name as territory_name,  DATE_FORMAT(u.createdAt, '%Y-%m-%d') as created_on,  u.id as agent_id, DATE_FORMAT(r.createdDate, '%Y-%m-%d') as lastLogin, u.enabled "
                . ' from RestApiBundle:Registration r'
                . ' JOIN r.owner u '
                . ' JOIN u.parent_id p '
                . ' JOIN u.region d '
                . ' JOIN u.territory t ' . $where . ' AND u.enabled = 1 AND u.status = 1'
                . ' GROUP BY r.owner ORDER BY r.id DESC')
            ->getQuery();

        $result1 = $query->getResult();

        $data = $active = array();
        $data[ 'active' ] = array();
        foreach ($result1 as $key => $value) {
            $data[ 'active' ][] = $value;
            $active[] = $value[ 'agent_id' ];
        }


        /* if (count($active) > 0) {
          $where2 .= " AND u.id NOT in (" . implode(',', $active) . ")";
          }
          $query2 = $em->createQueryBuilder()
          ->select("CONCAT(u.firstName , ' - ', u.lastName) as agent_names, u.username, p.mobileNumber as mobileNumber, CONCAT(p.firstName , ' ', p.lastName) as SuperAgentNames, d.name as region_name, t.name as territory_name,  DATE_FORMAT(u.createdAt, '%Y-%m-%d') as created_on, DATE_FORMAT(u.lastLogin, '%Y-%m-%d') as lastLogin "
          . " from RestApiBundle:User u"
          . " JOIN u.parent_id p "
          . " JOIN u.region d "
          . " JOIN u.territory t " . $where2
          . " GROUP BY u.id")
          ->getQuery();

          $result2 = $query2->getResult();

          $data['inactive'] = array();
          foreach ($result2 as $key2 => $value2) {
          $data['inactive'][] = $value2;
          }

         */
        echo json_encode($data);
        exit;
    }

    /**
     * active_agents Summary
     *
     * @Route("/inactive_agents/{start_date}/{end_date}", name="inactive_agents")
     * @Method("GET")
     * @Template()
     */
    public function inactive_agentsAction($start_date, $end_date) {

        ini_set('memory_limit', '1024M');
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = ' WHERE 1=1 ';
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array('ROLE_SUPERAGENT', $roles)) {
            //$where .= " AND u.parent_id = " . $user->getId();
            $where2 .= ' AND u.parent_id = ' . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(r.createdDate) >= '" . $start_date . "' AND date(r.createdDate) <= '" . $end_date . "' ";
        }

        //$where .= " AND u.roles LIKE '%ROLE_AGENT%' ";
        $where2 .= " AND u.roles LIKE '%ROLE_AGENT%' ";

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $activeAgentSubQuery = $em->createQueryBuilder()
            ->select('IDENTITY(r.owner)')
            ->from('RestApiBundle:Registration', 'r')
            ->where("date(r.createdDate) >= '${start_date}' AND date(r.createdDate) <= '${end_date}'")
            ->groupBy('r.owner');

        $qb = $em->createQueryBuilder();
        $query = $qb
            ->select("CONCAT(u.firstName , ' - ', u.lastName) as agent_names", 'u.username', 'p.mobileNumber as mobileNumber', "CONCAT(p.firstName , ' ', p.lastName) as SuperAgentNames", 'd.name as region_name', 't.name as territory_name', "DATE_FORMAT(u.createdAt, '%Y-%m-%d') as created_on", "DATE_FORMAT(u.lastLogin, '%Y-%m-%d') as lastLogin", 'u.enabled', 'u.status')
            ->from('RestApiBundle:User', 'u')
            ->innerJoin('u.parent_id', 'p')
            ->innerJoin('u.region', 'd')
            ->innerJoin('u.territory', 't')
            ->innerJoin('u.parent_id', $user->getId())
            ->andWhere("u.roles LIKE '%ROLE_AGENT%'")
            ->andWhere('u.status = 1')
            ->andWhere($qb->expr()->notIn('u.id', $activeAgentSubQuery->getDQL()))
            ->groupBy('u.id')
            ->getQuery();

        //$sql = "SELECT CONCAT(u0_.first_name, ' - ', u0_.last_name) AS sclr_0, u0_.username AS username_1, u1_.mobile_number AS mobile_number_2, CONCAT(u1_.first_name, ' ', u1_.last_name) AS sclr_3, r2_.name AS name_4, t3_.name AS name_5, DATE_FORMAT (u0_.created_at,'%Y-%m-%d') AS sclr_6, DATE_FORMAT (u0_.last_login,'%Y-%m-%d') AS sclr_7, u0_.enabled AS enabled_8 FROM user u0_ INNER JOIN user u1_ ON u0_.parent_id_id = u1_.id INNER JOIN Region r2_ ON u0_.region_id = r2_.id INNER JOIN Territory t3_ ON u0_.territory_id = t3_.id WHERE 1 = 1 AND u0_.roles LIKE '%ROLE_AGENT%' AND u0_.id NOT IN (SELECT u0_.id AS id_0 FROM registration r1_ INNER JOIN user u0_ ON r1_.owner_id = u0_.id WHERE 1 = 1 AND date(r1_.createdDate) >= '".$start_date."' AND date(r1_.createdDate) <= '".$end_date."' AND u0_.roles LIKE '%ROLE_AGENT%' GROUP BY r1_.owner_id ORDER BY r1_.id DESC) AND u0_.enabled = 1 GROUP BY u0_.id;";
        //$stmt = $em->getConnection()->prepare($sql);
        //$stmt->execute();
        //$result2 = $stmt->fetchAll();

        $result = $query->getResult();
        $data[ 'inactive' ] = $result; //array();
        /* foreach ($result as $key2 => $value2) {
          $data['inactive'][] = $value2;
          } */


        echo json_encode($data);
        exit;
    }

    /**
     * active_agents Summary
     *
     * @Route("/audit_daily_report/{start_date}/{end_date}/{timestamp}", name="audit_daily_report")
     * @Method("GET")
     * @Template()
     */
    public function audit_daily_reportAction($start_date, $end_date, $timestamp) {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = ' WHERE 1=1 ';
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array('ROLE_SUPERAGENT', $roles)) {
            $where .= ' AND u.parent_id = ' . $user->getId();
            $where2 .= ' AND u.parent_id = ' . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(s.auditDate) >= '" . $start_date . "' AND date(s.auditDate) <= '" . $end_date . "' ";
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

//$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "

        $query = $em->createQueryBuilder()
            ->select("s.id as sid, COUNT(r.id) as sum_regs, DATE_FORMAT(s.auditDate, '%Y-%m-%d') as created_on, s.auditState "
                . ' from RestApiBundle:RegistrationStatus s'
                . ' JOIN s.registrationId r '
                . ' JOIN r.owner u ' . $where
                . ' GROUP BY s.auditDate, s.auditState ORDER BY sum_regs DESC')
            ->getQuery();

        $result1 = $query->getResult();

        $data = $active = array();
        foreach ($result1 as $key => $value) {
            $data[ $value[ 'created_on' ] ] = (@$data[ $value[ 'created_on' ] ] ? $data[ $value[ 'created_on' ] ] : $value);

            $data[ $value[ 'created_on' ] ][ 'sum_total' ] = @$data[ $value[ 'created_on' ] ][ 'sum_total' ] + $value[ 'sum_regs' ];
            $data[ $value[ 'created_on' ] ][ 'sum_good' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_good' ] ? $data[ $value[ 'created_on' ] ][ 'sum_good' ] : 0);
            $data[ $value[ 'created_on' ] ][ 'sum_fair' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_fair' ] ? $data[ $value[ 'created_on' ] ][ 'sum_fair' ] : 0);
            $data[ $value[ 'created_on' ] ][ 'sum_bad' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_bad' ] ? $data[ $value[ 'created_on' ] ][ 'sum_bad' ] : 0);
            $data[ $value[ 'created_on' ] ][ 'sum_unrated' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_unrated' ] ? $data[ $value[ 'created_on' ] ][ 'sum_unrated' ] : 0);

            if ($value[ 'auditState' ] == 3) {
                $data[ $value[ 'created_on' ] ][ 'sum_total_rated' ] = @$data[ $value[ 'created_on' ] ][ 'sum_total_rated' ] + $value[ 'sum_regs' ];
                $data[ $value[ 'created_on' ] ][ 'sum_good' ] = $data[ $value[ 'created_on' ] ][ 'sum_good' ] + $value[ 'sum_regs' ];
            } elseif ($value[ 'auditState' ] == 2) {
                $data[ $value[ 'created_on' ] ][ 'sum_total_rated' ] = @$data[ $value[ 'created_on' ] ][ 'sum_total_rated' ] + $value[ 'sum_regs' ];
                $data[ $value[ 'created_on' ] ][ 'sum_fair' ] = $data[ $value[ 'created_on' ] ][ 'sum_fair' ] + $value[ 'sum_regs' ];
            } elseif ($value[ 'auditState' ] == 1) {
                $data[ $value[ 'created_on' ] ][ 'sum_total_rated' ] = @$data[ $value[ 'created_on' ] ][ 'sum_total_rated' ] + $value[ 'sum_regs' ];
                $data[ $value[ 'created_on' ] ][ 'sum_bad' ] = $data[ $value[ 'created_on' ] ][ 'sum_bad' ] + $value[ 'sum_regs' ];
            } else {
                $data[ $value[ 'created_on' ] ][ 'sum_unrated' ] = $data[ $value[ 'created_on' ] ][ 'sum_unrated' ] + $value[ 'sum_regs' ];
            }
        }

        echo json_encode($data);
        exit;
    }

    function runQueryaudit_agents_report($where, $limit, $start) {
        $em = $this->getDoctrine()->getManager();

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        //$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "

        $query = $em->createQueryBuilder()
            ->select(" u.id as sid, COUNT(r.id) as sum_regs, t.name as territory_name, d.name as region_name, CONCAT(u.firstName , ' ', u.lastName) as agent_names, u.username, DATE_FORMAT(s.auditDate, '%Y-%m-%d') as created_on, s.auditState "
                . ' from RestApiBundle:RegistrationStatus s'
                . ' JOIN s.registrationId r '
                . ' JOIN r.owner u '
                . ' JOIN u.region d '
                . ' JOIN u.territory t ' . $where
                . ' GROUP BY u.id, s.auditState ORDER BY sum_regs DESC')
            ->getQuery();

        $query->setMaxResults($limit);
        $query->setFirstResult($start * $limit);

        return $query->getResult();
    }

    /**
     * active_agents Summary
     *
     * @Route("/audit_agents_report/{start_date}/{end_date}", name="audit_agents_report")
     * @Method("GET")
     * @Template()
     */
    public function audit_agents_reportAction($start_date, $end_date) {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = ' WHERE 1=1 ';
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array('ROLE_SUPERAGENT', $roles)) {
            $where .= ' AND u.parent_id = ' . $user->getId();
            $where2 .= ' AND u.parent_id = ' . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(r.createdDate) >= '" . $start_date . "' AND date(r.createdDate) <= '" . $end_date . "' ";
        }

        $start = 0;
        $check_continue = true;


        $data = $active = array();
        $data[ 'active' ] = array();

        while ($check_continue) {

            $result1 = $this->runQueryaudit_agents_report($where, 5000, $start);

            if (count($result1) > 1) {

                foreach ($result1 as $key => $value) {

                    $data[ 'active' ][ $value[ 'sid' ] ] = (@$data[ 'active' ][ $value[ 'sid' ] ] ? $data[ 'active' ][ $value[ 'sid' ] ] : $value);


                    $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_total' ] = @$data[ 'active' ][ $value[ 'sid' ] ][ 'sum_total' ] + $value[ 'sum_regs' ];
                    $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_good' ] = (@$data[ 'active' ][ $value[ 'sid' ] ][ 'sum_good' ] ? $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_good' ] : 0);
                    $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_fair' ] = (@$data[ 'active' ][ $value[ 'sid' ] ][ 'sum_fair' ] ? $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_fair' ] : 0);
                    $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_bad' ] = (@$data[ 'active' ][ $value[ 'sid' ] ][ 'sum_bad' ] ? $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_bad' ] : 0);
                    $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_unrated' ] = (@$data[ 'active' ][ $value[ 'sid' ] ][ 'sum_unrated' ] ? $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_unrated' ] : 0);
                    $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_rated' ] = (@$data[ 'active' ][ $value[ 'sid' ] ][ 'sum_rated' ] ? $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_rated' ] : 0);

                    if ($value[ 'auditState' ] == 3) {
                        $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_good' ] = $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_good' ] + $value[ 'sum_regs' ];

                        $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_rated' ] = $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_rated' ] + $value[ 'sum_regs' ];
                    } elseif ($value[ 'auditState' ] == 2) {
                        $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_fair' ] = $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_fair' ] + $value[ 'sum_regs' ];

                        $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_rated' ] = $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_rated' ] + $value[ 'sum_regs' ];
                    } elseif ($value[ 'auditState' ] == 1) {
                        $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_bad' ] = $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_bad' ] + $value[ 'sum_regs' ];

                        $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_rated' ] = $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_rated' ] + $value[ 'sum_regs' ];
                    } else {
                        $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_unrated' ] = $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_unrated' ] + $value[ 'sum_regs' ];
                    }
                }
            } else {
                $check_continue = false;
            }

            $start = $start + 1;
        }

        echo json_encode($data);
        exit;
    }

    /**
     * active_agents Summary
     *
     * @Route("/audit_region_report/{start_date}/{end_date}/{timestamp}", name="audit_region_report")
     * @Method("GET")
     * @Template()
     */
    public function audit_region_reportAction($start_date, $end_date, $timestamp) {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = ' WHERE 1=1 ';
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array('ROLE_SUPERAGENT', $roles)) {
            $where .= ' AND u.parent_id = ' . $user->getId();
            //$where.= " AND u.parent_id = " . $user->getId();
        }
        if ($start_date != 1) { //echo "sdfsdf"; exit;
            $where .= " AND date(r.createdDate) >= '" . $start_date . "' AND date(r.createdDate) <= '" . $end_date . "' ";
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

//$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "

        $query = $em->createQueryBuilder()
            ->select("s.id as sid, COUNT(r.id) as sum_regs, t.name as territory_name, d.name as region_name, DATE_FORMAT(s.auditDate, '%Y-%m-%d') as created_on, s.auditState as auditState "
                . ' from RestApiBundle:RegistrationStatus s'
                . ' JOIN s.registrationId r '
                . ' JOIN r.owner u '
                . ' JOIN u.region d '
                . ' JOIN u.territory t ' . $where
                . ' GROUP BY region_name, territory_name, auditState ORDER BY sum_regs DESC')
            ->getQuery();

        $result1 = $query->getResult();

        $data = $active = array();
        foreach ($result1 as $key => $value) {
            $data[ $value[ 'region_name' ] ] = (@$data[ $value[ 'region_name' ] ] ? $data[ $value[ 'region_name' ] ] : $value);

            //first append total, good faor bad to array as zero
            $data[ $value[ 'region_name' ] ][ 'sum_total' ] = @$data[ $value[ 'region_name' ] ][ 'sum_total' ] + $value[ 'sum_regs' ];
            $data[ $value[ 'region_name' ] ][ 'sum_good' ] = (@$data[ $value[ 'region_name' ] ][ 'sum_good' ] ? $data[ $value[ 'region_name' ] ][ 'sum_good' ] : 0);
            $data[ $value[ 'region_name' ] ][ 'sum_fair' ] = (@$data[ $value[ 'region_name' ] ][ 'sum_fair' ] ? $data[ $value[ 'region_name' ] ][ 'sum_fair' ] : 0);
            $data[ $value[ 'region_name' ] ][ 'sum_bad' ] = (@$data[ $value[ 'region_name' ] ][ 'sum_bad' ] ? $data[ $value[ 'region_name' ] ][ 'sum_bad' ] : 0);
            $data[ $value[ 'region_name' ] ][ 'sum_unrated' ] = (@$data[ $value[ 'region_name' ] ][ 'sum_unrated' ] ? $data[ $value[ 'region_name' ] ][ 'sum_unrated' ] : 0);
            $data[ $value[ 'region_name' ] ][ 'sum_rated' ] = (@$data[ $value[ 'region_name' ] ][ 'sum_rated' ] ? $data[ $value[ 'region_name' ] ][ 'sum_rated' ] : 0);

            //now fill in the data
            if ($value[ 'auditState' ] == 3) {
                $data[ $value[ 'region_name' ] ][ 'sum_good' ] = $data[ $value[ 'region_name' ] ][ 'sum_good' ] + $value[ 'sum_regs' ];

                $data[ $value[ 'region_name' ] ][ 'sum_rated' ] = $data[ $value[ 'region_name' ] ][ 'sum_rated' ] + $value[ 'sum_regs' ];
            } elseif ($value[ 'auditState' ] == 2) {
                $data[ $value[ 'region_name' ] ][ 'sum_fair' ] = $data[ $value[ 'region_name' ] ][ 'sum_fair' ] + $value[ 'sum_regs' ];

                $data[ $value[ 'region_name' ] ][ 'sum_rated' ] = $data[ $value[ 'region_name' ] ][ 'sum_rated' ] + $value[ 'sum_regs' ];
            } elseif ($value[ 'auditState' ] == 1) {
                $data[ $value[ 'region_name' ] ][ 'sum_bad' ] = $data[ $value[ 'region_name' ] ][ 'sum_bad' ] + $value[ 'sum_regs' ];

                $data[ $value[ 'region_name' ] ][ 'sum_rated' ] = $data[ $value[ 'region_name' ] ][ 'sum_rated' ] + $value[ 'sum_regs' ];
            } else {
                $data[ $value[ 'region_name' ] ][ 'sum_unrated' ] = $data[ $value[ 'region_name' ] ][ 'sum_unrated' ] + $value[ 'sum_regs' ];
            }
        }

        echo json_encode($data);
        exit;
    }

    /**
     * active_agents Summary
     *
     * @Route("/audit_auditor_report/{start_date}/{end_date}/{timestamp}", name="audit_auditor_report")
     * @Method("GET")
     * @Template()
     */
    public function audit_auditor_reportAction($start_date, $end_date, $timestamp) {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = ' WHERE 1=1 ';
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array('ROLE_SUPERAGENT', $roles)) {
            $where .= ' AND u.parent_id = ' . $user->getId();
            $where2 .= ' AND u.parent_id = ' . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(s.auditDate) >= '" . $start_date . "' AND date(s.auditDate) <= '" . $end_date . "' ";
        }

        $where .= ' AND s.auditState in (1,2,3)';

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

//$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "

        $query = $em->createQueryBuilder()
            ->select("u.id as sid, COUNT(s.id) as sum_regs, CONCAT(u.firstName , ' ', u.lastName) as agent_names, u.username, s.auditState "
                . ' from RestApiBundle:RegistrationStatus s'
                . ' JOIN s.auditBy u ' . $where
                . ' GROUP BY s.auditBy, s.auditState ORDER BY sum_regs DESC')
            ->getQuery();

        $result1 = $query->getResult();

        $data = $active = array();
        $data[ 'active' ] = array();
        foreach ($result1 as $key => $value) {
            $data[ 'active' ][ $value[ 'sid' ] ] = (@$data[ 'active' ][ $value[ 'sid' ] ] ? $data[ 'active' ][ $value[ 'sid' ] ] : $value);

            $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_total' ] = @$data[ 'active' ][ $value[ 'sid' ] ][ 'sum_total' ] + $value[ 'sum_regs' ];
            $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_good' ] = (@$data[ 'active' ][ $value[ 'sid' ] ][ 'sum_good' ] ? $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_good' ] : 0);
            $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_fair' ] = (@$data[ 'active' ][ $value[ 'sid' ] ][ 'sum_fair' ] ? $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_fair' ] : 0);
            $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_bad' ] = (@$data[ 'active' ][ $value[ 'sid' ] ][ 'sum_bad' ] ? $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_bad' ] : 0);
            $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_unrated' ] = (@$data[ 'active' ][ $value[ 'sid' ] ][ 'sum_unrated' ] ? $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_unrated' ] : 0);

            if ($value[ 'auditState' ] == 3) {
                $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_good' ] = $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_good' ] + $value[ 'sum_regs' ];
            } elseif ($value[ 'auditState' ] == 2) {
                $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_fair' ] = $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_fair' ] + $value[ 'sum_regs' ];
            } elseif ($value[ 'auditState' ] == 1) {
                $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_bad' ] = $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_bad' ] + $value[ 'sum_regs' ];
            } else {
                $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_unrated' ] = $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_unrated' ] + $value[ 'sum_regs' ];
            }
        }

        echo json_encode($data);
        exit;
    }

    /**
     * verify_agents_report Summary
     *
     * @Route("/verify_agents_report/{start_date}/{end_date}/{timestamp}", name="verify_agents_report")
     * @Method("GET")
     * @Template()
     */
    public function verify_agents_reportAction($start_date, $end_date, $timestamp) {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = ' WHERE 1=1 ';
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array('ROLE_SUPERAGENT', $roles)) {
            $where .= ' AND registration.parent_id = ' . $user->getId();
            $where2 .= ' AND registration.parent_id = ' . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND RegistrationStatus.createdDate >= '" . $start_date . " 00:00:00' AND RegistrationStatus.createdDate <= '" . $end_date . " 23:59:59' ";
        }
        //$where .= " AND s.verifyState in (1,2,3) ";

        $start = 0;
        $check_continue = true;


        $data = $active = array();
        $data[ 'active' ] = array();

        /*
          select user.first_name, user.last_name, msisdn, Region.name as RegionName, Territory.name as TerritoryName, count(*) as Total, sum(IF(RegistrationStatus.verifyState not in (0, -1), 1, 0)) AS Rated, sum(IF(RegistrationStatus.verifyState in (0, -1), 1, 0)) AS Unrated, sum(IF(RegistrationStatus.verifyState = 1, 1, 0)) AS RegGood, sum(IF(RegistrationStatus.verifyState in (2, -2), 1, 0)) AS RegFair, sum(IF(RegistrationStatus.verifyState in (3, -3), 1, 0)) AS RegBad from registration left join Region on Region.code = registration.region left join Territory on Territory.id = registration.territory left join user on user.id = registration.owner_id left join RegistrationStatus on registration.id = RegistrationStatus.registrationId group by owner_id
         */

        $em = $this->getDoctrine()->getManager();

        $result1 = array();

        $query1 = "select user.first_name as FirstName, user.last_name AS LastName, msisdn AS MSISDN, user.region_id as RegionName, user.territory_id as TerritoryName, count(*) as Total, sum(IF(RegistrationStatus.verifyState not in (0, -1, 10), 1, 0)) AS Verified, sum(IF(RegistrationStatus.verifyState in (0, -1), 1, 0)) AS Unverified, sum(IF(RegistrationStatus.verifyState = 1, 1, 0)) AS RegGood, sum(IF(RegistrationStatus.verifyState in (2, -2), 1, 0)) AS RegFair, sum(IF(RegistrationStatus.verifyState in (3, -3), 1, 0)) AS RegBad from registration left join user on user.id = registration.owner_id left join RegistrationStatus on registration.id = RegistrationStatus.registrationId " . $where . " group by owner_id";

        $connection = $em->getConnection();
        $statement = $connection->prepare($query1);
        $statement->execute();
        $result1 = $statement->fetchAll();

        echo json_encode($result1);
        exit;
    }

    /**
     * View Reports
     *
     * @Route("/reports", name="reports")
     * @Method("GET")
     * @Template()
     */
    public function reportsAction() {

        if (!in_array(23, json_decode($this->session->get('user_role_perms'), true))) {
            //return $this->redirect($this->generateUrl('admin'));
        }

        /*
          $response = new StreamedResponse();
          $response->setCallback(function() {

          $em = $this->getDoctrine()->getManager();
          $handle = fopen('php://output', 'w+');

          // Add the header of the CSV file
          fputcsv($handle, array('Name', 'Surname', 'Age', 'Sex'), ',');
          // Query data from database
          $results = $em->createQuery("SELECT p.firstName, p.lastName, p.region, p.territory from RestApiBundle:Registration p ")
          ->getArrayResult();
          // Add the data queried from database
          foreach ($results as $key => $row) {
          fputcsv(
          $handle, // The file pointer
          array($row['firstName'], $row['lastName'], $row['region'], $row['territory']), // The fields
          ',' // The delimiter
          );
          }

          fclose($handle);
          });

          $response->setStatusCode(200);
          $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
          $response->headers->set('Content-Disposition', 'attachment; filename="export.csv"');

          return $response;

         */

        /*
          //--....... Sending Email
          // send random password to the email
          //modify mailer settings
          $transport = \Swift_SmtpTransport::newInstance($this->container->getParameter('mailer_host'), $this->container->getParameter('mailer_port'), 'ssl')
          ->setUsername($this->container->getParameter('mailer_user'))
          ->setPassword($this->container->getParameter('mailer_password'));

          // send random password to the email
          $message = \Swift_Message::newInstance()
          ->setSubject('E-reg 2.0 Report | 2015-11-20')
          ->setFrom($this->container->getParameter('system_email_address'))
          ->setTo("mutisya06@gmail.com")
          ->setBody(
          $this->renderView(
          // app/Resources/views/Emails/registration.html.twig
          'emails/incoming_report.html.twig', array('name' => "mutisyaaaaa",)
          ), 'text/html'
          );

          try {
          $mailer = \Swift_Mailer::newInstance($transport);
          $mailer->send($message);
          } catch (\Exception $exception) {
          #var_dump($exception->getMessage());
          }

          //--.......
         */
        $data = array(
            'title'       => 'Generate Reports',
            'title_descr' => 'View Reports for',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View agentperformance
     *
     * @Route("/agentperformance", name="agentperformance")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:agentperformance.html.twig")
     */
    public function agentperformanceAction() {

        $data = array(
            'title'       => 'Agent Registrations',
            'title_descr' => 'View Reports for Agent Registrations',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * Creates a new Faq entity.
     *
     * @Route("/newreportspost", name="newreportspost")
     * @Method("POST")
     * @Template("DashboardBundle:Admin:reports.html.twig")
     */
    public function newreportspostAction(Request $request) {

        global $post;
        $post = $request;

        $ttitle = false;

        $em = $this->getDoctrine()->getManager();
        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $where_clause = $where_clause_agents = ' 1=1 ';

        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles


        if (in_array('ROLE_SUPERAGENT', $roles)) {
            if ($post->request->get('report_type') == 'agent_registration') {
                $where_clause .= ' AND u.parent_id_id = ' . $user->getId();
            } else {
                $where_clause .= ' AND u.parent_id = ' . $user->getId();
                $where_clause_agents .= ' AND u.parent_id = ' . $user->getId();
            }
        }

        if (in_array('ROLE_VODASHOP', $roles)) {
            $where_clause .= ' AND u.vodashop_id = ' . $user->getId();
            $where_clause_agents .= ' AND u.vodashop_id = ' . $user->getId();
        }

        if ($post->request->get('region')) {
            $where_clause .= " AND p.region = '" . $post->request->get('region') . "'";
            $where_clause_agents .= " AND u.region = '" . $post->request->get('region') . "' ";
        }
        if ($post->request->get('territory')) {
            $where_clause .= ' AND p.territory = ' . $post->request->get('territory');
            $where_clause_agents .= ' AND u.territory = ' . $post->request->get('territory');
        }

        if ($post->request->get('report_type') != 'fullreg_report') {
            if ($post->request->get('from_date')) {
                $where_clause .= " AND date(p.createdDate) >= '" . date('Y-m-d', strtotime($post->request->get('from_date'))) . "'";
            }
            if ($post->request->get('to_date')) {
                $where_clause .= " AND date(p.createdDate) <= '" . date('Y-m-d', strtotime($post->request->get('to_date'))) . "'";
            }
        }
        if ($post->request->get('customer_msisdn')) {//----customer msisdn
            //$where_clause.= " AND p.msisdn = " . $post->request->get('customer_msisdn');
        }
        if ($post->request->get('agent_msisdn')) {//----customer msisdn
            $where_clause .= " AND u.username = '" . $post->request->get('agent_msisdn') . "'";
        }

        //$where_clause .= " AND u.status =1 AND u.enabled =1";
        //$handle = fopen('php://output', 'w+');

        $regionsAll = $this->getRegionsArrayCodeName();
        $teritoriesAll = $this->getTerritoryArrayIdName();
        $columnnames = array();

        //echo $post->request->get('report_type').":"; exit;
        switch ($post->request->get('report_type')) {
            case 'fullreg_report':

                if ($post->request->get('from_date')) {
                    $where_clause .= " AND date(r3_.fregDate) >= '" . date('Y-m-d', strtotime($post->request->get('from_date'))) . "'";
                }
                if ($post->request->get('to_date')) {
                    $where_clause .= " AND date(r3_.fregDate) <= '" . date('Y-m-d', strtotime($post->request->get('to_date'))) . "'";
                }


                $columnnames = array( 'CREATEDDATE', 'MSISDN', 'CUSTOMERNAME', 'GENDER', 'IDNUMBER', 'IDTYPE','TYPE', 'AGENTMOBILE', 'AGENTNAME', 'SUPERAGENTNAME', 'REGIONNAME', 'TERRITORYNAME', 'DEVICEMODEL', 'REGTYPE', 'MPESASTATE', 'ICAP_STATE', 'DBMS_STATE', 'EKYCSTATUS', 'TEMPREGSTATUS', 'TEMPREGDESCRIPTION', 'FULLREGSTATUS', 'FULLREGDESCR', 'FREGDATE', 'IMAGECOUNT' );

                $query = "SELECT DATE(p.createdDate) AS CREATEDDATE, p.msisdn AS MSISDN, CONCAT(p.firstName, ' ', p.lastName) AS CUSTOMERNAME, p.gender, IFNULL(p.identification, ' ') AS IDNUMBER, IFNULL(a1_.name, ' ') AS IDTYPE,CASE p.registration_type WHEN '2' THEN 'Biometric' ELSE 'Standard' END AS TYPE, IFNULL(u.username, ' ') AS AGENTMOBILE, CONCAT(u.first_name, ' ', u.last_name) AS AGENTNAME, CONCAT(u2_.first_name, ' ', u2_.last_name) AS SUPERAGENTNAME, IFNULL(r5_.name, ' ') as REGIONNAME, IFNULL(t3_.name, ' ') AS TERRITORYNAME, IFNULL(p.deviceModel, ' ') AS DEVICEMODEL, CASE r3_.reg_type WHEN '0' THEN 'NewReg' WHEN '1' THEN 'NewReg' WHEN '2' THEN 'Re-Reg' ELSE 'NewReg' END AS REGTYPE, CASE r3_.mpesa_state WHEN '0' THEN 'Pending' WHEN '1' THEN 'Successful' WHEN '2' THEN 'Failed' WHEN '3' THEN 'Already Registered' ELSE 'Pending' END AS MPESASTATE, CASE r3_.icap_state WHEN '0' THEN 'Pending' WHEN '4' THEN 'Failed' WHEN '6' THEN 'Success' WHEN '7' THEN 'Success' ELSE 'Pending' END AS ICAP_STATE, CASE r3_.dbms_state WHEN '0' THEN 'Pending' WHEN '4' THEN 'Failed' WHEN '6' THEN 'Success' WHEN '7' THEN 'Success' ELSE 'Pending' END AS DBMS_STATE, CASE CONCAT(r3_.temporaryRegStatus, '', r3_.fullRegStatus) WHEN '20' THEN 'TREG' WHEN '60' THEN 'TREG' WHEN '70' THEN 'TREG' WHEN '24' THEN 'TREG' WHEN '64' THEN 'TREG' WHEN '74' THEN 'TREG' WHEN '22' THEN 'FREG' WHEN '26' THEN 'FREG' WHEN '27' THEN 'FREG' WHEN '62' THEN 'FREG' WHEN '66' THEN 'FREG' WHEN '67' THEN 'FREG' WHEN '72' THEN 'FREG' WHEN '76' THEN 'FREG' WHEN '77' THEN 'FREG' WHEN '07' THEN 'FREG' WHEN '17' THEN 'FREG' WHEN '06' THEN 'FREG' WHEN '16' THEN 'FREG' WHEN '26' THEN 'FREG' WHEN '40' THEN 'DECLINED' WHEN '30' THEN 'DECLINED' WHEN '40' THEN 'DECLINED' WHEN '00' THEN 'PENDING' ELSE 'Pending' END AS EKYCSTATUS, CASE r3_.temporaryRegStatus WHEN '0' THEN 'Pending' WHEN '1' THEN 'Pending' WHEN '2' THEN 'Success' WHEN '3' THEN 'Declined' WHEN '4' THEN 'Fail' WHEN '5' THEN 'Retry' WHEN '6' THEN 'SuccessICAP' WHEN '7' THEN 'SuccessDBMS' ELSE 'Pending' END AS TEMPREGSTATUS, REPLACE(IFNULL(r3_.temporaryRegDesc, ' '), ',', ' ') AS TEMPREGDESCRIPTION, CASE r3_.fullRegStatus WHEN '0' THEN 'Pending' WHEN '1' THEN 'Pending' WHEN '2' THEN 'Success' WHEN '3' THEN 'Declined' WHEN '4' THEN 'Fail' WHEN '5' THEN 'Retry' WHEN '6' THEN 'SuccessICAP' WHEN '7' THEN 'SuccessDBMS' ELSE 'Pending' END AS FULLREGSTATUS, REPLACE(IFNULL(r3_.fullRegDesc, ' '), ',', ' ') AS FULLREGDESCR, DATE(r3_.fregDate) as FREGDATE, p.image_count AS IMAGECOUNT, CASE r3_.auditState WHEN '0' THEN 'NotAudited' WHEN '1' THEN 'Bad' WHEN '2' THEN 'Fair' WHEN '3' THEN 'Good' WHEN '-1' THEN 'Locked' ELSE 'NotAudited' END AS AUDITSTATE, CONCAT(u3_.first_name, ' ', u3_.last_name) as AUDITORNAME, REPLACE(r3_.auditDescr, ',', ' AND ') AS AUDITDESCR, DATE(r3_.auditDate) as AUDITDATE FROM RegistrationStatus r3_ LEFT JOIN registration p ON r3_.registrationId = p.id LEFT JOIN user u ON p.owner_id = u.id LEFT JOIN super_agent s4_ ON u.id = s4_.id LEFT JOIN agent a5_ ON u.id = a5_.id LEFT JOIN user u2_ ON u.parent_id_id = u2_.id LEFT JOIN super_agent s6_ ON u2_.id = s6_.id LEFT JOIN Region r5_ on r5_.code = p.region LEFT JOIN agent a7_ ON u2_.id = a7_.id LEFT JOIN Idtype a1_ on a1_.id = p.identificationType LEFT JOIN Territory t3_ on t3_.id = p.territory LEFT JOIN user u3_ ON r3_.auditBy = u3_.id WHERE " . $where_clause;

                // $query = "SELECT DATE(p.createdDate) AS CREATEDDATE, p.msisdn AS MSISDN, CONCAT(p.firstName, ' ', p.lastName) AS CUSTOMERNAME, IFNULL(p.identification, ' ') AS IDNUMBER, IFNULL(a1_.name, ' ') AS IDTYPE, IFNULL(u.username, ' ') AS AGENTMOBILE, CONCAT(u.first_name, ' ', u.last_name) AS AGENTNAME, CONCAT(u2_.first_name, ' ', u2_.last_name) AS SUPERAGENTNAME, IFNULL(r5_.name, ' ') as REGIONNAME, IFNULL(t3_.name, ' ') AS TERRITORYNAME, IFNULL(p.deviceModel, ' ') AS DEVICEMODEL, CASE r3_.reg_type WHEN '0' THEN 'NewReg' WHEN '1' THEN 'NewReg' WHEN '2' THEN 'Re-Reg' ELSE 'NewReg' END AS REGTYPE, CASE r3_.mpesa_state WHEN '0' THEN 'Pending' WHEN '1' THEN 'Successful' WHEN '2' THEN 'Failed' WHEN '3' THEN 'Already Registered' ELSE 'Pending' END AS MPESASTATE, CASE r3_.icap_state WHEN '0' THEN 'Pending' WHEN '4' THEN 'Failed' WHEN '6' THEN 'Success' WHEN '7' THEN 'Success' ELSE 'Pending' END AS ICAP_STATE, CASE r3_.dbms_state WHEN '0' THEN 'Pending' WHEN '4' THEN 'Failed' WHEN '6' THEN 'Success' WHEN '7' THEN 'Success' ELSE 'Pending' END AS DBMS_STATE, CASE CONCAT(r3_.temporaryRegStatus, '', r3_.fullRegStatus) WHEN '20' THEN 'TREG' WHEN '60' THEN 'TREG' WHEN '70' THEN 'TREG' WHEN '24' THEN 'TREG' WHEN '64' THEN 'TREG' WHEN '74' THEN 'TREG' WHEN '22' THEN 'FREG' WHEN '26' THEN 'FREG' WHEN '27' THEN 'FREG' WHEN '62' THEN 'FREG' WHEN '66' THEN 'FREG' WHEN '67' THEN 'FREG' WHEN '72' THEN 'FREG' WHEN '76' THEN 'FREG' WHEN '77' THEN 'FREG' WHEN '07' THEN 'FREG' WHEN '17' THEN 'FREG' WHEN '06' THEN 'FREG' WHEN '16' THEN 'FREG' WHEN '26' THEN 'FREG' WHEN '40' THEN 'DECLINED' WHEN '30' THEN 'DECLINED' WHEN '40' THEN 'DECLINED' WHEN '00' THEN 'PENDING' ELSE 'Pending' END AS EKYCSTATUS, CASE r3_.temporaryRegStatus WHEN '0' THEN 'Pending' WHEN '1' THEN 'Pending' WHEN '2' THEN 'Success' WHEN '3' THEN 'Declined' WHEN '4' THEN 'Fail' WHEN '5' THEN 'Retry' WHEN '6' THEN 'SuccessICAP' WHEN '7' THEN 'SuccessDBMS' ELSE 'Pending' END AS TEMPREGSTATUS, REPLACE(IFNULL(r3_.temporaryRegDesc, ' '), ',', ' ') AS TEMPREGDESCRIPTION, CASE r3_.fullRegStatus WHEN '0' THEN 'Pending' WHEN '1' THEN 'Pending' WHEN '2' THEN 'Success' WHEN '3' THEN 'Declined' WHEN '4' THEN 'Fail' WHEN '5' THEN 'Retry' WHEN '6' THEN 'SuccessICAP' WHEN '7' THEN 'SuccessDBMS' ELSE 'Pending' END AS FULLREGSTATUS, REPLACE(IFNULL(r3_.fullRegDesc, ' '), ',', ' ') AS FULLREGDESCR, p.image_count AS IMAGECOUNT, " . $additional_columname. " FROM RegistrationStatus r3_ LEFT JOIN registration p ON r3_.registrationId = p.id LEFT JOIN user u ON p.owner_id = u.id LEFT JOIN super_agent s4_ ON u.id = s4_.id LEFT JOIN agent a5_ ON u.id = a5_.id LEFT JOIN user u2_ ON u.parent_id_id = u2_.id LEFT JOIN super_agent s6_ ON u2_.id = s6_.id LEFT JOIN Region r5_ on r5_.code = p.region LEFT JOIN agent a7_ ON u2_.id = a7_.id LEFT JOIN Idtype a1_ on a1_.id = p.identificationType LEFT JOIN Territory t3_ on t3_.id = p.territory LEFT JOIN user u3_ ON r3_.auditBy = u3_.id WHERE " . $where_clause;


                break;
            case 'agent_registration':

                $addleftjoin = '';
                $columnnames = array( 'CREATEDDATE', 'MSISDN', 'CUSTOMERNAME', 'GENDER', 'IDNUMBER', 'IDTYPE','TYPE' ,'NATIONALITY', 'DOB', 'ALTERNATIVEMSISDN', 'AGENTMOBILE', 'AGENTNAME', 'SUPERAGENTNAME', 'REGIONNAME', 'TERRITORYNAME', 'DEVICEMODEL', 'REGTYPE', 'MPESASTATE', 'ICAP_STATE', 'DBMS_STATE', 'EKYCSTATUS', 'TEMPREGSTATUS', 'TEMPREGDESCRIPTION', 'FULLREGSTATUS', 'FULLREGDESCR', 'IMAGECOUNT' );
                $additional_fieldsname = $additional_columname = '';
                if ($post->request->get('verify_info')) {
                    if ($post->request->get('exclude_tigosys')) {
                        $where_clause .= " AND x.verifyBy != '3260'";
                    }
                    $columnnames[] = 'Verified State';
                    $columnnames[] = 'Verified By';
                    $columnnames[] = 'Verified Date';
                    $columnnames[] = 'Verified Descr';
                    $columnnames[] = 'Original Verified Descr';
                    $columnnames[] = 'TimeToVerify';
                    $column_name = 'verify';

                    $addleftjoin .= ' LEFT JOIN user u3_ ON r3_.verifyBy = u3_.id ';

                    $additional_columname .= ", CASE r3_.verifyState WHEN '1' THEN 'Good' WHEN '2' THEN 'Fair' WHEN '3' THEN 'Bad' ELSE 'Not Verified' END AS verifyState, CONCAT(u3_.first_name, ' ', u3_.last_name) as verifyBy, DATE_FORMAT(r3_.verifyDate, '%Y-%m-%d %H:%i%:%s') as verifyDate, REPLACE(r3_.verifyDescr, ',', ' '), REPLACE(r3_.originalVerifyDescr, ',', ' '),r3_.verifyLock ";
                }
                if ($post->request->get('audit_info')) {
                    $columnnames[] = 'Audit State';
                    $columnnames[] = 'Audited By';
                    $columnnames[] = 'Audited Date';
                    $columnnames[] = 'Audit Descr';
                    $column_name = 'audit';

                    $addleftjoin .= ' LEFT JOIN user u4_ ON r3_.auditBy = u4_.id ';

                    $additional_columname .= ", CASE r3_.auditState WHEN '1' THEN 'Bad' WHEN '2' THEN 'Fair' WHEN '3' THEN 'Good' ELSE 'Not Audited' END AS auditState, CONCAT(u4_.first_name, ' ', u4_.last_name) as auditBy, date(r3_.auditDate) as auditDate, r3_.auditDescr";

                    //$columnnames = array('CREATEDDATE', 'MSISDN', 'CUSTOMERNAME', 'GENDER', 'IDNUMBER', 'IDTYPE', 'AGENTMOBILE', 'AGENTNAME', 'SUPERAGENTNAME', 'REGIONNAME', 'TERRITORYNAME', 'DEVICEMODEL', 'REGTYPE', 'MPESASTATE', 'ICAP_STATE', 'DBMS_STATE', 'EKYCSTATUS', 'TEMPREGSTATUS', 'TEMPREGDESCRIPTION', 'FULLREGSTATUS', 'FULLREGDESCR', 'IMAGECOUNT', 'AUDITSTATE', 'AUDITORNAME', 'AUDITDATE', 'AUDITDESCR');
                }

                $columnnames[] = 'AppVersion';

                $sales_codes = '';
                if ($post->request->get('sales_code')) {
                    $columnnames[] = 'SALESCODE';
                    $sales_codes = ' , u2_.sales_code ';
                }

                //fputcsv($handle, $columnnames, ',');
                // Query data from database
                if ($post->request->get('verify_info') || $post->request->get('audit_info')) {
                    /* $query = $em->createQueryBuilder()
                      ->select("p.msisdn, CONCAT(p.firstName, ' ', p.lastName) as customer_name, p.identification, p.identificationType, u.username, CONCAT(u.firstName, ' ', u.lastName) as registrar_name,  CONCAT(s.firstName, ' ', s.lastName) as superAgent_name, p.region, p.territory, DATE_FORMAT(p.createdDate, '%Y-%m-%d %H:%i%:%s') as createdDate, p.deviceModel as AgentPhone, x.temporaryRegStatus as tregStatus, x.temporaryRegDesc as tregDescr,  x.fullRegDesc as fregDescr " . $additional_columname)
                      ->from("RestApiBundle:RegistrationStatus", "x")
                      ->leftJoin('x.registrationId', 'p')
                      ->leftJoin('x.verifyBy', 'y')
                      ->leftJoin('x.auditBy', 'z')
                      ->leftJoin('p.owner', 'u')
                      ->leftJoin('u.parent_id', 's')
                      ->where($where_clause)
                      ->getQuery(); */
                    //$query = "SELECT DATE(p.createdDate) AS CREATEDDATE, p.msisdn AS MSISDN, CONCAT(p.firstName, ' ', p.lastName) AS CUSTOMERNAME, IFNULL(p.identification, ' ') AS IDNUMBER, IFNULL(a1_.name, ' ') AS IDTYPE, IFNULL(u.username, ' ') AS AGENTMOBILE, CONCAT(u.first_name, ' ', u.last_name) AS AGENTNAME, CONCAT(u2_.first_name, ' ', u2_.last_name) AS SUPERAGENTNAME, IFNULL(r5_.name, ' ') as REGIONNAME, IFNULL(t3_.name, ' ') AS TERRITORYNAME, IFNULL(p.deviceModel, ' ') AS DEVICEMODEL, CASE r3_.reg_type WHEN '0' THEN 'NewReg' WHEN '1' THEN 'NewReg' WHEN '2' THEN 'Re-Reg' ELSE 'NewReg' END AS REGTYPE, CASE r3_.mpesa_state WHEN '0' THEN 'Pending' WHEN '1' THEN 'Successful' WHEN '2' THEN 'Failed' WHEN '3' THEN 'Already Registered' ELSE 'Pending' END AS MPESASTATE, CASE r3_.icap_state WHEN '0' THEN 'Pending' WHEN '4' THEN 'Failed' WHEN '6' THEN 'Success' WHEN '7' THEN 'Success' ELSE 'Pending' END AS ICAP_STATE, CASE r3_.dbms_state WHEN '0' THEN 'Pending' WHEN '4' THEN 'Failed' WHEN '6' THEN 'Success' WHEN '7' THEN 'Success' ELSE 'Pending' END AS DBMS_STATE, CASE CONCAT(r3_.temporaryRegStatus, '', r3_.fullRegStatus) WHEN '20' THEN 'TREG' WHEN '60' THEN 'TREG' WHEN '70' THEN 'TREG' WHEN '24' THEN 'TREG' WHEN '64' THEN 'TREG' WHEN '74' THEN 'TREG' WHEN '22' THEN 'FREG' WHEN '26' THEN 'FREG' WHEN '27' THEN 'FREG' WHEN '62' THEN 'FREG' WHEN '66' THEN 'FREG' WHEN '67' THEN 'FREG' WHEN '72' THEN 'FREG' WHEN '76' THEN 'FREG' WHEN '77' THEN 'FREG' WHEN '07' THEN 'FREG' WHEN '17' THEN 'FREG' WHEN '06' THEN 'FREG' WHEN '16' THEN 'FREG' WHEN '26' THEN 'FREG' WHEN '40' THEN 'DECLINED' WHEN '30' THEN 'DECLINED' WHEN '40' THEN 'DECLINED' WHEN '00' THEN 'PENDING' ELSE 'Pending' END AS EKYCSTATUS, CASE r3_.temporaryRegStatus WHEN '0' THEN 'Pending' WHEN '1' THEN 'Pending' WHEN '2' THEN 'Success' WHEN '3' THEN 'Declined' WHEN '4' THEN 'Fail' WHEN '5' THEN 'Retry' WHEN '6' THEN 'SuccessICAP' WHEN '7' THEN 'SuccessDBMS' ELSE 'Pending' END AS TEMPREGSTATUS, REPLACE(IFNULL(r3_.temporaryRegDesc, ' '), ',', ' ') AS TEMPREGDESCRIPTION, CASE r3_.fullRegStatus WHEN '0' THEN 'Pending' WHEN '1' THEN 'Pending' WHEN '2' THEN 'Success' WHEN '3' THEN 'Declined' WHEN '4' THEN 'Fail' WHEN '5' THEN 'Retry' WHEN '6' THEN 'SuccessICAP' WHEN '7' THEN 'SuccessDBMS' ELSE 'Pending' END AS FULLREGSTATUS, REPLACE(IFNULL(r3_.fullRegDesc, ' '), ',', ' ') AS FULLREGDESCR, p.image_count AS IMAGECOUNT, CASE r3_.auditState WHEN '0' THEN 'NotAudited' WHEN '1' THEN 'Bad' WHEN '2' THEN 'Fair' WHEN '3' THEN 'Good' WHEN '-1' THEN 'Locked' ELSE 'NotAudited' END AS AUDITSTATE, CONCAT(u3_.first_name, ' ', u3_.last_name) as AUDITORNAME, REPLACE(r3_.auditDescr, ',', ' AND ') AS AUDITDESCR, DATE(r3_.auditDate) as AUDITDATE FROM RegistrationStatus r3_ LEFT JOIN registration p ON r3_.registrationId = p.id LEFT JOIN user u ON p.owner_id = u.id LEFT JOIN super_agent s4_ ON u.id = s4_.id LEFT JOIN agent a5_ ON u.id = a5_.id LEFT JOIN user u2_ ON u.parent_id_id = u2_.id LEFT JOIN super_agent s6_ ON u2_.id = s6_.id LEFT JOIN Region r5_ on r5_.code = p.region LEFT JOIN agent a7_ ON u2_.id = a7_.id LEFT JOIN Idtype a1_ on a1_.id = p.identificationType LEFT JOIN Territory t3_ on t3_.id = p.territory LEFT JOIN user u3_ ON r3_.auditBy = u3_.id WHERE " . $where_clause;

                    $query = "SELECT DATE(p.createdDate) AS CREATEDDATE, p.msisdn AS MSISDN, CONCAT(p.firstName, ' ', p.lastName) AS CUSTOMERNAME, p.gender, IFNULL(p.identification, ' ') AS IDNUMBER, IFNULL(a1_.name, ' ') AS IDTYPE,CASE p.registration_type WHEN '2' THEN 'Biometric' ELSE 'Standard' END AS TYPE, p.nationality, p.dob, p.contact, IFNULL(u.username, ' ') AS AGENTMOBILE, CONCAT(u.first_name, ' ', u.last_name) AS AGENTNAME, CONCAT(u2_.first_name, ' ', u2_.last_name) AS SUPERAGENTNAME, IFNULL(r5_.name, ' ') as REGIONNAME, IFNULL(t3_.name, ' ') AS TERRITORYNAME, IFNULL(p.deviceModel, ' ') AS DEVICEMODEL, CASE r3_.reg_type WHEN '0' THEN 'NewReg' WHEN '1' THEN 'NewReg' WHEN '2' THEN 'Re-Reg' ELSE 'NewReg' END AS REGTYPE, CASE r3_.mpesa_state WHEN '0' THEN 'Pending' WHEN '1' THEN 'Successful' WHEN '2' THEN 'Failed' WHEN '3' THEN 'Already Registered' ELSE 'Pending' END AS MPESASTATE, CASE r3_.icap_state WHEN '0' THEN 'Pending' WHEN '4' THEN 'Failed' WHEN '6' THEN 'Success' WHEN '7' THEN 'Success' ELSE 'Pending' END AS ICAP_STATE, CASE r3_.dbms_state WHEN '0' THEN 'Pending' WHEN '4' THEN 'Failed' WHEN '6' THEN 'Success' WHEN '7' THEN 'Success' ELSE 'Pending' END AS DBMS_STATE, CASE CONCAT(r3_.temporaryRegStatus, '', r3_.fullRegStatus) WHEN '20' THEN 'TREG' WHEN '60' THEN 'TREG' WHEN '70' THEN 'TREG' WHEN '24' THEN 'TREG' WHEN '64' THEN 'TREG' WHEN '74' THEN 'TREG' WHEN '22' THEN 'FREG' WHEN '26' THEN 'FREG' WHEN '27' THEN 'FREG' WHEN '62' THEN 'FREG' WHEN '66' THEN 'FREG' WHEN '67' THEN 'FREG' WHEN '72' THEN 'FREG' WHEN '76' THEN 'FREG' WHEN '77' THEN 'FREG' WHEN '07' THEN 'FREG' WHEN '17' THEN 'FREG' WHEN '06' THEN 'FREG' WHEN '16' THEN 'FREG' WHEN '26' THEN 'FREG' WHEN '40' THEN 'DECLINED' WHEN '30' THEN 'DECLINED' WHEN '40' THEN 'DECLINED' WHEN '00' THEN 'PENDING' ELSE 'Pending' END AS EKYCSTATUS, CASE r3_.temporaryRegStatus WHEN '0' THEN 'Pending' WHEN '1' THEN 'Pending' WHEN '2' THEN 'Success' WHEN '3' THEN 'Declined' WHEN '4' THEN 'Fail' WHEN '5' THEN 'Retry' WHEN '6' THEN 'SuccessICAP' WHEN '7' THEN 'SuccessDBMS' ELSE 'Pending' END AS TEMPREGSTATUS, REPLACE(IFNULL(r3_.temporaryRegDesc, ' '), ',', ' ') AS TEMPREGDESCRIPTION, CASE r3_.fullRegStatus WHEN '0' THEN 'Pending' WHEN '1' THEN 'Pending' WHEN '2' THEN 'Success' WHEN '3' THEN 'Declined' WHEN '4' THEN 'Fail' WHEN '5' THEN 'Retry' WHEN '6' THEN 'SuccessICAP' WHEN '7' THEN 'SuccessDBMS' ELSE 'Pending' END AS FULLREGSTATUS, REPLACE(IFNULL(r3_.fullRegDesc, ' '), ',', ' ') AS FULLREGDESCR, p.image_count AS IMAGECOUNT " . $additional_columname . $sales_codes . ' , p.appVersion FROM RegistrationStatus r3_ LEFT JOIN registration p ON r3_.registrationId = p.id LEFT JOIN user u ON p.owner_id = u.id LEFT JOIN super_agent s4_ ON u.id = s4_.id LEFT JOIN agent a5_ ON u.id = a5_.id LEFT JOIN user u2_ ON u.parent_id_id = u2_.id LEFT JOIN super_agent s6_ ON u2_.id = s6_.id LEFT JOIN Region r5_ on r5_.code = p.region LEFT JOIN agent a7_ ON u2_.id = a7_.id LEFT JOIN Idtype a1_ on a1_.id = p.identificationType LEFT JOIN Territory t3_ on t3_.id = p.territory ' . $addleftjoin . ' WHERE ' . $where_clause;

                    //	echo $query; exit;
                } else {
                    /* $query = $em->createQueryBuilder()
                      ->select("p.msisdn, CONCAT(p.firstName, ' ', p.lastName) as customer_name, p.identification, p.identificationType, u.username, CONCAT(u.firstName, ' ', u.lastName) as registrar_name,  CONCAT(s.firstName, ' ', s.lastName) as superAgent_name, p.region, p.territory, date(p.createdDate), p.deviceModel as AgentPhone, x.temporaryRegStatus as tregStatus, x.temporaryRegDesc as tregDescr,  x.fullRegDesc as fregDescr ")
                      ->from("RestApiBundle:RegistrationStatus", "x")
                      ->leftJoin('x.registrationId', 'p')
                      ->leftJoin('p.owner', 'u')
                      ->leftJoin('u.parent_id', 's')
                      ->where($where_clause)
                      ->getQuery(); */
                    $query = "SELECT DATE(p.createdDate) AS CREATEDDATE, p.msisdn AS MSISDN, CONCAT(p.firstName, ' ', p.lastName) AS CUSTOMERNAME, p.gender, IFNULL(p.identification, ' ') AS IDNUMBER, a1_.name AS IDTYPE, p.nationality, p.dob, u.username AS AGENTMOBILE, CONCAT(u.first_name, ' ', u.last_name) AS AGENTNAME, CONCAT(u2_.first_name, ' ', u2_.last_name) AS SUPERAGENTNAME, IFNULL(r5_.name, ' ') as REGIONNAME, IFNULL(t3_.name, ' ') AS TERRITORYNAME, IFNULL(p.deviceModel, ' ') AS DEVICEMODEL, CASE r3_.reg_type WHEN '0' THEN 'NewReg' WHEN '1' THEN 'NewReg' WHEN '2' THEN 'Re-Reg' ELSE 'NewReg' END AS REGTYPE, CASE r3_.mpesa_state WHEN '0' THEN 'Pending' WHEN '1' THEN 'Successful' WHEN '2' THEN 'Failed' WHEN '3' THEN 'Already Registered' ELSE 'Pending' END AS MPESASTATE, CASE r3_.icap_state WHEN '0' THEN 'Pending' WHEN '4' THEN 'Failed' WHEN '6' THEN 'Success' WHEN '7' THEN 'Success' ELSE 'Pending' END AS ICAP_STATE, CASE r3_.dbms_state WHEN '0' THEN 'Pending' WHEN '4' THEN 'Failed' WHEN '6' THEN 'Success' WHEN '7' THEN 'Success' ELSE 'Pending' END AS DBMS_STATE, CASE CONCAT(r3_.temporaryRegStatus, '', r3_.fullRegStatus) WHEN '20' THEN 'TREG' WHEN '60' THEN 'TREG' WHEN '70' THEN 'TREG' WHEN '24' THEN 'TREG' WHEN '64' THEN 'TREG' WHEN '74' THEN 'TREG' WHEN '22' THEN 'FREG' WHEN '26' THEN 'FREG' WHEN '27' THEN 'FREG' WHEN '62' THEN 'FREG' WHEN '66' THEN 'FREG' WHEN '67' THEN 'FREG' WHEN '72' THEN 'FREG' WHEN '76' THEN 'FREG' WHEN '77' THEN 'FREG' WHEN '07' THEN 'FREG' WHEN '17' THEN 'FREG' WHEN '06' THEN 'FREG' WHEN '16' THEN 'FREG' WHEN '26' THEN 'FREG' WHEN '40' THEN 'DECLINED' WHEN '30' THEN 'DECLINED' WHEN '40' THEN 'DECLINED' WHEN '00' THEN 'PENDING' ELSE 'Pending' END AS EKYCSTATUS, CASE r3_.temporaryRegStatus WHEN '0' THEN 'Pending' WHEN '1' THEN 'Pending' WHEN '2' THEN 'Success' WHEN '3' THEN 'Declined' WHEN '4' THEN 'Fail' WHEN '5' THEN 'Retry' WHEN '6' THEN 'SuccessICAP' WHEN '7' THEN 'SuccessDBMS' ELSE 'Pending' END AS TEMPREGSTATUS, REPLACE(IFNULL(r3_.temporaryRegDesc, ' '), ',', ' ') AS TEMPREGDESCRIPTION, CASE r3_.fullRegStatus WHEN '0' THEN 'Pending' WHEN '1' THEN 'Pending' WHEN '2' THEN 'Success' WHEN '3' THEN 'Declined' WHEN '4' THEN 'Fail' WHEN '5' THEN 'Retry' WHEN '6' THEN 'SuccessICAP' WHEN '7' THEN 'SuccessDBMS' ELSE 'Pending' END AS FULLREGSTATUS, REPLACE(IFNULL(r3_.fullRegDesc, ' '), ',', ' ') AS FULLREGDESCR, p.image_count AS IMAGECOUNT " . $sales_codes . ', p.appVersion FROM RegistrationStatus r3_ LEFT JOIN registration p ON r3_.registrationId = p.id LEFT JOIN user u ON p.owner_id = u.id LEFT JOIN super_agent s4_ ON u.id = s4_.id LEFT JOIN agent a5_ ON u.id = a5_.id LEFT JOIN user u2_ ON u.parent_id_id = u2_.id LEFT JOIN super_agent s6_ ON u2_.id = s6_.id LEFT JOIN Region r5_ on r5_.code = p.region LEFT JOIN agent a7_ ON u2_.id = a7_.id LEFT JOIN Idtype a1_ on a1_.id = p.identificationType LEFT JOIN Territory t3_ on t3_.id = p.territory WHERE ' . $where_clause;
                }

                break;
            case 'registration_journey_report':

                $columnnames = array( 'CREATEDDATE', 'MSISDN', 'CUSTOMERNAME', 'IDNUMBER', 'IDTYPE','TYPE' ,'AGENTMOBILE', 'AGENTNAME', 'SUPERAGENTNAME', 'REGIONNAME', 'TERRITORYNAME', 'DEVICEMODEL', 'REGTYPE', 'VERIFYSTATE', 'VERIFYDATE', 'MPESASTATE', 'ICAP_STATE', 'DBMS_STATE', 'ICAPSTATE', 'TEMPREGSTATUS', 'TEMPREGDESCRIPTION', 'FULLREGSTATUS', 'FULLREGDESCR', 'IMAGECOUNT', 'registrationTime', 'Device Sent Time', 'TREGDATE', 'AllImagesReceived', 'FREGDATE', 'MPESATIME' );

                //fputcsv($handle, $columnnames, ',');

                $query = "SELECT p.createdDate AS CREATEDDATE, p.msisdn AS MSISDN, CONCAT(p.firstName, ' ', p.lastName) AS CUSTOMERNAME, IFNULL(p.identification, ' ') AS IDNUMBER, IFNULL(a1_.name, ' ') AS IDTYPE,CASE p.registration_type WHEN '2' THEN 'Biometric' ELSE 'Standard' END AS TYPE, IFNULL(a1_.name, ' ') AS AGENTMOBILE, CONCAT(u.first_name, ' ', u.last_name) AS AGENTNAME, CONCAT(u2_.first_name, ' ', u2_.last_name) AS SUPERAGENTNAME, IFNULL(r5_.name, ' ') as REGIONNAME, IFNULL(t3_.name, ' ') AS TERRITORYNAME, IFNULL(p.deviceModel, ' ') AS DEVICEMODEL, CASE r3_.reg_type WHEN '0' THEN 'NewReg' WHEN '1' THEN 'NewReg' WHEN '2' THEN 'Re-Reg' ELSE 'New' END AS REGTYPE, CASE r3_.verifyState WHEN '1' THEN 'Good' WHEN '2' THEN 'Fair' WHEN '3' THEN 'Bad' ELSE 'Not Verified' END AS verifyState, DATE_FORMAT(r3_.verifyDate, '%Y-%m-%d %H:%i%:%s') as verifyDate, CASE r3_.mpesa_state WHEN '0' THEN 'Pending' WHEN '1' THEN 'Successful' WHEN '2' THEN 'Failed' WHEN '3' THEN 'Already Registered' ELSE 'Pending' END AS MPESASTATE, CASE r3_.icap_state WHEN '0' THEN 'Pending' WHEN '4' THEN 'Failed' WHEN '6' THEN 'Success' WHEN '7' THEN 'Success' ELSE 'Pending' END AS ICAP_STATE, CASE r3_.dbms_state WHEN '0' THEN 'Pending' WHEN '4' THEN 'Failed' WHEN '6' THEN 'Success' WHEN '7' THEN 'Success' ELSE 'Pending' END AS DBMS_STATE, CASE CONCAT(r3_.temporaryRegStatus, '', r3_.fullRegStatus) WHEN '20' THEN 'TREG' WHEN '60' THEN 'TREG' WHEN '70' THEN 'TREG' WHEN '24' THEN 'TREG' WHEN '64' THEN 'TREG' WHEN '74' THEN 'TREG' WHEN '22' THEN 'FREG' WHEN '26' THEN 'FREG' WHEN '27' THEN 'FREG' WHEN '62' THEN 'FREG' WHEN '66' THEN 'FREG' WHEN '67' THEN 'FREG' WHEN '72' THEN 'FREG' WHEN '76' THEN 'FREG' WHEN '77' THEN 'FREG' WHEN '07' THEN 'FREG' WHEN '17' THEN 'FREG' WHEN '06' THEN 'FREG' WHEN '16' THEN 'FREG' WHEN '26' THEN 'FREG' WHEN '40' THEN 'DECLINED' WHEN '30' THEN 'DECLINED' WHEN '40' THEN 'DECLINED' WHEN '00' THEN 'PENDING' ELSE 'Pending' END AS ICAPSTATE, CASE r3_.temporaryRegStatus WHEN '0' THEN 'Pending' WHEN '1' THEN 'Pending' WHEN '2' THEN 'Success' WHEN '3' THEN 'Declined' WHEN '4' THEN 'Fail' WHEN '5' THEN 'Retry' WHEN '6' THEN 'SuccessICAP' WHEN '7' THEN 'SuccessDBMS' ELSE 'Pending' END AS TEMPREGSTATUS, REPLACE(IFNULL(r3_.temporaryRegDesc, ' '), ',', ' ') AS TEMPREGDESCRIPTION, CASE r3_.fullRegStatus WHEN '0' THEN 'Pending' WHEN '1' THEN 'Pending' WHEN '2' THEN 'Success' WHEN '3' THEN 'Declined' WHEN '4' THEN 'Fail' WHEN '5' THEN 'Retry' WHEN '6' THEN 'SuccessICAP' WHEN '7' THEN 'SuccessDBMS' ELSE 'Pending' END AS FULLREGSTATUS, REPLACE(IFNULL(r3_.fullRegDesc, ' '), ',', ' ') AS FULLREGDESCR, p.image_count AS IMAGECOUNT, p.registrationTime, p.deviceSentTime, r3_.tregDate AS TREGDATE, r3_.allimageDate AS AllImagesReceived, r3_.fregDate AS FREGDATE, r3_.mpesaDate FROM RegistrationStatus r3_ LEFT JOIN registration p ON r3_.registrationId = p.id LEFT JOIN user u ON p.owner_id = u.id LEFT JOIN super_agent s4_ ON u.id = s4_.id LEFT JOIN agent a5_ ON u.id = a5_.id LEFT JOIN user u2_ ON u.parent_id_id = u2_.id LEFT JOIN super_agent s6_ ON u2_.id = s6_.id LEFT JOIN Region r5_ on r5_.code = p.region LEFT JOIN agent a7_ ON u2_.id = a7_.id LEFT JOIN Idtype a1_ on a1_.id = p.identificationType LEFT JOIN Territory t3_ on t3_.id = p.territory LEFT JOIN user u3_ ON r3_.auditBy = u3_.id WHERE " . $where_clause;


                break;
            case 'id_documents_eport':

// Add the header of the CSV file
                //fputcsv($handle, array('ID Type', 'Number of Usage'), ',');
                $query = $em->createQueryBuilder()
                    ->select('p.identificationType, COUNT(p)')
                    ->from('RestApiBundle:Registration', 'p')
                    ->leftJoin('p.owner', 'u')
                    ->groupBy('p.identificationType')
                    ->where($where_clause)
                    ->getQuery();

                $query = "SELECT t.name, count('p.*') from registration p "
                    . "left join user u on p.owner_id = u.id "
                    . "left join Idtype t on t.id = p.identificationType WHERE " . $where_clause . " "
                    . "GROUP BY p.identificationType";

                break;
            case 'registration_region_report':

// Add the header of the CSV file
                //fputcsv($handle, array('Region Name', 'Territory Name', 'Registrations Done'), ',');
                $query = $em->createQueryBuilder()
                    ->select('p.region, p.territory, COUNT(p)')
                    ->from('RestApiBundle:Registration', 'p')
                    ->groupBy('p.region')
                    ->groupBy('p.territory')
                    ->where($where_clause)
                    ->getQuery();

                break;
            case 'device_report':

                //select device.msisdn as deviceMSISDN, device.imei as IMEI, device.isActive, date(device.createdOn) as deviceCreatedOn, device.appVersion as AppVersion, auser.username as AgentMobile, concat(auser.first_name, ' ', auser.last_name) as AgentName, Region.name as RegionName, Territory.name as TerritoryName, date(auser.created_at) as AgentCreatedOn, concat(xuser.first_name, ' ', xuser.last_name) as SuperAgentName from device left join user auser on device.user_id = auser.id left join Region on auser.region_id = Region.id left join Territory on auser.territory_id = Territory.id left join user xuser on auser.parent_id_id  = xuser.id

                $columnnames = array( 'deviceMSISDN', 'IMEI', 'isActive', 'deviceCreatedOn', 'AppVersion', 'AgentMobile', 'AgentName', 'RegionName', 'TerritoryName', 'AgentCreatedOn' );

                //fputcsv($handle, $columnnames, ',');
                $query = $em->createQueryBuilder()
                    ->select("d.msisdn as deviceMSISDN, d.imei as IMEI, d.isActive, date(d.createdOn) as deviceCreatedOn, d.appVersion as AppVersion, a.username as AgentMobile, concat(a.firstName, ' ', a.lastName) as AgentName, r.name as RegionName, t.name as TerritoryName, date(a.createdAt) as AgentCreatedOn ")
                    ->from('RestApiBundle:Device', 'd')
                    ->leftJoin('d.user', 'a')
                    ->leftJoin('a.region', 'r')
                    ->leftJoin('a.territory', 't')
                    ->leftJoin('a.parent_id', 'x')
                    //->where($where_clause)
                    ->getQuery();


                break;
            case 'all_agents_download':

                $columnnames = array( 'Created Date', 'AgentName', 'Agent Phone', 'Email', 'Region', 'Territory', 'Super Agent', 'Status' );

                $where_clause_agents .= " AND u.roles LIKE '%ROLE_AGENT%' ";

                $query = $em->createQueryBuilder()
                    ->select("u.createdAt as CreatedOn, CONCAT(u.firstName, ' ', u.lastName) as agentName, u.mobileNumber as phone, u.email, r.name as regionName, t.name as territoryName, CONCAT(s.firstName, ' ', s.lastName) as superAgentName, CASE u.status WHEN '0' THEN 'Disabled' WHEN '1' THEN 'Enabled' ELSE 'Disabled' END AS Status ")
                    ->from('RestApiBundle:User', 'u')
                    ->leftJoin('u.region', 'r')
                    ->leftJoin('u.territory', 't')
                    ->leftJoin('u.parent_id', 's')
                    ->where($where_clause_agents)
                    ->getQuery();

                break;
            case 'registrations_gender_report':

// Add the header of the CSV file
                //fputcsv($handle, array('Gender', 'Registrations Done'), ',');
                $query = $em->createQueryBuilder()
                    ->select('p.gender, COUNT(p)')
                    ->from('RestApiBundle:Registration', 'p')
                    ->leftJoin('p.owner', 'u')
                    ->groupBy('p.gender')
                    ->where($where_clause)
                    ->getQuery();
                break;

            case 'active_inactive':

                $from = date('Y-m-d', strtotime($post->request->get('from_date')));
                $to = date('Y-m-d', strtotime($post->request->get('to_date')));

                $titles1 = array( 'Registrations Count', 'Agent Name', 'Agent Number', 'Super-agent Number', 'Super Agent', 'Region', 'Territory', 'Created On', 'Last Login', 'Status', 'Active' );

                $sql1 = "SELECT COUNT(r0_.id) AS sclr_0, CONCAT(u1_.first_name, ' - ', u1_.last_name) AS sclr_1, u1_.username AS username_2, u2_.mobile_number AS mobile_number_3, CONCAT(u2_.first_name, ' ', u2_.last_name) AS sclr_4, r3_.name AS name_5, t4_.name AS name_6, DATE_FORMAT (u1_.created_at,'%Y-%m-%d') AS sclr_7, DATE_FORMAT (r0_.createdDate,'%Y-%m-%d') AS sclr_8, u1_.enabled AS enabled_9 FROM registration r0_ INNER JOIN user u1_ ON r0_.owner_id = u1_.id INNER JOIN user u2_ ON u1_.parent_id_id = u2_.id INNER JOIN Region r3_ ON u1_.region_id = r3_.id INNER JOIN Territory t4_ ON u1_.territory_id = t4_.id WHERE 1 = 1 AND DATE(r0_.createdDate) >= '" . $from . "' AND DATE(r0_.createdDate) <= '" . $to . "' AND u1_.roles LIKE '%ROLE_AGENT%' AND u1_.status = 1 GROUP BY r0_.owner_id ORDER BY r0_.id DESC;";


                $titles2 = array( 'Agent Name', 'Agent Number', 'Super-agent Number', 'Super Agent', 'Region', 'Territory', 'CreatedOn', 'Last Login', 'Status Active' );
                $sql2 = "SELECT CONCAT(u0_.first_name, ' - ', u0_.last_name) AS sclr_0, u0_.username AS username_1, u1_.mobile_number AS mobile_number_2, CONCAT(u1_.first_name, ' ', u1_.last_name) AS sclr_3, r2_.name AS name_4, t3_.name AS name_5, DATE_FORMAT (u0_.created_at,'%Y-%m-%d') AS sclr_6, DATE_FORMAT (u0_.last_login,'%Y-%m-%d') AS sclr_7, u0_.enabled AS enabled_8 FROM user u0_ INNER JOIN user u1_ ON u0_.parent_id_id = u1_.id INNER JOIN Region r2_ ON u0_.region_id = r2_.id INNER JOIN Territory t3_ ON u0_.territory_id = t3_.id WHERE 1 = 1 AND u0_.roles LIKE '%ROLE_AGENT%' AND u0_.id NOT IN (SELECT u0_.id AS id_0 FROM registration r1_ INNER JOIN user u0_ ON r1_.owner_id = u0_.id WHERE 1 = 1 AND date(r1_.createdDate) >= '" . $from . "' AND date(r1_.createdDate) <= '" . $to . "' AND u0_.roles LIKE '%ROLE_AGENT%' GROUP BY r1_.owner_id ORDER BY r1_.id DESC) AND u0_.status = 1 GROUP BY u0_.id;";

                break;
            case 'active_inactive_agents':
                //code to be executed if n = label3;
                //fputcsv($handle, array('Gender', 'Registrations Done'), ',');
                $query = $this->get_active_inactive($where_clause);
                break;
            default:
                //code to be executed if n is different from all labels;
                $results = $em->createQuery('SELECT p.firstName, p.lastName, p.region, p.territory from RestApiBundle:Registration p ')
                    ->getArrayResult();
        }

        if (in_array($post->request->get('report_type'), array( 'agent_registration', 'id_documents_eport', 'active_inactive', 'fullreg_report', 'registration_journey_report' ))) {
            //echo $query;exit;
            $results = $query;
        } else {
            $results = $query->getSql();
        }

        if ($post->request->get('report_type') == 'active_inactive') {
            //fputcsv($handle, $titles1, ',');
            $this->generateExportFile($post->request->get('report_type'), 'ACTIVEAGENT' . date('Ymd', strtotime($post->request->get('from_date'))) . 'TO' . date('Ymd', strtotime($post->request->get('to_date'))) . $ttitle, $titles1, $sql1, $post->request->get('emailAddress'));

            //fputcsv($handle, $titles2, ',');
            $this->generateExportFile($post->request->get('report_type'), 'INACTIVEAGENT' . date('Ymd', strtotime($post->request->get('from_date'))) . 'TO' . date('Ymd', strtotime($post->request->get('to_date'))) . $ttitle, $titles2, $sql2, $post->request->get('emailAddress'));

            //echo $sql1 . " :::: " . $post->request->get('emailAddress'); exit;
        } else {
            $this->generateExportFile($post->request->get('report_type'), $ttitle, $columnnames, $results, $post->request->get('emailAddress'));
        }


        //return $this->redirect($this->generateUrl('reports'));
    }

    function generateExportFile($report_type, $ttitle, $columnnames, $queryString, $email) {

        try {

            $title = ($ttitle ? $ttitle : 'ReportGeneration-' . $report_type);


            $kernel = $this->get('kernel');
            $application = new Application($kernel);
            $application->setAutoExit(false);
            $input = new ArrayInput(array( 'command' => 'dashboard:report_generator_command', 'headings' => implode(',', $columnnames), 'query' => $queryString, 'email' => $email, 'title' => $title ));
            $application->run($input, null);
        } catch (Exception $e) {
            echo $e;

            return null;
        }
    }

    /**
     * Creates a new Faq entity.
     * @Route("/reportspost", name="reportspost")
     * @Method("POST")
     * @Template("DashboardBundle:Admin:reports.html.twig")
     * @throws \LogicException
     */
    public function reportspostAction(Request $request) {

        ini_set('memory_limit', '16384M');
        global $post;
        $post = $request;

        $response = new StreamedResponse();
        $response->setCallback(function () {

            global $post;

            $em = $this->getDoctrine()->getManager();
            $emConfig = $em->getConfiguration();
            $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
            $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
            $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
            $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

            $where_clause = $where_clause_agents = ' 1=1';

            /** @var  $user User */
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $roles = $user->getRoles(); //array of roles

            if (in_array('ROLE_SUPERAGENT', $roles)) {
                $where_clause .= ' AND u.parent_id = ' . $user->getId();
                $where_clause_agents .= ' AND u.parent_id = ' . $user->getId();
            }

            if (in_array('ROLE_VODASHOP', $roles)) {
                $where_clause .= ' AND u.vodashop_id = ' . $user->getId();
                $where_clause_agents .= ' AND u.vodashop_id = ' . $user->getId();
            }

            if ($post->request->get('region')) {
                $where_clause .= " AND p.region = '" . $post->request->get('region') . "' ";
                $where_clause_agents .= " AND u.region = '" . $post->request->get('region') . "' ";
            }
            if ($post->request->get('territory')) {
                $where_clause .= ' AND p.territory = ' . $post->request->get('territory');
                $where_clause_agents .= ' AND u.territory = ' . $post->request->get('territory');
            }
            if ($post->request->get('from_date')) {
                if ($post->request->get('report_type') == 'full_reg_report') {
                    $where_clause .= " AND DATE(x.fregDate) >= '" . date('Y-m-d', strtotime($post->request->get('from_date'))) . "'";
                } else {
                    $where_clause .= " AND DATE(p.createdDate) >= '" . date('Y-m-d', strtotime($post->request->get('from_date'))) . "'";
                    $where_clause_agents .= " AND DATE(p.createdAt) >= '" . date('Y-m-d', strtotime($post->request->get('from_date'))) . "'";
                }
            }
            if ($post->request->get('to_date')) {
                if ($post->request->get('report_type') == 'full_reg_report') {
                    $where_clause .= " AND DATE(x.fregDate) <= '" . date('Y-m-d', strtotime($post->request->get('to_date'))) . "'";
                } else {
                    $where_clause .= " AND DATE(p.createdDate) <= '" . date('Y-m-d', strtotime($post->request->get('to_date'))) . "'";
                    $where_clause_agents .= " AND DATE(p.createdAt) <= '" . date('Y-m-d', strtotime($post->request->get('to_date'))) . "'";
                }
            }
            if ($post->request->get('customer_msisdn')) {//----customer msisdn
                //$where_clause.= " AND p.msisdn = " . $post->request->get('customer_msisdn');
            }
            if ($post->request->get('agent_msisdn')) {//----customer msisdn
                $where_clause .= " AND u.username = '" . $post->request->get('agent_msisdn') . "'";
            }

            // Get the report by registration type
	    if ($post->request->get('type')  && in_array($post->request->get('type'), [1,2])) { // Registration Type
                $where_clause .= " AND p.registration_type = '" . $post->request->get('type') . "'";
             }	

            //$where_clause_agents .= " AND roles like '%ROLE_AGENT%' ";

            $handle = fopen('php://output', 'w+');

            switch ($post->request->get('report_type')) {
                case 'agent_registration':

                    $columnnames = array( 'Created Date', 'Customer Number', 'Customer Name', 'ID Number', 'ID Type', 'Registrar MSISDN', 'Registrar Name', 'SuperAgent Name', 'Region Name', 'Territory Name', 'AgentPhone', 'RegistrationType', 'EKYCResponse', 'MpesaState', 'ICAPState', 'DBMSState', 'TREG State', 'TREG Description', 'FREG State', 'FREG Description', 'Image Count' );
                    $additional_fieldsname = $additional_columname = '';
                    if ($post->request->get('verify_info')) {
                        $columnnames[] = 'Verified State';
                        $columnnames[] = 'Verified By';
                        $columnnames[] = 'Verified Date';
                        $columnnames[] = 'Verified Descr';
                        $additional_columname .= ", x.verifyState, CONCAT(y.firstName, ' ', y.lastName) as verifyBy, date(x.verifyDate) as verifyDate, x.verifyDescr";
                    }
                    if ($post->request->get('audit_info')) {
                        $columnnames[] = 'Audit State';
                        $columnnames[] = 'Audited By';
                        $columnnames[] = 'Audited Date';
                        $columnnames[] = 'Audit Descr';
                        $additional_columname .= ", x.auditState, CONCAT(z.firstName, ' ', z.lastName) as auditBy, date(x.auditDate) as auditDate, x.auditDescr";
                    }

                    fputcsv($handle, $columnnames, ',');
                    // Query data from database
                    if ($post->request->get('verify_info') || $post->request->get('audit_info')) {
                        $query = $em->createQueryBuilder()
                            ->select("date(p.createdDate), p.msisdn, CONCAT(p.firstName, ' ', p.lastName) as customer_name, p.identification, p.identificationType, u.username, CONCAT(u.firstName, ' ', u.lastName) as registrar_name,  CONCAT(s.firstName, ' ', s.lastName) as superAgent_name, p.region, p.territory, p.deviceModel as AgentPhone, x.regType as regType, p.isSms as ekycState, x.mpesaState, x.icapState, x.dbmsState, x.temporaryRegStatus as tregStatus, x.temporaryRegDesc as tregDescr, x.fullRegStatus as fregStatus, x.fullRegDesc as fregDescr, p.image_count " . $additional_columname)
                            ->from('RestApiBundle:RegistrationStatus', 'x')
                            ->leftJoin('x.registrationId', 'p')
                            ->leftJoin('x.verifyBy', 'y')
                            ->leftJoin('x.auditBy', 'z')
                            ->leftJoin('p.owner', 'u')
                            ->leftJoin('u.parent_id', 's')
                            ->where($where_clause)
                            ->getQuery();
                    } else {
                        /* $query = $em->createQueryBuilder()
                          ->select("p.msisdn, x.temporaryRegStatus as tregStatus, x.fullRegStatus as fregStatus, CONCAT(p.firstName, ' ', p.lastName) as customer_name, p.identification, p.identificationType, u.username, CONCAT(u.firstName, ' ', u.lastName) as registrar_name,  CONCAT(s.firstName, ' ', s.lastName) as superAgent_name, p.region, p.territory, date(p.createdDate), p.deviceModel as AgentPhone ")
                          ->from("RestApiBundle:Registration", "p")
                          ->leftJoin('p.owner', 'u')
                          ->leftJoin('u.parent_id', 's')
                          ->where($where_clause)
                          ->getQuery(); */

                        $query = $em->createQueryBuilder()
                            ->select("date(p.createdDate), p.msisdn, CONCAT(p.firstName, ' ', p.lastName) as customer_name, p.identification, p.identificationType, u.username, CONCAT(u.firstName, ' ', u.lastName) as registrar_name,  CONCAT(s.firstName, ' ', s.lastName) as superAgent_name, p.region, p.territory, p.deviceModel as AgentPhone, x.regType as regType, p.isSms as ekycState, x.mpesaState, x.icapState, x.dbmsState, x.temporaryRegStatus as tregStatus, x.temporaryRegDesc as tregDescr, x.fullRegStatus as fregStatus, x.fullRegDesc as fregDescr, p.image_count ")
                            ->from('RestApiBundle:RegistrationStatus', 'x')
                            ->leftJoin('x.registrationId', 'p')
                            ->leftJoin('p.owner', 'u')
                            ->leftJoin('u.parent_id', 's')
                            ->where($where_clause)
                            ->getQuery();
                    }

                    break;
                case 'id_documents_eport':

                    fputcsv($handle, array( 'ID Type', 'Number of Usage' ), ',');
                    $query = $em->createQueryBuilder()
                        ->select('p.identificationType, COUNT(p)')
                        ->from('RestApiBundle:Registration', 'p')
                        ->leftJoin('p.owner', 'u')
                        ->groupBy('p.identificationType')
                        //->where($where_clause)
                        ->getQuery();

                    break;
                case 'newdevice_report_export':

                    fputcsv($handle, array( 'AgentName', 'AgentMSISDN', 'deviceModel' ), ',');
                    $query = $em->createQueryBuilder()
                        ->select("concat(u.firstName , ' ', u.lastName ) as AgentName, u.username as AgentMsisdn, max(p.deviceModel) as deviceModel")
                        ->from('RestApiBundle:Registration', 'p')
                        ->leftJoin('p.owner', 'u')
                        ->groupBy('p.deviceId')
                        //->where($where_clause)
                        ->getQuery();

                    break;
                case 'device_report':

                    //select device.msisdn as deviceMSISDN, device.imei as IMEI, device.isActive, date(device.createdOn) as deviceCreatedOn, device.appVersion as AppVersion, auser.username as AgentMobile, concat(auser.first_name, ' ', auser.last_name) as AgentName, Region.name as RegionName, Territory.name as TerritoryName, date(auser.created_at) as AgentCreatedOn, concat(xuser.first_name, ' ', xuser.last_name) as SuperAgentName from device left join user auser on device.user_id = auser.id left join Region on auser.region_id = Region.id left join Territory on auser.territory_id = Territory.id left join user xuser on auser.parent_id_id  = xuser.id

                    $columnnames = array( 'deviceMSISDN', 'IMEI', 'isActive', 'deviceCreatedOn', 'AppVersion', 'AgentMobile', 'AgentName', 'RegionName', 'TerritoryName', 'AgentCreatedOn', 'SuperAgentName' );

                    fputcsv($handle, $columnnames, ',');
                    $query = $em->createQueryBuilder()
                        ->select("d.msisdn as deviceMSISDN, d.imei as IMEI, d.isActive, date(d.createdOn) as deviceCreatedOn, d.appVersion as AppVersion, a.username as AgentMobile, concat(a.firstName, ' ', a.lastName) as AgentName, r.name as RegionName, t.name as TerritoryName, date(a.createdAt) as AgentCreatedOn, concat(x.firstName, ' ', x.lastName) as SuperAgentName ")
                        ->from('RestApiBundle:Device', 'd')
                        ->leftJoin('d.user', 'a')
                        ->leftJoin('a.region', 'r')
                        ->leftJoin('a.territory', 't')
                        ->leftJoin('a.parent_id', 'x')
                        //->where($where_clause)
                        ->getQuery();


                    break;
                case 'full_reg_report':

                    //SAME AS AGENT REGISTRATION ONLY DIFF IS FILTER DATES DOES FREG DATE NOT CREATED DATE
                    $columnnames = array( 'Customer Number', 'Customer Name', 'ID Number', 'ID Type', 'Registrar MSISDN', 'Registrar Name', 'SuperAgent Name', 'Region Name', 'Territory Name', 'Created Date', 'AgentPhone', 'RegistrationType', 'TREG State', 'TREG Description', 'FREG State', 'FREG Description' );
                    $additional_fieldsname = $additional_columname = '';
                    if ($post->request->get('verify_info')) {
                        $columnnames[] = 'Verified State';
                        $columnnames[] = 'Verified By';
                        $columnnames[] = 'Verified Date';
                        $columnnames[] = 'Verified Descr';
                        $additional_columname .= ", x.verifyState, CONCAT(y.firstName, ' ', y.lastName) as verifyBy, date(x.verifyDate) as verifyDate, x.verifyDescr";
                    }
                    if ($post->request->get('audit_info')) {
                        $columnnames[] = 'Audit State';
                        $columnnames[] = 'Audited By';
                        $columnnames[] = 'Audited Date';
                        $columnnames[] = 'Audit Descr';
                        $additional_columname .= ", x.auditState, CONCAT(z.firstName, ' ', z.lastName) as auditBy, date(x.auditDate) as auditDate, x.auditDescr";
                    }

                    fputcsv($handle, $columnnames, ',');
                    // Query data from database
                    if ($post->request->get('verify_info') || $post->request->get('audit_info')) {
                        $query = $em->createQueryBuilder()
                            ->select("p.msisdn, CONCAT(p.firstName, ' ', p.lastName) as customer_name, p.identification, p.identificationType, u.username, CONCAT(u.firstName, ' ', u.lastName) as registrar_name,  CONCAT(s.firstName, ' ', s.lastName) as superAgent_name, p.region, p.territory, date(p.createdDate), p.deviceModel as AgentPhone, x.regType as regType, x.temporaryRegStatus as tregStatus, x.temporaryRegDesc as tregDescr, x.fullRegStatus as fregStatus, x.fullRegDesc as fregDescr  " . $additional_columname)
                            ->from('RestApiBundle:RegistrationStatus', 'x')
                            ->leftJoin('x.registrationId', 'p')
                            ->leftJoin('x.verifyBy', 'y')
                            ->leftJoin('x.auditBy', 'z')
                            ->leftJoin('p.owner', 'u')
                            ->leftJoin('u.parent_id', 's')
                            ->where($where_clause)
                            ->getQuery();
                    } else {
                        /* $query = $em->createQueryBuilder()
                          ->select("p.msisdn, x.temporaryRegStatus as tregStatus, x.fullRegStatus as fregStatus, CONCAT(p.firstName, ' ', p.lastName) as customer_name, p.identification, p.identificationType, u.username, CONCAT(u.firstName, ' ', u.lastName) as registrar_name,  CONCAT(s.firstName, ' ', s.lastName) as superAgent_name, p.region, p.territory, date(p.createdDate), p.deviceModel as AgentPhone ")
                          ->from("RestApiBundle:Registration", "p")
                          ->leftJoin('p.owner', 'u')
                          ->leftJoin('u.parent_id', 's')
                          ->where($where_clause)
                          ->getQuery(); */

                        $query = $em->createQueryBuilder()
                            ->select("p.msisdn, CONCAT(p.firstName, ' ', p.lastName) as customer_name, p.identification, p.identificationType, u.username, CONCAT(u.firstName, ' ', u.lastName) as registrar_name,  CONCAT(s.firstName, ' ', s.lastName) as superAgent_name, p.region, p.territory, date(p.createdDate), p.deviceModel as AgentPhone, x.regType as regType, x.temporaryRegStatus as tregStatus, x.temporaryRegDesc as tregDescr, x.fullRegStatus as fregStatus, x.fullRegDesc as fregDescr ")
                            ->from('RestApiBundle:RegistrationStatus', 'x')
                            ->leftJoin('x.registrationId', 'p')
                            ->leftJoin('p.owner', 'u')
                            ->leftJoin('u.parent_id', 's')
                            ->where($where_clause)
                            ->getQuery();
                    }

                    break;
                case 'registration_region_report':

                    fputcsv($handle, array( 'Region Name', 'Territory Name', 'Registrations Done' ), ',');
                    $query = $em->createQueryBuilder()
                        ->select('p.region, p.territory, COUNT(p)')
                        ->from('RestApiBundle:Registration', 'p')
                        ->groupBy('p.region')
                        ->groupBy('p.territory')
                        ->where($where_clause)
                        ->getQuery();

                    break;
                case 'all_agents_download':

                    $where_clause_agents .= " AND u.roles LIKE '%ROLE_AGENT%' ";

                    fputcsv($handle, array( 'Agent Name', 'Phone', 'Email', 'Region', 'Territory', 'Super Agent' ), ',');
                    $query = $em->createQueryBuilder()
                        ->select("CONCAT(u.firstName, ' ', u.lastName) as agentName, u.mobileNumber as phone, u.email, r.name as regionName, t.name as territoryName, CONCAT(s.firstName, ' ', s.lastName) as superAgentName ")
                        ->from('RestApiBundle:User', 'u')
                        ->leftJoin('u.region', 'r')
                        ->leftJoin('u.territory', 't')
                        ->leftJoin('u.parent_id', 's')
                        ->where($where_clause_agents)
                        ->getQuery();

                    break;
                case 'registrations_gender_report':

                    fputcsv($handle, array( 'Gender', 'Registrations Done' ), ',');
                    $query = $em->createQueryBuilder()
                        ->select('p.gender, COUNT(p)')
                        ->from('RestApiBundle:Registration', 'p')
                        ->leftJoin('p.owner', 'u')
                        ->groupBy('p.gender')
                        ->where($where_clause)
                        ->getQuery();
                    break;
                case 'active_inactive_agents':
                    //code to be executed if n = label3;
                    break;
                default:
                    //code to be executed if n is different from all labels;
                    $results = $em->createQuery('SELECT p.firstName, p.lastName, p.region, p.territory from RestApiBundle:Registration p ')
                        ->getArrayResult();
            }

            try {
                //echo $results = $query->getSql(); exit; //ArrayResult();
                $results = $query->getArrayResult();
            } catch (\Doctrine\ORM\NoResultException $e) {
                return null;
            }

            //$results = $em->createQuery("SELECT p.firstName, p.lastName, p.region, p.territory from RestApiBundle:Registration p ")
            //            ->getArrayResult();
            // Add the data queried from database

            $regionsAll = $this->getRegionsArrayCodeName();
            $teritoriesAll = $this->getTerritoryArrayIdName();

            $num = 1;


            try {

                //->select("date(p.createdDate), p.msisdn, CONCAT(p.firstName, ' ', p.lastName) as customer_name, p.identification, p.identificationType, u.username, CONCAT(u.firstName, ' ', u.lastName) as registrar_name,  CONCAT(s.firstName, ' ', s.lastName) as superAgent_name, p.region, p.territory, p.deviceModel as AgentPhone, x.regType as regType, p.isSms as ekycState, x.mpesaState, x.icapState, x.dbmsState, x.temporaryRegStatus as tregStatus, x.temporaryRegDesc as tregDescr, x.fullRegStatus as fregStatus, x.fullRegDesc as fregDescr, p.image_count ")

                foreach ($results as $key => $row) {

                    if (isset($row[ 'tregStatus' ]) && isset($row[ 'fregStatus' ]) && isset($row[ 'image_count' ])) {
                        $row[ 'ekycState' ] = $this->getAimRegStatusDesc($row[ 'tregStatus' ], $row[ 'fregStatus' ], $row[ 'image_count' ]);
                    }
                    if (isset($row[ 'identificationType' ])) {
                        $row[ 'identificationType' ] = $this->getIDType($row[ 'identificationType' ]);
                    }
                    if (isset($row[ 'territory' ])) {
                        $row[ 'territory' ] = @$teritoriesAll[ $row[ 'territory' ] ]; //$this->getTerritoryFromId($row['territory']);
                    }
                    if (isset($row[ 'region' ])) {
                        $row[ 'region' ] = @$regionsAll[ $row[ 'region' ] ]; //$this->getRegionFromCode($row['region']);
                    }

                    if (isset($row[ 'icapStatus' ])) {
                        $row[ 'icapStatus' ] = $this->getIcapRegStatusDesc($row[ 'tregStatus' ], $row[ 'fregStatus' ]);
                    }

                    if (isset($row[ 'tregStatus' ])) {
                        $row[ 'tregStatus' ] = $this->getRegStatusDesc($row[ 'tregStatus' ]);
                    }

                    if (isset($row[ 'fregStatus' ])) {
                        $row[ 'fregStatus' ] = $this->getRegStatusDesc($row[ 'fregStatus' ]);
                    }

                    if (isset($row[ 'tregDescr' ])) {
                        $row[ 'tregDescr' ] = str_replace(',', ' ', $row[ 'tregDescr' ]);
                    }

                    if (isset($row[ 'fregDescr' ])) {
                        $row[ 'fregDescr' ] = str_replace(',', ' ', $row[ 'fregDescr' ]);
                    }

                    if (isset($row[ 'auditState' ])) {
                        $row[ 'auditState' ] = $this->getAuditStates($row[ 'auditState' ]);
                    }

                    if (isset($row[ 'mpesaState' ])) {
                        $row[ 'mpesaState' ] = $this->getMpesaStatus($row[ 'mpesaState' ]);
                    }
                    if (isset($row[ 'icapState' ])) {
                        $row[ 'icapState' ] = $this->getICAPStatus($row[ 'icapState' ]);
                    }
                    if (isset($row[ 'dbmsState' ])) {
                        $row[ 'dbmsState' ] = $this->getDBMSStatus($row[ 'dbmsState' ]);
                    }

                    // added this code to pull verification information
                    if (isset($row[ 'verifyState' ])) {
                        $row[ 'verifyState' ] = $this->getVerifyStates($row[ 'verifyState' ]);
                    }
                    if (isset($row[ 'verifyBy' ])) {
                        $row[ 'verifyBy' ] = $row[ 'verifyBy' ];
                    }
                    if (isset($row[ 'verifyDate' ])) {
                        $row[ 'verifyDate' ] = $row[ 'verifyDate' ];
                    }
                    if (isset($row[ 'verifyDescr' ])) {
                        $row[ 'verifyDesc' ] = $row[ 'verifyDescr' ];
                    }


                    if (isset($row[ 'regType' ])) {
                        $row[ 'regType' ] = $this->getRegType($row[ 'regType' ]);
                    }


                    //unset($row['tregStatus']);
                    //unset($row['fregStatus']);


                    if (isset($row[ 'customer_name' ])) {
                        $row[ 'customer_name' ] = str_replace(',', '', $row[ 'customer_name' ]);
                    }
                    if (isset($row[ 'superAgent_name' ])) {
                        $row[ 'superAgent_name' ] = str_replace(',', '', $row[ 'superAgent_name' ]);
                    }


                    //echo "|".json_encode($row)."|";


                    fputcsv(
                        $handle, // The file pointer
                        $row, // The fields
                        ',' // The delimiter
                    );

                    $num++;
                }
            } catch (Exception $e) {
                echo $e;

                return null;
            }

            fclose($handle);
        });

        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment; filename="export.csv"');


        return $response;

        /* return array(
          'title' => "Generate Reports",
          'title_descr' => "View Reports for",
          'message' => json_encode($request->getContent()) //"Report Request made successfully.. Report will be sent to email once generated",
          ); */
//return $this->redirect($this->generateUrl('reports'));
    }

    /**
     * Creates a new Faq entity.
     *
     * @Route("/registrations_filter", name="registrations_filter")
     * @Method("POST")
     * @Template("DashboardBundle:Admin:registrations.html.twig")
     */
    public function registrations_filterAction(Request $request) {

        $post = $request;
        $post->request->get('customer_msisdn');


        $results = 'empty';
        $searchResult = new SearchResult();

        $dao = new SearchDao($this->container);
        $results = $dao->searchAgent($post->request->get('first_name'));

//        $cnt = 1;
//        foreach ($results as $key => $value) {
//            echo "<p style='color:#fff;'>" . $cnt . " | " . $value->getFirstName() . " | " . $value->getlastName() . "</p>";
//            $cnt++;
//        }

        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('RestApiBundle:Registration')->findAll();

        $data = array(
            'title'         => 'View Registrations Report:',
            'title_descr'   => 'View Registrations Report | All registrations',
            'registrations' => $results,
            'message'       => json_encode($request->getContent()) //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/getNidaStatus/{id}/{ttime}", name="getNidaStatus")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:registration_info.html.twig")
     */
    public function getNidaStatusAction($id, $ttime) {
        $em = $this->getDoctrine()->getManager();
        //----------
        $result = $em->createQuery('SELECT v from DashboardBundle:Verifications v '
            . "WHERE v.customerMsisdn = '" . $id . "' ")
            ->getArrayResult();

        if (count($result) >= 1) {
            $data[ 'success' ] = 1;
            $data[ 'data' ] = $result[ 0 ];
        } else {
            $data[ 'success' ] = 0;
        }

        echo json_encode($data);

        exit;
        //-----------
        $data = array(
            'title' => 'View Registrations Report:'
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/registration_info/{id}", name="registration_info")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:registration_info.html.twig")
     */
    public function registrationInfoAction($id) {
        $em = $this->getDoctrine()->getManager();


        $entity = $em->getRepository('RestApiBundle:Registration')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Registration entity.');
        }

        $reg_status[ 'fullRegStatus' ] = $result[ 'fullRegDesc' ] = '';
        $result = $em->createQuery('SELECT  p from RestApiBundle:RegistrationStatus p '
            . "WHERE p.registrationId = '" . $entity->getId() . "' ")
            ->getArrayResult();

        $verificationAllDescr = '';
        if (count($result) == 1) {
            $reg_status = $result[ 0 ];

            $reg_status[ 'aimStatus' ] = $this->getAimRegStatusDesc($reg_status[ 'temporaryRegStatus' ], $reg_status[ 'fullRegStatus' ], $entity->getImage_count());

            $reg_status[ 'icapStatus' ] = $this->getIcapRegStatusDesc($reg_status[ 'temporaryRegStatus' ], $reg_status[ 'fullRegStatus' ]);

            $reg_status[ 'temporaryRegStatus' ] = $this->getRegStatusDesc($reg_status[ 'temporaryRegStatus' ]);
            $reg_status[ 'fullRegStatus' ] = $this->getRegStatusDesc($reg_status[ 'fullRegStatus' ]);


            $verify_state = $reg_status[ 'verifyState' ];
            $verify_description = $reg_status[ 'verifyDescr' ];
            $verify_date = $reg_status[ 'verifyDate' ];

            if (NULL == $verify_description) {
                $verify_description = 'NOT SET';
            } else {
                $verify_description = json_decode($reg_status[ 'verifyDescr' ]);
                if (count($verify_description) == 1) {
                    $verify_description = $verify_description[ 0 ];
                } else {
                    $v_decr = '';
                    foreach ($verify_description as $val) {
                        $v_decr = $v_decr . ',  ' . $val->fieldDescr . ' ';
                    }
                    $v_decr = substr($v_decr, 1, strlen($v_decr) - 1);
                    $verificationAllDescr = $v_decr;
                }
            }
        }

//        echo "<pre>"; print_r($reg_status); echo "</pre>";
//        echo ($reg_status['allimageDate'] ? date('F j,Y  H:i:s', strtotime($reg_status['allimageDate']->format('Y-m-d H:i:s'))) : "Not yet arrived" );
//        exit;

        $data = array(
            'title'                    => 'View Registrations Report:',
            'title_descr'              => 'View Registrations Report',
            'entity'                   => $entity,
            'region'                   => $this->get_regionName_fromKey($entity->getRegion()),
            'reg_status'               => $reg_status,
            'allImageDate'             => ($reg_status[ 'allimageDate' ] ? date('F j,Y  H:i:s', strtotime($reg_status[ 'allimageDate' ]->format('Y-m-d H:i:s'))) : 'No All Images'),
            'tregDate'                 => ($reg_status[ 'tregDate' ] ? date('F j,Y  H:i:s', strtotime($reg_status[ 'tregDate' ]->format('Y-m-d H:i:s'))) : 'Not yet TREGd'),
            'fregDate'                 => ($reg_status[ 'fregDate' ] ? date('F j,Y  H:i:s', strtotime($reg_status[ 'fregDate' ]->format('Y-m-d H:i:s'))) : 'Not Yet FREGd'),
            'gender'                   => $this->getGender($entity->getGender()),
            'idtype'                   => $this->getIDType($entity->getIdentificationType()),
            'verification_time'        => ($verify_date == NULL) ? 'NOT SET' : $verify_date->format('Y-m-d H:i:s'),
            'verification_state'       => $this->getVerifyStates($verify_state),
            'verification_description' => $verificationAllDescr
        );

        return $this->prepareResponse($data);
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/agent_edit/{id}", name="agent_edit")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:agent_edit.html.twig")
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RestApiBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        $query = $em->createQuery('SELECT d FROM RestApiBundle:Device d WHERE d.user = 1')//'".$entity->getId()."'")
        ->getResult();


        $editForm = $this->createEditForm($entity);

        $data = array(
            'title'       => 'Create New User',
            'title_descr' => 'Create New Agent',
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'devices'     => $query,
        );

        return $this->prepareResponse($data);
    }

    /**
     * Creates a form to edit a User entity.
     *
     * @param User $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(User $entity) {
        $form = $this->createForm(new UserType(), $entity, array(
            'action' => $this->generateUrl('agent_edit', array( 'id' => $entity->getId() )),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array( 'label' => 'Update' ));

        return $form;
    }

    /**
     * Creates a form to delete a User entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('agent_deactivate', array( 'id' => $id )))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array( 'label' => 'Deactivate' ))
            ->getForm();
    }

    /**
     * Deletes a User entity.
     *
     * @Route("/{id}", name="agent_deactivate")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('RestApiBundle:User')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find User entity.');
            }

            $results = $em->createQuery('UPDATE RestApiBundle:User u SET u.enabled = 0 WHERE u.id = ' . $id . ' ')
                ->getArrayResult();

//$em->remove($entity);
            $em->flush();

            $this->logUserEvent(UserLog::DEACTIVATE_USER, 'deactivating agent', array( 'agentid' => $id ));
        }

        return $this->redirect($this->generateUrl('admin/agents'));
    }

    /**
     * Edits an existing Faq entity.
     *
     * @Route("/agent_edit/{id}", name="agent_update")
     * @Method("PUT")
     * @Template("DashboardBundle:Admin:agent_edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RestApiBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->logUserEvent(UserLog::EDIT_USER, 'Editing user', array( 'agentid' => $id ));

            return $this->redirect($this->generateUrl('agent_edit', array( 'id' => $id )));
        }

        $data = array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/registration_images/{id}", name="registration_images")
     * @Method("GET")
     *
     */
    public function registrationImagesAction($id) {
        $resp = new JsonObject();
        $status = false;
        $message = '';
        try {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('RestApiBundle:RegImages')->findBy(array( 'registration' => $id ));
            if ($entity) {
                $resp->setItem($entity);

                $filesystem = $this->container->get('s3_filesystem');

                if ($filesystem && $filesystem->getAdapter()) {
                    $adapter = $filesystem->getAdapter();
                    if ($adapter->getClient()) {
                        $client = $adapter->getClient();
                        $webPathPrefix = $this->container->getParameter('filesystem_root_image_path') .
                            $this->container->getParameter('s3_bucket') .
                            '/' . $this->container->getParameter('s3_bucket_prefix');

                        foreach ($entity as $key => $row) {
                            $file_path = str_replace($webPathPrefix, '', $row->getWebPath());
                            $path = RegistrationUtil::getPresignedUrl($client, $adapter->getBucket(), $adapter->applyPathPrefix($file_path),
                                //$row->getWebPath(),
                                '+' . $this->container->getParameter('s3_expire_time_in_seconds') . ' seconds');
                            $row->setWebPath($row->getWebPath()); //$path
                        }
                    }
                }

                $status = true;
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }
        $resp->setMessage($message);
        $resp->setStatus($status);

        return $this->buildResponse($resp, Response::HTTP_OK);


//        $entity = $em->getRepository('RestApiBundle:RegImages')->findBy(
//            array("registration"=>$id)
//        );
//        if (!$entity) {
//            throw $this->createNotFoundException('Unable to find Registration entity.');
//        }
//        var_dump($entity);
    }

//    private function getPresignedUrl(\Aws\S3\S3Client $s3Client, $bucket, $path, $duration) {
//        $cmd = $s3Client->getCommand('GetObject', [
//            'Bucket' => $bucket,
//            'Key' => $path,
//        ]);
//        $request = $s3Client->createPresignedRequest($cmd, $duration);
    // Get the actual presigned-url
//        return (string) $request->getUri();
//    }

    /**
     * Finds and displays a Faq entity.
     *
     * @Route("/{id}", name="faq_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DashboardBundle:Faq')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Faq entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        $data = array(
            'title'       => 'Create New FAQ',
            'title_descr' => 'Create New Frequently Asked Questions',
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );

        return $this->prepareResponse($data);
    }

    /**
     * Creates a new User entity.
     *
     * @Route("/", name="user_create")
     * @Method("POST")
     * @Template("DashboardBundle:Admin:useragent_new.html.twig")
     */
    public function createAction(Request $request) {
        $entity = new User();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('view_agents', array( 'id' => $entity->getId() )));
        }

        $data = array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );

        return $this->prepareResponse($data);
    }

    /**
     * Creates a form to create a User entity.
     *
     * @param User $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(User $entity) {
        $form = $this->createForm(new UserType(), $entity, array(
            'action' => $this->generateUrl('user_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array( 'label' => 'Create' ));

        return $form;
    }

    /**
     * Displays a form to create a new User entity.
     *
     * @Route("/useragent_new", name="useragent_new")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:useragent_new.html.twig")
     */
    public function newAction() {
        $entity = new User();
        $form = $this->createCreateForm($entity);

        $data = array(
            'title'       => 'Create New FAQ',
            'title_descr' => 'Create New Frequently Asked Questions',
            'entity'      => $entity,
            'form'        => $form->createView(),
        );

        return $this->prepareResponse($data);
    }

    /**
     * .
     *
     * @Route("/user_account", name="user_account")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:my_account.html.twig")
     * @return mixed
     */
    public function userAccountAction() {

        $user = $this->get('security.token_storage')->getToken()->getUser();
        $user->getUsername();

        $data = array( 'first_name'   => $user->getFirstName(),
                       'middle_name'  => $user->getMiddleName(),
                       'last_name'    => $user->getLastName(),
                       'email'        => $user->getEmail(),
                       'mobile'       => $user->getMobileNumber(),
                       'organization' => $user->getLastName() );

        $data = array(
            'title'       => 'My Account',
            'title_descr' => 'List, create, delete, activate System Admins',
            'userInfo'    => $data
        );

        return $this->prepareResponse($data);
    }

    /**
     * @param Request $request
     * @Route("/list_registrations",name="list_registrations")
     * @Method({"POST","GET"})
     */
    public function listRegistrationsAction(Request $request) {
        $resp = new JTableResponse();
        try {

            $em = $this->getDoctrine()->getManager();
            $emConfig = $em->getConfiguration();
            $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
            $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
            $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
            $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');


            $attributes = $request->request->all();
            $where = ' WHERE 1=1 ';

            /** @var  $user User */
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $roles = $user->getRoles(); //array of roles

            if (in_array('ROLE_SUPERAGENT', $roles)) {
                $where .= ' AND a.parent_id = ' . $user->getId();
            }
            if (in_array('ROLE_VODASHOP', $roles)) {
                $where .= ' AND a.vodashop_id = ' . $user->getId();
            }
            if (strlen(@$attributes[ 'msisdn' ]) >= 9) {
                $where .= " AND u.msisdn = '" . substr($attributes[ 'msisdn' ], -9) . "'";
            }
            if (strlen(@$attributes[ 'first_name' ]) >= 2) {//echo "<<<<<<<<<<<<<<";
                $where .= " AND u.firstName like '%" . $attributes[ 'first_name' ] . "%'";
            }
            if (strlen(@$attributes[ 'last_name' ]) >= 2) {
                $where .= " AND u.lastName like '%" . $attributes[ 'last_name' ] . "%'";
            }
            if (@$attributes[ 'identificationType' ] >= 1) {
                $where .= " AND u.identificationType = '" . $attributes[ 'identificationType' ] . "'";
            }
            if (strlen(@$attributes[ 'id_number' ]) >= 4) {
                $where .= " AND u.identification like '" . $attributes[ 'id_number' ] . "'";
            }
            if (strlen(@$attributes[ 'agent_firstname' ]) >= 3) {
                $where .= " AND a.firstName like '" . $attributes[ 'agent_firstname' ] . "'";
            }
            if (strlen(@$attributes[ 'agent_lastname' ]) >= 3) {
                $where .= " AND a.lastName like '%" . $attributes[ 'agent_lastname' ] . "%'";
            }
            if (strlen(@$attributes[ 'agent_phone' ]) >= 9) {
                $where .= " AND a.username = '" . substr($attributes[ 'agent_phone' ], -9) . "'";
            }
            if (@$attributes[ 'territory' ] > 0) {
                $where .= " AND u.territory = '" . $attributes[ 'territory' ] . "'";
            }
            if (strlen(@$attributes[ 'from_date' ]) > 4) {
                $where .= " AND DATE(u.createdDate) >= '" . date('Y-m-d', strtotime($attributes[ 'from_date' ])) . "'";
            }
            if (strlen(@$attributes[ 'to_date' ]) > 4) {
                $where .= " AND DATE(u.createdDate) <= '" . date('Y-m-d', strtotime($attributes[ 'to_date' ])) . "'";
            }
            if (strlen(@$attributes[ 'icap_status' ]) > 2) {
                $where .= $this->getWhereForICAPSatus($attributes[ 'icap_status' ]);
            }
            if (strlen(@$attributes[ 'ereg_status' ]) > 2) {
                $where .= $this->getWhereForEREGSatus($attributes[ 'ereg_status' ]);
            }
            //echo @$attributes["date"];
            if (strlen(@$attributes[ 'date' ]) > 4) {
                if (strlen(@$attributes[ 'path' ]) > 2 && @$attributes[ 'path' ] == 'treg') {
                    //sent to TREG NOT sent to FREG
                    $where .= " AND x.temporaryRegStatus IN (1,2,6) AND x.fullRegStatus = 0 AND DATE(u.createdDate) = '" . @$attributes[ 'date' ] . "'";
                } else if (strlen(@$attributes[ 'path' ]) > 2 && @$attributes[ 'path' ] == 'tobesent') {
                    //NOT sent to TREG
                    $where .= " AND x.fullRegStatus not in (2,6,7,4,5,3) AND x.verifyState = 1 AND u.createdDate >= '" . @$attributes[ 'date' ] . " 00:00:00' AND u.createdDate <= '" . @$attributes[ 'date' ] . " 23:59:59' ";
                } else if (strlen(@$attributes[ 'path' ]) > 2 && @$attributes[ 'path' ] == 'declined') {
                    //DECLINED or FAILED
                    $where .= " AND x.temporaryRegStatus IN (3,4) AND DATE(u.createdDate) = '" . @$attributes[ 'date' ] . "'";
                } else if (strlen(@$attributes[ 'path' ]) > 2 && @$attributes[ 'path' ] == 'freg') {
                    $where .= " AND x.temporaryRegStatus IN (1,2,6) AND x.fullRegStatus IN (1,6,7) AND DATE(u.createdDate) = '" . @$attributes[ 'date' ] . "'";
                }

                if (strlen(@$attributes[ 'path' ]) > 2 && @$attributes[ 'path' ] == 'pending') {
                    $where .= " AND u.image_count < 3 AND DATE(u.createdDate) = '" . @$attributes[ 'date' ] . "'";
                }
            } else {
                if (strlen(@$attributes[ 'path' ]) > 2 && @$attributes[ 'path' ] == 'treg') {
                    //sent to TREG NOT sent to FREG
                    $where .= ' AND x.temporaryRegStatus IN (1,2,6) AND x.fullRegStatus = 0';
                } else if (strlen(@$attributes[ 'path' ]) > 2 && @$attributes[ 'path' ] == 'tobesent') {
                    //NOT sent to TREG
                    $where .= ' AND x.temporaryRegStatus = 0';
                } else if (strlen(@$attributes[ 'path' ]) > 2 && @$attributes[ 'path' ] == 'declined') {
                    //DECLINED or FAILED
                    $where .= ' AND x.temporaryRegStatus IN (3,4)';
                } else if (strlen(@$attributes[ 'path' ]) > 2 && @$attributes[ 'path' ] == 'freg') {
                    $where .= ' AND x.temporaryRegStatus IN (1,2,6) AND x.fullRegStatus IN (1,6,7)';
                }

                if (strlen(@$attributes[ 'path' ]) > 2 && @$attributes[ 'path' ] == 'pending') {
                    $where .= ' AND u.image_count < 3';
                }
            }

            $queryAttrib = $request->query->all();

            $queryString = " SELECT u.id,u.msisdn, x.temporaryRegStatus as tregStatus, x.fullRegStatus as fregStatus, x.auditState, x.verifyState, concat(u.firstName, ' ', u.lastName) as customerName, DATE_FORMAT(u.createdDate, '%Y-%m-%d') as created_on, DATE_FORMAT(u.createdDate, '%H:%i:%s') as created_time, u.image_count as image_count, u.identificationType, u.identification, a.username, concat(a.firstName,' ', a.lastName) as agentName, m.mobileNumber as superAgentPhone, concat(m.firstName,' ', m.lastName) as superAgentName, u.region, u.territory "
                . 'FROM RestApiBundle:RegistrationStatus x '
                . 'LEFT OUTER JOIN x.registrationId u '
                . 'LEFT OUTER JOIN u.owner a '
                . 'LEFT OUTER JOIN a.parent_id m  ' . $where
                . ' ORDER BY u.id desc ';
            $query = $em->createQuery($queryString);

            $query->setMaxResults($queryAttrib[ 'jtPageSize' ]);
            $query->setFirstResult($queryAttrib[ 'jtStartIndex' ]);
            // echo $agents = $query->getSql(); exit;
            $agents = $query->getResult();

            $entities = $em->getRepository('DashboardBundle:Idtype')->findAll();

            $id_array = array();
            foreach ($entities as $akey => $aval) {
                $id_array[ $aval->getId() ] = $aval->getName();
            }

            $territory_entiry = $em->getRepository('DashboardBundle:Territory')->findAll();

            $territory_array = array();
            $territory_array[ 'none' ] = '';
            foreach ($territory_entiry as $bkey => $bval) {
                $territory_array[ $bval->getId() ] = $bval->getName();
            }


            $returnData[ 'Result' ] = 'OK';
            $rowData = array();

            if ($agents) {

                $cnt = 1;
                foreach ($agents as $kkey => $vval) {
                    if ($vval[ 'territory' ]) {
                        $vval[ 'territory' ] = @$territory_array[ $vval[ 'territory' ] ];
                    }
                    $vval[ 'temporaryRegStatus' ] = $this->getRegStatusDesc($vval[ 'tregStatus' ]);

                    //$vval['msisdn'] = $cnt. ". " . $vval['msisdn'];

                    $vval[ 'aimStatus' ] = $this->getAimRegStatusDesc($vval[ 'tregStatus' ], $vval[ 'fregStatus' ], $vval[ 'image_count' ]);

                    $vval[ 'icapStatus' ] = $this->getIcapRegStatusDesc($vval[ 'tregStatus' ], $vval[ 'fregStatus' ]);

                    $vval[ 'region' ] = $this->get_regionName_fromKey($vval[ 'region' ]);
                    $vval[ 'identificationType' ] = $id_array[ $vval[ 'identificationType' ] ];
                    $rowData[] = $vval;

                    $cnt++;
                }
            }

            $returnData[ 'Records' ] = $rowData;
            $returnData[ 'Record' ] = null;
            $returnData[ 'Message' ] = null;
            $returnData[ 'Options' ] = null;

            echo json_encode($returnData);
            exit;

            $resp->setRecords($agents);
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult('ERROR');
        }

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @Route("/list_verify_registrations/{list}/{max_result}",name="list_verify_registrations")
     * @Method({"POST","GET"})
     */
    public function list_verify_registrationsAction($list, $max_result, Request $request) {
        $resp = new JTableResponse();
        try {

            $em = $this->getDoctrine()->getManager();
            $emConfig = $em->getConfiguration();
            $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
            $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
            $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
            $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

            $attributes = $request->request->all();
            $where = ' WHERE 1=1 ';

            /** @var  $user User */
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $roles = $user->getRoles(); //array of roles

            if (in_array('ROLE_SUPERAGENT', $roles)) {
                $where .= ' AND a.parent_id = ' . $user->getId();
            }
            if (in_array('ROLE_VODASHOP', $roles)) {
                $where .= ' AND a.vodashop_id = ' . $user->getId();
            }

            if (strlen(@$attributes[ 'msisdn' ]) >= 4) {
                $where .= " AND u.msisdn like '%" . $attributes[ 'msisdn' ] . "%'";
            }
            if (strlen(@$attributes[ 'first_name' ]) >= 2) {//echo "<<<<<<<<<<<<<<";
                $where .= " AND u.firstName like '%" . $attributes[ 'first_name' ] . "%'";
            }
            if (strlen(@$attributes[ 'last_name' ]) >= 2) {
                $where .= " AND u.lastName like '%" . $attributes[ 'last_name' ] . "%'";
            }
            if (@$attributes[ 'identificationType' ] >= 1) {
                $where .= " AND u.identificationType = '" . $attributes[ 'identificationType' ] . "'";
            }
            if (strlen(@$attributes[ 'id_number' ]) >= 4) {
                $where .= " AND u.identification like '%" . $attributes[ 'id_number' ] . "%'";
            }
            if (strlen(@$attributes[ 'agent_firstname' ]) >= 3) {
                $where .= " AND a.firstName like '%" . $attributes[ 'agent_firstname' ] . "%'";
            }
            if (strlen(@$attributes[ 'agent_lastname' ]) >= 3) {
                $where .= " AND a.lastName like '%" . $attributes[ 'agent_lastname' ] . "%'";
            }
            if (strlen(@$attributes[ 'agent_phone' ]) >= 4) {
                $where .= " AND a.username = '" . $attributes[ 'agent_phone' ] . "'";
            }
            if (strlen(@$attributes[ 'region' ]) > 1) {
                $where .= " AND u.region = '" . $attributes[ 'region' ] . "'";
            }
            if (@$attributes[ 'audit_state' ] >= 1) {
                $where .= " AND x.auditState = '" . $attributes[ 'audit_state' ] . "'";
            }
            if (@$attributes[ 'territory' ] > 0) {
                $where .= " AND u.territory = '" . $attributes[ 'territory' ] . "'";
            }

            $dateCheck = false;
            if (strlen(@$attributes[ 'from_date' ]) > 4) {
                $dateCheck = true;
                $where .= " AND u.createdDate >= '" . date('Y-m-d', strtotime($attributes[ 'from_date' ])) . " 00:00:00'";
            }
            if (strlen(@$attributes[ 'to_date' ]) > 4) {
                $dateCheck = true;
                $where .= " AND u.createdDate <= '" . date('Y-m-d', strtotime($attributes[ 'to_date' ])) . " 23:59:59'";
            }

            if (strlen(@$attributes[ 'verifyfrom_date' ]) > 4) {
                $dateCheck = true;
                $where .= " AND x.verifyDate >= '" . date('Y-m-d', strtotime($attributes[ 'verifyfrom_date' ])) . " 00:00:00'";
            }
            if (strlen(@$attributes[ 'verifyto_date' ]) > 4) {
                $dateCheck = true;
                $where .= " AND x.verifyDate <= '" . date('Y-m-d', strtotime($attributes[ 'verifyto_date' ])) . " 23:59:59'";
            }

            if (!$dateCheck) {
                $where .= " AND u.createdDate >= '" . date('Y-m-d') . " 00:00:00'";
            }

            if (isset($attributes[ 'verifyDescr' ]) && !empty($attributes[ 'verifyDescr' ])) {
                $where .= " AND x.verifyDescr LIKE '%" . $attributes[ 'verifyDescr' ] . "%'";
            }
            if (isset($attributes[ 'verifyState' ]) && !empty($attributes[ 'verifyState' ])) {
                if (in_array($attributes[ 'verifyState' ], array( 2, 3 ))) {
                    $where .= " AND (x.verifyState = '" . $attributes[ 'verifyState' ] . "' OR x.verifyState = '-" . $attributes[ 'verifyState' ] . "')";
                } else {
                    $where .= " AND x.verifyState = '" . $attributes[ 'verifyState' ] . "'";
                }
            }
            if (isset($attributes[ 'verifyBy' ]) && @$attributes[ 'verifyBy' ] >= 1) {
                $where .= ' AND v.id = ' . $attributes[ 'verifyBy' ];
            }

            $where .= " AND x.verifyBy != '541'";

            $additional_fields = '';
            $verifier_query_join = '';
            if ($list == 'search') {
                //$where .= " AND x.temporaryRegStatus in ('NULL', 0)";
                //$where .= " AND u.image_count >= 3";
                //$where .= " AND x.verifyState NOT IN (1,2,3)";
            } elseif ($list == 'list') {
                $where .= " AND x.verifyState IN ('1','2','3','-2','-3')";
                $additional_fields = ", DATE_FORMAT(x.verifyDate, '%Y-%m-%d') as Date, x.verifyState as State, v.username as Verifier, x.verifyDescr as Description ";
                $verifier_query_join = 'LEFT OUTER JOIN x.verifyBy v ';
            } else {
                //multiple
                $additional_fields = ", DATE_FORMAT(x.auditDate, '%Y-%m-%d') as Date, x.auditState as State, x.auditDescr as Description ";
            }

            $selectFields = "u.id,u.msisdn as MSISDN,u.registrationid, u.firstName as FirstName,u.lastName as LastName, DATE_FORMAT(u.createdDate, '%Y-%m-%d') as CreatedOn, u.dob as DoB, u.nationality as Nationality, u.identificationType as IDType, u.identification as IDNumber, a.username as AgentMobile, a.firstName as agentFirstName, a.lastName as agentLastName, u.region as Region, u.territory as Territory, u.deviceModel as DeviceModel ";

            $query = $em->createQueryBuilder()
                ->select(" u from DashboardBundle:ConfigMaster u WHERE u.name = 'verifierFieldList' ")
                ->getQuery();
            $configMaster = $query->getOneOrNullResult();
            if ($configMaster && $list == 'search') {
                $selectFields = str_replace(':::', ', ', $configMaster->getConfig());
            }

            $queryAttrib = $request->query->all();
            $queryString = ' SELECT u.id as regId, u.id, u.registrationid, ' . $selectFields . ' ' . $additional_fields
                . 'FROM RestApiBundle:RegistrationStatus x '
                . 'LEFT OUTER JOIN x.registrationId u '
                . $verifier_query_join
                . 'LEFT OUTER JOIN u.owner a ' . $where
                . ' ORDER BY u.id desc ';
            $query = $this->getDoctrine()->getManager()
                ->createQuery($queryString);

            if (false && isset($queryAttrib[ 'jtPageSize' ])) {
                $query->setMaxResults($queryAttrib[ 'jtPageSize' ]);
            } else {
                $query->setMaxResults(100);
            }
            if (isset($queryAttrib[ 'jtStartIndex' ])) {
                $query->setFirstResult($queryAttrib[ 'jtStartIndex' ]);
            }


            #echo $agents = $query->getSql(); exit; //Result();
            $agents = $query->getResult();

            $entities = $em->getRepository('DashboardBundle:Idtype')->findAll();

            $id_array = array();
            foreach ($entities as $akey => $aval) {
                $id_array[ $aval->getId() ] = $aval->getName();
            }

            $territory_entiry = $em->getRepository('DashboardBundle:Territory')->findAll();

            $territory_array = array();
            foreach ($territory_entiry as $bkey => $bval) {
                $territory_array[ $bval->getId() ] = $bval->getName();
            }

//            echo "<pre>";
//            print_r($territory_array);
//            echo "<pre>";

            $user = $this->get('security.token_storage')->getToken()->getUser();


            $returnData[ 'Result' ] = 'OK';
            $rowData = array();
            $rowData_send = array();
            if ($agents) {
                foreach ($agents as $kkey => $vval) {
                    if (@$vval[ 'Territory' ] > 1) {
                        $vval[ 'Territory' ] = @$territory_array[ $vval[ 'Territory' ] ];
                    }
                    if (isset($vval[ 'Region' ])) {
                        $vval[ 'Region' ] = $this->getRegionFromCode($vval[ 'Region' ]);
                    }
                    if (isset($vval[ 'IDType' ])) {
                        $vval[ 'IDType' ] = $id_array[ $vval[ 'IDType' ] ];
                    }

                    if (isset($vval[ 'State' ])) {
                        if ($list == 'list') {
                            $vval[ 'State' ] = $this->getVerifyStates($vval[ 'State' ]);
                        } else {
                            $vval[ 'State' ] = $this->getAuditStates($vval[ 'State' ]);
                        }
                    }
                    if (isset($vval[ 'Description' ])) {
                        $vval[ 'Description' ] = trim(str_replace('_', ' ', str_replace('descr', ' ', $vval[ 'Description' ])), ',');
                    }

                    $rowData_send[ 'id' ] = $vval[ 'registrationid' ];
                    unset($vval[ 'registrationid' ]);
                    unset($vval[ 'regId' ]);
                    $rowData_send[ 'jsonddata' ] = json_encode($vval);
                    $rowData[] = $rowData_send;
                }
            }
            $returnData[ 'Records' ] = $rowData;
            $returnData[ 'Record' ] = null;
            $returnData[ 'Message' ] = null;
            $returnData[ 'Options' ] = null;
            $returnData[ 'TotalRecordCount' ] = 3434;


//            echo "<pre>";
//            print_r($returnData);
//            echo "<pre>";
            echo json_encode($returnData);
            exit;

            $resp->setRecords($agents);
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult('ERROR');
        }

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * View Reports
     *
     * @Route("/audit_registrations/{recordID}/{rateValue}/{rateDescr}/{timestamp}", name="audit_registrations")
     * @Method("GET")
     */
    public function audit_registrationsAction($recordID, $rateValue, $rateDescr, $timestamp) {

        $user = $this->get('security.token_storage')->getToken()->getUser();

        $em = $this->getDoctrine()->getManager();
        $query1 = "UPDATE RegistrationStatus SET auditDate = '" . date('Y-m-d H:i:s') . "', "
            . " auditState = '" . $rateValue . "', "
            . " auditDescr = '" . trim(str_replace('___', '', $rateDescr), ',') . "', "
            . " auditBy = '" . $user->getId() . "' "
            . " WHERE registrationId = '" . $recordID . "'";
        $em->getConnection()->exec($query1);

        $this->logUserEvent(UserLog::AUDIT_REGISTRATION, 'audit registration', array(
            'auditDate'    => date('Y-m-d H:i:s'),
            'auditState'   => $rateValue,
            'description'  => $rateDescr,
            'registration' => $recordID ));

        $data[ 'response_msg' ] = 'Audited Successfully';
        echo json_encode($data);
        exit;
        $data = array(
            'title'       => 'Active & Inactive',
            'title_descr' => 'View Reports for Active Inactive',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * @param Request $request
     * @Route("/list_audit_registrations",name="list_audit_registrations")
     * @Method({"POST","GET"})
     */
    public function list_audit_registrationsAction(Request $request) {
        $resp = new JTableResponse();
        try {

            $em = $this->getDoctrine()->getManager();
            $emConfig = $em->getConfiguration();
            $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
            $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
            $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
            $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');


            $attributes = $request->request->all();
            $where = ' WHERE 1=1 ';


            $dateforce = true;

            /** @var  $user User */
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $roles = $user->getRoles(); //array of roles

            if (in_array('ROLE_SUPERAGENT', $roles)) {
                $where .= ' AND a.parent_id = ' . $user->getId();
            }
            if (in_array('ROLE_VODASHOP', $roles)) {
                $where .= ' AND a.vodashop_id = ' . $user->getId();
            }

            if (strlen(@$attributes[ 'msisdn' ]) >= 4) {
                $dateforce = false;
                $where .= " AND u.msisdn like '%" . $attributes[ 'msisdn' ] . "%'";
            }
            if (strlen(@$attributes[ "first_name" ]) >= 2) {//echo "<<<<<<<<<<<<<<";
                $dateforce = false;
                $where .= " AND u.firstName like '%" . $attributes[ "first_name" ] . "%'";
            }
            if (strlen(@$attributes[ "last_name" ]) >= 2) {
                $dateforce = false;
                $where .= " AND u.lastName like '%" . $attributes[ "last_name" ] . "%'";
            }
            if (@$attributes[ "identificationType" ] >= 1) {
                $dateforce = false;
                $where .= " AND u.identificationType = '" . $attributes[ "identificationType" ] . "'";
            }
            if (strlen(@$attributes[ "id_number" ]) >= 4) {
                $dateforce = false;
                $where .= " AND u.identification like '%" . $attributes[ "id_number" ] . "%'";
            }
            if (strlen(@$attributes[ "agent_firstname" ]) >= 3) {
                $dateforce = false;
                $where .= " AND a.firstName like '%" . $attributes[ "agent_firstname" ] . "%'";
            }


            if (strlen(@$attributes[ "region" ]) == 2) {
                $dateforce = false;
                $where .= " AND u.region = '" . $attributes[ "region" ] . "'";
            }
            if (strlen(@$attributes[ "verifyBy" ]) >= 3) {
                $dateforce = false;
                //$attributes["from_date"] = ($attributes["from_date"] ? $attributes["from_date"] : date('Y-m-d H:i:s', strtotime('-30 day', strtotime(date('Y-m-d H:i:s')))) );
                $where .= " AND x.verifyBy  = '" . $attributes[ "verifyBy" ] . "'";
            }
            if (strlen(@$attributes[ "verifyDescr" ]) >= 3) {
                $dateforce = false;
                $where .= " AND x.verifyDescr like '%" . $attributes[ "verifyDescr" ] . "%'";
            }
            if (in_array(@$attributes[ "verifyState" ], array( 1, 2, 3 ))) {
                $dateforce = false;
                $where .= " AND x.verifyState  = '" . $attributes[ "verifyState" ] . "'";
            } else {
                $where .= " AND x.verifyState IN (1,2,3)";
            }


            if (strlen(@$attributes[ "agent_lastname" ]) >= 3) {
                $where .= " AND a.lastName like '%" . $attributes[ "agent_lastname" ] . "%'";
            }
            if (strlen(@$attributes[ "agent_phone" ]) >= 4) {
                $where .= " AND a.username = '" . $attributes[ "agent_phone" ] . "'";
            }
            if (@$attributes[ "territory" ] > 0) {
                $where .= " AND u.territory = '" . $attributes[ "territory" ] . "'";
            }
            if (strlen(@$attributes[ "from_date" ]) > 4) {
                $where .= " AND u.createdDate >= '" . date('Y-m-d', strtotime($attributes[ "from_date" ])) . " 00:00:00'";
            }
            if (strlen(@$attributes[ "to_date" ]) > 4) {
                $where .= " AND u.createdDate <= '" . date('Y-m-d', strtotime($attributes[ "to_date" ])) . " 23:59:59'";
            }

            if ($dateforce) {
                $where .= " AND x.createdDate >= '" . date('Y-m-d') . " 00:00:00'";
            }

            $where .= " AND x.auditState NOT IN (1,2,3,'-1')";


            $additional_fields = ", DATE_FORMAT(x.verifyDate, '%Y-%m-%d') as VerifyDate, CASE x.verifyState WHEN '1' THEN 'Good' WHEN '2' THEN 'Fair' WHEN '3' THEN 'Bad' ELSE 'Not Verified' END AS VerifyState, v.username as Verifier, x.verifyDescr as VerifyDescription ";

            $queryAttrib = $request->query->all();
            $queryString = " SELECT u.id,u.msisdn as MSISDN,u.registrationid, u.firstName as FirstName, u.middleName as MiddleName, u.lastName as LastName, u.dob as DateOfBirth, u.gender as Gender, u.nationality as Country,u.identificationType as IDType, u.identification as IDNumber, u.deviceModel as DeviceModel "
                . $additional_fields
                . "FROM RestApiBundle:RegistrationStatus x "
                . " LEFT OUTER JOIN x.registrationId u LEFT OUTER JOIN x.verifyBy v " . $where
                . " ORDER BY u.id desc ";
            $query = $this->getDoctrine()->getEntityManager()
                ->createQuery($queryString);

            $query->setMaxResults(10);
            $query->setFirstResult($queryAttrib[ "jtStartIndex" ]);

            //echo $query->getSql(); exit;

            $agents = $query->getResult();


            $entities = $em->getRepository('DashboardBundle:Idtype')->findAll();

            $id_array = array();
            foreach ($entities as $akey => $aval) {
                $id_array[ $aval->getId() ] = $aval->getName();
            }

            $territory_entiry = $em->getRepository('DashboardBundle:Territory')->findAll();

            $territory_array = array();
            foreach ($territory_entiry as $bkey => $bval) {
                $territory_array[ $bval->getId() ] = $bval->getName();
            }


            $user = $this->get('security.token_storage')->getToken()->getUser();

            $returnData[ 'Result' ] = "OK";
            $rowData = array();
            $rowData_send = array();
            if (@$agents) {
                foreach ($agents as $kkey => $vval) {

                    //update lock
                    $em->getRepository("RestApiBundle:RegistrationStatus")->updateAuditStatus($vval[ 'id' ], '-1', 'lock', $user);

                    $vval[ 'IDType' ] = $id_array[ $vval[ 'IDType' ] ];

                    $rowData_send[ 'id' ] = $vval[ 'registrationid' ];
                    $rowData_send[ 'jsonddata' ] = json_encode($vval);
                    $rowData[] = $rowData_send;
                }
            }
            $returnData[ 'Records' ] = $rowData;
            $returnData[ 'Record' ] = null;
            $returnData[ 'Message' ] = null;
            $returnData[ 'Options' ] = null;


//            echo "<pre>";
//            print_r($returnData);
//            echo "<pre>";
            echo json_encode($returnData);
            exit;

            $resp->setRecords($agents);
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * Creates a new Faq entity.
     *
     * @Route("/editUserForm", name="editUserForm")
     * @Method("POST")
     * @Template("DashboardBundle:Admin:reports.html.twig")
     */
    public function editUserFormAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQueryBuilder()
            ->select('u.email '
                . " from RestApiBundle:User u WHERE u.email = '" . $request->request->get('email') . "' AND u.id != '" . $request->request->get('userid') . "' ")
            ->setMaxResults(1)
            ->getQuery();

        $data2 = $query->getOneOrNullResult();


        if (count($data2) == 0) {


            $ddata = array(
                'firstName'       => $request->request->get('firstName'),
                'middleName'      => $request->request->get('middleName'),
                'lastName'        => $request->request->get('lastName'),
                //"username" => $request->request->get('username'),
                'msisdn'          => $request->request->get('msisdn'),
                'email'           => $request->request->get('email'),
                'adminRole'       => $request->request->get('adminRole'),
                'adminDepartment' => $request->request->get('adminDepartment'),
                'dob'             => $request->request->get('dob'),
                'user_roles'      => $request->request->get('user_roles'),
                'userid'          => $request->request->get('userid')
            );

//            echo "<pre>";
//            print_r($ddata);
//            echo "</pre>"; exit;

            $user = $em->getRepository('RestApiBundle:User')->findOneBy(array( 'id' => $request->request->get('userid') ));
            if ($user) {


                $user->setFirstName($request->request->get('firstName'));
                $user->setMiddleName($request->request->get('middleName'));
                $user->setLastName($request->request->get('lastName'));
                //$user->SetUsername($request->request->get('username'));


                $user->setRegion($em->getRepository('DashboardBundle:Region')->find($request->request->get('region')));
                $user->setTerritory($em->getRepository('DashboardBundle:Territory')->find($request->request->get('territory')));


                $user->setMobileNumber($request->request->get('msisdn'));
                $user->setEmail($request->request->get('email'));
                $user->setAdminRole($request->request->get('adminRole'));
                $user->setAdminDepartment($request->request->get('adminDepartment'));
                //$user->setDob($request->request->get('dob'));
                $user->setRoles($request->request->get('user_roles'));
                $em->flush();
            }

            return $this->redirect($this->generateUrl('admin/users') . 'sys_admin/11');
            //return $this->redirect($this->generateUrl('admin'));
        }

        echo "count 0";

        exit;
    }

    /**
     * Add a idverificationConfig.
     *
     * @Route("/idverificationConfig", name="idverificationConfig")
     * @Method("POST")
     */
    public function idverificationConfigAction(Request $request) {

//        $logger = $this->container->get("monolog.logger.ereg");
//        /** @var  $loggedIn User */
//        $loggedIn = $this->get('security.token_storage')->getToken()->getUser();

        $resp = new JsonObject();
        $status = false;
        $message = 'ERROR SUBMITING REQUEST';
        /* try { */
        $attributes = json_decode($request->getContent(), true);
        if ($attributes) {

            $attributes[ 'idtype' ] = (is_array($attributes[ 'idtype' ]) ? $attributes[ 'idtype' ] : array( $attributes[ 'idtype' ] ));

            $em = $this->getDoctrine()->getManager();
            $result = $em->createQuery('SELECT p '
                . ' from DashboardBundle:ConfigMaster p '
                . " WHERE p.name = 'id_verification_list' ")
                ->getOneOrNullResult();

            if ($result) {
                $query1 = "UPDATE ConfigMaster SET config = '" . json_encode($attributes[ 'idtype' ]) . "' WHERE name = 'id_verification_list'";
                $em->getConnection()->exec($query1);
            } else {
                $query1 = "INSERT INTO `ConfigMaster` (`name`, `config`, `version`, `createdOn`) VALUES ('id_verification_list', '" . json_encode($attributes[ 'idtype' ]) . "', 1, '" . date('Y-m-d H:i:s') . "');";
                $em->getConnection()->exec($query1);
            }

            $this->logUserEvent(UserLog::CHANGE_CONFIGS, 'idverificationConfig', array( 'auditState' => $attributes[ 'idtype' ] ));

            $status = true;
            $message = 'Successfully Saved ID';
        }
        /* } catch (\Exception $e) {
          $message = $e->getMessage();
          } */
        $resp->setMessage($message);
        $resp->setStatus($status);

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @Route("/blacklisted",name="blacklisted")
     * @Method({"POST","GET"})
     * @Template("DashboardBundle:Admin:blacklisted.html.twig")
     */
    public function blacklistedAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('DashboardBundle:BlacklistedNumbers')->findAll();

        $data = array(
            'title'       => 'Black Listed Numbers',
            'title_descr' => 'View | Add List of Black Listed Numbers',
            'entities'    => $entities,
        );

        return $this->prepareResponse($data);
    }

    /**
     * get a list of all the territoires
     * @param Request $request
     * @Method({"GET","POST"})
     * @Route("/list_id_types",name="list_id_types")
     * @return \aimgroup\DashboardBundle\Controller\JsonResponse
     */
    public function listIdTypes(Request $request) {

        $idtype = array();
        $attribs = $request->query->all();
        $regionId = isset($attribs[ 'regionId' ]) ? $attribs[ 'regionId' ] : 0;


        $em = $this->getDoctrine()->getManager();
        $result = $em->createQuery('SELECT p '
            . ' from DashboardBundle:ConfigMaster p '
            . " WHERE p.name = 'id_verification_list' ")
            ->getArrayResult();
        $ids_used = array();
        if ($result) {
            foreach ($result as $kkey => $vval) {
                $ids_used = json_decode($vval[ 'config' ]);
            }
        }

        $query = $this->getDoctrine()->getManager()
            ->createQuery('SELECT t FROM DashboardBundle:Idtype t');

        $idtype = $query->getArrayResult();

        foreach ($idtype as $key => $val) {
            $id_used_flag = 0;
            $id_array[ $val[ 'name' ] ] = array( 'Value' => $val[ 'id' ], 'DisplayText' => $val[ 'name' ] );
            if (in_array($val[ 'name' ], $ids_used)) {
                $id_used_flag = 1;
            }

            $id_array[ $val[ 'name' ] ][ 'used' ] = $id_used_flag;
        }

        $return_array[ 'Options' ] = $id_array;

        echo json_encode($return_array);
        exit;
    }

    function get_regionName_fromKey($region_key) {
        $regionsArray = array(
            'PN'          => 'PEMBA',
            'PS'          => 'PEMBA',
            'KR'          => 'KAGERA',
            'MW'          => 'BUKOBA',
            'MA'          => 'BUNDA',
            'GE'          => 'GEITA',
            'PW'          => 'KARAGWE',
            'MW'          => 'MWANZA',
            'MA'          => 'TARIME',
            'DO'          => 'DODOMA',
            'MO'          => 'KILOSA',
            'DO'          => 'KONDOA',
            'SI'          => 'MANYONI',
            'MO'          => 'MOROGORO',
            'PW'          => 'MPWAPWA',
            'SD'          => 'SINGIDA',
            'TN'          => 'KOROGWE',
            'LI'          => 'LINDI',
            'MT'          => 'MTWARA',
            'TN'          => 'TANGA',
            'ZS'          => 'ZANZIBAR',
            'ZW'          => 'ZANZIBAR',
            'ZN'          => 'ZANZIBAR',
            'PW'          => 'MKURANGA',
            'PW'          => 'BAGAMOYO',
            'DS'          => 'DAR ES SALAAM',
            'AS'          => 'ARUSHA',
            'MY'          => 'BABATI',
            'KL'          => 'MOSHI',
            'MB'          => 'MBEYA',
            'RK'          => 'MPANDA',
            'MB'          => 'RUNGWE',
            'RK'          => 'SUMBAWANGA',
            'NJ'          => 'MBARALI',
            'IR'          => 'IRINGA',
            'NJ'          => 'NJOMBE',
            'RV'          => 'SONGEA',
            'IR'          => 'MAKAMBAKO',
            'SH'          => 'BARIADI',
            'KA'          => 'KAHAMA',
            'KL'          => 'KASULU',
            'KM'          => 'KIGOMA',
            'TB'          => 'NZEGA',
            'SH'          => 'SHINYANGA',
            'TB'          => 'TABORA',
            'all_regions' => 'EntireCountry'
        );

        $regionsname = '';

        if (isset($regionsArray[ $region_key ])) {
            $regionsname = $regionsArray[ $region_key ];
        }

        return $regionsname;
    }

    function get_regionKey_fromName($region_name = NULL) {
        $regionskey = array(
            'PEMBA'         => 'PN',
            'PEMBA'         => 'PS',
            'KAGERA'        => 'KR',
            'BUKOBA'        => 'MW',
            'BUNDA'         => 'MA',
            'GEITA'         => 'GE',
            'KARAGWE'       => 'PW',
            'MAGU'          => 'MW',
            'MWANZA'        => 'MW',
            'TARIME'        => 'MA',
            'DODOMA'        => 'DO',
            'IFAKARA'       => 'MO',
            'KILOSA'        => 'MO',
            'KONDOA'        => 'DO',
            'MANYONI'       => 'SI',
            'MOROGORO'      => 'MO',
            'MPWAPWA'       => 'PW',
            'SINGIDA'       => 'SD',
            'KOROGWE'       => 'TN',
            'LINDI'         => 'LI',
            'MASASI'        => 'MT',
            'MTWARA'        => 'MT',
            'TANGA'         => 'TN',
            'ZANZIBAR'      => 'ZS',
            'ZANZIBAR'      => 'ZW',
            'ZANZIBAR'      => 'ZN',
            'MKURANGA'      => 'PW',
            'BAGAMOYO'      => 'PW',
            'BUGURUNI'      => 'DS',
            'CBD'           => 'DS',
            'KAWE'          => 'DS',
            'KINONDONI'     => 'DS',
            'TEMEKE'        => 'DS',
            'KIBAHA'        => 'DS',
            'UKONGA'        => 'DS',
            'ARUSHA NORTH'  => 'AS',
            'ARUSHA SOUTH'  => 'AS',
            'ARUSHA WEST'   => 'AS',
            'BABATI'        => 'MY',
            'HAI'           => 'KL',
            'MOSHI'         => 'KL',
            'SAME'          => 'KL',
            'CHUNYA'        => 'MB',
            'MBEYA'         => 'MB',
            'MBOZI'         => 'MB',
            'MPANDA'        => 'RK',
            'RUNGWE'        => 'MB',
            'SUMBAWANGA'    => 'RK',
            'MBARALI'       => 'NJ',
            'IRINGA'        => 'IR',
            'NJOMBE'        => 'NJ',
            'SONGEA'        => 'RV',
            'MAKAMBAKO'     => 'IR',
            'BARIADI'       => 'SH',
            'KAHAMA'        => 'KA',
            'KASULU'        => 'KL',
            'KIGOMA'        => 'KM',
            'NZEGA'         => 'TB',
            'SHINYANGA'     => 'SH',
            'TABORA'        => 'TB',
            'URAMBO'        => 'TB',
            'EntireCountry' => 'all_regions'
        );

        return $regionskey[ $region_name ];
    }

    function getGender($gender = null) {
        $gender_array = array(
            'm' => 'Male',
            'f' => 'female',
        );

        return $gender_array[ strtolower($gender) ];
    }

    function getRegStatusDesc($status) {
        $regstatus = array(
            '0' => 'Pending',
            '1' => 'Sent,Waiting Response',
            '2' => 'success',
            '3' => 'declined',
            '4' => 'fail',
            '5' => 'retry',
            '6' => 'success_cap',
            '7' => 'success_dbms'
        );

        return $regstatus[ strtolower($status) ];
    }

    private function getRegType($reg_type) {

        $states = array( '1' => 'Re-Reg', '0' => 'New', '2' => 'Declined' );

        return @$states[ $reg_type ]; // .$tstatus . $fstatus;
    }

    private function getMpesaStatus($status) {

        $states = array( '0' => 'Pending', '1' => 'Successful', '2' => 'Failed', '3' => 'Already Registered' );

        return @$states[ $status ]; // .$tstatus . $fstatus;
    }

    private function getDBMSStatus($status) {

        $states = array( '0' => 'Pending', '4' => 'Failed', '6' => 'Success', '7' => 'Success' );

        return @$states[ $status ]; // .$tstatus . $fstatus;
    }

    private function getICAPStatus($status) {

        $states = array( '0' => 'Pending', '4' => 'Failed', '6' => 'Success', '7' => 'Success' );

        return @$states[ $status ]; // .$tstatus . $fstatus;
    }

    private function getIcapRegStatusDesc($tstatus, $fstatus) {
        $ret = 'Not Sent';
        if (in_array($tstatus, array( '2', '6' ))) {
            $ret = 'TREG';
        }

        if (in_array($tstatus, array( '3', '4' ))) {
            $ret = 'Declined';
        }

        if (in_array($fstatus, array( '2', '6', '7' ))) {
            $ret = 'FREG';
        } else if (in_array($tstatus, array( '3', '4' ))) {
            $ret = 'Declined';
        }

        return $ret; // .$tstatus . $fstatus;
    }

    private function getAimRegStatusDesc($tstatus, $fstatus, $imageCount) {
        $ret = 'Pending';


        if ($imageCount < 3) {

            $ret = 'Pending';
        } else {

            if (in_array($tstatus, array( '1', '2', '6' )) || in_array($fstatus, array( '1', '2', '6', '7' ))) {
                $ret = 'Submitted';
            } else if (in_array($tstatus, array( '3', '4' )) || in_array($fstatus, array( '3', '4' ))) {
                $ret = 'Declined';
            } else {
                $ret = 'Received';
            }
        }

        return $ret; // .$tstatus . $fstatus;
    }

    private function getWhereForICAPSatus($status) {
        if ($status != null) {
            if ($status == 'notsent') {
                return ' AND x.temporaryRegStatus = 0';
            } else if ($status == 'declined') {
                return ' AND x.temporaryRegStatus IN (3,4) AND x.fullRegStatus = 0';
            } else if ($status == 'freg') {
                return ' AND x.temporaryRegStatus IN (1,2,6) AND x.fullRegStatus IN (1,6,7)';
            } else if ($status == 'treg') {
                return ' AND x.temporaryRegStatus IN (1,2,6) AND x.fullRegStatus = 0';
            }
        }

        return '';
    }

    private function getWhereForEREGSatus($status) {
        if ($status != null) {
            if ($status == 'pending') {
                return ' AND x.temporaryRegStatus = 0';
            } else if ($status == 'submitted') {
                return ' AND x.temporaryRegStatus IN (1,2,6)';
            } else if ($status == 'declined') {
                return ' AND x.temporaryRegStatus IN (3, 4) OR x.fullRegStatus IN (3, 4)';
            } else if ($status == 'received') {
                return ' AND u.image_count >= 3';
            } else if ($status == 'audited') {
                return ' AND x.auditState IN (1,2,3)';
            }

            return ' AND x.temporaryRegStatus IN (1,2,6) AND x.fullRegStatus = 0';
        }

        return '';
    }

    private function getDateTime($date, $time) {
        return $date . '<br/>' . $time;
    }

    private function getIDType($type = null) {
        $em = $this->getDoctrine()->getManager();
        $result = $em->createQuery('SELECT p from DashboardBundle:Idtype p ')
            ->getArrayResult();

        foreach ($result as $key => $val) {
            $idArray[ $val[ 'id' ] ] = $val[ 'name' ];
        }

        return @$idArray[ $type ];
    }

    private function getTerritoryFromId($regionCode = null) {
        $em = $this->getDoctrine()->getManager();
        $result = $em->createQuery('SELECT p from DashboardBundle:Territory p ')
            ->getArrayResult();
        $territoryName = '';
        foreach ($result as $key => $val) {
            $territoryArray[ $val[ 'id' ] ] = $val[ 'name' ];
        }

        if (isset($territoryArray[ $regionCode ])) {
            $territoryName = $territoryArray[ $regionCode ];
        }

        return $territoryName;
    }

    private function getRegionFromCode($regionCode = null) {
        $em = $this->getDoctrine()->getManager();
        $result = $em->createQuery('SELECT p from DashboardBundle:Region p ')
            ->getArrayResult();

        $regionName = '';

        foreach ($result as $key => $val) {
            $regionArray[ $val[ 'code' ] ] = $val[ 'name' ];
        }

        if (isset($regionArray[ $regionCode ])) {
            $regionName = @$regionArray[ $regionCode ];
        }

        return $regionName;
    }

    private function getRegionsArrayCodeName() {
        $em = $this->getDoctrine()->getManager();
        $result = $em->createQuery('SELECT p from DashboardBundle:Region p ')
            ->getArrayResult();

        foreach ($result as $key => $val) {
            $regionArray[ $val[ 'code' ] ] = $val[ 'name' ];
        }

        return $regionArray;
    }

    private function getTerritoryArrayIdName() {
        $em = $this->getDoctrine()->getManager();
        $result = $em->createQuery('SELECT p from DashboardBundle:Territory p ')
            ->getArrayResult();

        foreach ($result as $key => $val) {
            $territoryArray[ $val[ 'id' ] ] = $val[ 'name' ];
        }

        return $territoryArray;
    }

    /**
     * active_agents Summary
     *
     * @Route("/verify_region_report/{start_date}/{end_date}/{timestamp}", name="verify_region_report")
     * @Method("GET")
     * @Template()
     */
    public function verify_region_reportAction($start_date, $end_date, $timestamp) {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = ' WHERE 1=1 ';
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array('ROLE_SUPERAGENT', $roles)) {
            $where .= ' AND u.parent_id = ' . $user->getId();
            $where2 .= ' AND u.parent_id = ' . $user->getId();
        }
        if ($start_date != 1) {
            $where2 .= " AND s.createdDate >= '" . $start_date . " 00:00:00' AND s.createdDate <= '" . $end_date . " 23:59:59' ";
        }
        $where2 .= ' AND s.verifyState not in (10) ';

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

//$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "


        $territory_entiry = $em->getRepository('DashboardBundle:Territory')->findAll();

        $territory_array = array();
        $territory_array[ 'none' ] = "";
        foreach ($territory_entiry as $bkey => $bval) {
            $territory_array[ $bval->getId() ] = $bval->getName();
        }


        $region_entiry = $em->getRepository('DashboardBundle:Region')->findAll();

        $region_array = array();
        $region_array[ 'none' ] = "";
        foreach ($region_entiry as $bkey => $bval) {
            $region_array[ $bval->getId() ] = $bval->getName();
        }

        $query1 = "SELECT r0_.id AS sid, COUNT(r1_.id) AS sum_regs, u4_.territory_id AS territory_name, u4_.region_id AS region_name, DATE_FORMAT (r1_.createdDate,'%Y-%m-%d') AS created_on, r0_.verifyState AS verifyState FROM RegistrationStatus r0_ INNER JOIN registration r1_ ON r0_.registrationId = r1_.id INNER JOIN user u4_ ON r1_.owner_id = u4_.id WHERE 1 = 1 AND r0_.createdDate >= '2017-10-05 00:00:00' AND r0_.createdDate <= '2017-11-03 23:59:59' AND r0_.verifyState NOT IN (10) GROUP BY region_name, territory_name, verifyState ORDER BY sum_regs DESC";

        $connection = $em->getConnection();
        $statement = $connection->prepare($query1);
        $statement->execute();
        $result1 = $statement->fetchAll();

        $data = $active = array();
        foreach ($result1 as $key => $value) {

            $value[ 'territory_name' ] = @$territory_array[ $value[ 'territory_name' ] ];
            $value[ 'region_name' ] = @$region_array[ $value[ 'region_name' ] ];

            $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ] = (@$data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ] ? $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ] : $value);

            //first append total, good faor bad to array as zero
            $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_total' ] = @$data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_total' ] + $value[ 'sum_regs' ];
            $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_total_verified' ] = (@$data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_total_verified' ] ? $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_total_verified' ] : 0);
            $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_good' ] = (@$data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_good' ] ? $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_good' ] : 0);
            $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_fair' ] = (@$data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_fair' ] ? $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_fair' ] : 0);
            $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_bad' ] = (@$data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_bad' ] ? $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_bad' ] : 0);
            $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_unrated' ] = (@$data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_unrated' ] ? $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_unrated' ] : 0);


            if (in_array($value[ 'verifyState' ], array( 1, 2, 3, '-2', '-3' ))) {
                $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_total_verified' ] = $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_total_verified' ] + $value[ 'sum_regs' ];
            }

            //now fill in the data
            if ($value[ 'verifyState' ] == 1) {
                $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_good' ] = $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_good' ] + $value[ 'sum_regs' ];
            } elseif ($value[ 'verifyState' ] == 2 || $value[ 'verifyState' ] == -2) {
                $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_fair' ] = $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_fair' ] + $value[ 'sum_regs' ];
            } elseif ($value[ 'verifyState' ] == 3 || $value[ 'verifyState' ] == -3) {
                $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_bad' ] = $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_bad' ] + $value[ 'sum_regs' ];
            } else {
                $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_unrated' ] = $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_unrated' ] + $value[ 'sum_regs' ];
            }
        }
//        echo "<pre>";
//        print_r($data);
//        echo "<pre>";
        echo json_encode($data);
        exit;
    }


    /**
     * View Reports
     *
     * @Route("/verify_auditor_reports", name="verify_auditor_reports")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:verify_auditor_reports.html.twig")
     */
    public function verify_auditor_reportsAction() {

        if (!in_array(23, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title'       => 'Verifiers Report',
            'title_descr' => 'View Reports for Auditor | Verifier Registration Performance',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * active_agents Summary
     *
     * @Route("/getverify_auditor_reports/{start_date}/{end_date}/{timestamp}", name="getverify_auditor_reports")
     * @Method("GET")
     * @Template()
     */
    public function getverify_auditor_reportsAction($start_date, $end_date, $timestamp) {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = ' WHERE 1=1 ';
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array('ROLE_SUPERAGENT', $roles)) {
            $where .= ' AND u.parent_id = ' . $user->getId();
            $where2 .= ' AND u.parent_id = ' . $user->getId();
        }
        if ($start_date != 1) {
            $where2 .= " AND date(s.verifyDate) >= '" . $start_date . "' AND date(s.verifyDate) <= '" . $end_date . "' ";
        }
        $where2 .= " AND s.verifyState in (1, 3, 2, '-2', '-3')";

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

//$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "

        $query = $em->createQueryBuilder()
            ->select("u.id as sid, COUNT(s.id) as sum_regs, CONCAT(u.firstName , ' ', u.lastName) as agent_names, u.username, DATE_FORMAT(s.verifyDate, '%Y-%m-%d') as created_on, s.verifyState "
                . ' from RestApiBundle:RegistrationStatus s'
                . ' JOIN s.verifyBy u ' . $where2
                . ' GROUP BY s.verifyBy, s.verifyState')
            ->getQuery();

        $result1 = $query->getResult();

        $data = $active = array();
        $data[ 'active' ] = array();
        foreach ($result1 as $key => $value) {
            $data[ 'active' ][ $value[ 'sid' ] ] = (@$data[ 'active' ][ $value[ 'sid' ] ] ? $data[ 'active' ][ $value[ 'sid' ] ] : $value);

            $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_total' ] = @$data[ 'active' ][ $value[ 'sid' ] ][ 'sum_total' ] + $value[ 'sum_regs' ];
            $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_good' ] = (@$data[ 'active' ][ $value[ 'sid' ] ][ 'sum_good' ] ? $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_good' ] : 0);
            $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_fair' ] = (@$data[ 'active' ][ $value[ 'sid' ] ][ 'sum_fair' ] ? $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_fair' ] : 0);
            $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_bad' ] = (@$data[ 'active' ][ $value[ 'sid' ] ][ 'sum_bad' ] ? $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_bad' ] : 0);
            $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_unrated' ] = (@$data[ 'active' ][ $value[ 'sid' ] ][ 'sum_unrated' ] ? $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_unrated' ] : 0);

            if ($value[ 'verifyState' ] == 1) {
                $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_good' ] = $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_good' ] + $value[ 'sum_regs' ];
            } elseif ($value[ 'verifyState' ] == 2 || $value[ 'verifyState' ] == -2) {
                $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_fair' ] = $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_fair' ] + $value[ 'sum_regs' ];
            } elseif ($value[ 'verifyState' ] == 3 || $value[ 'verifyState' ] == -3) {
                $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_bad' ] = $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_bad' ] + $value[ 'sum_regs' ];
            } else {
                $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_unrated' ] = $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_unrated' ] + $value[ 'sum_regs' ];
            }
        }

        echo json_encode($data);
        exit;
    }

    /**
     * active_agents Summary
     *
     * @Route("/getverifyeditor_auditor_reports/{start_date}/{end_date}/{timestamp}", name="getverifyeditor_auditor_reports")
     * @Method("GET")
     * @Template()
     */
    public function getverifyeditor_auditor_reportsAction($start_date, $end_date, $timestamp) {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = ' WHERE 1=1 ';
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array('ROLE_SUPERAGENT', $roles)) {
            $where .= ' AND u.parent_id = ' . $user->getId();
            $where2 .= ' AND u.parent_id = ' . $user->getId();
        }
        if ($start_date != 1) {
            $where2 .= " AND date(s.verifyDate) >= '" . $start_date . "' AND date(s.verifyDate) <= '" . $end_date . "' ";
        }
        $where2 .= " AND s.verifyState in (1, 3, 2, '-2', '-3')";

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

//$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "

        $query = $em->createQueryBuilder()
            ->select("u.id as sid, COUNT(s.id) as sum_regs, CONCAT(u.firstName , ' ', u.lastName) as agent_names, u.username, DATE_FORMAT(s.verifyDate, '%Y-%m-%d') as created_on, s.verifyState "
                . ' from RestApiBundle:RegistrationStatus s'
                . ' JOIN s.verifyBy u ' . $where2
                . ' GROUP BY s.verifyBy, s.verifyState')
            ->getQuery();

        $result1 = $query->getResult();

        $data = $active = array();
        $data[ 'active' ] = array();
        foreach ($result1 as $key => $value) {
            $data[ 'active' ][ $value[ 'sid' ] ] = (@$data[ 'active' ][ $value[ 'sid' ] ] ? $data[ 'active' ][ $value[ 'sid' ] ] : $value);

            $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_total' ] = @$data[ 'active' ][ $value[ 'sid' ] ][ 'sum_total' ] + $value[ 'sum_regs' ];
            $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_good' ] = (@$data[ 'active' ][ $value[ 'sid' ] ][ 'sum_good' ] ? $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_good' ] : 0);
            $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_fair' ] = (@$data[ 'active' ][ $value[ 'sid' ] ][ 'sum_fair' ] ? $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_fair' ] : 0);
            $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_bad' ] = (@$data[ 'active' ][ $value[ 'sid' ] ][ 'sum_bad' ] ? $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_bad' ] : 0);
            $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_unrated' ] = (@$data[ 'active' ][ $value[ 'sid' ] ][ 'sum_unrated' ] ? $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_unrated' ] : 0);

            if ($value[ 'verifyState' ] == 1) {
                $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_good' ] = $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_good' ] + $value[ 'sum_regs' ];
            } elseif ($value[ 'verifyState' ] == 2 || $value[ 'verifyState' ] == -2) {
                $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_fair' ] = $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_fair' ] + $value[ 'sum_regs' ];
            } elseif ($value[ 'verifyState' ] == 3 || $value[ 'verifyState' ] == -3) {
                $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_bad' ] = $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_bad' ] + $value[ 'sum_regs' ];
            } else {
                $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_unrated' ] = $data[ 'active' ][ $value[ 'sid' ] ][ 'sum_unrated' ] + $value[ 'sum_regs' ];
            }
        }

        echo json_encode($data);
        exit;
    }

    /**
     * active_agents Summary
     *
     * @Route("/verify_daily_report/{start_date}/{end_date}/{timestamp}/{pull_all_records}", name="verify_daily_report")
     * @Method("GET")
     * @Template()
     */
    public function verify_daily_reportAction($start_date, $end_date, $timestamp, $pull_all_records) {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if ($start_date != 1) {
            $where .= " AND DATE(r0_.verifyDate) >= '" . date('Y-m-d', strtotime($start_date)) . "' AND DATE(r0_.verifyDate) <= '" . date('Y-m-d', strtotime($end_date)) . "' ";
        }
        $where .= " AND r0_.verifyState in (1, 2, 3, '-2', '-3') ";

        $where .= " AND DATE(r0_.createdDate) >= '" . date("Y-m-d", strtotime("-3 months")) . "'";


        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

//$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "

        /* $query = $em->createQueryBuilder()
          ->select("s.id as sid, COUNT(s.id) as sum_regs, DATE_FORMAT(s.verifyDate, '%Y-%m-%d') as created_on, s.verifyState "
          . " from RestApiBundle:RegistrationStatus s" . $where
          . " GROUP BY created_on, s.verifyState ORDER BY created_on DESC")
          ->getQuery();
          $result1 = $query->getResult(); */

        $query1 = "SELECT r0_.id AS sid, COUNT(r0_.id) AS sum_regs, DATE_FORMAT (r0_.verifyDate,'%Y-%m-%d') AS created_on, r0_.verifyState AS verifyState "
            . " FROM RegistrationStatus r0_ "
            . $where . " GROUP BY created_on, r0_.verifyState ORDER BY created_on DESC";

        $connection = $em->getConnection();
        $statement = $connection->prepare($query1);
        $statement->execute();
        $result1 = $statement->fetchAll();

        $data = $active = array();
        foreach ($result1 as $key => $value) {
            $data[ $value[ 'created_on' ] ] = (@$data[ $value[ 'created_on' ] ] ? $data[ $value[ 'created_on' ] ] : $value);

            $data[ $value[ 'created_on' ] ][ 'sum_total' ] = @$data[ $value[ 'created_on' ] ][ 'sum_total' ] + $value[ 'sum_regs' ];
            $data[ $value[ 'created_on' ] ][ 'sum_good' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_good' ] ? $data[ $value[ 'created_on' ] ][ 'sum_good' ] : 0);
            $data[ $value[ 'created_on' ] ][ 'sum_fair' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_fair' ] ? $data[ $value[ 'created_on' ] ][ 'sum_fair' ] : 0);
            $data[ $value[ 'created_on' ] ][ 'sum_bad' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_bad' ] ? $data[ $value[ 'created_on' ] ][ 'sum_bad' ] : 0);
            $data[ $value[ 'created_on' ] ][ 'sum_unrated' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_unrated' ] ? $data[ $value[ 'created_on' ] ][ 'sum_unrated' ] : 0);

            if ($value[ 'verifyState' ] == 1) {
                $data[ $value[ 'created_on' ] ][ 'sum_good' ] = $data[ $value[ 'created_on' ] ][ 'sum_good' ] + $value[ 'sum_regs' ];
            } elseif ($value[ 'verifyState' ] == 2 || $value[ 'verifyState' ] == -2) {
                $data[ $value[ 'created_on' ] ][ 'sum_fair' ] = $data[ $value[ 'created_on' ] ][ 'sum_fair' ] + $value[ 'sum_regs' ];
            } elseif ($value[ 'verifyState' ] == 3 || $value[ 'verifyState' ] == -3) {
                $data[ $value[ 'created_on' ] ][ 'sum_bad' ] = $data[ $value[ 'created_on' ] ][ 'sum_bad' ] + $value[ 'sum_regs' ];
            } else {
                $data[ $value[ 'created_on' ] ][ 'sum_unrated' ] = $data[ $value[ 'created_on' ] ][ 'sum_unrated' ] + $value[ 'sum_regs' ];
            }
        }

        echo json_encode($data);
        exit;
    }

    /**
     * View Reports
     *
     * @Route("/verification_reports", name="verification_reports")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:verification_reports.html.twig")
     */
    public function verification_reportsAction() {

        if (!in_array(55, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'isAdmin'     => true,
            'title'       => 'Verification Report',
            'title_descr' => 'View Reports for Registration Verification',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/verify_daily_reports", name="verify_daily_reports")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:verify_daily_reports.html.twig")
     */
    public function verify_daily_reportsAction() {

        if (!in_array(56, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title'       => 'Daily Count Report',
            'title_descr' => 'View Agents Daily Verify Breakdown Report',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/verify_daily_reports_system", name="verify_daily_reports_system")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:verify_daily_reports_system.html.twig")
     */
    public function verify_daily_reports_systemAction() {

        if (!in_array(56, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title'       => 'Agents Daily Verify Breakdown Report :: Includes System Verified',
            'title_descr' => 'View Agents Daily Verify Breakdown Report :: Includes System Verified',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/verify_agent_reports", name="verify_agent_reports")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:verify_agent_reports.html.twig")
     */
    public function verify_agent_reportsAction() {

        $em = $this->getDoctrine()->getManager();

        if (!in_array(55, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $territory_entiry = $em->getRepository('DashboardBundle:Territory')->findAll();
        $territory_array = array();
        $territory_array[ 'none' ] = "";
        foreach ($territory_entiry as $bkey => $bval) {
            $territory_array[ $bval->getId() ] = $bval->getName();
        }

        $region_entiry = $em->getRepository('DashboardBundle:Region')->findAll();
        $region_array = array();
        $region_array[ 'none' ] = "";
        foreach ($region_entiry as $bkey => $bval) {
            $region_array[ $bval->getId() ] = $bval->getName();
        }

        $data = array(
            'title'       => 'Agents Report',
            'territory'   => json_encode($territory_array),
            'region'      => json_encode($region_array),
            'title_descr' => 'View Reports for Agents Registration Verification',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/verify_region_reports", name="verify_region_reports")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:verify_region_reports.html.twig")
     */
    public function verify_region_reportsAction() {

        if (!in_array(56, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title'       => 'Regions Report',
            'title_descr' => 'View Reports for Regions Registration Verification',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/thirdparty_verifications", name="thirdparty_verifications")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:thirdparty_verifications.html.twig")
     */
    public function thirdparty_verificationsAction() {

        if (!in_array(55, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title'       => 'Verification Report',
            'title_descr' => 'View Reports for Registration Verification',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/listthirdparty_verifications", name="listthirdparty_verifications")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:thirdparty_verificationslist.html.twig")
     */
    public function listthirdparty_verificationsAction() {

        if (!in_array(55, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title'       => 'Thirt Party Verification Report',
            'title_descr' => 'View Reports for Third Party Registration Verification',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/verify_listview", name="verify_listview")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:verify_listview.html.twig")
     */
    public function verify_listviewAction() {

        $data = array(
            'title'       => 'Verification List View Report & Result',
            'title_descr' => 'Verification List View Report & Result',
            'users'       => $this->loadUsers('ROLE_VERIFYER'),
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    private function loadUsers($userType) {
        $em = $this->getDoctrine()->getManager();

        $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();

        $users = $queryBuilder->select('u')
            ->from('RestApiBundle:User', 'u')
            ->where($queryBuilder->expr()->like('u.roles', ':userType'))//('u.type = :userType ')
            ->setParameter('userType', '%' . $userType . '%')
            ->getQuery()
            ->getArrayResult();

        return $users;
    }

    /**
     * View Reports
     *
     * @Route("/verify_timebreakdown", name="verify_timebreakdown")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:verify_timebreakdown.html.twig")
     */
    public function verify_timebreakdownAction() {

        if (!in_array(56, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title'       => 'Verification Report TimeTaken Breakdown Report',
            'title_descr' => 'Verification Report TimeTaken Breakdown Report',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * active_agents Summary
     *
     * @Route("/verify_timebreakdown_report/{start_date}/{end_date}/{timestamp}", name="verify_timebreakdown_report")
     * @Method("GET")
     * @Template()
     */
    public function verify_timebreakdown_reportAction($start_date, $end_date, $timestamp) {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = ' WHERE 1=1 ';
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array('ROLE_SUPERAGENT', $roles)) {
            $where .= ' AND u.parent_id = ' . $user->getId();
            $where2 .= ' AND u.parent_id = ' . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(s.verifyDate) >= '" . $start_date . "' AND date(s.verifyDate) <= '" . $end_date . "' ";
        }

        $where .= ' AND s.verifyBy NOT IN (3260) ';


        $where .= ' AND s.verifyState IN (1,2,3)';

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');
        $emConfig->addCustomDatetimeFunction('DateDiff', 'DoctrineExtensions\Query\Mysql\Date');
        $emConfig->addCustomDatetimeFunction('Now', 'DoctrineExtensions\Query\Mysql\Date');

//$emConfig->addCustomStringFunction($name, 'aimgroup\DashboardBundle\DQL');
        $emConfig->addCustomNumericFunction('FLOOR', 'aimgroup\DashboardBundle\DQL\MysqlFloor');
//$emConfig->addCustomDatetimeFunction($name, 'aimgroup\DashboardBundle\DQL');

        $query = $em->createQuery("SELECT COUNT(s.id) as RegistrationsCount, (CASE WHEN s.verifyLock <=20 THEN '0-20' WHEN s.verifyLock >=20 AND s.verifyLock <=40 THEN '21-40' WHEN s.verifyLock >=41 AND s.verifyLock <=60 THEN '41-60' WHEN s.verifyLock >=61 AND s.verifyLock <=80 THEN '61-80' WHEN s.verifyLock >=81 AND s.verifyLock <=100 THEN '81-100' WHEN s.verifyLock >=101 AND s.verifyLock <=120 THEN '101-120' WHEN s.verifyLock >=121 AND s.verifyLock <=140 THEN '121-140' WHEN s.verifyLock >=141 AND s.verifyLock <=160 THEN '141-160' WHEN s.verifyLock >=161 THEN '161+' ELSE '1+' END) as verifyTimeRange "
            . ' from RestApiBundle:RegistrationStatus s ' . $where
            . ' GROUP BY verifyTimeRange')
            ->getArrayResult();

        $data = array( '0-20' => 0, '21-40' => 0, '41-60' => 0, '61-80' => 0, '81-100' => 0, '101-120' => 0, '121-140' => 0, '141-160' => 0, '161+' => 0 );
        foreach ($query as $key => $value) {
            $data[ $value[ 'verifyTimeRange' ] ] = $value[ 'RegistrationsCount' ];
        }

        //ksort($data);
        echo json_encode($data);
        exit;
    }

    function getVerifyStates($status = null) {
        $statename = 'Not Rated';
        $verifyStates = array(
            '1'  => 'Good',
            '2'  => 'Fair',
            '-2' => 'Fair',
            '3'  => 'Bad',
            '-3' => 'Bad'
        );
        if (isset($verifyStates[ strtolower($status) ])) {
            $statename = $verifyStates[ strtolower($status) ];
        }

        return $statename;
    }

    function getAuditStates($status = null) {
        $statename = 'Not Rated';
        $verifyStates = array(
            '1' => 'Bad',
            '2' => 'Fair',
            '3' => 'Good'
        );
        if (isset($verifyStates[ $status ])) {
            $statename = $verifyStates[ $status ];
        }

        return $statename;
    }

    /**
     * View Reports
     *
     * @Route("/registration_info_edit/{id}/{timestamp}", name="registration_info_edit")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:registration_info_edit.html.twig")
     */
    public function registrationInfoEditAction($id, $timestamp) {
        $em = $this->getDoctrine()->getManager();


        $entity = $em->getRepository('RestApiBundle:Registration')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Registration entity.');
        }

        $reg_status[ 'fullRegStatus' ] = $result[ 'fullRegDesc' ] = '';
        $result = $em->createQuery('SELECT  p from RestApiBundle:RegistrationStatus p '
            . "WHERE p.registrationId = '" . $entity->getId() . "' ")
            ->getArrayResult();

        if (count($result) == 1) {
            $reg_status = $result[ 0 ];
            $reg_status[ 'fullRegStatus' ] = $this->getRegStatusDesc($reg_status[ 'fullRegStatus' ]);
        }

        $id_types = array();
        $idtypes_list = $em->getRepository('DashboardBundle:Idtype')->findAll();
        foreach ($idtypes_list as $key => $value) {
            $id_types[ $value->getName() ] = $value->getName();
        }


        return array(
            'title'       => 'View Registrations Report:',
            'title_descr' => 'View Registrations Report',
            'id'          => $id,
            'datetime'    => strtotime(date('Y-m-d H:i:s')),
            'entity'      => $entity,
            'region'      => $this->get_regionName_fromKey($entity->getRegion()),
            'reg_status'  => $reg_status,
            'gender'      => $this->getGender($entity->getGender()),
            'idtypes'     => json_encode($id_types),
            'idtype'      => $this->getIDType($entity->getIdentificationType())
        );
    }

    /**
     * View Reports
     *
     * @Route("/verifyRecordsCount/{userId}/{timeStamp}/{randomNumb}", name="verifyRecordsCount")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:active_inactive.html.twig")
     */
    public function verifyRecordsCountAction($userId, $timeStamp, $randomNumb) {
        $em = $this->getDoctrine()->getManager();
        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $query1 = 'SELECT COUNT(r0_.id) AS sum_regs FROM RegistrationStatus r0_ WHERE r0_.verifyBy = ' . $userId . " AND r0_.verifyState IN (1, 3, 2, '-2', '-3') AND DATE(r0_.verifyDate) = '" . date('Y-m-d') . "' ";

        $connection = $em->getConnection();
        $statement = $connection->prepare($query1);
        $statement->execute();
        $result1 = $statement->fetchAll();


        $data[ 'ccount' ] = '';
        $data[ 'ccount' ] = 0;


        if ($result1) {
            $data[ 'ccount' ] = @$result1[ 0 ][ 'sum_regs' ];
        }

        echo json_encode($data);
        exit;

        $data = array(
            'title'       => 'Active & Inactive',
            'title_descr' => 'View Reports for Active Inactive',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/auditRecordsCount/{userId}/{timeStamp}/{randomNumb}/{gettype}", name="auditRecordsCount")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:active_inactive.html.twig")
     */
    public function auditRecordsCountAction($userId, $timeStamp, $randomNumb, $gettype) {
        $em = $this->getDoctrine()->getManager();
        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $timefilter = "and DATE(s.auditDate) = '" . date('Y-m-d') . "'";

        if ($gettype == 'month') {
            $query_date = date('Y-m-d');

            $timefilter = "and DATE(s.auditDate) >= '" . date('Y-m-01', strtotime($query_date)) . "' AND DATE(s.auditDate) <= '" . date('Y-m-d', strtotime($query_date)) . "'";
        }


        $query = $em->createQueryBuilder()
            ->select("COUNT(s.id) as sum_regs from RestApiBundle:RegistrationStatus s WHERE s.auditBy = '" . $userId . "' " . $timefilter . ' ')
            ->getQuery();

        $result1 = $query->getResult();

        $data[ 'ccount' ] = '';
        $data[ 'ccount' ] = 0;

        if ($result1) {
            $data[ 'ccount' ] = $result1[ 0 ][ 'sum_regs' ];
        }

        echo json_encode($data);
        exit;

        $data = array(
            'title'       => 'Active & Inactive',
            'title_descr' => 'View Reports for Active Inactive',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * verify_agents_report Summary
     *
     * @Route("/thirdPartyVerification/{start_date}/{end_date}/{timestamp}", name="thirdPartyVerification")
     * @Method("GET")
     * @Template()
     */
    public function thirdPartyVerificationAction($start_date, $end_date, $timestamp) {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = ' WHERE 1=1 ';

        if ($start_date != 1) {
            $where .= " AND date(u.createdDate) >= '" . $start_date . "' AND date(u.createdDate) <= '" . $end_date . "' ";
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

//$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "

        $query = $em->createQueryBuilder()
            ->select(" u.id, DATE_FORMAT(u.createdDate, '%Y-%m-%d') as verifiedOn, u.idType, u.resultType, u.timeTaken, u.customerRecordNames, u.customerMsisdn, u.nidaResponseString "
                . ' from DashboardBundle:Verifications u' . $where . ' ORDER BY u.id DESC')
            ->getQuery();

        $result1 = $query->getResult();

        foreach ($result1 as $key => $value) {
            $data[] = $value;
        }

        echo json_encode($data);
        exit;
    }

    /**
     * active_agents Summary
     *
     * @Route("/audit_total_report/{start_date}/{end_date}/{timestamp}", name="audit_total_report")
     * @Method("GET")
     * @Template()
     */
    public function audit_total_reportAction($start_date, $end_date, $timestamp) {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = ' WHERE 1=1 ';
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array('ROLE_SUPERAGENT', $roles)) {
            $where .= ' AND u.parent_id = ' . $user->getId();
            $where2 .= ' AND u.parent_id = ' . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(r.createdDate) >= '" . $start_date . "' AND date(r.createdDate) <= '" . $end_date . "' ";
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

//$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "

        $query = $em->createQueryBuilder()
            ->select("s.id as sid, r.image_count, COUNT(r.id) as sum_regs, DATE_FORMAT(r.createdDate, '%Y-%m-%d') as created_on, s.temporaryRegStatus as treg, s.fullRegStatus as freg, s.auditState "
                . ' from RestApiBundle:RegistrationStatus s'
                . ' JOIN s.registrationId r '
                . ' JOIN r.owner u ' . $where
                . ' GROUP BY created_on, r.image_count, s.temporaryRegStatus, s.auditState ORDER BY sum_regs DESC')
            ->getQuery();

        $result1 = $query->getResult();

        $data = $active = array();

        foreach ($result1 as $key => $value) {
            $data[ $value[ 'created_on' ] ] = (@$data[ $value[ 'created_on' ] ] ? $data[ $value[ 'created_on' ] ] : $value);

            $data[ $value[ 'created_on' ] ][ 'sum_total_reg' ] = @$data[ $value[ 'created_on' ] ][ 'sum_total_reg' ] + $value[ 'sum_regs' ];
            $data[ $value[ 'created_on' ] ][ 'sum_total_approved' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_total_approved' ] ? $data[ $value[ 'created_on' ] ][ 'sum_total_approved' ] : 0);
            $data[ $value[ 'created_on' ] ][ 'sum_total_pending' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_total_pending' ] ? $data[ $value[ 'created_on' ] ][ 'sum_total_pending' ] : 0);
            $data[ $value[ 'created_on' ] ][ 'sum_total_declined' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_total_declined' ] ? $data[ $value[ 'created_on' ] ][ 'sum_total_declined' ] : 0);
            $data[ $value[ 'created_on' ] ][ 'sum_total_audited' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_total_audited' ] ? $data[ $value[ 'created_on' ] ][ 'sum_total_audited' ] : 0);
            $data[ $value[ 'created_on' ] ][ 'sum_good' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_good' ] ? $data[ $value[ 'created_on' ] ][ 'sum_good' ] : 0);
            $data[ $value[ 'created_on' ] ][ 'sum_fair' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_fair' ] ? $data[ $value[ 'created_on' ] ][ 'sum_fair' ] : 0);
            $data[ $value[ 'created_on' ] ][ 'sum_bad' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_bad' ] ? $data[ $value[ 'created_on' ] ][ 'sum_bad' ] : 0);
            $data[ $value[ 'created_on' ] ][ 'sum_unrated' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_unrated' ] ? $data[ $value[ 'created_on' ] ][ 'sum_unrated' ] : 0);

            if (in_array($value[ 'auditState' ], array( 1, 2, 3 ))) {
                $data[ $value[ 'created_on' ] ][ 'sum_total_audited' ] = $data[ $value[ 'created_on' ] ][ 'sum_total_audited' ] + $value[ 'sum_regs' ];

                if ($value[ 'auditState' ] == 3) {
                    $data[ $value[ 'created_on' ] ][ 'sum_good' ] = $data[ $value[ 'created_on' ] ][ 'sum_good' ] + $value[ 'sum_regs' ];
                } elseif ($value[ 'auditState' ] == 2) {
                    $data[ $value[ 'created_on' ] ][ 'sum_fair' ] = $data[ $value[ 'created_on' ] ][ 'sum_fair' ] + $value[ 'sum_regs' ];
                } elseif ($value[ 'auditState' ] == 1) {
                    $data[ $value[ 'created_on' ] ][ 'sum_bad' ] = $data[ $value[ 'created_on' ] ][ 'sum_bad' ] + $value[ 'sum_regs' ];
                }
            } else {
                $data[ $value[ 'created_on' ] ][ 'sum_unrated' ] = $data[ $value[ 'created_on' ] ][ 'sum_unrated' ] + $value[ 'sum_regs' ];
            }

            if ($value[ 'image_count' ] >= 3) {
                $data[ $value[ 'created_on' ] ][ 'sum_total_approved' ] = $data[ $value[ 'created_on' ] ][ 'sum_total_approved' ] + $value[ 'sum_regs' ];
            } else {
                $data[ $value[ 'created_on' ] ][ 'sum_total_pending' ] = $data[ $value[ 'created_on' ] ][ 'sum_total_pending' ] + $value[ 'sum_regs' ];
            }
            if (in_array($value[ 'treg' ], array( 4, 5, 3 ))) {
                $data[ $value[ 'created_on' ] ][ 'sum_total_declined' ] = $data[ $value[ 'created_on' ] ][ 'sum_total_declined' ] + $value[ 'sum_regs' ];
            }
        }

        echo json_encode($data);
        exit;
    }

    /**
     * View Reports
     *
     * @Route("/audit_total_reports", name="audit_total_reports")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:audit_total_reports.html.twig")
     */
    public function audit_total_reportsAction() {

        if (!in_array(23, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title'       => 'Total Registrations Report',
            'title_descr' => 'Total Registrations Report',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/reg_type_report", name="reg_type_report")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:reg_type_report.html.twig")
     */
    public function reg_type_reportAction() {

        $data = array(
            'title'       => 'Registration Type : New vs Old Registrations',
            'title_descr' => 'View Reports for Active Inactive',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * active_agents Summary
     *
     * @Route("/getdatareg_type_report/{start_date}/{end_date}/{timestamp}", name="getdatareg_type_report")
     * @Method("GET")
     * @Template()
     */
    public function getdatareg_type_reportAction($start_date, $end_date, $timestamp) {
        $em = $this->getDoctrine()->getManager();

        $where = $where = ' WHERE 1=1 ';
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if ($start_date != 1) {
            $where .= " AND date(r.createdDate) >= '" . $start_date . "' AND date(r.createdDate) <= '" . $end_date . "' ";
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

//$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "

        $query = $em->createQueryBuilder()
            ->select("count(r.id) as ccount, date(r.createdDate) as ddate, case when r.regType IS NULL or r.regType = '' then '0' else r.regType end as regType  "
                . ' from RestApiBundle:RegistrationStatus r' . $where
                . ' GROUP BY ddate, regType ORDER BY r.id desc')
            ->getQuery();

        $result1 = $query->getResult();

        $data[ 'active' ] = array();
        foreach ($result1 as $key => $value) {

            $data[ 'active' ][ $value[ 'ddate' ] ][ 'ddate' ] = (@$data[ 'active' ][ $value[ 'ddate' ] ][ 'ddate' ] ? $data[ 'active' ][ $value[ 'ddate' ] ][ 'ddate' ] : $value[ 'ddate' ]);
            $data[ 'active' ][ $value[ 'ddate' ] ][ $this->getRegType(0) ] = (@$data[ 'active' ][ $value[ 'ddate' ] ][ $this->getRegType(0) ] ? $data[ 'active' ][ $value[ 'ddate' ] ][ $this->getRegType(0) ] : 0);
            $data[ 'active' ][ $value[ 'ddate' ] ][ $this->getRegType(1) ] = (@$data[ 'active' ][ $value[ 'ddate' ] ][ $this->getRegType(1) ] ? $data[ 'active' ][ $value[ 'ddate' ] ][ $this->getRegType(1) ] : 0);
            $data[ 'active' ][ $value[ 'ddate' ] ][ $this->getRegType(2) ] = (@$data[ 'active' ][ $value[ 'ddate' ] ][ $this->getRegType(2) ] ? $data[ 'active' ][ $value[ 'ddate' ] ][ $this->getRegType(2) ] : 0);

            $data[ 'active' ][ $value[ 'ddate' ] ][ $this->getRegType($value[ 'regType' ]) ] = $value[ 'ccount' ];
        }

        $returndata[ 'active' ] = array();
        foreach ($data[ 'active' ] as $keyy => $val) {
            $returndata[ 'active' ][] = $val;
        }

//        echo "<pre>";
//        print_r($data);
//        echo "<pre>";

        echo json_encode($returndata);
        exit;
    }

    /**
     * Creates a new Faq entity.
     *
     * @Route("/editUserFormPost/{id}", name="editUserFormPost")
     * @Method("POST")
     * @Template("DashboardBundle:Admin:reports.html.twig")
     */
    public function editUserFormPostAction($id, Request $request) {

        $keyId = $request->request->get('id');

        if ($request->request->get('value') == null || $request->request->get('value') == '') {
            echo '';
            exit;
        }

        $em = $this->getDoctrine()->getManager();


        $registration = $em->getRepository('RestApiBundle:Registration')->find($id);
        if (!$registration) {
            throw $this->createNotFoundException('Unable to find Registration entity.');
        }

        $registrationId = $registration->getRegistrationid();

        $strlenth = strlen($request->request->get('value'));

        if ($keyId == 'FirstName') {
            $registration->setFirstName($request->request->get('value'));
        } elseif ($keyId == 'MiddleName') {
            $registration->setMiddleName($request->request->get('value'));
        } elseif ($keyId == 'LastName') {
            $registration->setLastName($request->request->get('value'));
        } elseif ($keyId == 'Gender' && $strlenth >= 1) {
            $registration->setGender($request->request->get('value'));
        } elseif ($keyId == 'DoB') {
            $registration->setDob(date('Y-m-d', strtotime($request->request->get('value'))));
        } elseif ($keyId == 'SimSerial') {
            $registration->setSimSerial($request->request->get('value'));
        } elseif ($keyId == 'IDNumber') {//Identification
            $registration->setIdentification(trim($request->request->get('value')));
        } elseif ($keyId == 'IdentificationType') {
            $registration->setIdentificationType($request->request->get('value'));
        } elseif ($keyId == 'Email') {
            $registration->setEmail($request->request->get('value'));
        } elseif ($keyId == 'Nationality' && $strlenth >= 1) {
            $registration->setNationality($request->request->get('value'));
        }


        $em->flush();


        /*
          $registrationStatus = $em->getRepository('RestApiBundle:RegistrationStatus')->findBy(array("registrationId" => $registration->getId()));
          if (!$registrationStatus) {
          throw $this->createNotFoundException('Unable to find Registration entity.');
          }

          //echo "<pre>"; print_r($registrationStatus); echo "</pre>"; exit;

          $registrationStatus->setTemporaryRegStatus('0');
          $registrationStatus->setFullRegStatus('0');
          $registrationStatus->setFullRegDesc('Reset By EditRecord');
          $em->flush();
         */

        $query1 = "UPDATE RegistrationStatus SET temporaryRegStatus = '0', icap_state = '0', icap_counter = '0', fullRegStatus = '0', temporaryRegDesc = 'Reset By EditRecord2', fullRegDesc = '' WHERE registrationId = " . $id;
        $em->getConnection()->exec($query1);

        echo $request->request->get('value');

        exit;

        return array(
            'title' => 'View Registrations Report:'
        );
    }

    /**
     * active_agents Summary
     *
     * @Route("/pullsmsreg_type/{start_date}/{end_date}/{timestamp}", name="pullsmsreg_type")
     * @Method("GET")
     * @Template()
     */
    public function pullsmsreg_typeAction($start_date, $end_date, $timestamp) {
        $em = $this->getDoctrine()->getManager();

        /*
         * SELECT `phone`, `users`.`first_name` as first_name, `users`.`last_name` as last_name, count(sreg_dashboard_registrations.id) as count
         * FROM (`sreg_dashboard_registrations`)
         * LEFT JOIN `users` ON `sreg_dashboard_registrations`.`created_by` = `users`.`id`
         * GROUP BY `sreg_dashboard_registrations`.`created_by`
         * ORDER BY `count` desc
         * LIMIT 10
         */

        $where = $where2 = ' WHERE 1=1 AND r.isSms = 1 ';
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array('ROLE_SUPERAGENT', $roles)) {
            $where .= ' AND u.parent_id = ' . $user->getId();
            $where2 .= ' AND u.parent_id = ' . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(r.createdDate) >= '" . $start_date . "' AND date(r.createdDate) <= '" . $end_date . "' ";
        }


        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $query = $em->createQueryBuilder()
            ->select("count(r.id) as ccount, DATE_FORMAT(r.createdDate, '%Y-%m-%d') as created_on, r.appVersion, r.deviceModel "
                . ' from RestApiBundle:Registration r '
                . ' JOIN r.owner u ' . $where
                . ' GROUP BY r.deviceModel, r.appVersion')
            ->getQuery();

        $result1 = $query->getResult();

        echo json_encode($result1);
        exit;

        return array(
            'title'         => 'View Reports for:',
            'title_descr'   => 'View Reports for',
            'entities_user' => $entities_user,
            //'entities' => $entities,
        );
    }

    /**
     * View Reports
     *
     * @Route("/smsreg_type", name="smsreg_type")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:smsreg_type.html.twig")
     */
    public function smsreg_typeAction() {

        $data = array(
            'title'       => 'Registration Type [ SMS / Web ]',
            'title_descr' => 'View Reports for Registration Type',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/day_summary", name="day_summary")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:day_summary.html.twig")
     */
    public function day_summaryAction() {

        $data = array(
            'title'       => 'Day Summary',
            'title_descr' => 'View Day Summary Report',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/app_verion_report", name="app_verion_report")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:app_verion_report.html.twig")
     */
    public function app_verion_reportAction() {

        $data = array(
            'title'       => 'App Version Report',
            'title_descr' => 'View Day Summary Report',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * active_agents Summary
     *
     * @Route("/pullapp_verion_report/{region}/{territory}/{timestamp}", name="pullapp_verion_report")
     * @Method("GET")
     * @Template()
     */
    public function pullapp_verion_reportAction($region, $territory, $timestamp) {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = ' WHERE 1=1 AND r.appVersion IS NOT NULL ';
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array('ROLE_SUPERAGENT', $roles)) {
            $where .= ' AND u.parent_id = ' . $user->getId();
            $where2 .= ' AND u.parent_id = ' . $user->getId();
        }

        if ($region != 'ALL') {
            $where .= ' AND u.region = ' . $region;
        }
        if ($territory != 'ALL') {
            $where .= ' AND u.territory = ' . $territory;
        }

        $query = $em->createQueryBuilder()
            ->select('count(r.id) as ccount, r.appVersion '
                . ' from RestApiBundle:Device r '
                . ' JOIN r.user u ' . $where
                . ' GROUP BY r.appVersion')
            ->getQuery();

        $result1 = $query->getResult();

        echo json_encode($result1);
        exit;

        return array(
            'title'         => 'View Reports for:',
            'title_descr'   => 'View Reports for',
            'entities_user' => $entities_user,
            //'entities' => $entities,
        );
    }

    /**
     * View Reports
     *
     * @Route("/externalvisual", name="externalvisual")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:externalvisual.html.twig")
     */
    public function externalvisualAction() {

        if (!in_array(53, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title'       => 'User: viewer Pass: viewer',
            'title_descr' => 'Reports External Visual',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * active_agents Summary
     *
     * @Route("/pullimage_matrix/{start_date}/{end_date}/{timestamp}", name="pullimage_matrix")
     * @Method("GET")
     * @Template()
     */
    public function pullimage_matrixAction($start_date, $end_date, $timestamp) {

        $where = ' WHERE 1=1 ';
        $em = $this->getDoctrine()->getManager();
        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');
        $emConfig->addCustomDatetimeFunction('DateDiff', 'DoctrineExtensions\Query\Mysql\Date');
        $emConfig->addCustomDatetimeFunction('Now', 'DoctrineExtensions\Query\Mysql\Date');

        $where .= " AND DATE(createdOn) >= '" . $start_date . "' ";
        $where .= " AND DATE(createdOn) <= '" . $end_date . "' ";


        $sql = "select count(*) as RecordsCount, PotraitImage, IDFrontImage, IDBackImage, SignatureImage, concat(PotraitImage, IDFrontImage, IDBackImage, SignatureImage) as conc from (select CASE MAX(IF(imageType = 'potrait', webPath, NULL)) WHEN ISNULL(webPath) THEN '1' ELSE '0' END  AS PotraitImage, CASE MAX(IF(imageType = 'front-pic', webPath, NULL)) WHEN ISNULL(webPath) THEN '1' ELSE '0' END AS IDFrontImage, CASE MAX(IF(imageType = 'rear-pic', webPath, NULL)) WHEN ISNULL(webPath) THEN '1' ELSE '0' END AS IDBackImage, CASE MAX(IF(imageType = 'signature', webPath, NULL)) WHEN ISNULL(webPath) THEN '1' ELSE '0' END AS SignatureImage FROM reg_images " . $where . ' group by registration) as t group by conc;';

        $em = $this->getDoctrine()->getManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $count = $stmt->fetchAll();

        echo json_encode($count);
        exit;


        return array(
            'title'         => 'View Reports for:',
            'title_descr'   => 'View Reports for',
            'entities_user' => $entities_user,
            //'entities' => $entities,
        );
    }

    /**
     * View Reports
     *
     * @Route("/image_matrix", name="image_matrix")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:image_matrix.html.twig")
     */
    public function image_matrixAction() {

        $data = array(
            'title'       => 'Registration Image Matrix',
            'title_descr' => 'View Registration Image Matrix',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * active_agents Summary
     *
     * @Route("/pullimage_matrix_registrations/{portait}/{idfront}/{idback}/{signature}/{start_date}/{end_date}/{timestamp}", name="pullimage_matrix_registrations")
     * @Method("GET")
     * @Template()
     */
    public function pullimage_matrix_registrationsAction($portait, $idfront, $idback, $signature, $start_date, $end_date, $timestamp) {

        $where = ' WHERE 1=1 ';
        $em = $this->getDoctrine()->getManager();
        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');
        $emConfig->addCustomDatetimeFunction('DateDiff', 'DoctrineExtensions\Query\Mysql\Date');
        $emConfig->addCustomDatetimeFunction('Now', 'DoctrineExtensions\Query\Mysql\Date');

        $where .= " AND DATE(createdOn) >= '" . $start_date . "' ";
        $where .= " AND DATE(createdOn) <= '" . $end_date . "' ";


        //sorry for the sql... have fun :)
        $sql = "SELECT DATE(p.createdDate) AS CREATEDDATE, p.msisdn AS MSISDN, CONCAT(p.firstName, ' ', p.lastName) AS CUSTOMERNAME, IFNULL(p.identification, ' ') AS IDNUMBER, IFNULL(a1_.name, ' ') AS IDTYPE, IFNULL(u.username, ' ') AS AGENTMOBILE, CONCAT(u.first_name, ' ', u.last_name) AS AGENTNAME, CONCAT(u2_.first_name, ' ', u2_.last_name) AS SUPERAGENTNAME, IFNULL(r5_.name, ' ') as REGIONNAME, IFNULL(t3_.name, ' ') AS TERRITORYNAME, IFNULL(p.deviceModel, ' ') AS DEVICEMODEL, CASE r3_.reg_type WHEN '0' THEN 'NewReg' WHEN '1' THEN 'NewReg' WHEN '2' THEN 'Re-Reg' ELSE 'New' END AS REGTYPE, CASE r3_.mpesa_state WHEN '0' THEN 'Pending' WHEN '1' THEN 'Successful' WHEN '2' THEN 'Failed' WHEN '3' THEN 'Already Registered' ELSE 'Pending' END AS MPESASTATE, CASE r3_.icap_state WHEN '0' THEN 'Pending' WHEN '4' THEN 'Failed' WHEN '6' THEN 'Success' WHEN '7' THEN 'Success' ELSE 'Pending' END AS ICAP_STATE, CASE r3_.dbms_state WHEN '0' THEN 'Pending' WHEN '4' THEN 'Failed' WHEN '6' THEN 'Success' WHEN '7' THEN 'Success' ELSE 'Pending' END AS DBMS_STATE, CASE CONCAT(r3_.temporaryRegStatus, '', r3_.fullRegStatus) WHEN '20' THEN 'TREG' WHEN '60' THEN 'TREG' WHEN '70' THEN 'TREG' WHEN '24' THEN 'TREG' WHEN '64' THEN 'TREG' WHEN '74' THEN 'TREG' WHEN '22' THEN 'FREG' WHEN '26' THEN 'FREG' WHEN '27' THEN 'FREG' WHEN '62' THEN 'FREG' WHEN '66' THEN 'FREG' WHEN '67' THEN 'FREG' WHEN '72' THEN 'FREG' WHEN '76' THEN 'FREG' WHEN '77' THEN 'FREG' WHEN '07' THEN 'FREG' WHEN '17' THEN 'FREG' WHEN '06' THEN 'FREG' WHEN '16' THEN 'FREG' WHEN '26' THEN 'FREG' WHEN '40' THEN 'DECLINED' WHEN '30' THEN 'DECLINED' WHEN '40' THEN 'DECLINED' WHEN '00' THEN 'PENDING' ELSE 'Pending' END AS ICAPSTATE, CASE r3_.temporaryRegStatus WHEN '0' THEN 'Pending' WHEN '1' THEN 'Pending' WHEN '2' THEN 'Success' WHEN '3' THEN 'Declined' WHEN '4' THEN 'Fail' WHEN '5' THEN 'Retry' WHEN '6' THEN 'SuccessICAP' WHEN '7' THEN 'SuccessDBMS' ELSE 'Pending' END AS TEMPREGSTATUS, REPLACE(IFNULL(r3_.temporaryRegDesc, ' '), ',', ' ') AS TEMPREGDESCRIPTION, CASE r3_.fullRegStatus WHEN '0' THEN 'Pending' WHEN '1' THEN 'Pending' WHEN '2' THEN 'Success' WHEN '3' THEN 'Declined' WHEN '4' THEN 'Fail' WHEN '5' THEN 'Retry' WHEN '6' THEN 'SuccessICAP' WHEN '7' THEN 'SuccessDBMS' ELSE 'Pending' END AS FULLREGSTATUS, REPLACE(IFNULL(r3_.fullRegDesc, ' '), ',', ' ') AS FULLREGDESCR, CASE r3_.auditState WHEN '0' THEN 'NotAudited' WHEN '1' THEN 'Bad' WHEN '2' THEN 'Fair' WHEN '3' THEN 'Good' WHEN '-1' THEN 'Locked' ELSE 'NotAudited' END AS AUDITSTATE, CONCAT(u3_.first_name, ' ', u3_.last_name) as AUDITORNAME, REPLACE(r3_.auditDescr, ',', ' AND ') AS AUDITDESCR, DATE(r3_.auditDate) as AUDITDATE FROM (select registration, count(*), CASE MAX(IF(imageType = 'potrait', webPath, NULL)) WHEN ISNULL(webPath) THEN 1 ELSE 0 END  AS potraitImage, LEFT(MAX(IF(imageType = 'potrait', webPath, NULL)), 10) as PORTRAITorg,  CASE MAX(IF(imageType = 'front-pic', webPath, NULL)) WHEN ISNULL(webPath) THEN 1 ELSE 0 END AS frontpic, LEFT(MAX(IF(imageType = 'front-pic', webPath, NULL)), 10) as FRONTORG, CASE MAX(IF(imageType = 'rear-pic', webPath, NULL)) WHEN ISNULL(webPath) THEN 1 ELSE 0 END AS rearpic, LEFT(MAX(IF(imageType = 'rear-pic', webPath, NULL)), 10) as REARPICorg, CASE MAX(IF(imageType = 'signature', webPath, NULL)) WHEN ISNULL(webPath) THEN 1 ELSE 0 END AS signatureImage, LEFT(MAX(IF(imageType = 'signature', webPath, NULL)), 10) as SIGNATUREorg FROM reg_images where date(createdOn) >= '" . $start_date . "' AND date(createdOn) <= '" . $end_date . "' group by registration) as rc_ left join registration p on p.registrationid = rc_.registration LEFT JOIN RegistrationStatus r3_ ON r3_.registrationId = p.id LEFT JOIN user u ON p.owner_id = u.id LEFT JOIN super_agent s4_ ON u.id = s4_.id LEFT JOIN agent a5_ ON u.id = a5_.id LEFT JOIN user u2_ ON u.parent_id_id = u2_.id LEFT JOIN super_agent s6_ ON u2_.id = s6_.id LEFT JOIN Region r5_ on r5_.code = p.region LEFT JOIN agent a7_ ON u2_.id = a7_.id LEFT JOIN Idtype a1_ on a1_.id = p.identificationType LEFT JOIN Territory t3_ on t3_.id = p.territory LEFT JOIN user u3_ ON r3_.auditBy = u3_.id where rc_.potraitImage = " . $portait . ' and rc_.frontpic = ' . $idfront . ' and rc_.rearpic = ' . $idback . ' and rc_.signatureImage = ' . $signature . '; ';

        $em = $this->getDoctrine()->getManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $count = $stmt->fetchAll();

        echo json_encode($count);
        exit;


        return array(
            'title'         => 'View Reports for:',
            'title_descr'   => 'View Reports for',
            'entities_user' => $entities_user,
            //'entities' => $entities,
        );
    }

    function checkSessionPermission($check) {
        #return true;
        $permsArray = json_decode($this->session->get('user_role_perms'), true);
        foreach ($permsArray as $key => $val) {
            if ($check == $val) {
                return TRUE;
            }
        }

        return FALSE;
    }

    /**
     * View Reports
     *
     * @Route("/newverification", name="newverification")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:newverification.html.twig")
     */
    public function newverificationAction() {

        if (!$this->checkSessionPermission(49)) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $em = $this->getDoctrine()->getManager();
        $idtype = $em->getRepository('DashboardBundle:Idtype')->findAll();

        $user = $this->get('security.token_storage')->getToken()->getUser();

        //--
        $configArray = array();
        $numRows = 1;
        $secsCount = 40;
        $query = $em->createQueryBuilder()
            ->select(" u from DashboardBundle:ConfigMaster u WHERE u.name = 'verifierFieldList' ")
            ->getQuery();

        /** @var  $configMaster ConfigMaster */
        $configMaster = $query->getOneOrNullResult();

        $allconfig = null;

        if ($configMaster) {
            $allconfig = json_decode($configMaster->getConfig(), true);

            //        echo "<p>**** * dbstring * ****</p><pre><small><small><small>";
            //        print_r($allconfig['validations']);
            //        echo "</small></small></small></pre><p>**** * /dbstring * ****</p>";


            $numRows = 1;
            $configArray = explode(':::', $allconfig[ 'queryString' ]);
            $settingMerged = explode(':::', $configMaster->getConfigType());

            $numRows = @$settingMerged[ 0 ];
            $secsCount = @$settingMerged[ 1 ];
        }
        //--
        //echo json_encode($allconfig['validations']);

        $data = array(
            'title'       => 'New Verification Page',
            'title_descr' => 'Verify registration',
            'idtype'      => $idtype,
            'numRows'     => $numRows,
            'verifyArray' => $allconfig ? json_encode($allconfig[ 'validations' ]) : json_encode(array()),
            'secsCount'   => $secsCount,
            'user_id'     => $user->getId(),
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/editnewverification", name="editnewverification")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:newverification_edit.html.twig")
     */
    public function newverificationeditAction() {

        if (!$this->checkSessionPermission(71)) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $em = $this->getDoctrine()->getManager();
        $idtype = $em->getRepository('DashboardBundle:Idtype')->findAll();

        $user = $this->get('security.token_storage')->getToken()->getUser();

        //--
        $configArray = array();
        $numRows = 1;
        $secsCount = 40;
        $query = $em->createQueryBuilder()
            ->select(" u from DashboardBundle:ConfigMaster u WHERE u.name = 'verifierFieldList' ")
            ->getQuery();

        /** @var  $configMaster ConfigMaster */
        $configMaster = $query->getOneOrNullResult();

        $allconfig = null;

        if ($configMaster) {
            $allconfig = json_decode($configMaster->getConfig(), true);

            //        echo "<p>**** * dbstring * ****</p><pre><small><small><small>";
            //        print_r($allconfig['validations']);
            //        echo "</small></small></small></pre><p>**** * /dbstring * ****</p>";


            $numRows = 1;
            $configArray = explode(':::', $allconfig[ 'queryString' ]);
            $settingMerged = explode(':::', $configMaster->getConfigType());

            $numRows = @$settingMerged[ 0 ];
            $secsCount = @$settingMerged[ 1 ];
        }
        //--

        $data = array(
            'title'       => 'Edit Verification Page',
            'title_descr' => 'Edit Verified registration',
            'idtype'      => $idtype,
            'numRows'     => $numRows,
            'verifyArray' => $allconfig ? json_encode($allconfig[ 'validations' ]) : json_encode(array()),
            'secsCount'   => $secsCount,
            'user_id'     => $user->getId(),
            'id'          => $user->getId(),
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    function getRecordForVerification($verifyState) {
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();

        $where3 = "";
        if ($verifyState == 2) {
            $vstate = 2;
        }

        $where3 .= " AND registry_type = 1 AND createdDate >= '2016-" . date("m-d", strtotime("-1 week")) . "'";

        $sql1 = "select registrationid, verifyDescr, verifyState from RegistrationStatus where registrationid = "
            . "(select registrationid from RegistrationStatus where verifyState = " . $verifyState .
            " and allimageDate is not null " . $where3 . " order by id desc limit 1) and verifyState = " . $verifyState . " FOR UPDATE";

        if ($verifyState === 0) {
            $sql1 = "select registrationid, verifyDescr, verifyState from RegistrationStatus where registrationid = "
                . "(select registrationid from RegistrationStatus where verifyState in (" . $verifyState .
                ") and allimageDate is not null " . $where3 . " order by id desc limit 1) and verifyState in (" . $verifyState . ") FOR UPDATE";
        }


        $stmt1 = $em->getConnection()->prepare($sql1);
        $stmt1->execute();

        $resultRegStatus = $stmt1->fetch();

        if ($resultRegStatus) {
            $user = $this->get('security.token_storage')->getToken()->getUser();

            $locker = ($verifyState == '0' || $verifyState == 0) ? '-1' : '-' . @$resultRegStatus[ 'verifyState' ];
            $locked = $this->lockRegistrationForVerification($resultRegStatus[ 'registrationid' ], $locker, $user);

            if ($locked) {
                return $resultRegStatus;
            }
        } else {
            return "no_record";
        }

        return FALSE;
    }

    /**
     * @param Request $request
     * @Route("/newlist_verify_registrations/{limit}/{timestamp}/{verifyState}",name="newlist_verify_registrations")
     * @Method({"POST","GET"})
     */
    public function newlist_verify_registrationsAction($limit, $timestamp, $verifyState = 0, Request $request) {
        $em = $this->getDoctrine()->getManager();
        $response[ 'success' ] = 0;

        try {

            // exit('Hellow');
            //Get List Fields
            $query = $em->createQueryBuilder()
                ->select(" u from DashboardBundle:ConfigMaster u WHERE u.name = 'verifierFieldList' ")
                ->getQuery();
            $configMaster = $query->getOneOrNullResult();

            //exit('Hapa..');
            if ($configMaster) {
                $allconfig = json_decode($configMaster->getConfig(), true);

                $selectFields = str_replace(":::", ", ", $allconfig[ 'queryString' ]);

                //-----------------------------------

                $try = true;
                $nn = 1;
                while ($try) {
                    $resultRegStatus = $this->getRecordForVerification($verifyState);
                    if ($resultRegStatus) {
                        if ($resultRegStatus == 'no_record') {
                            $resultRegStatus = FALSE;
                        }
                        $try = FALSE;
                    }

                    $nn++;

                    if ($nn == 20) {
                        $try = FALSE;
                    }
                }

                //-----------------------------------

                if ($resultRegStatus) {

                    //Pull Records..
                    $sql = "select u.registrationid, u.id as reg_id, " . $selectFields . " FROM registration u "
                        . " left join Idtype i on i.id = u.identificationType "
                        . " where u.id = "
                        . $resultRegStatus[ 'registrationid' ];

                    $stmt = $em->getConnection()->prepare($sql);
                    $stmt->execute();
                    $result = $stmt->fetchAll();


                    if ($result) {
                        $response[ 'success' ] = 1;
                        foreach ($result as $index => $result) {
                            $result[ 'reg_images' ] = $this->getImagesFromRegid($result[ 'registrationid' ]);
                            $response[ 'verification_description' ] = $resultRegStatus[ 'verifyDescr' ];
                            $response[ 'records' ][] = $result;

                            //$locker = ($verifyState == '0' || $verifyState == 0) ? '-1' : '-' . $verifyState;
                            //$this->lockRegistrationForVerification($result['reg_id'], $locker, $user);
                            //unset($result['reg_id']);
                        }
                    }

                    $em->getConnection()->commit();
                } else {
                    $response[ 'success' ] = 0;
                    $response[ 'resp_message' ] = "There are currently no registrations to verify.. please refresh screen in a few minutes to continue the verification process. ";
                }
            }
            echo json_encode($response);
        } catch (OptimisticLockException $e) {
            //echo "OptimisticLockException: " . $e->getMessage();
            $em->getConnection()->rollback();
            throw $e;
        } catch (\Exception $ex) {
            $em->getConnection()->rollback();
            throw $ex;
        }
        exit;

        return $this->prepareResponse($data);
    }

    function getImagesFromRegid($registrationid) {
        $images[ 'potrait' ] = 'No Image Yet';
        $images[ 'front-pic' ] = 'No Image Yet';
        $images[ 'rear-pic' ] = 'No Image Yet';
        $images[ 'signature' ] = 'No Image Yet';

        $sql = "select * FROM reg_images where registration = '" . $registrationid . "';";

        $em = $this->getDoctrine()->getManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();

        if ($result) {
            foreach ($result as $kkey => $vval) {
                $images[ $vval[ 'imageType' ] ] = $vval[ 'webPath' ];
            }
        }

        return $images;
    }

    public function lockRegistrationForVerification($registration_id, $verifyState, $user) {
        $em = $this->getDoctrine()->getManager();

        $sql = "select verifyState from RegistrationStatus where registrationId = " . $registration_id;

        $em = $this->getDoctrine()->getManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();


        if (in_array(@$result[ 0 ][ 'verifyState' ], array( 0, 2 ))) {

            $apiHelper = $this->container->get('api.helper');
            $apiHelper->logInfo('VERIFY', 'lockRegistrationForVerification', array(
                'RegID:: '       => $registration_id,
                'ResultFROMdb: ' => json_encode(@$result),
                'USERID: '       => $user->getId(),
                'dateTIME'       => date('Y-m-d H:i:s')
            ));


            $query1 = "UPDATE RegistrationStatus SET verifyState =  '" . $verifyState . "', verifyDate = '" . date('Y-m-d H:i:s') . "', verifyLock = '" . date('Y-m-d H:i:s') . "', verifyBy = '" . $user->getId() . "'  WHERE registrationid = " . $registration_id;
            $em->getConnection()->exec($query1);

            return TRUE;
        }

        return FALSE;
    }

    public function lockVerifytatus($registration_id, $verifyState, $verifyDesc, $user, $auditDescr) {
        $em = $this->getDoctrine()->getManager();

        $query1 = "UPDATE RegistrationStatus SET icap_state = 88, dbms_state = dbms_state + 1, auditDescr = CONCAT(auditDescr, ' ::" . $user->getId() . '-' . date('Y-m-d H:i:s') . ":: '), verifyState =  '" . $verifyState . "', verifyDate = '" . date('Y-m-d H:i:s') . "', verifyLock = '" . date('Y-m-d H:i:s') . "', verifyBy = '" . $user->getId() . "' WHERE registrationid = " . $registration_id;
        $em->getConnection()->exec($query1);
    }

    public function updateVerificationStatus(Registration $registration, $tempRegStatus, $fullRegStatus, $tempRegDesc, $fullRegDesc, $verifyState, $verifyDesc, $user, $timetaken, $action) {

        $em = $this->getDoctrine()->getManager();
        // if declined
        $setDeclined = '';
        if ($verifyState == 3) {


            $registration->setSimSerial($registration->getSimSerial() . '_' . date('YmdHis'));
            $em->persist($registration);
            $em->flush();

            $setDeclined = ", fullRegStatus = '4', fullRegDesc = 'Verification Declined'  ";
        } else {
            $setDeclined = ", fullRegStatus = '0', fullRegDesc = '' ";
        }

        $query1 = '';
        if ($action == 1) {
            $query1 = "UPDATE RegistrationStatus SET verifyState =  '" . $verifyState . "', verifyDate = '" . date('Y-m-d H:i:s') . "', verifyLock = '" . $timetaken . "', verifyBy = '" . $user->getId() . "', verifyDescr = '" . addslashes($verifyDesc) . "', originalVerifyBy = '" . $user->getId() . "', originalVerifyDescr = '" . $verifyDesc . "', originalVerifyDate = '" . date('Y-m-d H:i:s') . "'" . $setDeclined . " WHERE registrationid = " . $registration->getId() . " AND fullRegStatus  IN (0,4)";
        } else {
            $query1 = "UPDATE RegistrationStatus SET verifyState =  '" . $verifyState . "', verifyDate = '" . date('Y-m-d H:i:s') . "', verifyLock = '" . $timetaken . "', verifyBy = '" . $user->getId() . "', verifyDescr = '" . addslashes($verifyDesc) . "'" . $setDeclined . " WHERE registrationid = " . $registration->getId() . " AND fullRegStatus  IN (0,4)";
        }
        $em->getConnection()->exec($query1);
    }

    /**
     * Edits an existing user dept perm
     *
     * @Route("/validationPost", name="validationPost")
     * @Method("POST")
     * @Template("DashboardBundle:Faq:edit.html.twig")
     */
    public function validationPostAction(Request $request) {

        $em = $this->getDoctrine()->getManager();

        $attributes = $request->request->all();
        echo json_encode($attributes);
        exit;

        $validations = array();

        foreach ($attributes as $key => $val) {
            if (strpos($key, 'dataFieldName') !== false) {
                $num = str_replace('dataFieldName', '', $key);
                foreach ($attributes[ 'validationOption' . $num ] as $kkey => $vval) {
                    $validations[ $val ][ $attributes[ 'validationCheckName' . $num ] ][ $vval ] = $attributes[ 'validationValue' . $num ][ $kkey ];
                }
            }
        }

        $queryString = '';
        foreach ($attributes[ 'verifierFields' ] as $fkey => $fval) {
            $queryString .= $fval . ':::';
        }


        $dbstring[ 'ratingScore' ] = array( 'bad' => $attributes[ 'ratingScrorebad' ], 'fair' => $attributes[ 'ratingScrorefair' ] );
        $dbstring[ 'validations' ] = $validations;
        $dbstring[ 'queryString' ] = rtrim($queryString, ':::');

        $configType = $attributes[ 'numRow' ] . ':::' . $attributes[ 'secsCount' ];
        $em = $this->getDoctrine()->getManager();

        $query1 = "UPDATE ConfigMaster SET config =  '" . json_encode($dbstring) . "', configType = '" . $configType . "' "
            . " WHERE name = 'verifierFieldList' ";
        $em->getConnection()->exec($query1);

        return $this->redirect($this->generateUrl('admin/app_config') . 'verifier_view_fields');

        exit;

        return $this->prepareResponse(array());
    }

    /**
     * View rectifyImageCount
     *
     * @Route("/updateRecordImage/{id}/{reg_id}/{notLoadedImages}/{loadedImageCount}/{times}", name="updateRecordImage")
     * @Method("GET")
     * @Template()
     */
    public function OLDupdateRecordImageAction($id, $reg_id, $notLoadedImages, $loadedImageCount, $times) {
        $em = $this->getDoctrine()->getManager();


        // $results = $em->createQuery("UPDATE RestApiBundle:Registration u SET u.image_count = '2', u.email = 'xy@reset.com' WHERE u.registrationid = '" . $id . "' ")
        //       ->getArrayResult();
        //$results = $em->createQuery("UPDATE RestApiBundle:RegistrationStatus u SET u.verifyState = '5' WHERE u.registrationId = '" . $reg_id . "' ")
        //      ->getArrayResult();

        echo json_encode(array( 'message' => 'true' ));
        exit;

        return array(
            'title'       => 'View Reports for:',
            'title_descr' => 'View Reports for',
            //'entities' => $entities,
        );
    }

    /**
     * View rectifyImageCount
     *
     * @Route("/updateRecordImage/{id}/{reg_id}/{notLoadedImages}/{loadedImageCount}/{times}", name="updateRecordImage")
     * @Method("GET")
     * @Template()
     */
    public function updateRecordImageAction($id, $reg_id, $notLoadedImages, $loadedImageCount, $times) {
        //updateRecordImage/1484664256488767843360/18664874/potrait,front-pic,rear-pic,signature,/0/1487583772785
        //return true;
        //https://vodalive.registersim.com/admin/updateRecordImage/1478761129384744544844/2986354/front-pic,/3/1481699643273
        $em = $this->getDoctrine()->getManager();

        $sql = "UPDATE RegistrationStatus SET verifyDescr = ' :: TRIEDunsuccessfuly4times MISSING: " . $notLoadedImages . "' , verifyDate = '" . date('Y-m-d H:i:s') . "', verifyState = 5,verifyBy=NULL WHERE registrationId = " . $reg_id . '';

        $em->getConnection()->exec($sql);

        $sql = "UPDATE registration SET image_count = '" . $loadedImageCount . "' WHERE id = " . $reg_id . '';
        $em->getConnection()->exec($sql);

        echo "'" . rtrim(str_replace(',', "','", $notLoadedImages), ',') . "'";

        //$sql = "UPDATE reg_images SET registration = concat('corruptIMAGE_', registration) WHERE registration = ".$id." and ";
        //$em->getConnection()->exec($sql);


        $sql;

        echo json_encode(array( 'message' => 'true' ));
        exit;

        return array(
            'title'       => 'View Reports for:',
            'title_descr' => 'View Reports for',
            //'entities' => $entities,
        );
    }

    /**
     * View Reports
     *
     * @Route("/verifyaction_registrations/{timestamp}/{recordID}/{action}", name="verifyaction_registrations")
     * @Method("POST")
     */
    public function verifyaction_registrationsAction($timestamp, $recordID, $action = 1, Request $request) {
        $data[ 'recid' ] = $recordID;
        $descr = '';
        $rate = 0;

        try {
        $em = $this->getDoctrine()->getManager();
        $registration = $em->getRepository('RestApiBundle:Registration')->find($recordID);
        if (!$registration) {
            throw $this->createNotFoundException('Unable to find Registration entity.');
        }
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $query = $em->createQueryBuilder()
            ->select("u.id as sid, DATE_FORMAT(u.verifyDate, '%Y-%m-%d %H:%i%:%s') as verifyDate,  DATE_FORMAT(u.verifyLock, '%Y-%m-%d %H:%i%:%s') as verifyLock"
                . ' from RestApiBundle:RegistrationStatus u '
                . ' WHERE u.registrationId = ' . $registration->getId())
            ->getQuery();

        $result1 = $query->getResult();


        $timetaken = 0;
        if ($result1) {
            $timetaken = strtotime(date('Y-m-d H:i:s')) - strtotime($result1[ 0 ][ 'verifyLock' ]);
        }

        $configArray = array();
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
            ->select(" u from DashboardBundle:ConfigMaster u WHERE u.name = 'verifierFieldList' ")
            ->getQuery();

        $configMaster = $query->getOneOrNullResult();

        $allconfig = json_decode($configMaster->getConfig(), true);


        $numRows = 1;
        if ($configMaster) {
            $configArray = explode(':::', $allconfig[ 'queryString' ]);
            $ScoreArray = $allconfig[ 'ratingScore' ];

            $settingMerged = explode(':::', $configMaster->getConfigType());

            $numRows = @$settingMerged[ 0 ];
            $secsCount = @$settingMerged[ 1 ];
        }

        $attributes = json_decode($request->getContent(), true);

        $data[ 'message' ] = 'Invalid request sent :: ' . json_encode($attributes);
        $data[ 'response' ] = 'Registration NOT verified';


        $apiHelper = $this->container->get('api.helper');
        $apiHelper->logInfo('newADMINcontroller', 'verifyRegistration', array( 'ccount:: ' => count($attributes), 'ATTRIBUTESposted: ' => $attributes, 'RECORDid: ' => $recordID, 'userID: ' => $user->getId() ));


        if (count($attributes) > 5) {


            $errorDescription = '';
            foreach ($attributes as $kkey => $vval) {
                if ($kkey == 'allDescr') {
                    $descr = json_encode($vval);
                } else {
                    $rate = $rate + $vval[ 'value' ];

                    if ($vval[ 'value' ] > 0) {
                        //$vval['fieldName'] . " " . $vval['fieldDescr'];
                        $errorDescription = $errorDescription . ', ' . $vval[ 'descr' ];
                    }
                }
            }

            $rate = 100 - $rate;

            $score = 0;
            if ($rate > $ScoreArray[ 'fair' ]) { //Good
                $score = 1;
            } elseif ($rate >= $ScoreArray[ 'bad' ] && $rate <= $ScoreArray[ 'fair' ]) {
                $score = 2;
            } else {
                $score = 3;
            }


            //        echo " scrore: " . $score;
            //        echo " rate: " . $rate;
            //        echo " descr: " . $descr;
            //updateVerificationStatus( $registration, $tempRegStatus, $fullRegStatus, $tempRegDesc, $fullRegDesc, $verifyState, $verifyDesc, $user, $timetaken) {
            $this->updateVerificationStatus($registration, 0, 0, '', 'Live Verification :: Approved ', $score, $descr, $user, $timetaken, $action);
            //---$registration, $tempRegStatus, $fullRegStatus, $tempRegDesc, $fullRegDesc, $verifyState, $verifyDesc, $user, $timetaken, $action) {
            // submit freg
            $activeLiveVerification = $this->container->getParameter('live_verification_active');
            $fregVerificationThreshold = $this->container->getParameter('freg_score_threshold');


            if ($activeLiveVerification) {
                if ($score == 3) {
                    $this->triggerNotfication($registration, 'register.verify.status.fregdeclined', $errorDescription, 3);
                } else if ($score > 0 && $score <= $fregVerificationThreshold) {
                    $receivedImages = $em->getRepository('RestApiBundle:RegImages')->findBy(
                        array( 'registration' => $registration->getRegistrationid() )
                    );
                    //         $this->container->get("vodacom.endpoint")->processImages($receivedImages);
                }
            }

            $data[ 'message' ] = 'ScoreArray: ' . json_encode($ScoreArray) . ' :: recordID: ' . $recordID . ' score: ' . $score . ' rate: ' . $rate . ' descr: ' . $descr;
            $data[ 'response' ] = 'Registration verified Successfully';
        }
        echo json_encode($data);

        exit;

        return array(
            'title'       => 'Active & Inactive',
            'title_descr' => 'View Reports for Active Inactive',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );
           }catch (\Exception $e) {

            // Uncomment this line to show errors on prod
           // echo $e->getMessage();
            //exit;
        }
    }

    private function triggerNotfication($registration, $messageKey, $errorDescription, $level) {
        $msg = $this->container->get('api.helper')->getTranslatedLanguage($messageKey, array( '%msisdn%' => $registration->getMsisdn(), '%errordescription%' => $errorDescription ), $registration->getLanguage());
        $message = new Message();
        $message->setDeviceId($registration->getDeviceId());
        $message->setMessage($msg);
        $message->setData(array( 'status' => $level, 'registrationId' => $registration->getRegistrationid() ));
        $message->setTopic($this->container->getParameter('mqtt_registration_status'));
        $this->container->get('api.helper')->sendMessage($message);
    }

    /**
     * .
     *
     * @Route("/change_password", name="change_password")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:change_password.html.twig")
     */
    public function changePasswordAction(Request $request) {

        $this->session->set('forceChangePass', 1);

        $data = array(
            'title'            => "My Account",
            'escapeChangePass' => 22,
            'title_descr'      => "List, create, delete, activate System Admins"
        );

        return $this->prepareResponse($data);
    }


    /**
     * .
     *
     * @Route("/change_password_redirect", name="change_password_redirect")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:change_password.html.twig")
     */
    public function changePasswordRedirectAction(Request $request) {

        $this->session->set('forceChangePass', FALSE);

        return $this->redirect($this->generateUrl('admin'));
    }

    /**
     * View Reports
     *
     * @Route("/verify_total_reports", name="verify_total_reports")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:verify_total_reports.html.twig")
     */
    public function verify_total_reportsAction() {

        if (!in_array(23, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title'       => 'Total Registrations Report',
            'title_descr' => 'Total Registrations Report',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/verify_daily_reports_detail/{dday}/{sstate}", name="verify_daily_reports_detail")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:verify_daily_reports_detail.html.twig")
     */
    public function verify_daily_reports_detailAction($dday, $sstate) {

        if (!in_array(56, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title'       => 'Records For State ' . $this->getVerifyStates($sstate) . ' Handled on ' . $dday,
            'dday'        => $dday,
            'sstate'      => $sstate,
            'title_descr' => 'Verify Breakdown Report',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * active_agents Summary
     *
     * @Route("/list_verify_daily_report_detail/{dday}/{sstate}/{ccount}/{timestamp}", name="list_verify_daily_report_detail")
     * @Method({"POST","GET"})
     * @Template()
     */
    public function list_detail_verify_daily_reportAction($dday, $sstate, $ccount, $timestamp, Request $request) {
        $resp = new JTableResponse();
        try {

            $em = $this->getDoctrine()->getManager();
            $emConfig = $em->getConfiguration();
            $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
            $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
            $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
            $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');


            $attributes = $request->request->all();
            $where = ' WHERE 1=1 ';

            $where .= " AND date(x.verifyDate) = '" . $dday . "' ";

            if (in_array($sstate, array( '1', '2', '3' ))) {
                if ($sstate != 1) {
                    $where .= " AND (x.verifyState = '" . $sstate . "' OR x.verifyState = '-" . $sstate . "')";
                } else {
                    $where .= " AND x.verifyState = '" . $sstate . "' ";
                }
            }

            $queryAttrib = $request->query->all();

            $queryString = " SELECT u.id,u.msisdn, x.temporaryRegStatus as tregStatus, x.fullRegStatus as fregStatus, x.auditState, x.verifyState, concat(u.firstName, ' ', u.lastName) as customerName, DATE_FORMAT(u.createdDate, '%Y-%m-%d') as created_on, DATE_FORMAT(u.createdDate, '%H:%i:%s') as created_time, u.image_count as image_count, u.identificationType, u.identification, a.username, concat(a.firstName,' ', a.lastName) as agentName, m.mobileNumber as superAgentPhone, concat(m.firstName,' ', m.lastName) as superAgentName, u.region, u.territory "
                . 'FROM RestApiBundle:RegistrationStatus x '
                . 'LEFT OUTER JOIN x.registrationId u '
                . 'LEFT OUTER JOIN u.owner a '
                . 'LEFT OUTER JOIN a.parent_id m  ' . $where
                . ' ORDER BY u.id desc ';
            $query = $em->createQuery($queryString);

            $query->setMaxResults($queryAttrib[ 'jtPageSize' ]);
            $query->setFirstResult($queryAttrib[ 'jtStartIndex' ]);
            //echo $agents = $query->getSql(); exit;
            $agents = $query->getResult();

            if ($ccount == 0) {
                $queryString = ' SELECT count(x.id) as ccount '
                    . 'FROM RestApiBundle:RegistrationStatus x '
                    . 'LEFT OUTER JOIN x.registrationId u '
                    . 'LEFT OUTER JOIN u.owner a '
                    . 'LEFT OUTER JOIN a.parent_id m  ' . $where
                    . ' ORDER BY u.id desc ';
                $query = $em->createQuery($queryString);
                //echo $agents = $query->getSql(); exit;
                $resultCount = $query->getResult();

                $ccount = $resultCount[ 0 ][ 'ccount' ];
            }

            $entities = $em->getRepository('DashboardBundle:Idtype')->findAll();

            $id_array = array();
            foreach ($entities as $akey => $aval) {
                $id_array[ $aval->getId() ] = $aval->getName();
            }

            $territory_entiry = $em->getRepository('DashboardBundle:Territory')->findAll();

            $territory_array = array();
            $territory_array[ 'none' ] = '';
            foreach ($territory_entiry as $bkey => $bval) {
                $territory_array[ $bval->getId() ] = $bval->getName();
            }


            $returnData[ 'Result' ] = 'OK';
            $rowData = array();

            if ($agents) {
                foreach ($agents as $kkey => $vval) {
                    if ($vval[ 'territory' ]) {
                        $vval[ 'territory' ] = @$territory_array[ $vval[ 'territory' ] ];
                    }
                    $vval[ 'temporaryRegStatus' ] = $this->getRegStatusDesc($vval[ 'tregStatus' ]);

                    $vval[ 'aimStatus' ] = $this->getAimRegStatusDesc($vval[ 'tregStatus' ], $vval[ 'fregStatus' ], $vval[ 'image_count' ]);

                    $vval[ 'icapStatus' ] = $this->getIcapRegStatusDesc($vval[ 'tregStatus' ], $vval[ 'fregStatus' ]);

                    $vval[ 'verifyState' ] = $this->getVerifyStates($vval[ 'verifyState' ]);

                    $vval[ 'region' ] = $this->get_regionName_fromKey($vval[ 'region' ]);
                    $vval[ 'identificationType' ] = $id_array[ $vval[ 'identificationType' ] ];
                    $rowData[] = $vval;
                }
            }
            $returnData[ 'Records' ] = $rowData;
            $returnData[ 'Record' ] = null;
            $returnData[ 'TotalRecordCount' ] = $ccount;
            $returnData[ 'Message' ] = null;
            $returnData[ 'Options' ] = null;

            echo json_encode($returnData);
            exit;

            $resp->setRecords($agents);
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult('ERROR');
        }

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * active_agents Summary
     *
     * @Route("/verify_total_report/{start_date}/{end_date}/{timestamp}", name="verify_total_report")
     * @Method("GET")
     * @Template()
     */
    public function verify_total_reportAction($start_date, $end_date, $timestamp) {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = ' WHERE 1=1 ';
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array('ROLE_SUPERAGENT', $roles)) {
            $where .= ' AND u.parent_id = ' . $user->getId();
            $where2 .= ' AND u.parent_id = ' . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(r.createdDate) >= '" . $start_date . "' AND date(r.createdDate) <= '" . $end_date . "' ";
        }
        //$where .= " AND s.verifyState != 10 ";

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        //$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "

        $query = $em->createQueryBuilder()
            ->select("s.id as sid, r.image_count, COUNT(r.id) as sum_regs, DATE_FORMAT(r.createdDate, '%Y-%m-%d') as created_on, s.temporaryRegStatus as treg, s.fullRegStatus as freg, s.verifyState "
                . ' from RestApiBundle:RegistrationStatus s'
                . ' JOIN s.registrationId r '
                . ' JOIN r.owner u ' . $where
                . ' GROUP BY created_on, r.image_count, s.temporaryRegStatus, s.verifyState ORDER BY sum_regs DESC')
            ->getQuery();

        $result1 = $query->getResult();

        $data = $active = array();


        /*
          $result = $em->createQuery("SELECT e.image_count, p.temporaryRegStatus as treg, p.fullRegStatus as freg, p.verifyState as vstate, DATE(e.createdDate) as ddate, COUNT(p.id) as ccount "
          . " from RestApiBundle:RegistrationStatus p "
          //. "WHERE date(created_date) >= '2015-09-28' AND date(created_date) <= '2015-10-08' "
          . " JOIN p.registrationId e"
          . " JOIN e.owner a" . $where
          . " GROUP BY ddate, e.image_count, treg, freg, vstate "
          . " ORDER BY ddate desc")
          ->getArrayResult(); //getSingleResult();

          if( in_array($value['treg'], array(0, 2, 6)) && $value['image_count'] >= 4 && in_array($value['vstate'], array(0, 1, 2, 3, '-1', '-2', '-3'))){
          if(in_array($value['vstate'], array(1,2,3,'-2','-3'))) {
          $data[$value['ddate']][30]['count'] = @$data[$value['ddate']][30]['count'] + $value['ccount'];

          if ($value['vstate'] == 1) {
          //vgood
          $data[$value['ddate']][32]['count'] = @$data[$value['ddate']][32]['count'] + $value['ccount'];
          } elseif ($value['vstate'] == 2 || $value['vstate'] == '-2') {
          //vfair
          $data[$value['ddate']][33]['count'] = @$data[$value['ddate']][33]['count'] + $value['ccount'];
          } elseif ($value['vstate'] == 3 || $value['vstate'] == '-3') {
          //vbad
          $data[$value['ddate']][34]['count'] = @$data[$value['ddate']][34]['count'] + $value['ccount'];
          }
          } else {
          //notverified
          $data[$value['ddate']][31]['count'] = @$data[$value['ddate']][31]['count'] + $value['ccount'];
          }
          }
         */
        foreach ($result1 as $key => $value) {
            $data[ $value[ 'created_on' ] ] = (@$data[ $value[ 'created_on' ] ] ? $data[ $value[ 'created_on' ] ] : $value);

            $data[ $value[ 'created_on' ] ][ 'sum_total_reg' ] = @$data[ $value[ 'created_on' ] ][ 'sum_total_reg' ] + $value[ 'sum_regs' ];
            $data[ $value[ 'created_on' ] ][ 'sum_total_approved' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_total_approved' ] ? $data[ $value[ 'created_on' ] ][ 'sum_total_approved' ] : 0);
            $data[ $value[ 'created_on' ] ][ 'sum_total_pending' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_total_pending' ] ? $data[ $value[ 'created_on' ] ][ 'sum_total_pending' ] : 0);
            $data[ $value[ 'created_on' ] ][ 'sum_total_declined' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_total_declined' ] ? $data[ $value[ 'created_on' ] ][ 'sum_total_declined' ] : 0);
            $data[ $value[ 'created_on' ] ][ 'sum_total_audited' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_total_audited' ] ? $data[ $value[ 'created_on' ] ][ 'sum_total_audited' ] : 0);
            $data[ $value[ 'created_on' ] ][ 'sum_good' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_good' ] ? $data[ $value[ 'created_on' ] ][ 'sum_good' ] : 0);
            $data[ $value[ 'created_on' ] ][ 'sum_fair' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_fair' ] ? $data[ $value[ 'created_on' ] ][ 'sum_fair' ] : 0);
            $data[ $value[ 'created_on' ] ][ 'sum_bad' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_bad' ] ? $data[ $value[ 'created_on' ] ][ 'sum_bad' ] : 0);
            $data[ $value[ 'created_on' ] ][ 'sum_unrated' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_unrated' ] ? $data[ $value[ 'created_on' ] ][ 'sum_unrated' ] : 0);

            /*
              if( in_array($value['treg'], array(0, 2, 6)) && $value['image_count'] >= 4 && in_array($value['vstate'], array(0, 1, 2, 3, '-1', '-2', '-3'))){
              if(in_array($value['vstate'], array(1,2,3,'-2','-3'))) {
             */
            if (in_array($value[ 'verifyState' ], array( 0, 1, 2, 3, '-1', '-2', '-3' )) && in_array($value[ 'treg' ], array( 0, 2, 6 )) && $value[ 'image_count' ] >= 4) {
                if (in_array($value[ 'verifyState' ], array( 1, 2, 3, '-2', '-3' ))) {
                    //
                } else {

                    $data[ $value[ 'created_on' ] ][ 'sum_unrated' ] = $data[ $value[ 'created_on' ] ][ 'sum_unrated' ] + $value[ 'sum_regs' ];
                }
            }

            if (in_array($value[ 'verifyState' ], array( 1, 2, 3, -3, -2 ))) {
                $data[ $value[ 'created_on' ] ][ 'sum_total_audited' ] = $data[ $value[ 'created_on' ] ][ 'sum_total_audited' ] + $value[ 'sum_regs' ];

                if ($value[ 'verifyState' ] == 1) {
                    $data[ $value[ 'created_on' ] ][ 'sum_good' ] = $data[ $value[ 'created_on' ] ][ 'sum_good' ] + $value[ 'sum_regs' ];
                } elseif ($value[ 'verifyState' ] == 2 || $value[ 'verifyState' ] == -2) {
                    $data[ $value[ 'created_on' ] ][ 'sum_fair' ] = $data[ $value[ 'created_on' ] ][ 'sum_fair' ] + $value[ 'sum_regs' ];
                } elseif ($value[ 'verifyState' ] == 3 || $value[ 'verifyState' ] == -3) {
                    $data[ $value[ 'created_on' ] ][ 'sum_bad' ] = $data[ $value[ 'created_on' ] ][ 'sum_bad' ] + $value[ 'sum_regs' ];
                }
            }


            if ($value[ 'image_count' ] >= 3) {
                $data[ $value[ 'created_on' ] ][ 'sum_total_approved' ] = $data[ $value[ 'created_on' ] ][ 'sum_total_approved' ] + $value[ 'sum_regs' ];
            } else {
                $data[ $value[ 'created_on' ] ][ 'sum_total_pending' ] = $data[ $value[ 'created_on' ] ][ 'sum_total_pending' ] + $value[ 'sum_regs' ];
            }
            if (in_array($value[ 'treg' ], array( 4, 5, 3 ))) {
                $data[ $value[ 'created_on' ] ][ 'sum_total_declined' ] = $data[ $value[ 'created_on' ] ][ 'sum_total_declined' ] + $value[ 'sum_regs' ];
            }
        }

        echo json_encode($data);
        exit;
    }

    /**
     * View Reports
     *
     * @Route("/verifyer", name="verifyer")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:index_verifyer.html.twig")
     */
    public function verifyerAction() {

        if (!$this->checkSessionPermission(70)) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $this->session->set('user_role_perms', json_encode($this->get_department_perms_arrayAction(), true));


        $where = '';
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array('ROLE_SUPERAGENT', $roles)) {
            $where .= ' AND a.parent_id = ' . $user->getId();
        }

        $data = array(
            'title'       => 'Index',
            'title_descr' => 'View Frequently Asked Questions',
            'ddate'       => date('Y-m-d'),
            'akuku'       => json_encode($user->getRoles()) . ' | ' . $this->session->get('user_role_perms'),
            'date_today'  => date('Y-m-d'),
            'date_start'  => date_format(date_sub(date_create(date('Y-m-d')), date_interval_create_from_date_string('10 days')), 'Y-m-d')
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/verifyer_summary", name="verifyer_summary")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:index_verifyer_summary.html.twig")
     */
    public function verifyerSummaryAction() {

        $this->session->set('user_role_perms', json_encode($this->get_department_perms_arrayAction(), true));


        $where = '';
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array('ROLE_SUPERAGENT', $roles)) {
            $where .= ' AND a.parent_id = ' . $user->getId();
        }

        $data = array(
            'title'       => 'Index',
            'title_descr' => 'View Frequently Asked Questions',
            'ddate'       => date('Y-m-d'),
            'akuku'       => json_encode($user->getRoles()) . ' | ' . $this->session->get('user_role_perms'),
            'date_today'  => date('Y-m-d'),
            'date_start'  => date('Y-m-d') //date_format(date_sub(date_create(date('Y-m-d')), date_interval_create_from_date_string("10 days")), "Y-m-d")
        );

        return $this->prepareResponse($data);
    }

    /**
     * hourly_summary Summary
     *
     * @Route("/ccount_verifysummary/{start_date}/{end_date}/{timestamp}", name="ccount_verifysummary")
     * @Method("GET")
     * @Template()
     */
    public function ccount_verifysummaryAction($start_date, $end_date, $timestamp) {
        $em = $this->getDoctrine()->getManager();

        $where = ' WHERE 1=1 ';
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if ($start_date != 1) {
            $where .= " AND r0_.createdDate >= '" . date('Y-m-d', strtotime($start_date)) . "' AND r0_.createdDate <= '" . date('Y-m-d', strtotime($end_date)) . " 23:59:59' ";
        }
        //$where .= " AND date(r.verifyDate) >= '2016-07-01' "; date_stored_createdDate

        if (in_array('ROLE_VERIFYER_SUPERVISOR', $roles)) {
            $where .= ' AND r0_.verifyBy != 3260 ';
        }

        $where .= ' AND r1_.image_count >= 3 ';
        $where .= ' AND r0_.allimageDate IS NOT NULL ';
        $where .= ' AND r0_.temporaryRegStatus IN (0, 2, 6) ';
        $where .= " AND r0_.verifyState in (0, 1, 2, 3, '-1', '-2', '-3') ";

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');


        $treg = $freg = $fail = $pending = 0;


        $query1 = "SELECT count(r0_.id) AS reg_count, DATE_FORMAT (r0_.createdDate,'%Y-%m-%d') AS created_on, r0_.verifyState AS verifyState "
            . ' FROM RegistrationStatus r0_ INNER JOIN registration r1_ ON r0_.registrationId = r1_.id '
            . $where . ' GROUP BY r0_.verifyState';
        $connection = $em->getConnection();
        $statement = $connection->prepare($query1);
        $statement->execute();
        $count_tfreg = $statement->fetchAll();


        $data[ 'total' ] = $data[ 'good' ] = $data[ 'fair' ] = $data[ 'pending' ] = $data[ 'bad' ] = 0;
        foreach ($count_tfreg as $key => $val) {
            $data[ 'total' ] = $data[ 'total' ] + $val[ 'reg_count' ];

            if ($val[ 'verifyState' ] == 3 || $val[ 'verifyState' ] == '-3') {
                $data[ 'bad' ] = $data[ 'bad' ] + $val[ 'reg_count' ];
            } elseif ($val[ 'verifyState' ] == 2 || $val[ 'verifyState' ] == '-2') {
                $data[ 'fair' ] = $data[ 'fair' ] + $val[ 'reg_count' ];
            } elseif ($val[ 'verifyState' ] == 1) {
                $data[ 'good' ] = $data[ 'good' ] + $val[ 'reg_count' ];
            } else {
                $data[ 'pending' ] = $data[ 'pending' ] + $val[ 'reg_count' ];
            }
        }


        echo json_encode($data);
        exit;
    }

    /**
     * hourly_summary Summary
     *
     * @Route("/avarage_verification_time/{start_date}/{end_date}/{timestamp}", name="avarage_verification_time")
     * @Method("GET")
     * @Template()
     */
    public function avarage_verification_timeAction($start_date, $end_date, $timestamp) {
        $em = $this->getDoctrine()->getManager();

        $where = " WHERE 1=1 AND r0_.verifyState in (1, 2, 3, '-2', '-3') AND r0_.verifyLock > 1 AND r0_.verifyLock < 1000 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array('ROLE_SUPERAGENT', $roles)) {
            $where .= ' AND r.parent_id = ' . $user->getId();
        }
        if ($start_date != 1) {
            $where .= ' AND r0_.date_stored_verifyDate >= ' . date('Ymd', strtotime($start_date)) . ' AND r0_.date_stored_verifyDate <= ' . date('Ymd', strtotime($end_date)) . ' ';
        }


        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');


        $treg = $freg = $fail = $pending = 0;

        $query1 = 'SELECT AVG(r0_.verifyLock) AS verifyLock '
            . ' FROM RegistrationStatus r0_ ' . $where;

        $connection = $em->getConnection();
        $statement = $connection->prepare($query1);
        $statement->execute();
        $count_tfreg = $statement->fetchAll();


        $count_tfreg2[ 'verifyLock' ] = (@$count_tfreg[ 0 ][ 'verifyLock' ] ? floor($count_tfreg[ 0 ][ 'verifyLock' ]) . ' Seconds' : 0);

        echo json_encode($count_tfreg2);
        exit;
    }

    /**
     * Get verifiers idle time
     *
     * @Route("/verifier_idle_time", name="verifier_idle_time")
     * @Method({"POST","GET"})
     */
    public function verifierIdleTime() {
        $rsm = new \Doctrine\ORM\Query\ResultSetMapping();
        $rsm->addEntityResult('RestApiBundle:user', 'u');
        $rsm->addScalarResult('first_name', 'first_name');
        $rsm->addScalarResult('last_name', 'last_name');
        $rsm->addScalarResult('counter', 'counter');
        $rsm->addScalarResult('idletime', 'idletime');


        echo $queryString = 'select u.first_name, u.last_name, count(r2.id) as counter, r2.verifyBy, r2.id as maxid, '
            . "r2.verifyDate, MAX(r2.verifyDate) as maxdate, TIMEDIFF('" . date('Y-m-d H:i:s') . "', MAX(r2.verifyDate)) as idletime FROM RegistrationStatus r2, user u WHERE r2.verifyBy = u.id AND r2.date_stored_verifyDate = " . date('Ymd') . ' AND r2.verifyBy != 3260 GROUP BY r2.verifyBy ORDER BY maxdate';
        exit;

        $query = $this->getDoctrine()->getManager()->createNativeQuery($queryString, $rsm);

        $agents = $query->getResult();

        return $this->buildResponse($agents, Response::HTTP_OK);
    }

    /**
     * Get verifiers idle time
     *
     * @Route("/verifier_today_count", name="verifier_today_count")
     * @Method({"POST","GET"})
     */
    public function verifierTodayCounts() {
        $rsm = new \Doctrine\ORM\Query\ResultSetMapping();
        $rsm->addEntityResult('RestApiBundle:user', 'u');
        $rsm->addScalarResult('first_name', 'first_name');
        $rsm->addScalarResult('last_name', 'last_name');
        $rsm->addScalarResult('counter', 'counter');

        //$queryString = "select u.first_name, u.last_name, count(r2.id) as counter, r2.verifyBy, r2.id as maxid, r2.verifyDate, MAX(r2.verifyDate) as maxdate, TIMEDIFF(now(), MAX(r2.verifyDate)) as idletime FROM RegistrationStatus r2, user u WHERE r2.verifyBy = u.id AND DATE(r2.verifyDate) = CURDATE() GROUP BY r2.verifyBy ORDER BY maxdate";
        //$queryString = "select u.first_name, u.last_name, count(r2.id) as counter FROM RegistrationStatus r2, user u "
        //        . "WHERE r2.verifyBy = u.id AND r2.verifyBy != 3260 AND DATE(r2.verifyDate) = CURDATE() AND r2.verifyState > 0 GROUP BY r2.verifyBy ORDER BY counter";

        $queryString = 'select u.first_name, u.last_name, count(r2.id) as counter FROM RegistrationStatus r2, user u '
            . 'WHERE (r2.verifyBy = u.id OR r2.originalVerifyBy = u.id) AND r2.verifyBy != 3260 AND r2.date_stored_verifyDate = ' . date('Ymd') . " AND r2.verifyState in (1, 2, 3, '-2', '-3') GROUP BY r2.verifyBy ORDER BY counter";


        $query = $this->getDoctrine()->getManager()->createNativeQuery($queryString, $rsm);

        $agents = $query->getResult();

        return $this->buildResponse($agents, Response::HTTP_OK);
    }

    function runQueryverify_agents_report($where, $limit, $start) {
        $em = $this->getDoctrine()->getManager();

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        //$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "

        $query = $em->createQueryBuilder()
            ->select(" u.id as sid, COUNT(r.id) as sum_regs, t.name as territory_name, d.name as region_name, CONCAT(u.firstName , ' ', u.lastName) as agent_names, u.username, DATE_FORMAT(s.verifyDate, '%Y-%m-%d') as created_on, s.verifyState "
                . ' from RestApiBundle:RegistrationStatus s'
                . ' JOIN s.registrationId r '
                . ' JOIN r.owner u '
                . ' JOIN u.region d '
                . ' JOIN u.territory t ' . $where
                . ' GROUP BY u.id, s.auditState ORDER BY sum_regs DESC')
            ->getQuery();

        $query->setMaxResults($limit);
        $query->setFirstResult($start * $limit);

        return $query->getResult();
    }

    /**
     * View Reports
     *
     * @Route("/reports_verifyer", name="reports_verifyer")
     * @Method("GET")
     * @Template()
     */
    public function reports_verifyerAction() {

        $data = array(
            'title'       => 'Generate Reports',
            'title_descr' => 'View Reports for',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/resetUserReg/{id}", name="resetUserReg")
     * @Method("GET")
     * @Template()
     */
    public function resetUserRegAction($id) {
        exit("Sorry, your no longer allowed to use this endpoint. Contact tech");

        $em = $this->getDoctrine()->getManager();

        $query1 = "Update RegistrationStatus set temporaryRegStatus = 0, fullRegStatus = 0, temporaryRegDesc = '', fullRegDesc = '', icap_state = 0, icap_counter = 0, mpesa_state = 0, dbms_state = 0, tregDate = '0000-00-00 00:00:00', fregDate = '0000-00-00 00:00:00', dbmsDate = '0000-00-00 00:00:00', mpesaDate = '0000-00-00 00:00:00' where registrationID = '" . $id . "' ";

        $connection = $em->getConnection();
        $statement = $connection->prepare($query1);
        $statement->execute();


        $query1 = "select * from RegistrationStatus where registrationId = '" . $id . "' ";

        $connection = $em->getConnection();
        $statement = $connection->prepare($query1);
        $statement->execute();
        $result1 = $statement->fetchAll();

        echo '<pre>';
        print_r($result1);
        echo '</pre>';

        exit;

        $data = array(
            'title'       => 'Generate Reports',
            'title_descr' => 'View Reports for',
            'message'     => '' //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

}
