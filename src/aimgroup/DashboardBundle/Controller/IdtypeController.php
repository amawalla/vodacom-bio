<?php

namespace aimgroup\DashboardBundle\Controller;

use aimgroup\DashboardBundle\Dao\JTableResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use aimgroup\DashboardBundle\Entity\Idtype;
use aimgroup\DashboardBundle\Form\IdtypeType;
use Symfony\Component\HttpFoundation\Response;

/**
 * Idtype controller.
 *
 * @Route("admin/idtype")
 */
class IdtypeController extends AbstractController
{

    /**
     * Lists all Idtype entities.
     *
     * @Route("", name="idtype_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {

        $data = array(
            'title' => "Id Types",
            'title_descr' => "Id Types in Use",
        );
        
        return $this->prepareResponse($data);
    }

    /**
     * @param Request $request
     * @Route("/list",name="idtype_list")
     * @Method({"POST","GET"})
     */
    public function listIdTypeAction(Request $request)
    {
        $resp = new JTableResponse();
        try {
            $em = $this->getDoctrine()->getManager();
            $IdTypes = $em->getRepository("DashboardBundle:Idtype")
                ->createQueryBuilder('e')
                ->select('e.id, e.name, e.nameAlt, e.regexString')
                ->getQuery()
                ->getResult();
            $resp->setRecords($IdTypes);
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @Route("/create",name="idtype_create")
     * @Method({"POST","GET"})
     */
    public function createIdTypeAction(Request $request)
    {
        $resp = new JTableResponse();
        try {

            $em = $this->getDoctrine()->getManager();
            $attributes = $request->request->all();
            $idType = new Idtype();
            $idType->setName($attributes["name"]);
            $idType->setName($attributes["nameAlt"]);
            $idType->setRegexString($attributes["regexString"]);

            $em->persist($idType);
            $em->flush();
            $resp->setRecord(
                array(
                    'id' => $idType->getId(),
                    'name' => $idType->getName(),
                    'nameAlt' => $idType->getNameAlt(),
                    'regexString' => $idType->getRegexString()
                )
            );
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }

        return $this->buildResponse($resp, Response::HTTP_OK);
    }


    /**
     * @param Request $request
     * @Route("/update",name="idtype_update")
     * @Method({"POST","GET"})
     */
    public function updateIdTypeAction(Request $request)
    {
        $resp = new JTableResponse();
        try {

            $em = $this->getDoctrine()->getManager();
            $attributes = $request->request->all();

            $idType = $em->getRepository('DashboardBundle:Idtype')->findOneBy(array('id' => $attributes['id']));
            if ($idType) {
                $idType->setName($attributes["name"]);
                $idType->setNameAlt($attributes["nameAlt"]);
                $idType->setRegexString($attributes["regexString"]);
                $em->flush();
                $resp->setRecord(
                    array(
                        'id' => $idType->getId(),
                        'name' => $idType->getName(),
                        'nameAlt' => $idType->getNameAlt(),
                        'regexString' => $idType->getRegexString()
                    )
                );
            } else {
                $resp->setMessage("NO ID TYPE WITH ID " . $attributes['id'] . " FOUND");
                $resp->setResult("ERROR");
            }


        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }

        return $this->buildResponse($resp, Response::HTTP_OK);
    }
}
