<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\DashboardBundle\Controller;

use aimgroup\DashboardBundle\Dao\JsonObject;
use aimgroup\DashboardBundle\Dao\JTableResponse;
use aimgroup\DashboardBundle\Entity\Idtype;
use aimgroup\DashboardBundle\Util\UsersHelper;
use aimgroup\RestApiBundle\Entity\Device;
use aimgroup\RestApiBundle\Entity\User;
use Doctrine\Common\Persistence\AbstractManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use aimgroup\DashboardBundle\Entity\Anto;
use aimgroup\DashboardBundle\Form\AntoType;
use Symfony\Component\HttpFoundation\Response;
//use FOS\ElasticaBundle\Repository;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\Util\SecureRandom;
//use aimgroup\DashboardBundle\Dao\SearchDao;
//use aimgroup\DashboardBundle\Entity\SearchResult;
use Symfony\Component\HttpFoundation\Session\Session;
use aimgroup\DashboardBundle\Entity\UserLog;

/**
 * This ManageAgentsController is used to manage agents in the system.
 *
 * @author Michael Tarimo
 *
 * @Route("admin/agents")
 */
class ManageAgentsController extends AbstractController
{

    var $session;

    public function __construct()
    {
        $this->session = new Session();
    }

    /**
     *
     * @Route("/", name="admin/agents")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {

        if (!in_array(13, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $user = $this->get('security.token_storage')->getToken()->getUser();
        $show_agent = true;
        $roles = $user->getRoles(); //array of roles
        if (in_array("TM_HOR_ACCESS", $roles)) {
            $show_agent = false; //'<a href="#agent" class="btn btn-primary btn-ms" onclick="loadNewAgentForm(event)">New Agent</a>';
        }

        $data = array(
            'title' => "Manage Agents",
            'show_agent' => $show_agent,
            'title_descr' => "List, create, delete, activate Agents",
            'posts' => "",
            'user_role' => json_encode($user->getRoles()),
            'user_id' => $user->getId(),
            'mqttMessageTopic' => $this->container->getParameter("mqtt_notify_msg"),
            "operator" => strtolower($this->container->getParameter("operator_name"))
        );

        return $this->prepareResponse($data);
    }

    /**
     * @param Request $request
     * @Route("/list_agents",name="list_agents")
     * @Method({"POST","GET"})
     * @return JsonResponse
     */
    public function listAgentAction(Request $request)
    {
        $resp = new JTableResponse();

        try {
            $attributes = $request->request->all();
            $where = " WHERE u.roles LIKE :roles ";

            /** @var  $user User */
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $roles = $user->getRoles(); //array of roles

            if (in_array("ROLE_SUPERAGENT", $roles)) {
                $where .= " AND u.parent_id = " . $user->getId();
            }
            if (in_array("ROLE_VODASHOP", $roles)) {
                $where .= " AND u.vodashop_id = " . $user->getId();
            }

            if (isset($attributes["msisdn"])) {
                $where .= " AND u.username like '%" . $attributes["msisdn"] . "%'";
            }
            if (isset($attributes["first_name"])) {
                $where .= " AND u.firstName like '%" . $attributes["first_name"] . "%'";
            }
            if (isset($attributes["last_name"])) {
                $where .= " AND u.lastName like '%" . $attributes["last_name"] . "%'";
            }
            if (@$attributes["region"] > 0) {
                $where .= " AND u.region = " . $attributes["region"];
            }
            if (@$attributes["territory"] > 0) {
                $where .= " AND u.territory = " . $attributes["territory"];
            }
            if (isset($attributes["from_date"])) {
                //    $where.= " AND u.msisdn like '%" . $attributes["from_date"]."%'";
            }
            if (isset($attributes["to_date"])) {
                //    $where.= " AND u.msisdn like '%" . $attributes["to_date"]."%'";
            }

            $where .= " AND u.enabled = 1 ";

            //join only when filtered by imei
            $leftJoin = "";
            //imei filter
            if (isset($attributes["agent_imei_new"]) && strlen($attributes["agent_imei_new"]) >= 10) {
                $where .= " AND device2.imei =" . $attributes["agent_imei_new"];
                $leftJoin = "LEFT JOIN RestApiBundle:Device device2 with u.id=device2.user";
                // $where .= " AND device2.user = 18";
            }

            $queryAttrib = $request->query->all();

            $queryString = "SELECT u.id,concat(u.firstName, ' ',u.lastName) as agentName,DATE_FORMAT(u.createdAt, '%Y-%m-%d') as createdAt,u.username as mobileNumber,u.status,u.type as userType,u.username,u.idNumber,u.agentCode,
u.email, CONCAT(h.lastName, ' ', h.firstName) as speragent, h.id as superagent_id, u.agent_exclusivity as exclusive,u.number_devices,r.id as region,t.id as territory,u.firstName,u.lastName, d.id as identificationType, u.idNumber as identificationNumber, u.isEnrolledToTest as isEnrolledToTest
                     FROM RestApiBundle:User u
                     LEFT OUTER JOIN u.region r
                     LEFT OUTER JOIN u.territory t
                     LEFT OUTER JOIN u.idType d
                     LEFT JOIN u.parent_id h " . $leftJoin . $where;

            $query = $this->getDoctrine()->getManager()
                ->createQuery($queryString)
                ->setParameter("roles", '%ROLE_AGENT%');

            $query->setMaxResults($queryAttrib["jtPageSize"]);
            $query->setFirstResult($queryAttrib["jtStartIndex"]);

            $agents = $query->getResult();

            //TODO get total count of all the agents, currently shows the page limit set by jTable

            $returnData['Result'] = "OK";
            $rowData = array();
            foreach ($agents as $key => $row) {
                $rowData[] = $row;
            }

            $resp->setRecords($rowData);

        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * Add a new Agent.
     *
     * @Route("/createUser", name="createUser")
     * @Method("POST")
     */
    public function createAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $resp = new JsonObject();
        $status = false;
        $message = "";
        try {
            $attributes = json_decode($request->getContent(), true);
            if ($attributes) {

                //check if we received a valid mobile number
                $mobile_number =  preg_replace('/\s+/', '', $attributes['msisdn']);
                $attributes['msisdn'] = $mobile_number;
                if(!$this->isValidMsisdn($mobile_number)){
                    $resp->setMessage("Invalid mobile number format");
                    $resp->setStatus(UsersHelper::SUCCESS);
                    return $this->buildResponse($resp, Response::HTTP_OK);
                }


                $status = $this->container->get('users.helper')->createUser($attributes);
                if ($status == UsersHelper::EXISTS) {
                    $status = false;
                    $message = "MOBILE NUMBER ALREADY EXITS";
                } else if ($status == UsersHelper::SUCCESS) {
                    $status = true;
                    $message = "SUCCESS";
                }
                $this->logUserEvent(UserLog::CREATE_USER, "create user", array("attributes" => $attributes, 'status' => $status));
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }
        $resp->setMessage($message);
        $resp->setStatus($status);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * Add a new Agent.
     *
     * @Route("/updateUser", name="updateUser")
     * @Method({"POST","GET"})
     */
    public function updateAction(Request $request)
    {
        $resp = new JTableResponse();
        try {
            $attributes = $request->request->all();

            $em = $this->getDoctrine()->getManager();

            if (isset($attributes["id"])) {
                /** @var  $user User */
                $user = $em->getRepository('RestApiBundle:User')->find($attributes["id"]);
                if ($user) {
					
					if($user->getStatus() != $attributes['status'] && $attributes['status'] == 0){
                        $user_devices = $em->getRepository('RestApiBundle:Device')->findBy(array('user' => $user));
						foreach ($user_devices as $user_device){
							
							$this->container->get('api.helper')->publish(
								$user_device->getDeviceId(),
								$this->container->getParameter('mqtt_block_unblock_device'),
								json_encode(array("status" => true))
							);

							$user_device->setStatus(0);
							$user_device->setIsBlocked(1);							
							$em->flush();
						}
                    }

                    $superagent = $em->getRepository('RestApiBundle:User')->find($attributes['superagent_id']);
                    if ($superagent) {
                        $user->setParentId($superagent);
                    }
                    $territory = $em->getRepository('DashboardBundle:Territory')->find(@$attributes['territory']);

                    if (!$territory) {
                        throw $this->createNotFoundException('Unable to find Territory entity.');
                    }

                    $region = $em->getRepository('DashboardBundle:Region')->find(@$attributes['region']);

                    if (!$region) {
                        throw $this->createNotFoundException('Unable to find Region entity.');
                    }

                    $user->setFirstName($attributes['firstName']);
                    $user->setLastName($attributes['lastName']);
                    $user->setIdNumber($attributes['identificationNumber']);
		    $user->setIsEnrolledToTest($attributes['isEnrolledToTest']);

                    if (isset($attributes['identificationType']) && is_numeric($attributes['identificationType'])) {
                        $user->setIdType($em->getRepository('DashboardBundle:Idtype')->find($attributes['identificationType']));
                    }

                    if (count(@$attributes['number_devices']) > 4) {
                        $user->setNumberDevices($attributes['number_devices']);
                    }
                    
                    				                     //	exit("update action before set status -- pass");

                    
                     // --- - -- - -- -- - -- - -- - -- -- - -- - -- - -- -- - -
                    if ($user->setStatus($attributes['status']) == 0) {
                        $passedInStatus = ($attributes['status'] == 1 ? FALSE : TRUE); //TRUE to disable, FALSE to enable

                        /*
                        message:{"id":2,"shouldBlock":true, "version":60000, "filePath":"https://vodalive.registersim.com"}
                        topic:iot-2/Device/id/1491403327174/evt/apkupdate_msg/fmt/json
                        */

			$configurations = $em->getRepository("DashboardBundle:ConfigMaster")->findOneBy( array("configType" => "formConfig") );

                        $configVAlue["status"] = $passedInStatus;
                        $configVAlue["type"] = "apkupdate_msg";

                        $configVAlue["shouldBlock"] = TRUE;
                        $configVAlue["version"] = 60000;
                        $configVAlue["filePath"] = "https://vodalive.registersim.com/appdownload";
                        	//exit("update action after file-path --pass");

                        $configVAlue["operator"] = $this->container->getParameter("operator_id");
                        //	exit("update action after operator_id --pass");

                        //$respond = $this->get("api.helper")->publish($device->getDeviceId(), 'apkupdate_msg', json_encode($configVAlue)); //breaks here, will update
                                            	//exit("update action after publish device --fail");

                    }

                    // --- - -- - -- -- - -- - -- - -- -- - -- - -- - -- -- - -
                        	//exit("update action after set status --fail");
 
                    
                    
                    if ($user->setStatus($attributes['status']) == 0) {

                        $user->setStatus($attributes['status']);
                        //$user->setEnabled($attributes['status']);

                        $encoder = $this->container->get('security.password_encoder');
                        $user->setPassword("atoticome");
                        $encoder = $this->container->get('security.password_encoder');
                        $encoded = $encoder->encodePassword($user, $user->getPassword());
                        $user->setPassword($encoded);
                    }

                    $user->setRegion($region);
                    $user->setTerritory($territory);
                    $user->setAgentCode($attributes['agentCode']);

                    if (count(@$attributes['exclusive']) > 4) {
                        $user->setAgentExclusivity($attributes['exclusive']);
                    }

                    $em->flush();
                    $resp->setMessage("SUCCESSFULLY");
                    $resp->setRecord(array(
                        "agentName" => $user->getFirstName() . " " . $user->getLastName(),
                        "status" => $user->getStatus(),
                        "agentCode" => $user->getAgentCode(),
                        "type" => $user->getType(),
                        "exclusive" => $user->getAgentExclusivity(),
                        "id" => $user->getId(),
                        "number_devices" => $user->getNumberDevices(),
                    ));
                    $this->logUserEvent(UserLog::EDIT_USER, "update user action", (array) $attributes);
                } else {
                    $resp->setResult("ERROR");
                    $resp->setMessage("USER NOT FOUND");
                }
                //$this->logUserEvent(UserLog::UPDATE_USER, "update user",  array("attributes" => $attributes));
            } else {
                $resp->setResult("ERROR");
                $resp->setMessage("INVALID REQUEST,SERVER ERROR");
            }
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * View Reports
     *
     * @Route("/create_access_token/{id}", name="create_access_token")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:create_access_token.html.twig")
     */
    public function CreateAccessTokenAction($id)
    {

        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('RestApiBundle:User')->findAll();

        $data = array(
            'title' => "View Registrations Report:",
            'title_descr' => "View Registrations Report",
            'mobileNumber' => $id,
            'accessToken' => rand(10000000, 99999999),
            'agents' => $users
        );

        return $this->prepareResponse($data);
    }

    /**
     * Activate an Agent.
     *
     * @Route("/activate", name="activate")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function activateAction(Request $request)
    {

    }

    /**
     * Deactivate an Agent.
     *
     * @Route("/deactivate", name="deactivate")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function deactivateAction(Request $request)
    {

    }

    /**
     * Assign an Agent to a network.
     *
     * @Route("/assign", name="assign")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function assignNetworkAction(Request $request)
    {

    }

    /**
     * Activate Agents using CVS file.
     *
     * @Route("/activate_group", name="activate_group")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function batchActivateAction(Request $request)
    {

    }

    /**
     * Deactivate Agents using CVS file.
     *
     * @Route("/deactivate_group", name="deactivate_group")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function batchDeactivateAction(Request $request)
    {

    }

    /**
     * Add Agents using CVS file.
     *
     * @Route("/bulk_upload", name="bulk_upload")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function batchCreateAction(Request $request)
    {
        $resp = new JsonObject();
        $status = false;
        try {
            $attributes = $request->request->all();
            /** @var  $file UploadedFile */
            $file = $request->files->get('fileLocation');

            $fileName = $file->getClientOriginalName();
            $ext = pathinfo($fileName)["extension"];
            if (strcmp($ext, "csv") == 0) {
                $uploadedURL = $this->container->getParameter("resource_path") . DIRECTORY_SEPARATOR . "agent_uploads" . DIRECTORY_SEPARATOR . date("Ymd");
                $newFileName = date("YmdHi") . $fileName;
                //Move the file to the directory where brochures are stored
                $file->move($uploadedURL, $newFileName);
                //command to publish message
                $kernel = $this->get('kernel');
                $application = new Application($kernel);
                $application->setAutoExit(false);

                $input = new ArrayInput(array(
                    'command' => 'dashboard:bulk_agent_upload_command',
                    'file_path' => $uploadedURL . "/" . $newFileName
                ));
                // You can use NullOutput() if you don't need the output
                $application->run($input, null);
                $resp->setMessage("SUCCESS");
                $status = true;

                $this->logUserEvent(UserLog::BULK_AGENTS_UPLOAD, "bulk agents upload", array("file" => $fileName, 'status' => 'success'));
            } else {
                $resp->setMessage("Invalid File Type");

                $this->logUserEvent(UserLog::BULK_AGENTS_UPLOAD, "bulk agents upload", array("file" => $fileName, 'status' => 'failed'));
            }
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
        }
        $resp->setStatus($status);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * Add Agents using CVS file.
     *
     * @Route("/bulk_agent_delete", name="bulk_agent_delete")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function batchAgentDeleteAction(Request $request)
    {
        $resp = new JsonObject();
        $status = false;
        try {
            $attributes = $request->request->all();
            /** @var  $file UploadedFile */
            $file = $request->files->get('fileLocation');

            $fileName = $file->getClientOriginalName();
            $ext = pathinfo($fileName)["extension"];
            if (strcmp($ext, "csv") == 0) {
                $uploadedURL = $this->container->getParameter("resource_path") . DIRECTORY_SEPARATOR . "agent_delete" . DIRECTORY_SEPARATOR . date("Ymd");
                $newFileName = date("YmdHi") . $fileName;
                //Move the file to the directory where brochures are stored
                $file->move($uploadedURL, $newFileName);
                //command to publish message
                $kernel = $this->get('kernel');
                $application = new Application($kernel);
                $application->setAutoExit(false);

                $input = new ArrayInput(array(
                    'command' => 'dashboard:bulk_delete_command',
                    'file_path' => $uploadedURL . "/" . $newFileName
                ));
                // You can use NullOutput() if you don't need the output
                $application->run($input, null);
                $resp->setMessage("SUCCESS");
                $status = true;

                $this->logUserEvent(UserLog::BULK_AGENTS_DELETE, "bulk agents upload", array("file" => $fileName));
            } else {
                $resp->setMessage("Invalid File Type");

                $this->logUserEvent(UserLog::BULK_AGENTS_DELETE, "bulk agents upload", array("file" => $fileName));
            }
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
        }
        $resp->setStatus($status);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * Assign Agents to a network
     *
     * @Route("/assign_group", name="assign_group")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function batchAssignNetworkAction(Request $request)
    {

    }

    /**
     * Approve Agent
     *
     * @Route("/approve", name="approve")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function approveAgentAction(Request $request)
    {

    }

    public static function isValidMsisdn($number)
    {
        if(preg_match("/^((\+)*255|0)*(7)([4|5|6])[2-9][0-9]{6}$/", preg_replace('/\s+/','',$number)))
            return true;
        return false;
    }

}
