<?php

namespace aimgroup\DashboardBundle\Controller;

use aimgroup\DashboardBundle\Dao\JsonObject;
use aimgroup\DashboardBundle\Dao\JTableResponse;
use aimgroup\DashboardBundle\DashboardBundle;
use aimgroup\DashboardBundle\Entity\ConfigMaster;
use aimgroup\DashboardBundle\Entity\FieldNames;
use aimgroup\DashboardBundle\Entity\FieldTypes;
use aimgroup\DashboardBundle\Entity\Gender;
use aimgroup\DashboardBundle\Entity\Idtype;
use aimgroup\DashboardBundle\Entity\Region;
use aimgroup\DashboardBundle\Entity\Service;
use aimgroup\DashboardBundle\Entity\ServiceTypes;
use aimgroup\DashboardBundle\Entity\Step;
use aimgroup\DashboardBundle\Entity\StepField;
use aimgroup\DashboardBundle\Entity\Territory;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Process\ProcessBuilder;

/**
 * This FormController is used to manage configuration which are supposed to be sent to mobile devices.
 *
 * @author Oscar Makala
 *
 * @Route("admin/forms")
 */
class FormController extends AbstractController
{

    var $session;

    public function __construct()
    {
        $this->session = new Session();
    }

    /**
     *
     * @Route("/", name="admin/forms")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($name)
    {
        return $this->render('', array('name' => $name));
    }

    /**
     * @param Request $request
     * @Route("/lists",name="lists")
     * @Method({"POST","GET"})
     */
    public function listStepAction(Request $request)
    {
        $resp = new JTableResponse();
        try {

            $em = $this->getDoctrine()->getManager();
            $steps = $em->createQuery(
                "SELECT s.id, s.title,s.titleAlt,s.next,concat('step',s.id) as step FROM DashboardBundle:Step s"
            )
                ->getResult();
            $resp->setRecords($steps);
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @Route("/listStepField",name="listStepField")
     * @Method({"POST","GET"})
     *
     */
    public function listStepFieldAction(Request $request)
    {
        $resp = new JTableResponse();
        try {
            $em = $this->getDoctrine()->getManager();
            $attributes = $request->query->all();
            $fields = $em->createQuery(
                "SELECT f.id as fieldId,f.skey,f.required,f.regex,f.hint,f.hintLangAlt,f.regexErrorAlt,f.type,f.regex,f.regexError,f.action
                    FROM DashboardBundle:StepField f WHERE f.step =:step"
            )
                ->setParameter('step', $attributes["stepId"])
                ->getResult();
            $resp->setRecords($fields);
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @Route("/listFieldKeys",name="listFieldKeys")
     * @Method({"POST","GET"})
     */
    public function listFieldKeysAction(Request $request)
    {
        $resp = new JTableResponse();
        try {
            $em = $this->getDoctrine()->getManager();
            $fieldNames = $em->createQuery(
                "SELECT f.id as Value,f.name as DisplayText FROM DashboardBundle:FieldNames f"
            )
                ->getResult();
            $resp->setOptions($fieldNames);
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @Route("/listFieldTypes",name="listFieldTypes")
     * @Method({"POST","GET"})
     */
    public function listFieldKeysTypeAction(Request $request)
    {
        $resp = new JTableResponse();
        try {
            $em = $this->getDoctrine()->getManager();
            $fieldNames = $em->createQuery(
                "SELECT f.id as Value,f.name as DisplayText FROM DashboardBundle:FieldTypes f"
            )
                ->getResult();
            $resp->setOptions($fieldNames);
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @Route("/createStep",name="createStep")
     * @Method({"POST","GET"})
     */
    public function createStepAction(Request $request)
    {
        $resp = new JTableResponse();
        try {

            $em = $this->getDoctrine()->getManager();
            $attributes = $request->request->all();
            $step = new Step();
            $step->setNext($attributes["next"]);
            $step->setTitle($attributes["title"]);
            $step->setTitleAlt($attributes['titleAlt']);

            $em->persist($step);
            $em->flush();
            $resp->setRecord(
                array(
                    'id' => $step->getId(),
                    'title' => $step->getTitle(),
                    'next' => $step->getNext(),
                    'title' => $step->getTitleAlt(),
                )
            );
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @Route("/createStepField",name="createStepField")
     * @Method({"POST","GET"})
     */
    public function createStepFieldAction(Request $request)
    {
        $resp = new JTableResponse();
        try {

            $em = $this->getDoctrine()->getManager();
            $attributes = $request->request->all();

            $step = $em->getRepository('DashboardBundle:Step')->findOneBy(array('id' => $attributes['id']));
            if ($step) {
                $field = new StepField();

                $field->setSkey($attributes['skey']);
                $field->setHint($attributes['hint']);
                $field->setRequired($attributes['required']);
                $field->setType($attributes['type']);


                if (isset($attributes['action'])) {
                    $field->setAction($attributes['action']);
                }
                if (isset($attributes["regex"])) {
                    $field->setRegex($attributes['regex']);
                }
                if (isset($attributes["regexError"])) {
                    $field->setRegexError($attributes['regexError']);
                }
                if (isset($attributes["hintLangAlt"])) {
                    $field->setHintLangAlt($attributes["hintLangAlt"]);
                }
                if (isset($attributes["regexErrorAlt"])) {
                    $field->setRegexErrorAlt($attributes["regexErrorAlt"]);
                }
                //maxLengthErrorAlt
                $field->setStep($step);
                $em->persist($field);
                $em->flush();

                $resp->setRecord(
                    array(
                        "fieldId" => $field->getId(),
                        "hint" => $field->getHint(),
                        "skey" => $field->getSkey(),
                        "required" => $field->getRequired(),
                        "type" => $field->getType(),
                        "regex" => $field->getRegex(),
                        "regexError" => $field->getRegexError(),
                        "hintLangAlt" => $field->getHintLangAlt(),
                        "regexErrorAlt" => $field->getRegexErrorAlt(),
                    )
                );
            }
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @Route("/updateStepField",name="updateStepField")
     * @Method({"POST","GET"})
     */
    public function updateStepFieldAction(Request $request)
    {
        $resp = new JTableResponse();
        try {

            $em = $this->getDoctrine()->getManager();
            $attributes = $request->request->all();
            $stepField = $em->getRepository('DashboardBundle:StepField')->findOneBy(
                array('id' => $attributes['fieldId'])
            );
            if ($stepField) {

                $stepField->setSkey($attributes['skey']);
                $stepField->setHint($attributes['hint']);
                $stepField->setRequired($attributes['required']);
                $stepField->setType($attributes['type']);


                if (isset($attributes['action'])) {
                    $stepField->setAction($attributes['action']);
                }
                if (isset($attributes["regex"])) {
                    $stepField->setRegex($attributes['regex']);
                }
                if (isset($attributes["regexError"])) {
                    $stepField->setRegexError($attributes['regexError']);
                }


                if (isset($attributes["action"])) {
                    $stepField->setAction($attributes['action']);
                }
                if (isset($attributes["hintLangAlt"])) {
                    $stepField->setHintLangAlt($attributes['hintLangAlt']);
                }
                if (isset($attributes["regexErrorAlt"])) {
                    $stepField->setRegexErrorAlt($attributes["regexErrorAlt"]);
                }


                $em->flush();

                $resp->setRecord(
                    array(
                        "fieldId" => $stepField->getId(),
                        "skey" => $stepField->getSkey(),
                        "required" => $stepField->getRequired(),
                        "type" => $stepField->getType(),
                        "regex" => $stepField->getRegex(),
                        "regexError" => $stepField->getRegexError(),
                        "hintLangAlt" => $stepField->getHintLangAlt(),
                        "regexErrorAlt" => $stepField->getRegexErrorAlt(),
                    )
                );
            }
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @Route("/updateStep",name="updateStep")
     * @Method({"POST","GET"})
     */
    public function updateStepAction(Request $request)
    {
        $resp = new JTableResponse();
        try {
            $em = $this->getDoctrine()->getManager();
            $attributes = $request->request->all();
            $step = $em->getRepository('DashboardBundle:Step')->findOneBy(array('id' => $attributes['id']));
            if ($step) {
                $step->setNext($attributes["next"]);
                $step->setTitle($attributes["title"]);
                $step->setTitleAlt($attributes['titleAlt']);
                $em->flush();

                $resp->setRecord(
                    array(
                        'id' => $step->getId(),
                        'title' => $step->getTitle(),
                        'next' => $step->getNext(),
                        'titleAlt' => $step->getTitleAlt(),
                    )
                );
            }
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * Load existing Dynamic Form for the mobile App.
     *
     * @Route("/load_form", name="load_form")
     * @Method("GET")
     * @Template("DashboardBundle:AppConfigurations:dynamic_form.html.twig")
     */
    public function loadDynamicFormAction(Request $request)
    {
        if (!in_array(29, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }


        $em = $this->getDoctrine()->getManager();
        $dataColumns = $em->getConnection()->getSchemaManager()->listTableColumns('registration');

        $data = array(
            'title' => "Dynamic Form Configurations",
            'title_descr' => "List, create, delete, activate System Admins",
            'dataColumns' => $dataColumns,
            "operator" => strtolower($this->container->getParameter("operator_name"))
        );

        return $this->prepareResponse($data);
    }

    /**
     *
     * This code is roughly built, find better way to optimize this
     *
     * Save existing Dynamic Form ready for publishing
     * @param Request $request
     * @Route("/save_form",name="save_form")
     * @Method("GET")
     *
     */
    public function saveDynamicFormAction(Request $request)
    {

        $lang1 = $this->container->getParameter("primary_language");
        $lang2 = $this->container->getParameter("secondary_language");

        $resp = new JsonObject();
        $status = false;
        try {
            $em = $this->getDoctrine()->getManager();
            $steps = $em->getRepository("DashboardBundle:Step")->findAll();
            $fields = $em->getRepository("DashboardBundle:StepField")->findAll();
            $fieldNames = $em->getRepository("DashboardBundle:FieldNames")->findAll();
            $regions = $em->getRepository("DashboardBundle:Region")->findAll();


            /** @var  $fieldName FieldNames */
            foreach ($fieldNames as $fieldName) {
                $nameArray[$fieldName->getId()] = $fieldName->getName();
            }

            $fieldTypes = $em->getRepository("DashboardBundle:FieldTypes")->findAll();
            /** @var  $fieldName FieldTypes */
            foreach ($fieldTypes as $fieldType) {
                $typeArray[$fieldType->getId()] = $fieldType->getName();
            }
            if ($steps) {
                /** @var  $step Step */
                foreach ($steps as $step) {
                    $stepName = "step" . $step->getId();
                    $top["next"] = $step->getNext();


                    $titleArray = array();
                    $titleArray[$lang1] = $step->getTitle();
                    $titleArray[$lang2] = $step->getTitleAlt();
                    $top["title"] = $titleArray;


                    $top["fields"] = array();
                    /** @var  $field StepField */
                    foreach ($fields as $field) {
                        if ($field->getStep() === $step) {
                            $inner = array();
                            $inner["key"] = $nameArray[$field->getSkey()];

                            $inner["hint"] = $this->generateLanguageArray($field->getHint(), $field->getHintLangAlt());
                            $inner["type"] = $typeArray[$field->getType()];

                            if ($field->getRequired()) {
                                $inner["required"] = $field->getRequired();
                            }
                            if (!empty($field->getRegex())) {
                                $inner["regex"] = array();
                                $inner["regex"]["value"] = $field->getRegex();
                                $inner["regex"]["err"] = $field->getRegexError();
                                $inner["regex"]["err"] = $this->generateLanguageArray(
                                    $field->getRegexError(),
                                    $field->getRegexErrorAlt()
                                );
                            } else {
                                if (strcmp($inner["key"], "gender") == 0) {
                                    $inner["options"] = array();

                                    $genders = $em->getRepository("DashboardBundle:Gender")->findAll();
                                    $inner["options"] = array();
                                    /** @var  $gender Gender */
                                    foreach ($genders as $gender) {
                                        $innderGender = array();
                                        $innderGender["value"] = $gender->getShortHand();
                                        $innderGender["key"] = $this->generateLanguageArray($gender->getName(), $gender->getNameAlt());
                                        array_push($inner["options"], $innderGender);
                                    }
                                } else {
                                    if (strcmp($inner["key"], "region") == 0) {
                                        $inner["options"] = array();
                                        $inner["child"] = "territory";
                                        $t = array();
                                        $t["value"] = "Select Region";
                                        $t["key"] = "";
                                        array_push($inner["options"], $t);
                                        /** @var  $region Region */
                                        foreach ($regions as $region) {
                                            $t["value"] = $region->getCode();
                                            $t["key"] = $region->getName();
                                            array_push($inner["options"], $t);
                                        }
                                    } else {
                                        if (strcmp($inner["key"], "territory") == 0) {
                                            $inner["v_options"] = array();
                                            /** @var  $region Region */
                                            foreach ($regions as $region) {
                                                $territories = $region->getTerritories();
                                                $r = array();
                                                $s = array();


                                                $v = array();
                                                $v["value"] = "Select Territory";
                                                $v["key"] = "";
                                                array_push($r, $v);
                                                /** @var  $territory Territory */
                                                foreach ($territories as $territory) {
                                                    $v["value"] = $territory->getId();
                                                    $v["key"] = $territory->getName();
                                                    array_push($r, $v);
                                                }
                                                $s[$region->getCode()] = $r;
                                                array_push($inner["v_options"], $s);
                                            }
                                        } else {
                                            if (strcmp($inner["key"], "id-type") == 0) {
                                                $inner["options"] = array();
                                                $idtypes = $em->getRepository("DashboardBundle:Idtype")->findBy(array("isActive" => 1));
                                                /** @var  $idtype Idtype */
                                                foreach ($idtypes as $idtype) {
                                                    $this->container->get("api.helper")->logInfo("form", "form", array($idtype->getId(), $idtype->getName(), $idtype->getIsActive()));
                                                    $innerIdType = array();
                                                    $innerIdType["value"] = $idtype->getId();
                                                    $innerIdType["key"] = $this->generateLanguageArray($idtype->getName(), $idtype->getNameAlt());
                                                    array_push($inner["options"], $innerIdType);
                                                }

                                            }
                                        }
                                    }
                                }
                            }

                            array_push($top["fields"], $inner);
                        }
                    }

                    $json[$stepName] = $top;
                }
            }
//            /**
//             * remove null values
//             */
//            echo "not working";
//            echo "<pre>";
//            print_r($json);
//            echo "</pre>";


            $json["count"] = sizeof($json);
            $jsonString = json_encode($json);

            $configMaster = new ConfigMaster();
            $configMaster->setConfig($jsonString);
            $configMaster->setName("forms");
            $configMaster->setCreatedOn(new \DateTime());
            $configMaster->setConfigType("formConfig");


            /** @var  $configuration ConfigMaster */
            $configuration = $em->getRepository("DashboardBundle:ConfigMaster")->findOneBy(
                array("configType" => "formConfig")
            );
            if (!$configuration) {
                $em->persist($configMaster);
            } else {
                $configuration->setConfig($configMaster->getConfig());
                $configuration->setVersion($configuration->getVersion() + 1);
            }

            $em->flush();
            $status = true;
            $resp->setMessage("SUCCESS");
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
        }
        $resp->setStatus($status);

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @Route("/display_form",name="display_form")
     * @Method("GET")
     * @return \aimgroup\DashboardBundle\Controller\JsonResponse
     */
    public function displaySavedDynamicFormAction(Request $request)
    {
        $resp = new JsonObject();
        $status = false;
        try {
            $em = $this->getDoctrine()->getManager();

            $configurations = $em->getRepository("DashboardBundle:ConfigMaster")->findBy(
                array("configType" => "formConfig")
            );

            $resp->setItem($configurations);
            $status = true;
            $resp->setMessage("SUCCESS");
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
        }
        $resp->setStatus($status);

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @Method("POST")
     * @Route("/publish",name="publishForm")
     */
    public function publishFormAction(Request $request)
    {
        $resp = new JsonObject();
        $status = false;
        try {
            $attributes = $request->request->all();
            $em = $this->getDoctrine()->getManager();

            $deviceId = $attributes["deviceId"];
	        $formId = $attributes["formId"];

            //$configurations = $em->getRepository("DashboardBundle:ConfigMaster")->findOneBy(
                //array("configType" => "formConfig")
            //);

            $service = $em->getRepository("DashboardBundle:Service")->find(1);

            //$configVAlue["form"] = json_decode($configurations->getConfig(), true);
            $configVAlue["form"] = json_decode($service->getFormJson(), true);  
            
            
            //$configVAlue["type"] = "form_config";
            $configVAlue["version"] = 13;
            $configVAlue["ordering"] = $service->getOrdering();
            
            
	        $configVAlue["name"] 		= $service->getName();
	        $configVAlue["type"] 		= $service->getServiceCode();
	        $configVAlue["formType"] 	= $service->getName();
            $configVAlue["operator"] 	= $this->container->getParameter("operator_id");

            $respond = $this->get("api.helper")->publish(
                $deviceId,
                $this->container->getParameter("mqtt_formupdate_msg"),
                json_encode($configVAlue)
            );

            $status = true;
            $resp->setItem($respond);
            $resp->setMessage("SUCCESS");
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
        }
        $resp->setStatus($status);

        return $this->buildResponse($resp, Response::HTTP_OK);
    }


    private function generateLanguageArray($first, $second = null)
    {
        $lang1 = $this->container->getParameter("primary_language");
        $lang2 = $this->container->getParameter("secondary_language");

        $result = array();
        if ($first) {
            $result[$lang1] = $first;
            $result[$lang2] = $second;

            return $result;
        } else {
            return $result;
        }
    }

    /**
     * Load existing Services.
     *
     * @Route("/load_services/", name="load_services")
     * @Method("GET")
     * @Template("DashboardBundle:AppConfigurations:services_list.html.twig")
     */
    public function loadServices() {

        if (!in_array(29, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $em = $this->getDoctrine()->getManager();

        $services = $em->getRepository('DashboardBundle:Service')->findAll();
        $service_types = $em->getRepository('DashboardBundle:ServiceTypes')->findBy(array('status'=>1));

        //echo "<pre>"; print_r($services);  echo "<pre>"; exit;
        //echo $this->container->getParameter("resource_path");
        //echo $this->container->getParameter("hostname");

        $data = array(
            'title'         => "Services",
            'title_descr'   => "List, create, delete, activate System Admins",
            'services'      => $services,
            'service_types' => $service_types,
            'hostname'      => $this->container->getParameter("hostname"),
            "operator"      => strtolower($this->container->getParameter("operator_name"))
        );

        return $this->prepareResponse($data);
    }


    /**
     * Load existing Services.
     *
     * @Route("/load_service_form/{id}", name="load_service_form")
     * @Method("GET")
     * @Template("DashboardBundle:AppConfigurations:service_form.html.twig")
     */
    public function loadServiceForm($id) {
        if (!in_array(29, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $em = $this->getDoctrine()->getManager();

        $services = $em->getRepository('DashboardBundle:Service')->findOneBy(array("id" => $id));

        $fieldTypes = $em->getRepository('DashboardBundle:FieldTypes')->findAll();

        $fieldNames = $em->getRepository('DashboardBundle:FieldNames')->findAll();

        $regions = $em->getRepository('DashboardBundle:Region')->findAll();
        foreach ($regions as $region) {
            $territories = $region->getTerritories();


            $r = array();
            $s = array();


            $v = array();
            $v["value"] = "Select Territory";
            $v["key"] = "";
            array_push($r, $v);
            /** @var  $territory Territory */
            foreach ($territories as $territory) {
                $v["value"] = $territory->getId();
                $v["key"] = $territory->getName();
                array_push($r, $v);
            }
            $s[$region->getCode()] = $r;
            //array_push($inner["v_options"], $s);
        }

        $t = array();
        $t["value"] = "Select Region";
        $t["key"] = "";

        $regionsArray = array();
        array_push($regionsArray, $t);
        /** @var  $region Region */
        foreach ($regions as $region) {
            $t["value"] = $region->getCode();
            $t["key"] = $region->getName();
            array_push($regionsArray, $t);
        }



        $idType = array();
        $idtypes = $em->getRepository("DashboardBundle:Idtype")->findBy(array("isActive" => 1));
        /** @var  $idtype Idtype */
        foreach ($idtypes as $idtype) {
            $this->container->get("api.helper")->logInfo("form", "form", array($idtype->getId(), $idtype->getName(), $idtype->getIsActive()));
            $innerIdType = array();
            $innerIdType["value"] = $idtype->getId();
            $innerIdType["key"] = $this->generateLanguageArray($idtype->getName(), $idtype->getNameAlt());
            array_push($idType, $innerIdType);
        }

        $jsonForm = json_decode($services->getFormJson(), true);
        unset($jsonForm["count"]);
        unset($jsonForm["form_service"]);
		
        $data = array(
            'title'         => "Services",
            'title_descr'   => "List, create, delete, activate System Admins",
            'services'      => $services,
            'id'            => $id,
            'formJson'      => $jsonForm,
            'regions'       => json_encode($regionsArray),
            'territory'     => json_encode($s),
            'idTypes'       => json_encode($idType),
            'fieldNames'    => $fieldNames,
            'fieldTypes'    => $fieldTypes,
            'hostname'      => $this->container->getParameter("hostname"),
            "operator"      => strtolower($this->container->getParameter("operator_name"))
        );

        return $this->prepareResponse($data);
    }

    /**
     * @param Request $request
     * @Route("/add_update_services",name="add_update_services")
     * @Method({"POST","GET"})
     */
    public function add_update_services(Request $request) {
        $resp = new JTableResponse();
        //echo "<pre>"; print_r($attributes); echo "</pre>";

        try {
            $em = $this->getDoctrine()->getManager();
            $attributes = $request->request->all();

            if(isset($attributes['id'])) {
                $service = $em->getRepository('DashboardBundle:Service')->findOneBy(array('id' => $attributes['id']));

                if($service) {
                    $service->setName($attributes["name"]);
                    $service->setNameAlt($attributes["name_alt"]);
                    $service->setDescription($attributes["description"]);
                    $service->setOrdering($attributes["ordering"]);
                    $service->setStatus($attributes["status"]);
                    $service->setServiceCode($attributes["service_code"]);
                    $em->flush();
                }

            } else {
                $service = new Service();

                $service->setName($attributes["name"]);
                $service->setNameAlt($attributes["name_alt"]);
                $service->setDescription($attributes["description"]);
                $service->setOrdering($attributes["ordering"]);
                $service->setStatus($attributes["status"]);
                $service->setServiceCode($attributes["service_code"]);

                $date = new \DateTime("now");
                $service->setCreatedDate($date);

                $em->persist($service);
                $em->flush();
            }

        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }

        return $this->buildResponse($resp, Response::HTTP_OK);
    }




    /**
     * @param Request $request
     * @Route("/update_json_form", name="update_json_form")
     * @Method({"POST","GET"})
     */
    public function update_json_form(Request $request) {
        $resp = new JTableResponse();

        try {
            $em = $this->getDoctrine()->getManager();
            $attributes = $request->request->all();
            //echo "<pre>"; print_r($attributes); exit;

            $service = $em->getRepository('DashboardBundle:Service')->findOneBy(array('id' => $attributes["itemId"]));

            if($service) {
                unset($attributes["itemId"]);				

				$steps = count($attributes);
				
				$attributes = json_encode($attributes);
				
				$attributes = json_decode($attributes);
				
				foreach($attributes as $aKey => $aValue){
					foreach($aValue->fields as $bKey => $bValue){
						if(isset($bValue->options)){
							$attributes->$aKey->fields[$bKey]->options = json_decode($bValue->options);
						}
						if(isset($bValue->v_options)){
							$attributes->$aKey->fields[$bKey]->v_options = json_decode($bValue->v_options);
						}
					}
				}					
				
                $attributes->count = $steps;
                $attributes->form_service = $service->getServiceCode();
				
                $service->setFormJson(json_encode($attributes));
                $em->flush();
            }

        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }
	
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

}
