<?php
/**
 * Created by PhpStorm.
 * User: Roby
 * Date: 1/18/18
 * Time: 2:34 PM
 */

namespace aimgroup\DashboardBundle\Controller;

use Carbon\Carbon;
use FOS\RestBundle\Controller\Annotations\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;


/**
 * Class BiometricController
 *
 * @Route("/admin/biometric/")
 */
class BiometricController extends AbstractController {

    var $session;

    public function __construct() {
        $this->session = new Session();
    }

    /**
     * Show Biometric Dashboard
     * @Route("", name="biometric_index")
     * @Method("Get")
     * @Template("DashboardBundle:Biometric:index.html.twig")
     */
    public function indexAction() {

        $user_roles = json_decode($this->get('session')->get('user_role_perms'), true);

        if (is_null($user_roles) || !in_array(2, $user_roles)) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $this->container->get("api.helper")->logInfo("BiometricController", "GET", array( "request" => $user_roles, true ));
        $user = $this->get('security.token_storage')->getToken()->getUser();

        return $this->prepareResponse([
            'title'       => 'Biometric',
            'title_descr' => 'Biometric',
            'dt_date'     => date('Y-m-d'),
            'akuku'       => json_encode($user->getRoles()) . ' | ' . (new Session())->get('user_role_perms'),
            'date_today'  => date('Y-m-d'),
            'date_start'  => date_format(date_sub(date_create(date('Y-m-d')), date_interval_create_from_date_string('10 days')), 'Y-m-d')
        ]);
    }


    /**
     * hourly_summary Summary
     *
     * @Route("get_count_summary/{start_date}/{end_date}", name="biometric_summary")
     * @Method("Get")
     */
    public function count_summaryAction($start_date, $end_date) {
        $em = $this->getDoctrine()->getManager();

        $where = " WHERE r.registryType = 2 ";

        $where .= " AND 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        $leftJOIN = "";
        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND a.parent_id = " . $user->getId();
            $leftJOIN = " JOIN x.owner a ";
        }
        if ($start_date != 1) {
            $where .= " AND date(x.createdDate) >= '" . $start_date . "' AND date(x.createdDate) <= '" . $end_date . "' ";
        } else {
            $end_date = date_format(date_sub(date_create(date('Y-m-d')), date_interval_create_from_date_string("10 days")), "Y-m-d");
            //$where .= " AND date(x.createdDate) >= '" . $end_date . "' ";
            if (true) {
                $where .= " AND date(x.createdDate) >= '" . $end_date . "' AND date(x.createdDate) <= '" . date('Y-m-d H:i:s') . "' ";
            }
        }


        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');


        $treg = $freg = $fail = $pending = 0;

        $count_tfreg = $em->createQuery("SELECT count(r) as reg_count, x.image_count, r.temporaryRegStatus as treg,r.verifyState as verifyState, r.fullRegStatus as freg FROM RestApiBundle:RegistrationStatus r"
            . " JOIN r.registrationId x " . $leftJOIN . $where
            . " GROUP BY x.image_count, r.temporaryRegStatus, r.fullRegStatus"
        )->getArrayResult();


        $data[ 'freg' ] = $data[ 'unverified' ] = $data[ 'pending_info' ] = $data[ 'verified' ] = $data[ 'declined' ] = $data[ 'total' ] = 0;

        foreach ($count_tfreg as $key => $val) {

            $data[ 'total' ] = $data[ 'total' ] + $val[ 'reg_count' ];

            if ($val[ 'image_count' ] != 1) {

                $data[ 'pending_info' ] = $data[ 'pending' ] + $val[ 'reg_count' ];

            } else {

                $data[ 'received' ] = $data[ 'received' ] + $val[ 'reg_count' ];
            }

            if ($val[ 'verifyState' ] == 1) {

                $data[ 'verified' ] = $data[ 'verified' ] + $val[ 'reg_count' ];
            }

            if ($val[ 'verifyState' ] == 0) {

                $data[ 'unverified' ] = $data[ 'unverified' ] + $val[ 'reg_count' ];
            }

            if ($val[ 'verifyState' ] == 4) {

                $data[ 'declined' ] = $data[ 'declined_nida' ] + $val[ 'reg_count' ];
            }

        }


        if ($start_date == 1 && !in_array("ROLE_SUPERAGENT", $roles)) {
            $result = $em->createQuery("SELECT s from DashboardBundle:Summary s "
                . "WHERE s.dataType = 'top_count' ")
                ->getArrayResult();

            if (count($result) >= 1) {
                $result = $result[ 0 ];

                foreach (json_decode($result[ 'dataString' ]) as $key => $val) {
                    $data[ $key ] = $data[ $key ] + $val;
                }
            }
        }

        echo json_encode($data);
        exit;
    }


    /**
     * hourly_summary Summary
     *
     * @Route("get_hourly_summary", name="biometric_hourly_summary")
     * @Method("GET")
     *
     */
    public function getRegistrationSummaryAction(Request $request) {

        $now = new \DateTime();

        $start_date =  $request->query->get('start', date('Y-m-d'));

        $end_date = $request->query->get('end', date('Y-m-d'));

        if (empty($start_date) || empty($end_date)) {

            return JsonResponse::create([ 'message' => 'There was an error, please try again' ], 422);

        } else {

            $where = " AND r.createdDate >= '" . $now->setTime(0, 0, 0)->format('Y-m-d H:i:s') . "' AND r.createdDate <= '" . (new \DateTime())->setTime(23, 59, 59)->format('Y-m-d H:i:s') . "'";

            $query = "SELECT HOUR(r.createdDate) AS dt_hour, COUNT(r.id) AS total FROM registration r WHERE r.registration_type = 2 AND 1 = 1 " . $where . " GROUP BY dt_hour ORDER BY dt_hour DESC";

            $connection = $this->getDoctrine()
                ->getManager()
                ->getConnection();

            $statement = $connection->prepare($query);
            $statement->execute();
            $result = $statement->fetchAll();
	    
            // Prepare the times

            $hour = 0;
            $data = array();

            while (++$hour <= 24) {

            //    $data[ (new \DateTime())->setTime($hour, 0, 0)->getTimestamp() ] = isset($result[ $hour ]) ? $result[ $hour ][ 'total' ] : 0;

                 $data[ $hour ] = in_array($hour, array_column($result, 'dt_hour')) ? $result[  $hour ][ 'total' ] : 0;

            }

            return JsonResponse::create( array_values($data) , 200);
        }
    }


    /**
     * Get verifiers idle time
     *
     * @Route("get_daily_count", name="biometric_daily_count")
     * @Method({"POST","GET"})
     */
    public function dailySummaryCounts() {
        $rsm = new \Doctrine\ORM\Query\ResultSetMapping();
        $rsm->addEntityResult('RestApiBundle:user', 'u');
        $rsm->addScalarResult('first_name', 'first_name');
        $rsm->addScalarResult('last_name', 'last_name');
        $rsm->addScalarResult('counter', 'counter');

        //$queryString = "select u.first_name, u.last_name, count(r2.id) as counter, r2.verifyBy, r2.id as maxid, r2.verifyDate, MAX(r2.verifyDate) as maxdate, TIMEDIFF(now(), MAX(r2.verifyDate)) as idletime FROM RegistrationStatus r2, user u WHERE r2.verifyBy = u.id AND DATE(r2.verifyDate) = CURDATE() GROUP BY r2.verifyBy ORDER BY maxdate";
        //$queryString = "select u.first_name, u.last_name, count(r2.id) as counter FROM RegistrationStatus r2, user u "
        //        . "WHERE r2.verifyBy = u.id AND r2.verifyBy != 3260 AND DATE(r2.verifyDate) = CURDATE() AND r2.verifyState > 0 GROUP BY r2.verifyBy ORDER BY counter";

        $queryString = 'select u.first_name, u.last_name, count(r2.id) as counter FROM RegistrationStatus r2, user u '
            . 'WHERE (r2.registry_type = 2 AND r2.verifyBy = u.id OR r2.originalVerifyBy = u.id) AND r2.verifyBy != 3260 AND r2.verifyDate = ' . date('Ymd') . " AND r2.verifyState in (1, 2, 3, '-2', '-3') GROUP BY r2.verifyBy ORDER BY counter";


        $query = $this->getDoctrine()->getManager()->createNativeQuery($queryString, $rsm);

        $agents = $query->getResult();

        return $this->buildResponse($agents, Response::HTTP_OK);
    }

    /**
     * active_agents Summary
     *
     * @Route("verify_daily_report/{start_date}/{end_date}", name="biometric_daily_report")
     * @Method("GET")
     * @Template()
     */
    public function verify_daily_reportAction($start_date, $end_date) {
        $em = $this->getDoctrine()->getManager();

        $where = ' WHERE r0_.registry_type = 2 ';

        $where .= " AND 1=1 ";
        $where2 = $where;
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if ($start_date != 1) {
            $where .= " AND DATE(r0_.verifyDate) >= '" . date('Y-m-d', strtotime($start_date)) . "' AND DATE(r0_.verifyDate) <= '" . date('Y-m-d', strtotime($end_date)) . "' ";
        }
        $where .= " AND r0_.verifyState in (1, 2, 3, '-2', '-3') ";

        $where .= " AND DATE(r0_.createdDate) >= '" . date("Y-m-d", strtotime("-3 months")) . "'";


        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

//$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "

        /* $query = $em->createQueryBuilder()
          ->select("s.id as sid, COUNT(s.id) as sum_regs, DATE_FORMAT(s.verifyDate, '%Y-%m-%d') as created_on, s.verifyState "
          . " from RestApiBundle:RegistrationStatus s" . $where
          . " GROUP BY created_on, s.verifyState ORDER BY created_on DESC")
          ->getQuery();
          $result1 = $query->getResult(); */

        $query1 = "SELECT r0_.id AS sid, COUNT(r0_.id) AS sum_regs, DATE_FORMAT (r0_.verifyDate,'%Y-%m-%d') AS created_on, r0_.verifyState AS verifyState "
            . " FROM RegistrationStatus r0_ "
            . $where . " GROUP BY created_on, r0_.verifyState ORDER BY created_on DESC";

        $connection = $em->getConnection();
        $statement = $connection->prepare($query1);
        $statement->execute();
        $result1 = $statement->fetchAll();

        $data = $active = array();
        foreach ($result1 as $key => $value) {
            $data[ $value[ 'created_on' ] ] = (@$data[ $value[ 'created_on' ] ] ? $data[ $value[ 'created_on' ] ] : $value);

            $data[ $value[ 'created_on' ] ][ 'sum_total' ] = @$data[ $value[ 'created_on' ] ][ 'sum_total' ] + $value[ 'sum_regs' ];
            $data[ $value[ 'created_on' ] ][ 'sum_good' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_good' ] ? $data[ $value[ 'created_on' ] ][ 'sum_good' ] : 0);
            $data[ $value[ 'created_on' ] ][ 'sum_fair' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_fair' ] ? $data[ $value[ 'created_on' ] ][ 'sum_fair' ] : 0);
            $data[ $value[ 'created_on' ] ][ 'sum_bad' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_bad' ] ? $data[ $value[ 'created_on' ] ][ 'sum_bad' ] : 0);
            $data[ $value[ 'created_on' ] ][ 'sum_unrated' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_unrated' ] ? $data[ $value[ 'created_on' ] ][ 'sum_unrated' ] : 0);

            if ($value[ 'verifyState' ] == 1) {
                $data[ $value[ 'created_on' ] ][ 'sum_good' ] = $data[ $value[ 'created_on' ] ][ 'sum_good' ] + $value[ 'sum_regs' ];
            } elseif ($value[ 'verifyState' ] == 2 || $value[ 'verifyState' ] == -2) {
                $data[ $value[ 'created_on' ] ][ 'sum_fair' ] = $data[ $value[ 'created_on' ] ][ 'sum_fair' ] + $value[ 'sum_regs' ];
            } elseif ($value[ 'verifyState' ] == 3 || $value[ 'verifyState' ] == -3) {
                $data[ $value[ 'created_on' ] ][ 'sum_bad' ] = $data[ $value[ 'created_on' ] ][ 'sum_bad' ] + $value[ 'sum_regs' ];
            } else {
                $data[ $value[ 'created_on' ] ][ 'sum_unrated' ] = $data[ $value[ 'created_on' ] ][ 'sum_unrated' ] + $value[ 'sum_regs' ];
            }
        }

        echo json_encode($data);
        exit;
    }


    /**
     * Verification Time
     *
     * @Route("verification_time/{start_date}/{end_date}", name="biometric_verification_time")
     * @Method("GET")
     * @Template()
     */
    public function avarage_verification_timeAction($start_date, $end_date) {

        try {

            $em = $this->getDoctrine()->getManager();

            $where = " r0_.registry_type = 2 AND 1=1 AND r0_.verifyState in (1, 2, 3, '-2', '-3') AND r0_.verifyLock > 1 AND r0_.verifyLock < 1000 ";
            /** @var  $user User */
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $roles = $user->getRoles(); //array of roles

            if (in_array('ROLE_SUPERAGENT', $roles)) {
                $where .= ' AND r.parent_id = ' . $user->getId();
            }
            if ($start_date != 1) {
                $where .= ' AND r0_.date_stored_verifyDate >= ' . date('Ymd', strtotime($start_date)) . ' AND r0_.date_stored_verifyDate <= ' . date('Ymd', strtotime($end_date)) . ' ';
            }


            $emConfig = $em->getConfiguration();
            $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
            $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
            $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
            $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');


            $treg = $freg = $fail = $pending = 0;

            $query1 = 'SELECT AVG(r0_.verifyLock) AS verifyLock '
                . ' FROM RegistrationStatus r0_ ' . $where;

            $connection = $em->getConnection();
            $statement = $connection->prepare($query1);
            $statement->execute();
            $count_tfreg = $statement->fetchAll();


            $count_tfreg2[ 'verifyLock' ] = (@$count_tfreg[ 0 ][ 'verifyLock' ] ? floor($count_tfreg[ 0 ][ 'verifyLock' ]) . ' Seconds' : 0);

            echo json_encode($count_tfreg2);

        } catch (\Exception $e) {

            echo $e->getMessage();
        }
        exit;
    }


    /**
     * Verification Time
     *
     * @Route("reports", name="biometric_report")
     * @Method("GET")
     * @Template("DashboardBundle:Biometric/report:index.html.twig")
     */
    public function reportsAction() {

        return $this->prepareResponse([ ]);
    }

    /**
     * Region reports
     *
     * @Route("reports/region", name="biometric_region_report")
     * @Method("GET")
     * @Template("DashboardBundle:Biometric/report:region.html.twig")
     */
    public function getRegionReport(Request $request) {

        if ($request->query->has('start') && $request->query->has('end')) {

            // $now = new Carbon();

            $start_date = $request->query->get('start');
            $end_date = $request->query->get('end');

            $em = $this->getDoctrine()->getManager();

            $where = $where2 = ' WHERE s.registry_type = 2 AND 1=1 ';
            /** @var  $user User */
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $roles = $user->getRoles(); //array of roles

            if (in_array('ROLE_SUPERAGENT', $roles)) {
                $where .= ' AND u.parent_id = ' . $user->getId();
                $where2 .= ' AND u.parent_id = ' . $user->getId();
            }
            if ($start_date != 1) {
                $where2 .= " AND s.createdDate >= '" . $start_date . " 00:00:00' AND s.createdDate <= '" . $end_date . " 23:59:59' ";
            }

            $where2 .= ' AND s.verifyState not in (10) ';

            $emConfig = $em->getConfiguration();
            $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
            $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
            $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
            $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');


            $territory_entiry = $em->getRepository('DashboardBundle:Territory')->findAll();

            $territory_array = array();
            $territory_array[ 'none' ] = "";
            foreach ($territory_entiry as $bkey => $bval) {

                $territory_array[ $bval->getId() ] = $bval->getName();
            }


            $region_entiry = $em->getRepository('DashboardBundle:Region')->findAll();

            $region_array = array();
            $region_array[ 'none' ] = "";
            foreach ($region_entiry as $bkey => $bval) {

                $region_array[ $bval->getId() ] = $bval->getName();
            }

            $query1 = "SELECT r0_.id AS sid, COUNT(r1_.id) AS sum_regs, u4_.territory_id AS territory_name, u4_.region_id AS region_name, DATE_FORMAT (r1_.createdDate,'%Y-%m-%d') AS created_on, r0_.verifyState AS verifyState FROM RegistrationStatus r0_ INNER JOIN registration r1_ ON r0_.registrationId = r1_.id INNER JOIN user u4_ ON r1_.owner_id = u4_.id WHERE 1 = 1 AND r0_.registry_type = 2 AND r0_.createdDate >= '" . $end_date . "' AND r0_.createdDate <= '" . $start_date . "' AND r0_.verifyState NOT IN (10) GROUP BY region_name, territory_name, verifyState ORDER BY sum_regs DESC";

            $connection = $em->getConnection();
            $statement = $connection->prepare($query1);
            $statement->execute();
            $result1 = $statement->fetchAll();

            $data = $active = array();
            foreach ($result1 as $key => $value) {

                $value[ 'territory_name' ] = @$territory_array[ $value[ 'territory_name' ] ];
                $value[ 'region_name' ] = @$region_array[ $value[ 'region_name' ] ];

                $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ] = (@$data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ] ? $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ] : $value);

                //first append total, good faor bad to array as zero
                $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_total' ] = @$data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_total' ] + $value[ 'sum_regs' ];
                $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_total_verified' ] = (@$data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_total_verified' ] ? $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_total_verified' ] : 0);
                $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_good' ] = (@$data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_good' ] ? $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_good' ] : 0);
                $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_fair' ] = (@$data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_fair' ] ? $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_fair' ] : 0);
                $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_bad' ] = (@$data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_bad' ] ? $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_bad' ] : 0);
                $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_unrated' ] = (@$data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_unrated' ] ? $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_unrated' ] : 0);


                if (in_array($value[ 'verifyState' ], array( 1, 2, 3, '-2', '-3' ))) {
                    $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_total_verified' ] = $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_total_verified' ] + $value[ 'sum_regs' ];
                }

                //now fill in the data
                if ($value[ 'verifyState' ] == 1) {
                    $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_good' ] = $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_good' ] + $value[ 'sum_regs' ];
                } elseif ($value[ 'verifyState' ] == 2 || $value[ 'verifyState' ] == -2) {
                    $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_fair' ] = $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_fair' ] + $value[ 'sum_regs' ];
                } elseif ($value[ 'verifyState' ] == 3 || $value[ 'verifyState' ] == -3) {
                    $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_bad' ] = $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_bad' ] + $value[ 'sum_regs' ];
                } else {
                    $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_unrated' ] = $data[ $value[ 'region_name' ] ][ $value[ 'territory_name' ] ][ 'sum_unrated' ] + $value[ 'sum_regs' ];
                }
            }
//
            echo json_encode($data);

            exit();

        } else {

            if (!in_array(56, json_decode($this->session->get('user_role_perms'), true))) {

                return $this->redirect($this->generateUrl('admin'));

            }

            return $this->prepareResponse([
                'title'       => 'Regions Report',
                'title_descr' => 'View Reports for Regions Biometric Verification',
            ]);

        }
    }

    /**
     * Agent reports
     *
     * @Route("reports/agent_score", name="biometric_agent_report")
     * @Method("GET")
     * @Template("DashboardBundle:Biometric/report:agent.html.twig")
     */
    public function getAgentScoreReport(Request $request) {

        return $this->prepareResponse([ ]);
    }


    /**
     *
     * @Route("get_day_summary", name="biometric_day_summary")
     * @Method("GET")
     * @param Request $request
     *
     * @return static
     */
    public function getDailyRegSummary(Request $request) {

        $now = new \DateTime();

        $start_date = $request->get('start', $now->modify('-10 days')->setTime(0,0,0)->format('Y-m-d H:i:s'));

        $end_date =  (new \DateTime())->setTime(23,59,59)->format('Y-m-d H:i:s');

        $em = $this->container->get("doctrine")->getManager();

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $result = $em->createQuery("SELECT e.image_count, p.temporaryRegStatus as treg, p.fullRegStatus as freg, p.verifyState as verify_state, DATE(e.createdDate) as dt_date, COUNT(p.id) as total "
            . " from RestApiBundle:RegistrationStatus p "
            . " JOIN p.registrationId e "
            . " WHERE p.registryType = 2 and e.createdDate >= '" . $start_date . "' and e.createdDate <= '" . $end_date . "' "
            . " GROUP BY dt_date, e.image_count, treg, freg, verify_state "
            . " ORDER BY dt_date desc")
            ->getArrayResult();


        $ranges = [ ];

        $start = new \DateTime($start_date);
        $end = new \DateTime();
        $interval = new \DateInterval('P1D'); // 1 day interval

        $period = new \DatePeriod($start, $interval, $end);

        $i = 0;

        foreach ($period as $key => $day) {
       
         $total = $bad = $pending = $verified = $activated = $declined = $unverified = 0;        

            foreach (array_filter($result, function ($item) use ($day) {

                return $item[ 'dt_date' ] == $day->format('Y-m-d');
		
            }) as $key => $item) {


                $total += $item[ 'total' ];

                // Get the pending counts ( for images only )
                $pending += $item[ 'image_count' ]  == 0 && $item['verify_state'] == 0 ? $item[ 'total' ] : 0;

                // Get those with 1 verify state
                $verified += $item[ 'verify_state' ] == 1 ? $item[ 'total' ] : 0;

                // Get those with unverified state
                $unverified += $item[ 'verify_state' ] == 0 && $item['image_count'] == 1 ?  $item[ 'total' ] : 0;

                // Get those with temporary reg status
                $activated +=  in_array($item[ 'treg' ], [7]) && in_array($item['freg'], [7]) ?  $item[ 'total' ] : 0;

		$bad += in_array($item[ 'verify_state' ],[4,3]) ?  $item[ 'total' ] : 0;

                // Get those with verify state
                $declined += $item['verify_state'] == 1 && !in_array($item['treg'], [2]) ?  $item[ 'total' ] : 0;

            }

            $ranges[$i++] = [
                'date'       => $day->format('Y-m-d'),
                'total'      => $total,
                'pending'    => $pending,
                'verified'   => $verified,
	        'bad'        => $bad, 
                'declined'   => $declined,
                'activated'  => $activated,
                'unverified' => $unverified

            ];

        }
	
	// Sort the arrays	
        usort($ranges,function($a,$b){
            return ($b['date'] < $a['date']) ? -1 : 1;;
        });
	 
      
        return JsonResponse::create($ranges);

    }


}
