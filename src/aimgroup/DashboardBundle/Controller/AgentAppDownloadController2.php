<?php

namespace aimgroup\DashboardBundle\Controller;

use aimgroup\DashboardBundle\Entity\AppUpload;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AgentAppDownloadController
 * @package aimgroup\DashboardBundle\Controller
 * @Route("/appdowload2")
 *
 */
class AgentAppDownloadController2 extends AbstractController
{

    public function indexAction($name)
    {
        return $this->render('', array('name' => $name));
    }

    /**
 * @Route("/", name="app_download_1")
 * @Method("GET")
 * @Template("DashboardBundle:Admin:app_download.html.twig")
 */
    public function app_downloadAction(Request $request)
    {
        $em = $this->container->get('doctrine')->getManager();
        try {
            $query = $em->createQueryBuilder()->select("p")->from("RestApiBundle:Role", "p")->getQuery();
            $results = $query->getArrayResult();

            /** @var $result AppUpload */
            $result = $em->getRepository("DashboardBundle:AppUpload")->createQueryBuilder("a")
                ->select("a.fileLocation")
                ->orderBy("a.createdDate", "DESC")
                ->getQuery()
                ->setMaxResults(1)
                ->getSingleResult();
            $url = "";
            if ($result) {
                $this->container->get("api.helper")->logInfo("AgentAppDownloadController", "app_downloadAction", array("result" => $result));
                $url = str_replace($this->container->getParameter("resource_path"), $this->container->getParameter("hostname") . "/euploads", $result["fileLocation"]);
            }

            //$this->logUserEvent(\aimgroup\DashboardBundle\Entity\UserLog::DOWNLOAD_APP, "User download App");
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }

        return array(
            'title' => "View User Roles Listing:",
            'title_descr' => "Listing for user permission roles departments ",
            'user_roles' => $results,
            'entities' => $url,
        );
    }

    /**
     * View resetrecordlock
     *
     * @Route("/resetrecordlock/{type}/{timestamp}", name="resetrecordlock")
     * @Method("GET")
     * @Template()
     */
    public function rectifyImageCountAction($type, $timestamp)
    {
        $em = $this->getDoctrine()->getManager();

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $data2 = $em->createQuery("SELECT r.id, r." . $type . "State "
            . " from RestApiBundle:RegistrationStatus r "
            . " WHERE DATE(r." . $type . "Date) <= '" . date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s')) - 600) . "' AND r." . $type . "State = '-1'")
            ->getArrayResult(); //getSingleResult();


        foreach ($data2 as $key => $value) {
            echo "<pre>";
            print_r($value);
            echo "</pre>";

            if ($type == "audit") {
                $results = $em->createQuery("UPDATE RestApiBundle:RegistrationStatus u SET u.auditState = 0, u.auditBy = NULL, u.auditDescr = '', u.auditDate = '0000-00-00 00:00:00' WHERE u.id = " . $value['id'] . " ")
                    ->getArrayResult();
            }
            if ($type == "verify") {
                $results = $em->createQuery("UPDATE RestApiBundle:RegistrationStatus u SET u.verifyState = 0, u.verifyBy = NULL, u.verifyDescr = '', u.verifyDate = '0000-00-00 00:00:00' WHERE u.id = " . $value['id'] . " ")
                    ->getArrayResult();
            }
        }
        exit;

        $data = array(
            'title' => "View Reports for:",
            'title_descr' => "View Reports for",
            //'entities' => $entities,
        );

        return $this->prepareResponse($data);
    }

}
