<?php

namespace aimgroup\DashboardBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class HelpController
 * @package aimgroup\DashboardBundle\Controller
 * @Route("/help")
 *
 */
class HelpController extends Controller {

    /**
     * @Route("/index")
     * @Template()
     */
    public function indexAction() {

        $data = array(
            'title' => 'Sample help content title',
            'content' => 'The generate:controller command generates a new controller including actions, tests, templates and routing. By default, the command is run in the interactive mode and asks questions to determine the bundle name, location, configuration format and default structure:'
        );
        
        $return[] = $data;
        $return[] = $data;
        $return[] = $data;

        //echo json_encode($return); exit
        return array(
                // ...
        );
    }

    /**
     * @Route("/english")
     * @Template()
     */
    public function englishAction() {

        $data = array(
            'title' => 'Sample help content title',
            'content' => 'The generate:controller command generates a new controller including actions, tests, templates and routing. By default, the command is run in the interactive mode and asks questions to determine the bundle name, location, configuration format and default structure:'
        );
        
        $return[] = $data;
        $return[] = $data;
        $return[] = $data;

        //echo json_encode($return); exit
        return array(
                // ...
        );
    }
    
    /**
     * @Route("/swahili")
     * @Template()
     */
    public function swahiliAction() {

        $data = array(
            'title' => 'Sample help content title',
            'content' => 'The generate:controller command generates a new controller including actions, tests, templates and routing. By default, the command is run in the interactive mode and asks questions to determine the bundle name, location, configuration format and default structure:'
        );
        
        $return[] = $data;
        $return[] = $data;
        $return[] = $data;

        //echo json_encode($return); exit
        return array(
                // ...
        );
    }

}
