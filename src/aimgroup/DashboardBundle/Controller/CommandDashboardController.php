<?php

namespace aimgroup\DashboardBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * @author Christopher Kikoti
 * 
 * CommandDashboardController
 * @package aimgroup\DashboardBundle\Controller
 * @Route("admin/command")
 */
class CommandDashboardController extends AbstractController {

    /**
     *
     * @Route("/", name="admin/command")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        return $this->render('', array('name' => 'chris'));
    }

}
