<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\DashboardBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use aimgroup\RestApiBundle\Dao\LogsDao;

/**
 * DiagnosisController controller.
 *
 * @Route("/log")
 */
class LogController extends AbstractController {

    
    
    /**
     * Log action.
     *
     */
    public function logAction($user, $action, $description, $metadata) {
        
        $logDao = new LogsDao();
        
        $data = $logDao->addUserLogs($user, $action, $description, $metadata);

        
        return $this->prepareResponse($data);
    }
    
    public function userActivitiesAction(Request $request) {
        
        $attributes = $request->request->all();
        
        $logDao = new LogsDao();
        
        $data = $logDao->getUserLogs($attributes['msisdn']);
 
        return $this->prepareResponse($data);
    }
}
