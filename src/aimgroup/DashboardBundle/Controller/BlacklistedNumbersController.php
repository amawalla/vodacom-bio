<?php

namespace aimgroup\DashboardBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use aimgroup\DashboardBundle\Entity\BlacklistedNumbers;
use aimgroup\DashboardBundle\Form\BlacklistedNumbersType;

/**
 * BlacklistedNumbers controller.
 *
 * @Route("/admin/blacklistednumbers")
 */
class BlacklistedNumbersController extends AbstractController
{

    /**
     * Lists all BlacklistedNumbers entities.
     *
     * @Route("/", name="admin_blacklistednumbers")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('DashboardBundle:BlacklistedNumbers')->findAll();

        $data = array(
            'title' => "Black Listed Numbers",
            'title_descr' => "View | Add List of Black Listed Numbers",
            'entities' => $entities,
        );
        
        return $this->prepareResponse($data);
    }
    /**
     * Creates a new BlacklistedNumbers entity.
     *
     * @Route("/", name="admin_blacklistednumbers_create")
     * @Method("POST")
     * @Template("DashboardBundle:BlacklistedNumbers:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new BlacklistedNumbers();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_blacklistednumbers_show', array('id' => $entity->getId())));
        }

        $data = array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
        
        return $this->prepareResponse($data);
    }

    /**
     * Creates a form to create a BlacklistedNumbers entity.
     *
     * @param BlacklistedNumbers $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(BlacklistedNumbers $entity)
    {
        $form = $this->createForm(new BlacklistedNumbersType(), $entity, array(
            'action' => $this->generateUrl('admin_blacklistednumbers_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new BlacklistedNumbers entity.
     *
     * @Route("/new", name="admin_blacklistednumbers_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new BlacklistedNumbers();
        $form   = $this->createCreateForm($entity);

        $data = array(
            'title' => "Black Listed Numbers",
            'title_descr' => "View | Add List of Black Listed Numbers",
            'entity' => $entity,
            'form'   => $form->createView(),
        );
        
        return $this->prepareResponse($data);
    }

    /**
     * Finds and displays a BlacklistedNumbers entity.
     *
     * @Route("/{id}", name="admin_blacklistednumbers_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DashboardBundle:BlacklistedNumbers')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find BlacklistedNumbers entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        $data = array(
            'title' => "Black Listed Numbers",
            'title_descr' => "View | Add List of Black Listed Numbers",
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
        
        return $this->prepareResponse($data);
    }

    /**
     * Displays a form to edit an existing BlacklistedNumbers entity.
     *
     * @Route("/{id}/edit", name="admin_blacklistednumbers_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DashboardBundle:BlacklistedNumbers')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find BlacklistedNumbers entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        $data = array(
            'title' => "Black Listed Numbers",
            'title_descr' => "View | Add List of Black Listed Numbers",
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
        
        return $this->prepareResponse($data);
    }

    /**
    * Creates a form to edit a BlacklistedNumbers entity.
    *
    * @param BlacklistedNumbers $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(BlacklistedNumbers $entity)
    {
        $form = $this->createForm(new BlacklistedNumbersType(), $entity, array(
            'action' => $this->generateUrl('admin_blacklistednumbers_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing BlacklistedNumbers entity.
     *
     * @Route("/{id}", name="admin_blacklistednumbers_update")
     * @Method("PUT")
     * @Template("DashboardBundle:BlacklistedNumbers:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DashboardBundle:BlacklistedNumbers')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find BlacklistedNumbers entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('admin_blacklistednumbers_edit', array('id' => $id)));
        }

        $data = array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
        
        return $this->prepareResponse($data);
    }
    /**
     * Deletes a BlacklistedNumbers entity.
     *
     * @Route("/{id}", name="admin_blacklistednumbers_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('DashboardBundle:BlacklistedNumbers')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find BlacklistedNumbers entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_blacklistednumbers'));
    }

    /**
     * Creates a form to delete a BlacklistedNumbers entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_blacklistednumbers_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
