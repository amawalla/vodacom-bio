<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */


namespace aimgroup\DashboardBundle\Controller;

use aimgroup\DashboardBundle\Dao\JTableResponse;
use aimgroup\DashboardBundle\Entity\UserLog;
use aimgroup\RestApiBundle\Entity\User;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use aimgroup\DashboardBundle\Entity\Faq;
use aimgroup\DashboardBundle\Form\FaqType;
use aimgroup\DashboardBundle\Util\LogsUtil;
use Symfony\Component\HttpFoundation\Response;

/**
 * DiagnosisController controller.
 *
 * @Route("/diagnosis")
 */
class DiagnosisController extends AbstractController
{

    /**
     * Lists all Faq entities.
     *
     * @Route("/", name="diagnosis")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {


        $data = array(
            'title' => "Diagnosis",
            'title_descr' => "Check the system",
            //'entities' => $entities,
        );

        return $this->prepareResponse($data);
    }

    /**
     * Track user activities.
     *
     * @Route("/user_activities", name="user_activities")
     * @Method({"GET", "POST"})
     * @Template("DashboardBundle:Diagnosis:user_activities.html.twig")
     */
    public function userActivitiesAction(Request $request)
    {
        $resp = new JTableResponse();
        $data = array(
            'title' => 'User Activities'
        );

        return $this->prepareResponse($data);

    }


    /**
     * @param Request $request
     * @Route("/list",name="user_log_list")
     * @Method({"POST","GET"})
     * @return JsonResponse
     */
    public function listDiagnosisAction(Request $request)
    {
        $resp = new JTableResponse();
        $data = array(
            'title' => 'User Activities'
        );

        $attributes = $request->request->all();
        $queryAttrib = $request->query->all();
        $resultJson = array();
        try {
            $results = $this->container->get("fos_elastica.manager")->getRepository("DashboardBundle:UserLog")->searchUserLogs($attributes, $queryAttrib);
            /** @var  $result UserLog */
            foreach ($results as $result) {
                /** @var  $user User */
                $user = $result->getUser();
                $obj = array();
                $obj["id"] = $result->getId();
                $obj["action"] = $result->getAction();
                $obj["userId"] = $user->getUsername();
                $obj["time"] = $result->getTimestamp()->format("Y-m-d H:i:s");
                $obj["desc"] = $result->getDescription();
                $obj["metadata"] = http_build_query($result->getMetadata(), ' ', '<br>');
                array_push($resultJson, $obj);
            }
            $resp->setRecords($resultJson);
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }
        return $this->buildResponse($resp, Response::HTTP_OK);

    }

    /**
     * Track through a registration .
     *
     * @Route("/track_registration", name="track_registration")
     * @Method({"GET", "POST"})
     * @Template("DashboardBundle:Diagnosis:track_registration.html.twig")
     */
    public function trackRegistrationAction(Request $request)
    {
        $data = array(
            'title' => 'Track Registration'
        );

        $attributes = $request->request->all();

        if (isset($attributes['customer_number'])) {
            $em = $this->getDoctrine()->getManager();
            $registrationDetails = $em->createQuery("SELECT r.id, r.msisdn, r.image_count, r.firstName, r.middleName, r.lastName,"
                . " r.identification, r.identificationType, r.region, r.dob, r.nationality, r.imsi, r.image_count, r.territory,"
                . " r.gender, r.nationality, DATE_FORMAT(r.createdDate, '%Y-%m-%d %H:%i:%s') createdDate, r.deviceModel, DATE_FORMAT(r.registrationTime, '%Y-%m-%d %H:%i:%s') registrationTime, DATE_FORMAT(r.deviceSentTime, '%Y-%m-%d %H:%i:%s') deviceSentTime,"
                . " r.locationLatLon, r.appVersion, r.agentMsisdn, r.agentImei, r.osVersion, r.address,"
                . " DATE_FORMAT(s.allimageDate, '%Y-%m-%d %H:%i:%s') allimageDate, DATE_FORMAT(s.tregDate, '%Y-%m-%d %H:%i:%s') tregDate,"
                . " DATE_FORMAT(s.fregDate, '%Y-%m-%d %H:%i:%s') fregDate, DATE_FORMAT(s.auditDate, '%Y-%m-%d %H:%i:%s') auditDate,"
                . " DATE_FORMAT(s.verifyDate, '%Y-%m-%d %H:%i:%s') verifyDate"
                . " from RestApiBundle:RegistrationStatus s LEFT JOIN s.registrationId r WHERE r.msisdn = :mobileNumber")
                ->setParameter("mobileNumber", $attributes['customer_number'])
                ->getArrayResult();

            if (isset($registrationDetails)) {
                foreach ($registrationDetails as $key => $val) {
                    //$registrationDetails[$val['id']] = $val['name'];
                }

                $data['data'] = $registrationDetails;
            }
        }

        return $this->prepareResponse($data);
    }

    /**
     * Check the logs.
     *
     * @Route("/logs", name="logs")
     * @Method("GET")
     * @Template("DashboardBundle:Diagnosis:logs.html.twig")
     */
    public function logsAction(Request $request)
    {
        //$logsUtil = new LogsUtil("/var/log/vodagateway/"); //put the proper path
        //$logsUtil = new LogsUtil("/var/log/ekyc");
        $logsUtil = new LogsUtil($this->container->get('kernel')->getLogDir());

        $data = $logsUtil->loadLogFiles();


        return $this->prepareResponse($data);
    }
}
