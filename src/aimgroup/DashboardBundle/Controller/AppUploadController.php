<?php

namespace aimgroup\DashboardBundle\Controller;

use aimgroup\DashboardBundle\Dao\JsonObject;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use aimgroup\DashboardBundle\Entity\AppUpload;
use aimgroup\DashboardBundle\Form\AppUploadType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use aimgroup\DashboardBundle\Models\Document;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * AppUpload controller.
 *
 * @Route("/appupload")
 */
class AppUploadController extends AbstractController
{

    var $session;

    public function __construct()
    {
        $this->session = new Session();
    }

    /**
     * Lists all AppUpload entities.
     *
     * @Route("/", name="appupload")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        if (!in_array(33, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('DashboardBundle:AppUpload')->findAll();

        $data = array(
            'entities' => $entities,
            'title' => 'App Uploads',
            'title_descr' => 'App Version',
            'mqttMessageTopic' => $this->container->getParameter("mqtt_apkupdate_msg"),
            "operator" => strtolower($this->container->getParameter("operator_name"))
        );

        return $this->prepareResponse($data);
    }

    /**
     * Creates a new AppUpload entity.
     *
     * @Route("/", name="appupload_create")
     * @Method("POST")
     * @Template("DashboardBundle:AppUpload:new.html.twig")
     *
     */
    public function createAction(Request $request)
    {

        $uploadedURL = "";
        $status = 'success,';
        $message = '';
        try {
            /** @var  $file UploadedFile */
            $file = $request->files->get('fileLocation');
            $fileName = $file->getClientOriginalName();

            $ext = pathinfo($fileName)["extension"];

            if (strcmp($ext, "apk") == 0) {
                $uploadedURL = $this->container->getParameter("resource_path") . DIRECTORY_SEPARATOR . "apk" . DIRECTORY_SEPARATOR . date("Ymd");
                //Move the file to the directory where brochures are stored
                $file->move($uploadedURL, $fileName);
                $attributes = $request->request->all();


                $entity = new AppUpload();
                $entity->setDescription($attributes["description"]);
                $entity->setVersionName($attributes["versionName"]);
                $entity->setFileLocation($uploadedURL . DIRECTORY_SEPARATOR . $fileName);
                $entity->setShouldBlock($attributes["shouldBlock"] == 1 ? true : false);
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
            } else {
                $status = 'failed';
                $message = 'Invalid File Type';
            }

        } catch (\Exception $e) {
            $status = 'failed';
            $message = $e->getMessage();
	    exit($message);
        }


        $data = array('title' => 'title hapa', 'sub_title' => 'sub title hapa', 'status' => $status, 'message' => $message, 'uploadedURL' => $uploadedURL);
        return $this->prepareResponse($data);
    }

    /**
     * Creates a form to create a AppUpload entity.
     *
     * @param AppUpload $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(AppUpload $entity)
    {
        $form = $this->createForm(new AppUploadType(), $entity, array(
            'action' => $this->generateUrl('appupload_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new AppUpload entity.
     *
     * @Route("/new", name="appupload_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new AppUpload();
        $form = $this->createCreateForm($entity);

        $data = array(
            'entity' => $entity,
            'form' => $form->createView(),
            'title' => 'New App Upload',
            'title_descr' => 'New App Upload',
            'uploadedURL' => "",
            "message" => "",
            "status" => ""
        );

        return $this->prepareResponse($data);
    }

    /**
     * Finds and displays a AppUpload entity.
     *
     * @Route("/{id}", name="appupload_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DashboardBundle:AppUpload')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AppUpload entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        $data = array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );

        return $this->prepareResponse($data);
    }

    /**
     * Displays a form to edit an existing AppUpload entity.
     *
     * @Route("/{id}/edit", name="appupload_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DashboardBundle:AppUpload')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AppUpload entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        $data = array(
            'title' => "Edit App Upload",
            'title_descr' => "Edit App Upload",
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );

        return $this->prepareResponse($data);
    }

    /**
     * Creates a form to edit a AppUpload entity.
     *
     * @param AppUpload $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(AppUpload $entity)
    {
        $form = $this->createForm(new AppUploadType(), $entity, array(
            'action' => $this->generateUrl('appupload_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing AppUpload entity.
     *
     * @Route("/{id}", name="appupload_update")
     * @Method("PUT")
     * @Template("DashboardBundle:AppUpload:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DashboardBundle:AppUpload')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AppUpload entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('appupload_edit', array('id' => $id)));
        }

        $data = array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );

        return $this->prepareResponse($data);
    }

    /**
     * Deletes a AppUpload entity.
     *
     * @Route("/{id}", name="appupload_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('DashboardBundle:AppUpload')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find AppUpload entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('appupload'));
    }

    /**
     * Creates a form to delete a AppUpload entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('appupload_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();
    }


    /**
     * @param Request $request
     * @Method("POST")
     * @Route("/publish/update",name="publish_update")
     */
    public function publishConfiguration(Request $request)
    {
        $resp = new JsonObject();
        $status = false;
        try {

            $attributes = $request->request->all();
            if ($attributes) {
                $topic = $attributes['type'];
                $deviceId = $attributes["topic"];
                $qos = 2;
                $em = $this->get('doctrine')->getManager();
                $applicationUpdate = $em->getRepository("DashboardBundle:AppUpload")->findOneBy(array("id" => $attributes["updateId"]));

                if ($applicationUpdate) {
                    $obj = array();

		    $obj["version"] = $applicationUpdate->getVersionName();
                    $explode = pathinfo($applicationUpdate->getFileLocation());
                    $httpPath = str_replace($this->container->getParameter("resource_path"), $this->container->getParameter("hostname") . DIRECTORY_SEPARATOR . "euploads", $explode["dirname"]);
                    $obj["filePath"] = $httpPath . "/" . $explode["basename"];
                    $obj["shouldBlock"] = $applicationUpdate->isShouldBlock();

                    $this->get('api.helper')->logInfo("AppUpload", "publishConfiguration", $obj);
                    $respond = $this->get("api.helper")->publish($deviceId, $topic, json_encode($obj));
                    if ($respond) {
                        $resp->setMessage("SUCCESS");
                        $status = true;
                    } else {
                        $resp->setMessage("PUBLISHED HAS FAILED");
                    }

                    $resp->setItem($respond);
                    $resp->setMessage("SUCCESFULLY PUBLISHED");
                    $status = true;

                }

            }
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
        }
        $resp->setStatus($status);
        return $this->buildResponse($resp, Response::HTTP_OK);

    }

}
