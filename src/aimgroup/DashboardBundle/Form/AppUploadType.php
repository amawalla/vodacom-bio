<?php

namespace aimgroup\DashboardBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\Exception\AccessException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use aimgroup\DashboardBundle\Entity\AppUpload;

class AppUploadType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('createdDate')
            ->add('versionName')
            ->add('description')
            ->add('fileLocation')
            ->add('shouldBlock', 'checkbox', array('label' => 'Should be manadatory', 'required' => false));
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @throws AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => AppUpload::class
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aimgroup_dashboardbundle_appupload';
    }
}
