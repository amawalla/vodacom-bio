<?php

/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 17/05/2016
 * Time: 12:50 PM
 */

namespace aimgroup\DashboardBundle\Util;

use aimgroup\DashboardBundle\Entity\LogMessage;
use aimgroup\RestApiBundle\Entity\Device;
use aimgroup\RestApiBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UsersHelper
{

    const EXISTS = 1;
    const SUCCESS = 2;
    const FAIL = 3;

    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function createUser($attributes, $iscommand = false)
    {
        try {

            /** @var EntityManager $em */
            $em = $this->container->get('doctrine')->getManager();

            $isExists = $em->getRepository('RestApiBundle:User')->isMobileExists(substr($attributes['msisdn'], -9));
            if ($isExists) {
                return UsersHelper::EXISTS;
            } else {
                $user = new User();
                $user->setFirstName($attributes['firstName']);
                $user->setMiddleName($attributes['middleName']);
                $user->setLastName($attributes['lastName']);
                $user->setMobileNumber(substr($attributes['msisdn'], -9));
                $user->setIdNumber($attributes['identification_number']);

                if(isset($attributes['identification_type']) && is_numeric($attributes['identification_type'])) {
                    $user->setIdType($em->getRepository('DashboardBundle:Idtype')->find($attributes['identification_type']));
                }

                
                
                $user->setUserType($attributes['userType']);
                $user->setUsername($user->getMobileNumber());
                $user->setEnabled(true);
                $user->setSalt(rand(1000, 9999));
                $user->setNumberDevices($attributes['number_devices']);
                $user->setAgentExclusivity($attributes['agent_exclusivity']);
                $user->setDob(new \DateTime($attributes['dob']));
                $user->setStatus(true);

                $gender = $attributes["gender"];
                if(strlen($gender) > 1) {
                    $gender = strtolower($gender) === 'female' ? 'F' : 'M';
                }
                
                $user->setGender($gender);
                $user->setRoles(array('ROLE_AGENT'));

                /** optional fields */
                if (isset($attributes['agentCode']))
                    $user->setAgentCode($attributes['agentCode']);
                if (isset($attributes['email'])) {
                    $user->setEmail($attributes['email']);
                    $user->setEmailCanonical($user->getEmail());
                }
                /** end optional fields */
                $region = $em->getRepository('DashboardBundle:Region')->findOneBy(array('id' => $attributes['region']));
                if ($region) {
                    $user->setRegion($region);
                }
                
                


                $territory = $em->getRepository('DashboardBundle:Territory')->findOneBy(array('id' => $attributes['territory']));
                if ($territory) {
                    $user->setTerritory($territory);
                }


                $encoder = $this->container->get('security.password_encoder');
                $encoded = $encoder->encodePassword($user, $user->getPassword());
                $user->setPassword($encoded);

                //generate secret and clientid
                $clientManager = $this->container->get('fos_oauth_server.client_manager.default');
                $client = $clientManager->createClient();
                $client->setRedirectUris(array($this->container->getParameter('hostname')));
                $client->setAllowedGrantTypes(array(
                    'authorization_code',
                    'password',
                    'refresh_token',
                    'token',
                    'client_credentials'
                ));
                $clientManager->updateClient($client);


                
                
                $confirmationCode = rand(10000000, 99999999); //md5($secureRandom->nextBytes(4));
                $user->setConfirmationToken($confirmationCode);


                if ($iscommand) {
                    if (isset($attributes['superagent'])) {
                        $parent = $em->getRepository('RestApiBundle:User')->findOneBy(array('username' => $attributes['superagent']));
                    } else {
                        $parent = $em->getRepository('RestApiBundle:User')->findOneBy(array('mobileNumber' => $attributes['superagent_mobile']));
                    }
                } else {
                    if (isset($attributes['superagent'])) {
                        $parent = $em->getRepository('RestApiBundle:User')->find($attributes['superagent']);
                    } else {
                        $thisuser = $this->container->get('security.token_storage')->getToken()->getUser();
                        $uid = $thisuser->getId();
                        $parent = $em->getRepository('RestApiBundle:User')->find($uid);
                    }
                }

                
                
                $user->setParentId($parent);
                $em->persist($user);
                
                /*

                $device = new Device();
                $device->setUser($user);
                $device->setMsisdn($user->getMobileNumber());
                $device->setPrivateKey($client->getSecret());
                $device->setPublicKey($client->getPublicId());

                $device->setAccessToken($confirmationCode);
                $em->persist($device);

                $em->flush();*/
                
                
                $query1 = "INSERT INTO `device` (user_id, accessToken, msisdn, imei, public_key, private_key, isActive, status, createdOn, activeOn, isTiedToDevice, deviceId, appVersion, isBlocked, userPermission)
VALUES(user->getId(), '".RAND(11111111,11114444)."', '".$user->getMobileNumber()."', '".RAND(11111111,11114444)."', '".$client->getPublicId()."', '".$client->getSecret()."', 1, 0, '".date('Y-m-d H:i:s')."', NULL, 0, '1485341598854', '0', 0, 0);";
        $em->getConnection()->exec($query1);

                
                //send token
                $this->container->get('api.helper')->sendSms($user->getUsername(), $user->getConfirmationToken());
                return UsersHelper::SUCCESS;
            }
        } catch (\Exception $e) {
            return UsersHelper::FAIL;
        }
    }

    public function logUserAction()
    {
        try {
            $em = $this->container->get('doctrine')->getManager();
            $log = new LogMessage();
        } catch (\Exception $e) {

        }
    }

    public function deleteUsers($attributes, $true)
    {
        try {
            $shouldDelete = true;
            $em = $this->container->get('doctrine')->getManager();
            /** @var  $user User */
            $user = $em->getRepository('RestApiBundle:User')->findByMsisdn(substr($attributes['msisdn'], -9));
            if ($user) {
                $devices = $em->getRepository('RestApiBundle:Device')->findBy(array('user' => $user));
                if ($devices) {
                    foreach ($devices as $device) {
                        if ($device->getIsActive() == 1) {
                            $shouldDelete = false;
                            break;
                        }
                        $em->remove($device);
                    }
                }
                if ($shouldDelete) {
                    $em->remove($user);
                    $em->flush();
                    return UsersHelper::SUCCESS;
                }
                return UsersHelper::FAIL;
            }
        } catch (\Exception $e) {
            return UsersHelper::FAIL;
        }
    }

}
