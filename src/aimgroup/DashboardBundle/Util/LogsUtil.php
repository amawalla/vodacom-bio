<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace aimgroup\DashboardBundle\Util;

/**
 * Description of LogsUtil
 *
 * @author Michael Tarimo
 */
class LogsUtil {
    
    const DISPLAY_REVERSE = true;
    
    private $logPath;
    
    public function __construct($logPath) {
        $this->logPath = $logPath;
    }
    
    public function loadLogFiles() {
        /* Files that you want to have access to, inside the LOG_PATH directory */
        $files = $this->getLogFiles($this->logPath);
        // Set a Smart default to 1:
        ksort($files);
        
        foreach ($files as $dir_name => $file_array) {
            ksort($file_array);
            foreach ($file_array as $key => $val) {
                $default = $key;
                $log_files[$key] = $val;
            }
        }
        /*
        // separate files from dirs:
        foreach ($files as $key => $val) {
                foreach ($val as $log_name => $log_array) {
                        $log_files[$log_name] = $log_array;
                }
        }
        */
        $log =(!isset($_GET['p'])) ? $default : urldecode($_GET['p']);
        $lines =(!isset($_GET['lines'])) ? '50': $_GET['lines'];
        //$file = $log_files[$log]['path'];
        $file = $log;
        //var_dump($log);
        $title = substr($log, (strrpos($log, '/')+1));
        
        $output = $this->tail($file, $lines);
        $output = explode("\n", $output);
        
        if(false){
            // Latest first
            $output = array_reverse($output);
        }
        $output = implode('<li>',$output);
        
        $data = array(
            'title' => 'System Logs',
            'file_title' => $title,
            'lines' => $lines,
            'file' => $file,
            'files' => $file_array,
            'output' => $output
        );
        
        return $data;
    }

    private function getLogFiles($dir, &$results = array()) {
	$files = scandir($dir);
	foreach($files as $key => $value){
            $path = realpath($dir.DIRECTORY_SEPARATOR . $value);
            //var_dump($path);
            if(!is_dir($path)) {
                $files_list[] = $path;
            }
            elseif ($value != "." && $value != "..") {
                $dirs_list[] = $path;
            }
	}
        //var_dump($dirs_list);
	foreach ($files_list as $path) {
            preg_match("/^.*\/(\S+)$/", $path, $matches);
            
            $name = $matches[1];
            $results[$dir][$name] = array('name' => $name, 'path' => $path);
	}
        if(isset($dirs_list)) {
            foreach ($dirs_list as $path) {
                $this->getLogFiles($path, $results);
            }
        }
        
	return $results;
    }
    
    private function tail($filename, $lines = 50, $buffer = 4096) {
        var_dump($filename);
        // Open the file
        $f = fopen($this->logPath.DIRECTORY_SEPARATOR.$filename, "rb");
        // Jump to last character
        fseek($f, -1, SEEK_END);
        // Read it and adjust line number if necessary
        // (Otherwise the result would be wrong if file doesn't end with a blank line)
        if(fread($f, 1) != "\n") $lines -= 1;
        // Start reading
        $output = '';
        $chunk = '';
        // While we would like more
        while(ftell($f) > 0 && $lines >= 0) {
            // Figure out how far back we should jump
            $seek = min(ftell($f), $buffer);
            // Do the jump (backwards, relative to where we are)
            fseek($f, -$seek, SEEK_CUR);
            // Read a chunk and prepend it to our output
            $output = ($chunk = fread($f, $seek)).$output;
            // Jump back to where we started reading
            fseek($f, -mb_strlen($chunk, '8bit'), SEEK_CUR);
            // Decrease our line counter
            $lines -= substr_count($chunk, "\n");
        }
        // While we have too many lines
        // (Because of buffer size we might have read too many)
        while($lines++ < 0) {
                // Find first newline and remove all text before that
                $output = substr($output, strpos($output, "\n") + 1);
        }
        // Close file and return
        fclose($f);
        return $output;
    }
}
