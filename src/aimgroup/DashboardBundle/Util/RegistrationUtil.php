<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\DashboardBundle\Util;

/**
 * Description of RegistrationUtil
 *
 * @author Michael Tarimo
 */
class RegistrationUtil {
    
    
    public static function get_regionName_fromKey($region_key) {
        $regionsArray = array(
            "MW" => "mwanza",
            "KG" => "Kigoma",
            "KR" => "kagera",
            "PW" => "pwani",
            "MO" => "morogoro",
            "NJ" => "njombe",
            "ZS" => "zanzibar",
            "ZW" => "zanzibar",
            "ZN" => "zanzibar",
            "KM" => "kigoma",
            "MT" => "mtwara",
            "RV" => "ruvuma",
            "PN" => "pemba",
            "PS" => "pemba",
            "SD" => "singida",
            "SH" => "shinyanga",
            "AS" => "arusha",
            "MY" => "manyara",
            "MA" => "mara",
            "SI" => "simiyu",
            "MB" => "mbeya",
            "RK" => "rukwa",
            "DS" => "dar-es-salaam",
            "DO" => "dodoma",
            "TB" => "tabora",
            "LI" => "lindi",
            "GE" => "Geita",
            "KL" => "kilimanjaro",
            "TN" => "tanga",
            "KA" => "kahama",
            "IR" => "iringa",
            "CS" => "iringa",
            "HL" => "High Lands"
        );

        $regionsname = "";

        if (isset($regionsArray[$region_key])) {
            $regionsname = $regionsArray[$region_key];
        }

        return $regionsname;
    }

    public static function get_regionKey_fromName($region_name = NULL) {
        $regionsArray = array(
            "mwanza" => "MW",
            "Kigoma" => "KG",
            "kagera" => "KR",
            "pwani" => "PW",
            "morogoro" => "MO",
            "njombe" => "NJ",
            "zanzibar" => "ZS",
            "zanzibar" => "ZW",
            "zanzibar" => "ZN",
            "kigoma" => "KM",
            "mtwara" => "MT",
            "ruvuma" => "RV",
            "pemba" => "PN",
            "pemba" => "PS",
            "singida" => "SD",
            "shinyanga" => "SH",
            "arusha" => "AS",
            "manyara" => "MY",
            "mara" => "MA",
            "simiyu" => "SI",
            "mbeya" => "MB",
            "rukwa" => "RK",
            "dar-es-salaam" => "DS",
            "dodoma" => "DO",
            "tabora" => "TB",
            "lindi" => "LI",
            "Geita" => "GE",
            "kilimanjaro" => "KL",
            "tanga" => "TN",
            "kahama" => "KA",
            "iringa" => "IR"
        );

        return $regionsArray[$region_name];
    }

    public static function getGender($gender = null) {
        $gender_array = array(
            'm' => 'Male',
            'f' => 'female',
        );

        return $gender_array[strtolower($gender)];
    }

    public static function getRegStatusDesc($status) {
        $regstatus = array(
            '0' => 'Pending',
            '1' => 'Sent,Waiting Response',
            '2' => 'success',
            '3' => 'declined',
            '4' => 'fail',
            '5' => 'retry',
            '6' => 'success_cap',
            '7' => 'success_dbms'
        );

        return $regstatus[strtolower($status)];
    }
    
    public static function getIcapRegStatusDesc($tstatus, $fstatus) {
        $ret = "Not Sent";
        if(in_array($tstatus, array('1','2','6')) && in_array($fstatus, array('1','0'))) {
            $ret = "TREG";
        } else if(in_array($fstatus, array('2','6','7'))) {
            $ret = "FREG";
        } else if(in_array($tstatus, array('3','4')) || in_array($fstatus, array('3','4'))) {
            $ret = "Declined";
        }
   
        return $ret; // .$tstatus . $fstatus;
    }
    
    public static function getAimRegStatusDesc($tstatus, $fstatus, $imageCount) {
        $ret = "Pending";
        if(in_array($tstatus, array('1', '2','6')) || in_array($fstatus, array('1','2','6','7'))) {
            $ret = "Submitted";
        } else if(in_array($tstatus, array('3','4')) || in_array($fstatus, array('3','4'))) {
            $ret = "Declined";
        } else if($imageCount >= 3) {
            $ret = "Received";
        }
   
        return $ret; // .$tstatus . $fstatus;
    }
    
    
    
    private function getDateTime($date, $time) {
        return $date . "<br/>" . $time;
    }

    public static function getIDType($type = null) {
        $em = $this->getDoctrine()->getManager();
        $result = $em->createQuery("SELECT p from DashboardBundle:Idtype p ")
                ->getArrayResult();

        foreach ($result as $key => $val) {
            $idArray[$val['id']] = $val['name'];
        }

        return $idArray[$type];
    }

    public static function getTerritoryFromId($type = null) {
        $em = $this->getDoctrine()->getManager();
        $result = $em->createQuery("SELECT p from DashboardBundle:Territory p ")
                ->getArrayResult();

        foreach ($result as $key => $val) {
            $territoryArray[$val['id']] = $val['name'];
        }

        if ($type != null) {
            return $territoryArray[$type];
        } else {
            return "";
        }
    }
   
    public static function getPresignedUrl(\Aws\S3\S3Client $s3Client, $bucket, $path, $duration) {
        $cmd = $s3Client->getCommand('GetObject', [
            'Bucket' => $bucket,
            'Key' => $path,
        ]);
        $request = $s3Client->createPresignedRequest($cmd, $duration);
        // Get the actual presigned-url
        
        return (string) $request->getUri();
    }
}
