<?php
/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\DashboardBundle\Dao;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
/**
 * Description of AbstractDao
 *
 * @author Michael Tarimo
 */
class AbstractDao extends Controller {
    
    // common dao functions
}
