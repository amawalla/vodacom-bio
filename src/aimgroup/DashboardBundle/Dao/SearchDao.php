<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\DashboardBundle\Dao;


use Elastica\Query;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Description of SearchDao
 *
 * @author robert
 */
class SearchDao {

    private $_seachManager;
    private $_container;

    public function __construct(ContainerInterface $container) {
        $this->_seachManager = $container->get('fos_elastica.manager');
        $this->_container = $container;
    }

    public function search($keyword) {
        $finder = $this->_container->get('fos_elastica.finder.ereg');

        $boolQuery = new Query\BoolQuery();

        $queryString = new Query\QueryString();
        $queryString->setDefaultField('_all');
        $queryString->setQuery($keyword);
        $boolQuery->addMust($queryString);

        $query = new Query($boolQuery);

        $query->setHighlight(array(
            'fields' => array('*' => new \stdClass)
        ));

        // Returns a mixed array of any objects mapped + highlights
        //$results = $finder->findHybrid($query);
        return $finder->find($query);
    }

    public function searchAgent($keyword) {

        $finder = $this->_container->get('fos_elastica.finder.ereg.registration');

        $boolQuery = new Query\BoolQuery();

        $queryString = new Query\QueryString();
        $queryString->setDefaultField('_all');
        $queryString->setQuery($keyword);

        $boolQuery->addMust($queryString);

        $query = new Query($boolQuery);
        $query->setHighLight(array(
            'fields' => array('*' => new \stdClass())
        ));

        return $finder->find($query);
    }


}
