<?php
/**
 * Created by PhpStorm.
 * User: HP User
 * Date: 11/3/2015
 * Time: 1:04 PM
 */

namespace aimgroup\DashboardBundle\Dao;

/**
 * JsonObject for interacting with javascript
 * Class JsonResponse
 * @package DashboardBundle\Entity
 */
class JsonObject
{
    /** @var  bool */
    private $status;
    /** @var  object */
    private $item;
    /** @var  string */
    private $message;

    /** @var  string */
    private $resultCode;

    /**
     * @return boolean
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param boolean $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return object
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param object $item
     */
    public function setItem($item)
    {
        $this->item = $item;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getResultCode()
    {
        return $this->resultCode;
    }

    /**
     * @param string $resultCode
     */
    public function setResultCode($resultCode)
    {
        $this->resultCode = $resultCode;
    }





}