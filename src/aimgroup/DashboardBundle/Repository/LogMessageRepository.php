<?php

namespace aimgroup\DashboardBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * LogMessageRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class LogMessageRepository extends EntityRepository
{
}
