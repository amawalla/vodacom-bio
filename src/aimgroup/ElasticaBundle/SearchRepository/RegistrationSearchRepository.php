<?php

/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 10/06/2016
 * Time: 4:17 PM
 */
namespace aimgroup\ElasticaBundle\SearchRepository;

use Elastica\Query;
use FOS\ElasticaBundle\Repository;

class RegistrationSearchRepository extends Repository
{
    public function findCountOfIdTypeAndIdNumber($idType, $idNumber)
    {
        $query = new Query();
        $bool = new Query\BoolQuery();
        $fieldQuery = new Query\Match();
        $fieldQuery->setField('identification', $idNumber);
        $bool->addMust(
            array(
                new Query\Term(array("identificationType" => $idType)),
                $fieldQuery
            ));
        $query->setQuery($bool);
        return count($this->find($query));
    }

    public function findByMsisdnAndSerial($msisdn, $simserial)
    {
        $query = new Query();
        $bool = new Query\BoolQuery();
        $mustArray = array();

        $fieldQuery = new Query\Match();
        $fieldQuery->setField('msisdn', $msisdn);
        array_push($mustArray, $fieldQuery);

#        $fieldQuery = new \Elastica\Query\Match();
#        $fieldQuery->setField('simSerial', $simserial);

        $fieldQuery = new Query\Prefix();
        $fieldQuery->setPrefix('simSerial', $simserial);
        array_push($mustArray, $fieldQuery);

        $bool->addMust($mustArray);
        $query->setQuery($bool);
        return $this->find($query);
    }

    public function findCountOfIdNumber($idnumber)
    {
        $query = new Query();
        $bool = new Query\BoolQuery();
        $fieldQuery = new Query\Match();
        $fieldQuery->setField('identification', $idnumber);
        $bool->addMust(
            array(
                $fieldQuery
            ));
        $query->setQuery($bool);
        return count($this->find($query));
    }


    public function findCountByMsisdnAndSerial($msisdn, $simserial)
    {
        $query = new Query();
        $bool = new Query\BoolQuery();
        $mustArray = array();

        $fieldQuery = new Query\Match();
        $fieldQuery->setField('msisdn', $msisdn);
        array_push($mustArray, $fieldQuery);

        $fieldQuery = new Query\Match();
        $fieldQuery->setField('simSerial', $simserial);
        array_push($mustArray, $fieldQuery);

        $bool->addMust($mustArray);
        $query->setQuery($bool);
        return count($this->find($query));
    }

    public function findCountByMsisdn($msisdn)
    {
        $query = new Query();
        $bool = new Query\BoolQuery();
        $mustArray = array();

        $fieldQuery = new Query\Match();
        $fieldQuery->setField('msisdn', $msisdn);
        array_push($mustArray, $fieldQuery);

        $bool->addMust($mustArray);
        $query->setQuery($bool);
        return count($this->find($query));
    }


}