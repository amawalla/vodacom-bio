<?php
/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 24/06/2016
 * Time: 12:25 PM
 */

namespace aimgroup\ElasticaBundle\SearchRepository;


use Elastica\Query;
use FOS\ElasticaBundle\Repository;

class UserLogSearchRepository extends Repository
{

    public function searchUserLogs($attributes, $queryAttrib)
    {
        $filterParams = array();
        $query = new Query();
        $boolean = new Query\BoolQuery();

        if (@$attributes["search"]) {

            $fieldQuery = new Query\Match();
            $fieldQuery->setField('metadata', $attributes["search"]);
            array_push($filterParams, $fieldQuery);

            $fieldQuery = new Query\Match();
            $fieldQuery->setField('user', $attributes["search"]);
            array_push($filterParams, $fieldQuery);

            $fieldQuery = new Query\Match();
            $fieldQuery->setField('action', $attributes["search"]);
            array_push($filterParams, $fieldQuery);

            $boolean->addShould($filterParams);

        }

        $query->setQuery($boolean);

        $query->setSort(array("timestamp" => array('order' => 'desc')));
        //dump(json_encode(array('query' => $query->getQuery()->toArray())));
        $data = $this->findPaginated($query);
        $data->setMaxPerPage($queryAttrib["jtPageSize"]);
        $data->setCurrentPage($queryAttrib["jtStartIndex"] + 1);
        return $data->getCurrentPageResults();
    }

}