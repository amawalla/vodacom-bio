<?php
/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 15/06/2016
 * Time: 3:25 PM
 */

namespace aimgroup\ElasticaBundle\SearchRepository;

use Elastica\Aggregation;
use Elastica\Script;
use Elastica\Query;
use FOS\ElasticaBundle\Repository;

class RegistrationStatusSearchRepository extends Repository
{

    public function getRegStatusByRegNoAndMsisdn($idnumber, $msisdn)
    {
        $query = new Query();

        $bool = new Query\BoolQuery();
        $mustArray = array();

        $fieldQuery = new Query\Match();
        $fieldQuery->setField('registrationId.registrationid', $idnumber);
        array_push($mustArray, $fieldQuery);

        $fieldQuery = new Query\Match();
        $fieldQuery->setField('registrationId.msisdn', $msisdn);
        array_push($mustArray, $fieldQuery);

        $bool->addMust($mustArray);
        $query->setQuery($bool);
        $data = $this->find($query);

        return $data;

    }

    public function listRegistrationStatus($attributes, $jtPageSize, $jtStartIndex)
    {
        $query = new \Elastica\Query();
        $query->addSort(array('registrationId.createdDate' => array('order' => 'desc')));
        $qb = new \Elastica\QueryBuilder();
        $boolQuery = $qb->filter()->bool();

        $filterParams = array();
        $dateRangeParams = array();

        $boolean = new \Elastica\Query\Bool();


        if (strlen(@$attributes['msisdn']) >= 4) {
            $fieldQuery = new \Elastica\Query\Match();
            $fieldQuery->setField('registrationId.msisdn', $attributes['msisdn']);
            array_push($filterParams, $fieldQuery);

//            $boolQuery->addMust(
//                $qb->filter()->term(['registrationId.msisdn' => $attributes["msisdn"]])
//            );
        }
        if (strlen(@$attributes['first_name']) >= 2) {
            $fieldQuery = new \Elastica\Query\Match();
            $fieldQuery->setField('registrationId.firstName', $attributes['first_name']);
            array_push($filterParams, $fieldQuery);

//            $boolQuery->addMust(
//                $qb->filter()->term(['registrationId.firstName' => strtolower($attributes["first_name"])])
//            );
        }
        if (strlen(@$attributes['last_name']) >= 2) {
            $fieldQuery = new \Elastica\Query\Match();
            $fieldQuery->setField('registrationId.lastName', $attributes['last_name']);
            array_push($filterParams, $fieldQuery);

//            $boolQuery->addMust(
//                $qb->filter()->term(['registrationId.lastName' => strtolower($attributes["last_name"])])
//            );
        }
        if (@$attributes['identificationType'] >= 1) {
            $fieldQuery = new \Elastica\Query\Match();
            $fieldQuery->setField('registrationId.identificationType', $attributes['identificationType']);
            array_push($filterParams, $fieldQuery);

//            $boolQuery->addMust(
//                $qb->filter()->term(['registrationId.identificationType' => $attributes["identificationType"]])
//            );
        }
        if (strlen(@$attributes['id_number']) >= 4) {
            $fieldQuery = new \Elastica\Query\Match();
            $fieldQuery->setField('registrationId.identification', $attributes['id_number']);
            array_push($filterParams, $fieldQuery);

//            $boolQuery->addMust(
//                $qb->filter()->term(['registrationId.identificationType' => $attributes["id_number"]])
//            );
        }
        if (strlen(@$attributes['agent_firstname']) >= 3) {
            $fieldQuery = new \Elastica\Query\Match();
            $fieldQuery->setField('registrationId.owner.firstName', $attributes['agent_firstname']);
            array_push($filterParams, $fieldQuery);
//            $boolQuery->addMust(
//                $qb->filter()->term(['registrationId.owner.firstName' => strtolower($attributes["agent_firstname"])])
//            );
        }
        if (strlen(@$attributes['agent_lastname']) >= 3) {
            $fieldQuery = new \Elastica\Query\Match();
            $fieldQuery->setField('registrationId.owner.lastName', $attributes['agent_lastname']);
            array_push($filterParams, $fieldQuery);

//
//            $boolQuery->addMust(
//                $qb->filter()->term(['registrationId.owner.lastName' => strtolower($attributes["agent_lastname"])])
//            );
        }
        if (strlen(@$attributes['agent_phone']) >= 4) {
            $fieldQuery = new \Elastica\Query\Match();
            $fieldQuery->setField('registrationId.agentMsisdn', $attributes['agent_phone']);
            array_push($filterParams, $fieldQuery);

//            $boolQuery->addMust(
//                $qb->filter()->term(['registrationId.agentMsisdn' => $attributes["agent_phone"]])
//            );
        }

        if (strlen(@$attributes['path']) >= 2) {
            $attributes['icap_status'] = $attributes['path'];
        }


        if (strlen(@$attributes['icap_status']) >= 1) {
            $status = $attributes['icap_status'];
            if ($status == 'notsent') {
                $fieldQuery = new \Elastica\Query\Term();
                $fieldQuery->setTerm('temporaryRegStatus', '0');
                array_push($filterParams, $fieldQuery);

//
//                $boolQuery->addMust(
//                    $qb->filter()->term(['temporaryRegStatus' => "0"])
//                );
            } else if ($status == 'declined') {
                $fieldQuery = new \Elastica\Query\Terms();
                $fieldQuery->setTerms('temporaryRegStatus', array(3, 4));
                array_push($filterParams, $fieldQuery);


                $fieldQuery = new \Elastica\Query\Term();
                $fieldQuery->setTerm('fullRegStatus', 0);
                array_push($filterParams, $fieldQuery);

//
//                $boolQuery->addMust(
//                    $qb->filter()->terms('temporaryRegStatus', array(3, 4))
//                );
//                $boolQuery->addMust(
//                    $qb->filter()->term(['fullRegStatus' => "0"])
//                );

            } else if ($status == 'freg') {
                $fieldQuery = new \Elastica\Query\Terms();
                $fieldQuery->setTerms('temporaryRegStatus', array(2, 6));
                array_push($filterParams, $fieldQuery);

                $fieldQuery = new \Elastica\Query\Terms();
                $fieldQuery->setTerms('fullRegStatus', array(6, 7));
                array_push($filterParams, $fieldQuery);

//
//                $boolQuery->addMust(
//                    $qb->filter()->terms('temporaryRegStatus', array(2, 6))
//                );
//                $boolQuery->addMust(
//                    $qb->filter()->terms('fullRegStatus', array(6, 7))
//                );


            } else if ($status == 'treg') {

                $fieldQuery = new \Elastica\Query\Terms();
                $fieldQuery->setTerms('temporaryRegStatus', array(1, 2, 6));
                array_push($filterParams, $fieldQuery);

                $fieldQuery = new \Elastica\Query\Term();
                $fieldQuery->setTerm('fullRegStatus', 0);


                array_push($filterParams, $fieldQuery);


//                $boolQuery->addMust(
//                    $qb->filter()->terms('temporaryRegStatus', array(1, 2, 6))
//                );
//                $boolQuery->addMust(
//                    $qb->filter()->term(['fullRegStatus' => "0"])
//                );


            } else if ($status == 'pending') {
                $rangeQuery = new \Elastica\Query\Range();
                $rangeQuery->addField('registrationId.image_count', array('lte' => 3));
                array_push($filterParams, $rangeQuery);

                $fieldQuery = new \Elastica\Query\Term();
                $fieldQuery->setTerm('temporaryRegStatus', 1);
                array_push($filterParams, $fieldQuery);

            } else if ($status == 'tobesent') {

                $fieldQuery = new \Elastica\Query\Term();
                $fieldQuery->setTerm('temporaryRegStatus', 0);
                array_push($filterParams, $fieldQuery);

            }
        }

        if (@$attributes['region']) {
            $fieldQuery = new \Elastica\Query\Match();

            $fieldQuery->setField('registrationId.region', $attributes['region']);
            array_push($filterParams, $fieldQuery);

//            $boolQuery->addMust(
//                $qb->filter()->term(['registrationId.region' => strtolower($attributes["region"])])
//            );
        }

        if (@$attributes['territory'] > 0) {
            $fieldQuery = new \Elastica\Query\Match();
            $fieldQuery->setField('registrationId.territory', $attributes['territory']);

            array_push($filterParams, $fieldQuery);

//            $boolQuery->addMust(
//                $qb->filter()->term(['registrationId.territory' => strtolower($attributes["territory"])])
//            );
        }

        if (strlen(@$attributes['from_date']) > 4) {
            $dateRangeParams['gte'] = \Elastica\Util::convertDate((new \DateTime($attributes['from_date']))->getTimestamp());
        }
        if (strlen(@$attributes['to_date']) > 4) {
            $dateRangeParams['lte'] = \Elastica\Util::convertDate((new \DateTime($attributes['to_date']))->getTimestamp());
        }


        if (strlen(@$attributes['date']) > 4) {
            $dateRangeParams['gte'] = \Elastica\Util::convertDate((new \DateTime($attributes['date']))->getTimestamp());
            $tomorrow = new \DateTime($attributes['date']);
            $tomorrow->modify('+1 day');
            $dateRangeParams['lt'] = \Elastica\Util::convertDate($tomorrow->getTimestamp());
        }


        if (count($dateRangeParams) > 0) {
            $rangeQuery = new \Elastica\Query\Range();
            $rangeQuery->addField('registrationId.createdDate', $dateRangeParams);
            $boolean->addMust($rangeQuery);

//            $boolQuery->addMust(
//                $qb->filter()->range('registrationId.createdDate', $dateRangeParams)
//            );
        }
        if (count($filterParams) > 0) {
            $boolean->addMust($filterParams);
        }
        $query->setQuery($boolean);

//        $query->setQuery(
//            $qb->query()->filtered(null, $boolQuery)
//        );

        #$query->setQuery($boolean);
        #$query->setSort(array("createdDate" => array('order' => 'desc')));
        #   file_put_contents('/var/log/ekyc/vodacom.log',json_encode(array('TTT' => $query->getQuery()->toArray())));
        $data = $this->findPaginated($query);
        if (isset($jtPageSize) && isset($jtStartIndex)) {
            $data->setMaxPerPage($jtPageSize);
            $data->setCurrentPage($jtStartIndex);
        }
        //return array("count" => $data->getNbResults(), "results" => $data->getCurrentPageResults());
        $theResult = $data->getCurrentPageResults();
        $theCount = count($data) * $jtPageSize;

        //echo $theCount; exit;
        return array('count' => $theCount, 'results' => $theResult);
    }

    public function _listRegistrationStatus($attributes, $jtPageSize, $jtStartIndex)
    {
        $query = new Query();


        $filterParams = array();
        $dateRangeParams = array();

        $boolean = new Query\BoolQuery();


        if (strlen(@$attributes['msisdn']) >= 4) {
            $fieldQuery = new Query\Match();
            $fieldQuery->setField('registrationId.msisdn', $attributes['msisdn']);
            array_push($filterParams, $fieldQuery);

//            $boolQuery->addMust(
//                $qb->filter()->term(['registrationId.msisdn' => $attributes["msisdn"]])
//            );
        }
        if (strlen(@$attributes['first_name']) >= 2) {
            $fieldQuery = new Query\Match();
            $fieldQuery->setField('registrationId.firstName', $attributes['first_name']);
            array_push($filterParams, $fieldQuery);

//            $boolQuery->addMust(
//                $qb->filter()->term(['registrationId.firstName' => strtolower($attributes["first_name"])])
//            );
        }
        if (strlen(@$attributes['last_name']) >= 2) {
            $fieldQuery = new Query\Match();
            $fieldQuery->setField('registrationId.lastName', $attributes['last_name']);
            array_push($filterParams, $fieldQuery);

//            $boolQuery->addMust(
//                $qb->filter()->term(['registrationId.lastName' => strtolower($attributes["last_name"])])
//            );
        }
        if (@$attributes['identificationType'] >= 1) {
            $fieldQuery = new Query\Match();
            $fieldQuery->setField('registrationId.identificationType', $attributes['identificationType']);
            array_push($filterParams, $fieldQuery);

//            $boolQuery->addMust(
//                $qb->filter()->term(['registrationId.identificationType' => $attributes["identificationType"]])
//            );
        }
        if (strlen(@$attributes['id_number']) >= 4) {
            $fieldQuery = new Query\Match();
            $fieldQuery->setField('registrationId.identification', $attributes['id_number']);
            array_push($filterParams, $fieldQuery);

//            $boolQuery->addMust(
//                $qb->filter()->term(['registrationId.identificationType' => $attributes["id_number"]])
//            );
        }
        if (strlen(@$attributes['agent_firstname']) >= 3) {
            $fieldQuery = new Query\Match();
            $fieldQuery->setField('registrationId.owner.firstName', $attributes['agent_firstname']);
            array_push($filterParams, $fieldQuery);
//            $boolQuery->addMust(
//                $qb->filter()->term(['registrationId.owner.firstName' => strtolower($attributes["agent_firstname"])])
//            );
        }
        if (strlen(@$attributes['agent_lastname']) >= 3) {
            $fieldQuery = new Query\Match();
            $fieldQuery->setField('registrationId.owner.lastName', $attributes['agent_lastname']);
            array_push($filterParams, $fieldQuery);

//
//            $boolQuery->addMust(
//                $qb->filter()->term(['registrationId.owner.lastName' => strtolower($attributes["agent_lastname"])])
//            );
        }
        if (strlen(@$attributes['agent_phone']) >= 4) {
            $fieldQuery = new Query\Match();
            $fieldQuery->setField('registrationId.agentMsisdn', $attributes['agent_phone']);
            array_push($filterParams, $fieldQuery);

//            $boolQuery->addMust(
//                $qb->filter()->term(['registrationId.agentMsisdn' => $attributes["agent_phone"]])
//            );
        }

        if (strlen(@$attributes['path']) >= 2) {
            $attributes['icap_status'] = $attributes['path'];
        }


        if (strlen(@$attributes['icap_status']) >= 1) {
            $status = $attributes['icap_status'];
            if ($status === 'notsent') {
                $fieldQuery = new Query\Term();
                $fieldQuery->setTerm('temporaryRegStatus', '0');
                array_push($filterParams, $fieldQuery);

//
//                $boolQuery->addMust(
//                    $qb->filter()->term(['temporaryRegStatus' => "0"])
//                );
            } else {
                if ($status === 'declined') {
                    $fieldQuery = new Query\Terms();
                    $fieldQuery->setTerms('temporaryRegStatus', array(3, 4));
                    array_push($filterParams, $fieldQuery);


                    $fieldQuery = new Query\Term();
                    $fieldQuery->setTerm('fullRegStatus', 0);
                    array_push($filterParams, $fieldQuery);

//
//                $boolQuery->addMust(
//                    $qb->filter()->terms('temporaryRegStatus', array(3, 4))
//                );
//                $boolQuery->addMust(
//                    $qb->filter()->term(['fullRegStatus' => "0"])
//                );

                } else {
                    if ($status === 'freg') {
                        $fieldQuery = new Query\Terms();
                        $fieldQuery->setTerms('temporaryRegStatus', array(2, 6));
                        array_push($filterParams, $fieldQuery);

                        $fieldQuery = new Query\Terms();
                        $fieldQuery->setTerms('fullRegStatus', array(6, 7));
                        array_push($filterParams, $fieldQuery);

//
//                $boolQuery->addMust(
//                    $qb->filter()->terms('temporaryRegStatus', array(2, 6))
//                );
//                $boolQuery->addMust(
//                    $qb->filter()->terms('fullRegStatus', array(6, 7))
//                );


                    } else {
                        if ($status === 'treg') {

                            $fieldQuery = new Query\Terms();
                            $fieldQuery->setTerms('temporaryRegStatus', array(1, 2, 6));
                            array_push($filterParams, $fieldQuery);

                            $fieldQuery = new Query\Term();
                            $fieldQuery->setTerm('fullRegStatus', 0);


                            array_push($filterParams, $fieldQuery);


//                $boolQuery->addMust(
//                    $qb->filter()->terms('temporaryRegStatus', array(1, 2, 6))
//                );
//                $boolQuery->addMust(
//                    $qb->filter()->term(['fullRegStatus' => "0"])
//                );


                        } else {
                            if ($status === 'pending') {


                                $fieldQuery = new Query\Terms();
                                $fieldQuery->setTerms('registrationId.image_count', array(0, 1, 2, 3));


                                $fieldQuery = new Query\Range();
                                $fieldQuery->addField('fullRegStatus', array('from' => 6, 'to' => 7));
                                $boolean->addMustNot($fieldQuery);

                                $fieldQuery = new Query\Match();
                                $fieldQuery->setField('temporaryRegStatus', 1);
                                $fieldQuery->setFieldParam('name', 'boost', 10);

                            } else {
                                if ($status === 'tobesent') {

                                    $fieldQuery = new Query\Terms();
                                    $fieldQuery->setTerms('temporaryRegStatus', array(0, 1));
                                    array_push($filterParams, $fieldQuery);
                                }
                            }
                        }
                    }
                }
            }
        }

        if (@$attributes['region']) {
            $fieldQuery = new Query\Match();

            $fieldQuery->setField('registrationId.region', $attributes['region']);
            array_push($filterParams, $fieldQuery);

//            $boolQuery->addMust(
//                $qb->filter()->term(['registrationId.region' => strtolower($attributes["region"])])
//            );
        }

        if (@$attributes['territory'] > 0) {
            $fieldQuery = new Query\Match();
            $fieldQuery->setField('registrationId.territory', $attributes['territory']);

            array_push($filterParams, $fieldQuery);

//            $boolQuery->addMust(
//                $qb->filter()->term(['registrationId.territory' => strtolower($attributes["territory"])])
//            );
        }

        if (strlen(@$attributes['from_date']) > 4) {
            $dateRangeParams['gte'] = \Elastica\Util::convertDate((new \DateTime($attributes['from_date']))->getTimestamp());
        }
        if (strlen(@$attributes['to_date']) > 4) {
            $dateRangeParams['lte'] = \Elastica\Util::convertDate((new \DateTime($attributes['to_date']))->getTimestamp());
        }


        if (strlen(@$attributes['date']) > 4) {
            $dateRangeParams['gte'] = \Elastica\Util::convertDate((new \DateTime($attributes['date']))->getTimestamp());
            $tomorrow = new \DateTime($attributes['date']);
            $tomorrow->modify('+1 day');
            $dateRangeParams['lt'] = \Elastica\Util::convertDate($tomorrow->getTimestamp());
        }


        if (count($dateRangeParams) > 0) {
            $rangeQuery = new Query\Range();
            $rangeQuery->addField('registrationId.createdDate', $dateRangeParams);
            $boolean->addMust($rangeQuery);

//            $boolQuery->addMust(
//                $qb->filter()->range('registrationId.createdDate', $dateRangeParams)
//            );
        }
        if (count($filterParams) > 0) {
            $boolean->addMust($filterParams);
            $query->setQuery($boolean);
            $query->setSort(array('createdDate' => array('order' => 'desc')));

        } else {
            $query = new Query\MatchAll();

        }


//        $query->setQuery(
//            $qb->query()->filtered(null, $boolQuery)
//        );


        $data = $this->findPaginated($query);
        if (isset($jtPageSize) && isset($jtStartIndex)) {
            $data->setMaxPerPage($jtPageSize);
            $data->setCurrentPage($jtStartIndex);
        }

        $theResult = $data->getCurrentPageResults();
        $theCount = count($data) * $jtPageSize;

        return array('count' => $theCount, 'results' => $theResult);
        //return array("count" => $data->getNbResults(), "results" => $data->getCurrentPageResults());
    }

    public function getRegistrationJourney()
    { 
        $query = new Query(new Query\MatchAll());

        $createdDate = new Script\Script("abs(doc['registrationId.createdDate'].value-doc['registrationId.deviceSentTime'].value)/1000");

        $avgRegTime = new Aggregation\Avg('avg_reg_time');
        $avgRegTime->setScript($createdDate);


        $allimageDate = new Script\Script("abs(doc['allimageDate'].value-doc['registrationId.deviceSentTime'].value)/1000");
        $avgImageTime = new Aggregation\Avg('avg_image_time');
        $avgImageTime->setScript($allimageDate);


        $tregDate = new Script\Script("abs(doc['tregDate'].value-doc['registrationId.deviceSentTime'].value)/1000");
        $avgTRegTime = new Aggregation\Avg('avg_treg_time');
        $avgTRegTime->setScript($tregDate);

        $fregDate = new Script\Script("abs(doc['fregDate'].value-doc['registrationId.deviceSentTime'].value)/1000");
        $avgFRegTime = new Aggregation\Avg('avg_freg_time');
        $avgFRegTime->setScript($fregDate);


        $dateAggregation = new Aggregation\DateHistogram('dateHistogram', 'registrationId.createdDate', 'day');
        $dateAggregation->setFormat('yyyy-MM-dd');

        $dateAggregation->addAggregation($avgRegTime);
        $dateAggregation->addAggregation($avgImageTime);
        $dateAggregation->addAggregation($avgTRegTime);
        $dateAggregation->addAggregation($avgFRegTime);

        $query->addAggregation($dateAggregation);
        $query->setSize(0);
        

        return $query;
    }

    public function getRegistrationStatsSummary($start_date, $end_date)
    {
        $query = new Query(new Query\MatchAll());
        $dateRangeParams = array();
        if (strlen($start_date)) {
            $dateRangeParams['gte'] = \Elastica\Util::convertDate((new \DateTime($start_date))->getTimestamp());
            $dateRangeParams['lte'] = \Elastica\Util::convertDate((new \DateTime($end_date))->getTimestamp());
        }


        $dateAggregation = new Aggregation\DateHistogram('dateHistogram', 'registrationId.createdDate', 'day');
        $dateAggregation->setFormat('yyyy-MM-dd');

        $terms = new Aggregation\Terms('image_count');
        $terms->setField('registrationId.image_count');
        $terms->setSize(0);
        $dateAggregation->addAggregation($terms);

        $terms = new Aggregation\Terms('icapState');
        $terms->setField('icapState');
        $terms->setSize(0);
        $dateAggregation->addAggregation($terms);

        $terms = new Aggregation\Terms('dbmsState');
        $terms->setField('dbmsState');
        $terms->setSize(0);
        $dateAggregation->addAggregation($terms);


        $terms = new Aggregation\Terms('mpesaState');
        $terms->setField('mpesaState');
        $terms->setSize(0);
        $dateAggregation->addAggregation($terms);

        $boolean = new Query\BoolQuery();
        if (count($dateRangeParams) > 0) {
            $rangeQuery = new Query\Range();
            $rangeQuery->addField('registrationId.createdDate', $dateRangeParams);
            $boolean->addMust($rangeQuery);
            $query->setQuery($boolean);
        }
        $query->setSort(array('createdDate' => array('order' => 'desc')));
        $query->addAggregation($dateAggregation);
        $query->setSize(0);

        return $query;
    }
}