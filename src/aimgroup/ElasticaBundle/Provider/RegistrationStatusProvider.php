<?php
namespace aimgroup\ElasticaBundle\Provider;

use FOS\ElasticaBundle\Provider\ProviderInterface;

/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 20/08/2016
 * Time: 9:27 AM
 */
class RegistrationStatusProvider implements ProviderInterface
{

    /**
     * Persists all domain objects to ElasticSearch for this provider.
     *
     * The closure can expect 2 or 3 arguments:
     *   * The step size
     *   * The total number of objects
     *   * A message to output in error conditions (not normally provided)
     *
     * @param \Closure $loggerClosure
     * @param array $options
     *
     * @return
     */
    public function populate(\Closure $loggerClosure = null, array $options = array())
    {

    }
}