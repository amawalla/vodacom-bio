#!/bin/bash
ROOT_DIR="/var/www/html/vodacomEKYC/";
MYSQL_CONN="mysql -uroot vodacom_ekyc"

#php $ROOT_DIR."app"

function createAdmin(){
`php $ROOT_DIR."app/console" fos:user:create vodacommanager vodacom@registersim.com admin`
`php $ROOT_DIR."app/console" vodacommanager ROLE_ADMIN`
}

function initDabase(){
$MYSQL_CONN << EOF
INSERT IGNORE INTO Idtype (id,name,use_count) VALUES (1,'Voters ID',0),(2,'Drivers License',0),(3,'National ID',0),(4,'Passport',0),(5,'Business Licence',0),(6,'TIN Certificate',0),(7,'VAT Certificate',0),(8,'Certificate of Incorporation',0),(9,'Health Insurance ID',0),(10,'Employer ID',0),(11,'Pension Fund ID',0),(12,'NSSF Pension Card',0),(13,'Student ID',0),(14,'WEO/VEO/REF',0);
INSERT IGNORE INTO FieldNames (id,name) VALUES (13,'address'),(10,'alternative-contact'),(16,'back-pic'),(18,'confirm'),(20,'customer-confirmation'),(5,'dateofbirth'),(8,'email'),(9,'esn'),(1,'firstname'),(15,'front-pic'),(4,'gender'),(21,'id-number'),(7,'id-type'),(2,'middlename'),(6,'nationality'),(14,'potrait'),(17,'rear-pic'),(11,'region'),(19,'signature'),(3,'surname'),(12,'territory'),(22,'ward');
INSERT IGNORE INTO FieldTypes (id,name) VALUES (4,'camera'),(9,'choice'),(10,'confirm'),(7,'country'),(8,'date'),(2,'integer'),(6,'signature'),(3,'spinner'),(1,'string'),(11,'string-barcode'),(5,'text');
INSERT IGNORE INTO Region (id,name,code) VALUES (1,'Dar es Salaam','DS'),(3,'Arusha','AS'),(5,'Dodoma','DO'),(6,'Geita','GE'),(7,'Iringa','IG'),(8,'Kagera','KG'),(9,'Katavi','KA'),(10,'Kigoma','KM'),(11,'Kilimanjaro','KL'),(12,'Lindi','LI'),(13,'Manyara','MY'),(14,'Mara','MA'),(15,'Mbeya','MB'),(16,'Morogoro','MO'),(17,'Mtwara','MT'),(18,'Mwanza','MZ'),(19,'Njombe','NJ'),(20,'Northern Pemba, Zanzibar','PN'),(21,'Southern Pemba, Zanzibar','PS'),(22,'Pwani','PW'),(23,'Rukwa','RU'),(24,'Ruvuma','RV'),(25,'Shinyanga','SY'),(26,'Simiyu','SI'),(27,'Singida','SD'),(28,'Tabora','TB'),(30,'Northern Unguja, Zanzibar','ZN'),(31,'Southern Unguja, Zanzibar','ZS'),(32,'Western City, Zanzibar','ZW');
INSERT IGNORE INTO Territory (id,region_id,name)  VALUES (1,1,'Ilala Municipal'),(2,1,'Kinondoni Municipal'),(3,1,'Temeke Municipal'),(5,3,'Arumeru District Council'),(6,3,'Arusha
 City Council'),(7,3,'Arusha District Council'),(8,3,'Karatu District Council'),(9,3,'Longido District Council'),(10,3,'Monduli District Council'),(11,3,
'Ngorongoro District Council'),(12,5,'Bahi District Council'),(13,5,'Chamwino District Council'),(14,6,'Bukombe District Council'),(15,6,'Chato District Council'),(16,6,'Geita District Council'),(17,6,'Mbongwe District Council'),(18,7,'Iringa District Council'),
(19,7,'Iringa Municipal Council'),(20,7,'Kilolo District Council'),(21,7,'Mafinga Town Council'),(22,7,'Mufindi District Council'),(23,9,'Mlele District Council'),(24,10,'Buhigwe District Council'),(25,11,'Hai District Council'),(26,12,'Kilwa District Council'),
(27,13,'Babati District Council'),(28,14,'Bunda District Council'),(29,15,'Biharamulo District Council'),(30,16,'Gairo District Council'),(31,17,'Masasi District Council'),(32,18,'Ilemela District Council'),(33,19,'Ludewa District Council'),(34,20,'Micheweni District'),
(35,20,'Wete District'),(36,30,'Kaskazini A District'),(37,30,'Kaskazini B District'),(41,21,'Kati District'),(42,21,'Kusini District'),(44,13,'Babati Town Council'),(50,5,'Chemba District'),(51,5,'Dodoma Municipal'),(52,5,'Kondoa District Council'),(53,5,'Kongwa District Council'),
(54,5,'Mpwapwa District Council'),(55,6,'Nyang\'hwale District Council'),(56,9,'Mpanda District Council'),(57,9,'Mpanda Town Council'),(59,10,'Kakonko District Council'),(60,10,'Kasulu District Council'),(61,10,'Kasulu Town Council'),(62,10,'Kibondo District Council'),
(63,10,'Kigoma District Council'),(64,10,'Kigoma-Ujiji Municipal'),(65,10,'Uvinza District Council'),(67,11,'Moshi District Council'),(68,11,'Moshi Municipal Council'),(69,11,'Mwanga District Council'),(70,11,'Same District Council'),(71,11,'Siha District Council'),
(72,12,'Lindi District Council'),(73,12,'Lindi Municipal Council'),(74,12,'Liwale District Council'),(75,12,'Nachingwa District Council'),(76,12,'Ruangwa District Council'),(77,13,'Hanang District Council'),(78,13,'Kiteto District Council'),(79,13,'Mbulu District Council'),
(80,13,'Simanjiro District Council'),(81,14,'Butiama District Council'),(82,14,'Musoma District Council'),(83,14,'Musoma Municipal Council'),(84,14,'Rorya District Council'),(85,14,'Serengeti District Council'),(86,14,'Tarime District Council'),(87,15,'Bukoba District Council'),
(88,15,'Karagwe District Council'),(89,15,'Kyerwa District Council'),(90,15,'Missenyi District Council'),(91,15,'Muleba District Council'),(92,15,'Ngara District Council'),(93,15,'Bukoba Municipal Council'),(94,16,'Kilombero District Council'),(95,16,'Kilosa District Council'),
(96,16,'Morogoro District Council'),(97,16,'Morogoro Municipal Council'),(98,16,'Mvomero District Council'),(99,16,'Ulanga District Council'),(107,17,'Masasi Town Council'),(108,17,'Mtwara District Council'),(109,17,'Mtwara Municipal Council'),(110,17,'Nanyumbu District Council'),
(111,17,'Newala District Council'),(112,17,'Tandahimba District Council'),(114,18,'Kwimba District Council'),(115,18,'Magu District Council'),(116,18,'Misungwi District Council'),(117,18,'Nyamagana Municipal Council'),(118,18,'Sengerema District Council'),(119,18,'Ukerewe District Council');
INSERT IGNORE INTO UserType (id,name,description,createdOn) VALUES (1,'freelancer','Freelancer','2015-11-10 13:46:18'),(2,'retailer','Retailer','2015-11-10 13:46:28'),(3,'atm_kiosk','ATM Kiosks','2015-11-10 13:46:32'),(4,'shops','Shops','2015-11-10 13:46:35');
INSERT IGNORE INTO role (id,name,role,dept_permissions) VALUES (1,'ROLE ADMIN','ROLE_ADMIN','[2,3,37,36,39,40,27,26,42,43,44,45,23,29,30,31,32,33,34,21,20,19,18,17,16,15,13,14,9,8,11,10,6,5]'),(4,'ROLE USER','ROLE_USER','[0]'),(6,'ROLE QA','ROLE_QA','0'),(7,'ROLE TEST','ROLE_TEST','[2,3,23,21,20,19,18,17]'),(8,'ROLE_TEST','ROLE_NEW','0');
EOF
}


initDabase