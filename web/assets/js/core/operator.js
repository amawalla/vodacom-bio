/**
 * Created by oscarmakala on 17/03/2016.
 */

var loadSavedOperatorConfigurations = function () {
    $.getJSON(URL_GET_OPERATOR_CONFIGS, function (data) {
        console.log(data);
        var inputs = document.getElementById("OperatorConfigurationsForm").elements;
        for (var i = 0; i < inputs.length; i++) {
            $.each(data.item, function (index, item) {
                if (inputs[i].name == index) {
                    $("#" + inputs[i].id).val(item);
                }
            });
        }
    });

}

var publishOperatorMQTT = function (event) {
    event.preventDefault();

    var msisdn = $("#form-msisdn").val();
    if (msisdn != undefined) {
        //get the device id
        var deviceId = keyValuePair[msisdn];
        //get the text area
        var fields = $("#OperatorConfigurationsForm").serializeArray();
        var json = {};
        jQuery.each(fields, function (i, field) {
            json[field.name] = field.value;
        });

        json["deviceId"] = deviceId;
        json["type"] = "new_operator";
        var topic = $("#publishDyanmicFormModal #form-topic").val();

        var request = $.ajax({
            url: URL_PUBLISH_SYSCONFIG,
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify(json),
            timeout: 10000
        });

        request.done(function (response) {
            hideBusyDialog();
            if (response != undefined) {
                alert(response.message);
            }
        });
        request.fail(function (error) {
            hideBusyDialog();
            alert("SYSTEM ERROR");
        });
    }
}

var saveOperatorConfig = function (event) {
    event.preventDefault();
    var fields = $("#OperatorConfigurationsForm").serializeArray();

    var json = {};
    jQuery.each(fields, function (i, field) {
        json[field.name] = field.value;
    });


    var request = $.ajax({
        url: URL_SAVE_OPERATOR,
        type: 'post',
        dataType: 'json',
        data: JSON.stringify(json),
        timeout: 10000
    });

    request.done(function (response) {
        hideBusyDialog();
        if (response != undefined) {
            if (response.status) {
                location.reload();
            }
            alert(response.message);
        }
    });

    request.fail(function (error) {
        hideBusyDialog();
        alert("SYSTEM ERROR");
    });
}


var toggle = function (id) {
    if ($('#' + id).is(':checked')) {
        document.getElementById(id).value = '1';
    } else {
        document.getElementById(id).value = '0';
    }
}