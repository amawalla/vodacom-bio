/**
 * Created by ssamson on 30/04/2017.
 */

$(document).ready(function () {

    $('#hjjdf').click(function(){
        var start = '', end = '';
        DashboardModule.getRegistrationSummary('/dashboard/registration_summary', start, end);
    });

    DashboardModule.getRegistrationSummary('/dashboard/registration_summary', 'start', 'end');
});


DashboardModule = {

    getRegistrationSummary: function (uri, startDate, endDate) {

        var remoteUrl = uri + '/' + startDate + '/' + endDate;

        var jqxhr = $.getJSON(remoteUrl, function (data) {

            $('.lloading.topsummary').hide();

            $.each(data, function(key, value) {
                $('#summary-' + key.replace(/_/g, '-')).html(value);
            });
        });

        jqxhr.fail(function() {
            console.log( "Error while getting registration summary.." );
        });
    }
}

jQuery.fn.extend({
    showProgressSpinner: function() {
        $(this).show();
    },
    hideProgressSpinner: function() {
        $(this).hide();
    }
});