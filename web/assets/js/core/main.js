window.URL_SAVE_ENDPOINT_CONFIG = "/admin/systems_config/createSysConfig";
window.URL_GET_CONFIGS = "/admin/systems_config/listSysConfig"
window.URL_GET_TOPICS = "/admin/app_config/get_topics";
window.URL_PUBLISH_SYSCONFIG = "/admin/systems_config/publish";
window.URL_PUBLISH_SYSCONFIG_FILE = "/admin/systems_config/publish_file";
window.URL_PUBLISH_APP_UPDATE = "/appupload/publish/update";
window.idSelected = "";

//var URL_PUBLISH_CONTENT = "/admin/app_config/publish_configuration";

window.URL_SAVE_IMAGE_CONFIG = "/admin/app_config/save_image_config";
window.URL_GET_IMAGE_CONFIGS = "/admin/app_config/list_image_config";
window.URL_GET_LOGIN_CONFIGS = "/admin/app_config/list_login_config";
window.URL_SAVE_LOGIN_CONFIGS = "/admin/app_config/save_login_config"


window.URL_LIST_IDTYPE = "/admin/idtype/list";
window.URL_CREATE_IDTYPE = "/admin/idtype/create";
window.URL_UPDATE_IDTYPE = "/admin/idtype/update";


window.URL_SAVE_OPERATOR = "/admin/operators/save";
window.URL_GET_OPERATOR_CONFIGS = "/admin/operators/list";


window.URL_BULK_UPLOAD_AGENT = "/admin/agents/bulk_upload";
window.URL_BULK_DELETE_AGENT = "/admin/agents/bulk_agent_delete";

window.GET_USER_LOGS = "/diagnosis/list"

var keyValuePair;
var availableTags;

$(function () {
    $(document).ajaxError(function (event, jqXHR) {
        if (403 === jqXHR.status) {
            window.location.reload();
        }
    });
    hideBusyDialog();
});

var hideBusyDialog = function () {
    $("#preloader").fadeOut("slow", function () {
        $(this).css({"display": "none"})
    });
};

var showBusyDialog = function () {
    $("#preloader").fadeIn("slow", function () {
        $(this).css({"display": "block"})
    });
}

var loadTopicSelector = function (operator) {
    if (keyValuePair == undefined) {
        $.getJSON(URL_GET_TOPICS, function (data) {
            keyValuePair = [];
            availableTags = [];
            $.each(data.item, function (index, item) { // Iterates through a collection
                keyValuePair[item.msisdn] = item.deviceId;
                availableTags.push(item.msisdn);
            });
            keyValuePair[operator] = operator;
            availableTags.push(operator);
            $("#form-msisdn").autocomplete({
                source: availableTags
            });
        });
    }

    $('#publishDyanmicFormModal').modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    });
};


/**
 * This is used for form paramter validations in conjuction with jquery validation plugin
 * @param field
 * @param rules
 * @param i
 * @param options
 * @returns {string}
 */
var onlyLetterNoNumberOrSpecialCharacter = function (field, rules, i, options) {
    var pattern = new RegExp(/^[a-zA-Z]+$/);
    if (!pattern.test(field.val())) return "No Special characters,spaces or numbers allowed";
}

