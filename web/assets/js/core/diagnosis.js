/**
 * Created by oscarmakala on 24/06/2016.
 */


var loadUserLogs = function () {
    $('#activities_table').jtable({
        title: 'UserLogs',
        paging: true,
        sorting: true,
        actions: {
            listAction: window.GET_USER_LOGS,
        },
        fields: {
            id: {
                key: true,
                create: false,
                edit: false,
                list: false
            },
            time: {
                title: 'Time',
                width: '15%',
                type: 'datetime',
                create: false,
                edit: false
            },
            action: {
                title: 'Action',
                width: '10%',
                edit: false,
            },
            userId: {
                title: 'User',
                width: '5%',
                edit: false,
            },
            desc: {
                title: 'Description',
                width: '30%',
                edit: false,
            },
            metadata: {
                title: 'meta',
                width: '40%',
                edit: false,
            }
        }
    });

    //Re-load records when user click 'load records' button.
    $('#filter_button').click(function (e) {
        e.preventDefault();
        $('#activities_table').jtable('load', {
            search: $('#search_string').val()
        });
    });

    $('#filter_button').click();
};