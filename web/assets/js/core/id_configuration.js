/**
 * Created by oscarmakala on 21/03/2016.
 */
var loadIdConfigurationTables = function () {
    //Prepare jTable
    $('#IdConfigurationContainer').jtable({
        title: 'ID TYPE CONFIGURATION',
        messages: {
            addNewRecord: 'Add new ID Type'
        },
        actions: {
            listAction: URL_LIST_IDTYPE,
            createAction: URL_CREATE_IDTYPE,
            updateAction: URL_UPDATE_IDTYPE,
        },
        fields: {
            id: {
                width: '5%',
                key: true,
                create: false,
                edit: false,
                list: true
            },
            name: {
                title: 'Name',
                width: '40%',
                edit: true,
                create: true
            },
            nameAlt: {
                title: 'Alt Name',
                width: '40%',
                edit: true,
                create: true
            },
            regexString: {
                title: 'RegexString',
                width: '20%',
                edit: true,
                create: true
            }
        }
    });

    //Load person list from server
    $('#IdConfigurationContainer').jtable('load');
}