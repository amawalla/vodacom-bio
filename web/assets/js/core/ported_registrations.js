var URL_GET_REGIONS = "/admin/region/list_regions";
var cachedTerritory = null;
var cachedRegions = null;
var cachedIdTypes = null;

var queryString = window.location.href.slice(window.location.href.indexOf('?'));
queryString = queryString && queryString.length > 1 ? queryString : '';
var PORT_IN_REQUEST_URL_ELASTIC = "/admin/registration/ported_numbers/list" + queryString;


var loadPortInRequestTables = function () {

    $('#PortInRequestTableContainer').jtable({
        title: 'Port In Requests Table',
        paging: true,
        create: false,
        sorting: false,
        actions: {
            listAction: PORT_IN_REQUEST_URL_ELASTIC
        },
        fields: {
            id: {
                key: true,
                list: false,
                create: false
            },
            created_on: {
                title: 'Date'
            },
            msisdn: {
                title: 'MSISDN'
            },
            customerName: {
                title: 'Customer Name'
            },
            identificationType: {
                title: 'ID Type'

            },
            username: {
                title: 'Agent Number'
            },
            agentName: {
                title: 'Agent Name'
            },
            superAgentName: {
                title: 'Super Agent',
                list: false,
                create: false
            },
            superAgentPhone: {
                title: 'superAgentPhone',
                list: false
            },
            region: {
                title: 'Region',
                /* options: function () {
                    if (cachedRegions) {
                        return cachedRegions;
                    }
                    var options = [];
                    $.ajax({
                        url: URL_GET_REGIONS,
                        type: 'GET',
                        async: false,
                        dataType: 'json',
                        success: function (data) {
                            if (data.Result != 'OK') {
                                alert(data.Message);
                                return;
                            }
                            for (var i = 0; i < data.Options.length; i++) {
                                data.Options[i].Value = data.Options[i].CodeName;
                            }
                            options = data.Options;

                        }


                    });
                    return cachedRegions = options;


                }

                 */
            },
            territory: {
                title: 'Territory',
              /*  options: function (data) {
                    if (cachedTerritory) {
                        return cachedTerritory;
                    }
                    var options = [];
                    var url = URL_GET_TERRITORY + '?regionId=0';
                    $.ajax({
                        url: url,
                        type: 'GET',
                        async: false,
                        dataType: 'json',
                        success: function (result) {
                            console.log(result);
                            if (result.Result != 'OK') {
                                alert(result.Message);
                                return;
                            }
                            options = result.Options;
                        }
                    });
                    return cachedTerritory = options;


                }
               */
            },
            aimStatus: {
                title: 'Ekyc  Status'
            },
            isPorted: {
                title: 'Port Status'
            },
            mpesaStatus: {
                title: 'Mpesa',
                list: false
            },
            dbmsStatus: {
                title: 'DBMS'
            },
            CustomAction: {
                title: '',
                width: '1%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    if (data.record) {
                        return "<a href='../registration_mnp_info/" + data.record.id + "'>View</a>";
                    }
                }
            }
        },
        recordsLoaded: function (event, data) {
            $('.jtable').ReStable({rowHeaders: false});
        }
    });


    function checkPortStatus(status){
        if(status=="3")
            return "Portin Failed"


    }

    $('#PortInRequestTableContainer').jtable('load', {
        path: getParameterByName('path'),
        date: getParameterByName('date')
    });

    //Re-load records when user click 'load records' button.
    $('#LoadRecordsButtonBB').on('click', function (e) {
        e.preventDefault();


        $('#PortInRequestTableContainer').jtable('load', {
            msisdn: $('#customer_msisdn').val(),
            first_name: $('#first_name').val(),
            last_name: $('#last_name').val(),
            identificationType: $('#id_type_name').val(),
            id_number: $('#id_number').val(),
            agent_firstname: $('#agent_firstname').val(),
            agent_lastname: $('#agent_lastname').val(),
            agent_phone: $('#agent_phone').val(),
            region: $('#search_region').val(),
            territory: $('#search_territory').val(),
            from_date: $('#from_date').val(),
            to_date: $('#to_date').val(),
            icap_status: $('#icap_status').val(),
            ereg_status: $('#ereg_status').val()
        });
    });

    $('#LoadRecordsButtonBBreset').click(function (e) {
        e.preventDefault();


        $('#customer_msisdn').val("");
        $('#first_name').val("");
        $('#last_name').val("");
        $('#id_type_name').val("");
        $('#id_number').val("");
        $('#agent_firstname').val("");
        $('#agent_lastname').val("");
        $('#agent_phone').val("");
        $('#search_region').val("");
        $('#search_territory').val("");
        $('#from_date').val("");
        $('#to_date').val("");
        $('#icap_status').val("");
        $('#ereg_status').val("");
        $('#PortInRequestTableContainer').jtable('load');
    });
    //Load all records when page is first shown
    //$('#LoadRecordsButtonBB').click();
    //Load all records when page is first shown
    //$('#LoadRecordsButtonBB').click();

}

function getParameterByName(name, url) {
    if (!url)
        url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results)
        return null;
    if (!results[2])
        return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

