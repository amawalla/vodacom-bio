var URL_CHECKSESSION = "/admin/session_page";
var URL_VERIFYACTION_REGISTRATIONS = "/admin/verifyaction_registrations/" + new Date().getTime();
var URL_LIST_AUDIT_REGISTRATIONS = "/admin/list_audit_registrations";
var URL_LIST_MULTIPLE_REGISTRATIONS = "/admin/list_verify_registrations/multiple/all";
var URL_AUDIT_REGISTRATIONS = "/admin/audit_registrations";
var URL_LIST_VERIFY_REGISTRATIONS = "/admin/list_verify_registrations/search/";
var URL_LIST_VERIFY_REGISTRATIONSVIEW = "/admin/list_verify_registrations/list/" + 1;
var URL_GET_REGIONS = "/admin/region/list_regions";
var cachedTerritory = null;
var cachedRegions = null;
var cachedSuperAgents = null;
var cachedIdTypes = null;

var customcustom = "";


var image_count = 0;
var req_image_count = 0;
var record_row = 0;
var record_ccount = 0;

var URL_LISTVERIFYDETAIL_REGISTRATIONS = "/admin/list_verify_daily_report_detail/";

var queryString = window.location.href.slice(window.location.href.indexOf('?'));
queryString = queryString && queryString.length > 1 ? queryString : '';
var URL_LIST_REGISTRATIONS = "/admin/list_registrations" + queryString;
var URL_LIST_REGISTRATIONS_ELASTIC = "/admin/registration/list";


var loadRegistrationsTables = function (ccustom) {
    //checkSession();
    $('#RegistrationsTableContainer').jtable({
        title: 'Registrations Table',
        paging: true,
        create: false,
        sorting: false,
        actions: {
            listAction: (ccustom == 22 ? URL_LIST_REGISTRATIONS : URL_LIST_REGISTRATIONS_ELASTIC)  //URL_LIST_REGISTRATIONS_ELASTIC
        },
        fields: {
            id: {
                key: true,
                list: false,
                create: false
            },
            created_on: {
                title: 'Date'
            },
            msisdn: {
                title: 'MSISDN'
            },
            customerName: {
                title: 'Name'
            },
            identificationType: {
                title: 'ID Type',
                // options: function () {
                //     if (cachedIdTypes) {
                //         return cachedIdTypes;
                //     }
                //     var options = [];
                //     var url = URL_LIST_IDTYPE
                //     $.ajax({
                //         url: url,
                //         type: 'GET',
                //         async: false,
                //         dataType: 'json',
                //         success: function (result) {
                //             if (result.Result != 'OK') {
                //                 alert(result.Message);
                //                 return;
                //             }
                //             for (var i = 0; i < result.Records.length; i++) {
                //                 var obj = {Value: result.Records[i].id, DisplayText: result.Records[i].name};
                //                 options.push(obj);
                //             }
                //         }
                //     });
                //     return cachedIdTypes = options;
                // }
            },
            username: {
                title: 'Agent Number'
            },
            type: {
                title: 'Module'
            },
            agentName: {
                title: 'AgentName'
            },
            superAgentName: {
                title: 'superAgentName'
            },
            superAgentPhone: {
                title: 'superAgentPhone'
            },
            region: {
                title: 'Region',
                // options: function () {
                //     if (cachedRegions) {
                //         return cachedRegions;
                //     }
                //     var options = [];
                //     $.ajax({
                //         url: URL_GET_REGIONS,
                //         type: 'GET',
                //         async: false,
                //         dataType: 'json',
                //         success: function (data) {
                //             if (data.Result != 'OK') {
                //                 alert(data.Message);
                //                 return;
                //             }
                //             for (var i = 0; i < data.Options.length; i++) {
                //                 data.Options[i].Value = data.Options[i].CodeName;
                //             }
                //             options = data.Options;
                //
                //         }
                //     });
                //     return cachedRegions = options;
                // }
            },
            territory: {
                title: 'Territory',
                // options: function (data) {
                //     if (cachedTerritory) {
                //         return cachedTerritory;
                //     }
                //     var options = [];
                //     var url = URL_GET_TERRITORY + '?regionId=0';
                //     $.ajax({
                //         url: url,
                //         type: 'GET',
                //         async: false,
                //         dataType: 'json',
                //         success: function (result) {
                //             console.log(result);
                //             if (result.Result != 'OK') {
                //                 alert(result.Message);
                //                 return;
                //             }
                //             options = result.Options;
                //         }
                //     });
                //     return cachedTerritory = options;
                // }
            },
            aimStatus: {
                title: 'EReg Status'
            },
            icapStatus: {
                title: 'iCAP'
            },
            mpesaStatus: {
                title: 'Mpesa'
            },
            dbmsStatus: {
                title: 'Dbms'
            },
            CustomAction: {
                title: '',
                width: '1%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    if (data.record) {
                        return "<a href='../registration_info/" + data.record.id + "'>View</a>";
                    }
                }
            }
        },
        recordsLoaded: function (event, data) {
            $('.jtable').ReStable({rowHeaders: false});
        }
    });

    $('#RegistrationsTableContainer').jtable('load', {
        path: getParameterByName('path'),
        date: getParameterByName('date')
    });

    //Re-load records when user click 'load records' button.
    $('#LoadRecordsButtonBB').on('click', function (e) {
        e.preventDefault();

        //checkSession();
        $('#RegistrationsTableContainer').jtable('load', {
            msisdn: $('#customer_msisdn').val(),
            first_name: $('#first_name').val(),
            last_name: $('#last_name').val(),
            identificationType: $('#id_type_name').val(),
            id_number: $('#id_number').val(),
            agent_firstname: $('#agent_firstname').val(),
            agent_lastname: $('#agent_lastname').val(),
            agent_phone: $('#agent_phone').val(),
            region: $('#search_region').val(),
            territory: $('#search_territory').val(),
            from_date: $('#from_date').val(),
            to_date: $('#to_date').val(),
            icap_status: $('#icap_status').val(),
            ereg_status: $('#ereg_status').val()
        });
    });

    $('#LoadRecordsButtonBBreset').click(function (e) {
        e.preventDefault();

        //checkSession();
        $('#customer_msisdn').val("");
        $('#first_name').val("");
        $('#last_name').val("");
        $('#id_type_name').val("");
        $('#id_number').val("");
        $('#agent_firstname').val("");
        $('#agent_lastname').val("");
        $('#agent_phone').val("");
        $('#search_region').val("");
        $('#search_territory').val("");
        $('#from_date').val("");
        $('#to_date').val("");
        $('#icap_status').val("");
        $('#ereg_status').val("");
        $('#RegistrationsTableContainer').jtable('load');
    });

}


var loadInactiveAgentsTable = function () {

    $('#InactiveTableContainer').jtable({
        title: 'InactiveAgents',
        paging: true,
        pageSize: 7,
        create: false,
        sorting: false,
        toolbar: {
            items: [{
                // icon: '/images/excel.png', #uncomment when we have you icon for export button
                text: 'Export to Excel',
                click: function () {
                    //Download report when button is clicked
                    $("#InactiveTableContainer .jtable-toolbar-item-text").click(function () {
                        console.log("clicked export button on inactive agents table");
                        $("#InactiveTableContainer").excelexportjs({
                            containerid: "InactiveTableContainer"
                           , datatype: 'table'
                           ,worksheetName: "Inactive report"
                        });
                    });

                }
            }]
        },
        actions: {

            listAction: function (postData, jtParams) {
            return $.Deferred(function ($dfd) {  
                var dateStart = "";
                var dateEnd = "";
                var dates_dates = "";
                var dates_array = "";
                
                dates_dates = $('#reports_search').text();
                dates_array = dates_dates.split(' - ');
                 dateStart = $.datepicker.formatDate('yy-mm-dd', new Date(dates_array[0]));
                 dateEnd = $.datepicker.formatDate('yy-mm-dd', new Date(dates_array[1]));

                var url = "/admin/inactive_agents/" +dateStart + "/" +dateEnd + "?recordCount=" + (sessionStorage.getItem("recordCount") == null ? 0 : sessionStorage.getItem("recordCount") ) + "&jtStartIndex=" + jtParams.jtStartIndex + '&jtPageSize=' + jtParams.jtPageSize;

                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    data: postData,
                    success: function (data) {
                        $dfd.resolve(data);
                        sessionStorage.setItem("recordCount",data.TotalRecordCount);
                        console.log(sessionStorage.getItem("recordCount"));
                    },
                    error: function () {
                        $dfd.reject();
                    }
                });
            });
        },
        },
        fields: {
            id: {
                key: true,
                list: false,
                create: false
            },
            agent_names: {
                title: 'Agent name'
            },
            username: {
                title: 'Agent Number'
            },
            mobileNumber: {
                title: 'superAgent number'
            },
            SuperAgentNames: {
                title: 'Super Agent'
            },
            region_name: {
                title: 'Region'
            },
            territory_name: {
                title: 'Territory'
            },
            created_on: {
                title: 'Created on'
            },
            lastLogin: {
                title: 'Last Login'
            },
            status: {
                title: 'Status'
            },

        },
        recordsLoaded: function (event, data) {
            $('.jtable').ReStable({rowHeaders: false});
        }
    });


        //checkSession();
        $('#InactiveTableContainer').jtable('load', {
            msisdn: $('#customer_msisdn').val(),
            first_name: $('#first_name').val(),
            last_name: $('#last_name').val(),
            identificationType: $('#id_type_name').val(),
            id_number: $('#id_number').val(),
            agent_firstname: $('#agent_firstname').val(),
            agent_lastname: $('#agent_lastname').val(),
            agent_phone: $('#agent_phone').val(),
            region: $('#search_region').val(),
            territory: $('#search_territory').val(),
            from_date: $('#from_date').val(),
            to_date: $('#to_date').val(),
            icap_status: $('#icap_status').val(),
            ereg_status: $('#ereg_status').val()
        });
   

}

var loadActiveAgentsTable = function () {
    //checkSession();
    $('#ActiveTableContainer').jtable({
        title: 'Active Agents',
        paging: true,
        pageSize: 8,
        create: false,
        sorting: false,
        toolbar: {
            items: [{
                // icon: '/images/excel.png', #uncomment when we have an icon for export button
                text: 'Export to Excel',
                click: function () {
                    //Download report when export button is clicked
                    $("#ActiveTableContainer .jtable-toolbar-item-text").click(function () {
                        console.log("clicked export button on Active agents table");
                        $("#ActiveTableContainer").excelexportjs({
                            containerid: "ActiveTableContainer"
                           , datatype: 'table'
                           ,worksheetName: "Active report"
                        });
                    });
                }
            }]
        },
        actions: {
            listAction: function (postData, jtParams) {
            return $.Deferred(function ($dfd) {

                var dateStart = "";
                var dateEnd = "";
                var dates_dates = "";
                var dates_array = "";
                
                dates_dates = $('#reports_search').text();
                dates_array = dates_dates.split(' - ');
                 dateStart = $.datepicker.formatDate('yy-mm-dd', new Date(dates_array[0]));
                 dateEnd = $.datepicker.formatDate('yy-mm-dd', new Date(dates_array[1]));

                var url = "/admin/active_agents/" +dateStart + "/" +dateEnd + "?recordCount=" + (sessionStorage.getItem("recordCountActive") == null ? 0 : sessionStorage.getItem("recordCountActive") ) + "&jtStartIndex=" + jtParams.jtStartIndex + '&jtPageSize=' + jtParams.jtPageSize;


                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    data: postData,
                    success: function (data) {
                        $dfd.resolve(data);
                        sessionStorage.setItem("recordCountActive",data.TotalRecordCount);
                    },
                    error: function () {
                        $dfd.reject();
                    }
                });
            });
        },
        },
        fields: {
            id: {
                key: true,
                list: false,
                create: false
            },
            agent_names: {
                title: 'Agent name'
            },
            username: {
                title: 'Agent Number'
            },
            mobileNumber: {
                title: 'superAgent number'
            },
            SuperAgentNames: {
                title: 'Super Agent'
            },
            region_name: {
                title: 'Region'
            },
            territory_name: {
                title: 'Territory'
            },
            created_on: {
                title: 'Created on'
            },
            lastLogin: {
                title: 'Last Login'
            },
            status: {
                title: 'Status'
            },

        },
        recordsLoaded: function (event, data) {
            $('.jtable').ReStable({rowHeaders: false});
        }
    });


        //checkSession();
        $('#ActiveTableContainer').jtable('load', {
            msisdn: $('#customer_msisdn').val(),
            first_name: $('#first_name').val(),
            last_name: $('#last_name').val(),
            identificationType: $('#id_type_name').val(),
            id_number: $('#id_number').val(),
            agent_firstname: $('#agent_firstname').val(),
            agent_lastname: $('#agent_lastname').val(),
            agent_phone: $('#agent_phone').val(),
            region: $('#search_region').val(),
            territory: $('#search_territory').val(),
            from_date: $('#from_date').val(),
            to_date: $('#to_date').val(),
            icap_status: $('#icap_status').val(),
            ereg_status: $('#ereg_status').val()
        });
   

}



var verifyRegistrationsTables = function (recordsLimit) {

    $('#RegistrationsTableContainer').jtable({
        title: 'Registrations Table',
        paging: false,
        create: false,
        sorting: false,
        actions: {
            listAction: URL_LIST_VERIFY_REGISTRATIONS + recordsLimit
        },
        fields: {
            id: {
                key: true,
                list: false,
                create: false
            },
            jsonddata: {
                width: '15%',
                title: 'Data'
            },
            CustomAction4: {
                title: 'Portrait',
                width: '12%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction1: {
                title: 'IDFront',
                width: '10%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction2: {
                title: 'IDBack',
                width: '10%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction3: {
                title: 'FingerPrint',
                width: '10%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction3: {
                title: 'Signature',
                width: '10%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction: {
                title: '',
                width: '1%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                }
            }
        },
        recordsLoaded: function (event, data) {
            styleandfetch('verify');
            //verifyActionListen();
            $('.jtable').ReStable({
                rowHeaders: false
            });
            actionListen();
            setCounter();
        }
    });

    $('#RegistrationsTableContainer').jtable('load');

    //Re-load records when user click 'load records' button.
    $('#LoadRecordsButtonBB').click(function (e) {
        e.preventDefault();

        //checkSession();
        $('#RegistrationsTableContainer').jtable('load', {
            created_on: $('#customer_msisdn').val(),
            msisdn: $('#customer_msisdn').val(),
            first_name: $('#first_name').val(),
            last_name: $('#last_name').val(),
            identificationType: $('#id_type_name').val(),
            id_number: $('#id_number').val(),
            agent_firstname: $('#agent_firstname').val(),
            agent_lastname: $('#agent_lastname').val(),
            agent_phone: $('#agent_phone').val(),
            territory: $('#territory').val(),
            from_date: $('#from_date').val(),
            to_date: $('#to_date').val()
        });
    });
    //Load all records when page is first shown
    //$('#LoadRecordsButtonBB').click();

    $('#LoadRecordsButtonBBresetVeriy').click(function (e) {
        e.preventDefault();

        //checkSession();
        $('#customer_msisdn').val("");
        $('#first_name').val("");
        $('#last_name').val("");
        $('#id_type_name').val("");
        $('#id_number').val("");
        $('#agent_firstname').val("");
        $('#agent_lastname').val("");
        $('#agent_phone').val("");
        $('#search_region').val("");
        $('#search_territory').val("");
        $('#from_date').val("");
        $('#to_date').val("");
        $('#RegistrationsTableContainer').jtable('load');
    });

}


var loadReportDetTables = function (dday, sstate) {

    $('#RegistrationsTableContainer').jtable({
        title: 'Registrations Table',
        paging: true,
        create: false,
        sorting: false,
        actions: {
            listAction: URL_LISTVERIFYDETAIL_REGISTRATIONS + dday + "/" + sstate + "/" + record_ccount + "/" + new Date().getTime()
        },
        fields: {
            id: {
                key: true,
                list: false,
                create: false
            },
            created_on: {
                title: 'Date'
            },
            msisdn: {
                title: 'MSISDN'
            },
            customerName: {
                title: 'Name'
            },
            identificationType: {
                title: 'ID Type'
            },
            username: {
                title: 'Agent Number'
            },
            agentName: {
                title: 'AgentName'
            },
            region: {
                title: 'Region'
            },
            territory: {
                title: 'Territory'
            },
            aimStatus: {
                title: 'EReg Status'
            },
            verifyState: {
                title: 'VerifyState'
            },
            icapStatus: {
                title: 'Operator'
            },
            CustomAction: {
                title: '',
                width: '1%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    if (data.record) {
                        return "<a href='/admin/registration_info/" + data.record.id + "'>View</a>";
                    }
                }
            }
        },
        recordsLoaded: function (event, data) {
            $('.jtable').ReStable({rowHeaders: false});
            //record_ccount = 
        }
    });
    
    $('#RegistrationsTableContainer').jtable('load', {
        path: getParameterByName('path'),
        date: getParameterByName('date')
    });

    //Re-load records when user click 'load records' button.
    $('#LoadRecordsButtonBB').on('click', function (e) {
        e.preventDefault();

        //checkSession();
        $('#RegistrationsTableContainer').jtable('load', {
            msisdn: $('#customer_msisdn').val(),
            first_name: $('#first_name').val(),
            last_name: $('#last_name').val(),
            identificationType: $('#id_type_name').val(),
            id_number: $('#id_number').val(),
            agent_firstname: $('#agent_firstname').val(),
            agent_lastname: $('#agent_lastname').val(),
            agent_phone: $('#agent_phone').val(),
            region: $('#search_region').val(),
            territory: $('#search_territory').val(),
            sim_serial: $('#sim_serial').val(),
            id_number: $('#id_number').val(),
            from_date: $('#from_date').val(),
            to_date: $('#to_date').val(),
            icap_status: $('#icap_status').val()
        });
    });
    
    $('#LoadRecordsButtonBBreset').click(function (e) {
        e.preventDefault();

        //checkSession();
        $('#customer_msisdn').val("");
        $('#first_name').val("");
        $('#last_name').val("");
        $('#id_type_name').val("");
        $('#id_number').val("");
        $('#agent_firstname').val("");
        $('#agent_lastname').val("");
        $('#agent_phone').val("");
        $('#search_region').val("");
        $('#search_territory').val("");
        $('#from_date').val("");
        $('#to_date').val("");
        $('#icap_status').val("");
        $('#ereg_status').val("");
        $('#RegistrationsTableContainer').jtable('load');
    });
    //Load all records when page is first shown
    //$('#LoadRecordsButtonBB').click();
    //Load all records when page is first shown
    //$('#LoadRecordsButtonBB').click();

}

var ViewVerifyRegistrationsTables = function () {

    $('#RegistrationsTableContainer').jtable({
        title: 'Registrations Table',
        paging: true,
        create: false,
        sorting: false,
        actions: {
            listAction: URL_LIST_VERIFY_REGISTRATIONSVIEW
        },
        fields: {
            id: {
                key: true,
                list: false,
                create: false
            },
            jsonddata: {
                width: '15%',
                title: 'Data'
            },
            CustomAction4: {
                title: 'Portrait',
                width: '12%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction1: {
                title: 'IDFront',
                width: '10%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction2: {
                title: 'IDBack',
                width: '10%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction3: {
                title: 'FingerPrint',
                width: '10%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction3: {
                title: 'Signature',
                width: '10%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction: {
                title: '',
                width: '1%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                }
            }
        },
        recordsLoaded: function (event, data) {
            styleandfetch('verify_list');
            //verifyActionListen();
            $('.jtable').ReStable({
                rowHeaders: false
            });
            actionListen();
	     
            styleDescription();
        }
    });

    $('#RegistrationsTableContainer').jtable('load');

    //Re-load records when user click 'load records' button.
    $('#LoadRecordsButtonBB').click(function (e) {
        e.preventDefault();

        //checkSession();
        $('#RegistrationsTableContainer').jtable('load', {
            created_on: $('#customer_msisdn').val(),
            msisdn: $('#customer_msisdn').val(),
            first_name: $('#first_name').val(),
            last_name: $('#last_name').val(),
            identificationType: $('#id_type_name').val(),
            id_number: $('#id_number').val(),
            agent_firstname: $('#agent_firstname').val(),
            agent_lastname: $('#agent_lastname').val(),
            agent_phone: $('#agent_phone').val(),
            region: $('#search_region').val(),
            territory: $('#search_territory').val(),
            verifyfrom_date: $('#verifyfrom_date').val(),
            verifyto_date: $('#verifyto_date').val(),
            verifyDescr: $('#verifyDescr').val(),
            verifyState: $('#verifyState').val(),
            verifyBy: $('#verifyBy').val(),
	    from_date: $('#from_date').val(),
            to_date: $('#to_date').val()

        });
    });
    //Load all records when page is first shown
    //$('#LoadRecordsButtonBB').click();

    $('#LoadRecordsButtonBBresetVeriy').click(function (e) {
        e.preventDefault();

        //checkSession();
        $('#customer_msisdn').val("");
        $('#first_name').val("");
        $('#last_name').val("");
        $('#id_type_name').val("");
        $('#id_number').val("");
        $('#agent_firstname').val("");
        $('#agent_lastname').val("");
        $('#agent_phone').val("");
        $('#search_region').val("");
        $('#search_territory').val("");
        $('#verifyfrom_date').val("");
        $('#verifyto_date').val("");
        $('#verifyState').val("");
        $('#verifyDescr').val("");
        
        $('#RegistrationsTableContainer').jtable('load');
    });

}



var loadRegistrationsAuditTables = function () {

    $('#RegistrationsTableContainer').jtable({
        title: 'Registrations Table',
        paging: true,
        create: false,
        sorting: false,
        actions: {
            listAction: URL_LIST_AUDIT_REGISTRATIONS
        },
        fields: {
            id: {
                key: true,
                list: false,
                create: false
            },
            jsonddata: {
                width: '15%',
                title: 'Data'
            },
            CustomAction4: {
                title: 'Portrait',
                width: '20%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction1: {
                title: 'IDFront',
                width: '1%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction2: {
                title: 'IDBack',
                width: '1%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction3: {
                title: 'Signature',
                width: '1%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction: {
                title: '',
                width: '15%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                }
            }
        },
        recordsLoaded: function (event, data) {
            styleandfetch('auditaudit');
            $('.jtable').ReStable({rowHeaders: false});
            auditListen();
        }
    });

    $('#RegistrationsTableContainer').jtable('load');

    //Re-load records when user click 'load records' button.
    $('#LoadRecordsButtonBB').click(function (e) {
        e.preventDefault();

        //checkSession();
        $('#RegistrationsTableContainer').jtable('load', {
            created_on: $('#customer_msisdn').val(),
            msisdn: $('#customer_msisdn').val(),
            first_name: $('#first_name').val(),
            last_name: $('#last_name').val(),
            identificationType: $('#id_type_name').val(),
            id_number: $('#id_number').val(),
            agent_firstname: $('#agent_firstname').val(),
            agent_lastname: $('#agent_lastname').val(),
            agent_phone: $('#agent_phone').val(),
            territory: $('#territory').val(),
            from_date: $('#from_date').val(),
            to_date: $('#to_date').val(),

            verifyState: $('#verifyState').val(),
            verifyDescr: $('#verifyDescr').val(),
            verifyBy: $('#verifyBy').val(),

            region: $('#search_region').val(),
            territory: $('#search_territory').val(),
        });
    });
    //Load all records when page is first shown
    //$('#LoadRecordsButtonBB').click();


}


var loadRegistrationsMultipleTables = function () {

    $('#RegistrationsTableContainer').jtable({
        title: 'Registrations Table',
        paging: true,
        create: false,
        sorting: false,
        actions: {
            listAction: URL_LIST_MULTIPLE_REGISTRATIONS
        },
        fields: {
            id: {
                key: true,
                list: false,
                create: false
            },
            jsonddata: {
                width: '15%',
                title: 'Data'
            },
            CustomAction4: {
                title: 'Portrait',
                width: '12%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction1: {
                title: 'IDFront',
                width: '10%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction2: {
                title: 'IDBack',
                width: '10%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction3: {
                title: 'Signature',
                width: '10%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction: {
                title: '',
                width: '15%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                }
            }
        },
        recordsLoaded: function (event, data) {
            styleandfetch('multiple');
            $('.jtable').ReStable({rowHeaders: false});
            auditListen();
        }
    });

    $('#RegistrationsTableContainer').jtable('load');

    //Re-load records when user click 'load records' button.
    $('#LoadRecordsButtonBB').click(function (e) {
        e.preventDefault();

        //checkSession();
        $('#RegistrationsTableContainer').jtable('load', {
            created_on: $('#customer_msisdn').val(),
            msisdn: $('#customer_msisdn').val(),
            first_name: $('#first_name').val(),
            last_name: $('#last_name').val(),
            identificationType: $('#id_type_name').val(),
            id_number: $('#id_number').val(),
            agent_firstname: $('#agent_firstname').val(),
            agent_lastname: $('#agent_lastname').val(),
            agent_phone: $('#agent_phone').val(),
            region: $('#search_region').val(),
            territory: $('#search_territory').val(),
            verifyfrom_date: $('#verifyfrom_date').val(),
            verifyto_date: $('#verifyto_date').val(),
            verifyDescr: $('#verifyDescr').val(),
            verifyState: $('#verifyState').val(),
            to_date: $('#to_date').val(),
            from_date: $('#from_date').val()
        });
    });
    //Load all records when page is first shown
    //$('#LoadRecordsButtonBB').click();


}

function verifyActionListen() {

    $('.verify_btn').click(function () {
        //SEND TO FULL

        var recid = $(this).attr('fooID');

        $('.lloading.' + recid).show();

        var ccolor = '#23C6C8';
        if ($(this).attr('aaction') == 'decline') {
            ccolor = '#ED5565';
        }
        $('.lloading.' + recid).css('color', ccolor);

        jQuery.ajax({
            url: URL_VERIFYACTION_REGISTRATIONS + '/' + recid + '/' + $(this).attr('aaction'),
            type: "GET",
            dataType: "json",
            success: function (data) {
                $('.lloading.' + recid).hide();
                $('.jtable tbody tr[data-record-key="' + data.recid + '"] td .msggdiv').html("<p class='msgg'>" + data.message + "</p>");
                $('.jtable tbody tr[data-record-key="' + data.recid + '"] td .msggdiv .msgg').css('background', ccolor);
            },
            error: function () {
                $('.lloading.' + recid).hide();
                $('.jtable tbody tr[data-record-key="' + data.recid + '"] td .msggdiv').html("<p class='msgg'>Error. Refresh Page</p>");
                $('.jtable tbody tr[data-record-key="' + data.recid + '"] td .msggdiv .msgg').css('background', ccolor);
            }
        });


    });
}


function styleandfetch(verify) {
    $(".jtable tbody tr td:first-child").each(function () {


        var text = $(this).html();
        $(this).html("");
        var object = JSON.parse(text);

        var myHtml = "";
        var recordId = "";

        myHtml = "<ul class='list-unstyled' style='margin: 10px;line-height: 17px; font-size:12px;'>";
        $.each(object, function (key, val) {
            if (key === "id") {
                recordId = val;
            }
            if(key === "VerifyDescription"){
                var vddescr = instyleDescription(val);
                myHtml += "<li class='" + key + "'><p class='" + key + "'>"+key+"</p>" + vddescr + "<br /></li>";
            }else{
                myHtml += "<li class='" + key + "'><strong>" + key + "</strong>: <span>" + val + "</span></li>";
            }
        });
        myHtml += "</ul>";

        $(this).html("<div class='auditInfoDiv'>" + myHtml + "</div>");


        var portrait = "<a href='javascript:;' class='potrait reg_picc white-modal-80'><img class=' " + $(this).parent().data('record-key') + " regimagee' name='" + $(this).parent().data('record-key') + "potrait'></a>";
        var frontId = "<a href='javascript:;' class='front-pic reg_picc white-modal-80'><img  class='" + $(this).parent().data('record-key') + " regimagee' name='" + $(this).parent().data('record-key') + "front-pic'></a>"
        var backID = "<a href='javascript:;' class='rear-pic reg_picc white-modal-80'><img class='" + $(this).parent().data('record-key') + " regimagee' name='" + $(this).parent().data('record-key') + "rear-pic'></a>";
        var signature = "<a href='javascript:;' class='signature reg_picc white-modal-80'><img class='" + $(this).parent().data('record-key') + " regimagee' name='" + $(this).parent().data('record-key') + "signature'></a>";

        var tableimg = "<div class='imagelists'>" + portrait + frontId + backID + signature + "</div>";

        if (verify === "auditaudit") {
            $($(this).parent().find('td:nth-child(2)')).html(tableimg);
        } else {
            $($(this).parent().find('td:nth-child(2)')).append(portrait);
            $($(this).parent().find('td:nth-child(3)')).append(frontId);
            $($(this).parent().find('td:nth-child(4)')).append(backID);
            $($(this).parent().find('td:nth-child(5)')).append(signature);
        }

        getImages("/admin/registration_images/" + $(this).parent().data('record-key'), $(this).parent().data('record-key'));
        injectAuditRating($(this).parent().data('record-key'), verify);

        $('.jtable tbody tr[data-record-key="' + $(this).parent().data('record-key') + '"] td:nth-child(1) ul').before('<div class="msggdiv"></div>');

        if (verify === 'verify') {
            injectVerifyId($(this).parent().data('record-key'));
        } else if (verify === 'verify_list') {
            $('.jtable tbody tr[data-record-key="' + $(this).parent().data('record-key') + '"] .verification_date').html($('.jtable tbody tr[data-record-key="' + $(this).parent().data('record-key') + '"] .Date').html());
            $('.jtable tbody tr[data-record-key="' + $(this).parent().data('record-key') + '"] .verification_descr').html($('.jtable tbody tr[data-record-key="' + $(this).parent().data('record-key') + '"] .Description').html());
            $('.jtable tbody tr[data-record-key="' + $(this).parent().data('record-key') + '"] .verification_rate').html($('.jtable tbody tr[data-record-key="' + $(this).parent().data('record-key') + '"] .State').html());
            $('.jtable tbody tr[data-record-key="' + $(this).parent().data('record-key') + '"] .verification_by').html($('.jtable tbody tr[data-record-key="' + $(this).parent().data('record-key') + '"] .Verifier').html());

            $('.jtable tbody tr[data-record-key="' + $(this).parent().data('record-key') + '"] .lloadReg').attr('href', 'registration_info_edit/' + recordId + '/' + new Date().getTime());
        }

    });


    $(".reg_picc").colorbox({iframe: true, width: "60%", height: "80%"});


    $(document).ready(function () {

        var rattingArray = ["", "Bad", "Fair", "Good"];

        var akid = "";

        $('.starrr.lead').on('starrr:change', function (e, value) {
            $('.bad').show();
            $('.fair').show();

            akid = $(this).parent().parent().parent().data('record-key');
            console.log(':::¬¬¬¬¬¬¬record:KEY::  ' + akid + ' ¬¬¬¬¬¬¬¬value::: ' + value);

            $('.jtable tbody tr[data-record-key="' + akid + '"] td .cchecks input[type=checkbox]').each(function () {
                $(this).prop("checked", false);
            });

            $('.jtable tbody tr[data-record-key="' + akid + '"] td .vcount').html("0");
            $('.jtable tbody tr[data-record-key="' + akid + '"] td .vcount').html(rattingArray[value]);
            $('.jtable tbody tr[data-record-key="' + akid + '"] td .rate_val').val(value);
            if (value != 3) {
                //$('.jtable tbody tr td#' + akid + ' .rate_descr').removeAttr('disabled');
                $('.jtable tbody tr[data-record-key="' + akid + '"] td .rall').removeAttr('disabled');
            }
            if (value == 3) {
                //$('.jtable tbody tr td#' + akid + ' .rate_descr').val("");
                $('.jtable tbody tr[data-record-key="' + akid + '"] td .rall').prop('disabled', true);
            }
            if (value == 1) {
                $('.jtable tbody tr[data-record-key="' + akid + '"] td .fair').addClass("hidden");
                $('.jtable tbody tr[data-record-key="' + akid + '"] td .bad').removeClass("hidden");
            }
            if (value == 2) {
                $('.jtable tbody tr[data-record-key="' + akid + '"] td .bad').addClass("hidden");
                $('.jtable tbody tr[data-record-key="' + akid + '"] td .fair').removeClass("hidden");
            }
        });


    });
}


function instyleDescription(vval) { 
    
    console.log('instyleDescription instyleDescription instyleDescriptioninstyleDescription');
    
    console.log(vval);
    console.log('instyleDescription instyleDescription instyleDescriptioninstyleDescription');
    
    var descTable = "";
    
    try {
        json = $.parseJSON(vval);
        $.each(json, function (index, val) {
            descTable += "<div class='row'><div class='col-xs-4'><strong>" + val.fieldName + "</strong> </div><div class='col-xs-8'>" + val.fieldDescr + "</div></div>";
        });
    } catch (e) {
        // not json
        descTable += vval;
    }

   // vval = jQuery.parseJSON(vval);
    
    descTable += "";
    
    return descTable;
}



function getImages(URL, id) {

    $.getJSON(URL, function (data) {
        if (data && data.item != null) {
            $.each(data.item, function (index, item) { // Iterates through a collection

                if (item.imageType != null) {
                    var image = document.getElementsByName(id + item.imageType)[0];
                    var downloadingImage = new Image();
                    downloadingImage.onload = function () {
                        $('.' + id + ' [name="' + item.imageType + '"]').attr("src", item.imageType);
                        image.src = this.src;
                        $('.jtable tbody tr[data-record-key="' + item.registration + '"] td a.' + item.imageType).attr("href", item.webPath)
                    }
                    downloadingImage.src = item.webPath;
                }
            });
        } else {
            $('.jtable tbody tr[data-record-key="' + id + '"] td a img').remove();
            $('.jtable tbody tr[data-record-key="' + id + '"] td a').append('No Images Yet');

        }
    });
}

function injectVerifyId(id) {

    //$('.jtable tbody tr[data-record-key="' + id + '"] td:nth-child(1) ul').before('<div class="msggdiv"></div>');
    $('.jtable tbody tr[data-record-key="' + id + '"] td:nth-child(6) .verify_btn').attr('fooID', id);
    $('.jtable tbody tr[data-record-key="' + id + '"] td:nth-child(6) .lloading').addClass(id);
}

function injectAuditRating(id) {
    $('.jtable tbody tr[data-record-key="' + id + '"] td:nth-child(6)').append($('.audit_rate').html());
    $(function () {
        return $('.jtable tbody tr[data-record-key="' + id + '"] td:nth-child(6) .starrr').starrr();
    });
}

function injectAuditRating(id, verify) {
    $('.jtable tbody tr[data-record-key="' + id + '"] td:nth-child(6)').append("<div class='auditActionDiv'>" + $('.audit_rate').html() + "</div>");
    if (verify !== "verify") {
        $(function () {
            return $('.jtable tbody tr[data-record-key="' + id + '"] td:nth-child(6) .starrr').starrr();
        });
    } else {
        $('.jtable tbody tr[data-record-key="' + id + '"] td:nth-child(6) .simpleConfirm').addClass(id);
        $('.jtable tbody tr[data-record-key="' + id + '"] td:nth-child(6) .simpleConfirm').attr('data-foo', id);

        $('.jtable tbody tr[data-record-key="' + id + '"] td:nth-child(6) .simpleDecline').addClass(id);
        $('.jtable tbody tr[data-record-key="' + id + '"] td:nth-child(6) .simpleDecline').attr('data-foo', id);
    }
}

function checkSession() {
    jQuery.ajax({
        url: URL_CHECKSESSION,
        type: "GET",
        dataType: "json",
        success: function (data) {
            console.log("••••••••  checkSession  ••••••••");
            console.log(data);
            console.log("••••••••  checkSession  ••••••••");
        },
        error: function () {
            //Do alert is error
            window.location.href = '/admin/login';
        }
    });
}


function actionListen() {

    var declineTText = $('#declineTText').html();
    $(".simpleConfirm").confirm({
        title: "Approve Registration confirmation",
        text: "Confim do you want to approve the registration?",
        confirm: function (button) {
            button.fadeOut(2000).fadeIn(2000);

            var recid = button.data('foo');

            $('.lloading.' + recid).show();
            $('.lloading.' + recid).css('color', '#23C6C8');

            jQuery.ajax({
                url: URL_VERIFYACTION_REGISTRATIONS + '/' + recid + '/approved',
                type: "GET",
                dataType: "json",
                success: function (data) {

                    console.log('confirm resp•••••••••••••••••••••••••••••••••••••');
                    console.log(data);
                    console.log('confirm resp•••••••••••••••••••••••••••••••••••••');
                    $('.lloading.' + recid).hide();
                    $('.jtable tbody tr[data-record-key="' + data.recid + '"] td .msggdiv').html("<p class='msgg'>" + data.message + "</p>");
                    $('tr#' + data.recid + ' td:nth-child(2) .msggdiv .msgg').css('background', '#23C6C8');

                    $('.simpleConfirm.' + data.recid).addClass('inactivated');
                    $('.simpleDecline.' + data.recid).addClass('inactivated');
                    $('.simpleConfirm.' + data.recid).removeClass('simpleConfirm');
                },
                error: function () {
                    $('.lloading.' + recid).hide();
                    $('tr#' + recid + ' td:nth-child(2) .msggdiv').html("<p class='msgg'>Error. Refresh Page</p>");
                    $('tr#' + data.recid + ' td:nth-child(2) .msggdiv .msgg').css('background', '#23C6C8');
                }
            });

        },
        cancel: function (button) {
            button.fadeOut(2000).fadeIn(2000);
            //alert("You aborted the operation.");
        },
        confirmButton: "Approve Record",
        cancelButton: "Cancel"
    });


    $(".simpleDecline").confirm({
        title: "Approve Registration confirmation",
        text: declineTText,
        confirm: function (button) {

            var recid = button.data('foo');

            var validationCheck = true;
            var id = button.data('foo');
            var rate_descr3 = "";

            $('.cchecks .rate_descr2:checked').each(function () {
                rate_descr3 += ',' + $(this).val();
            });

            console.log('rate_descr3::' + rate_descr3);

            if (rate_descr3 == "") {
                alert("Please select one or more reasons for declining registration ");
                validationCheck = false;
            }

            if (validationCheck) {

                button.fadeOut(2000).fadeIn(2000);

                $('.lloading.' + recid).show();
                $('.lloading.' + recid).css('color', '#ED5565');

                jQuery.ajax({
                    url: URL_VERIFYACTION_REGISTRATIONS + '/' + recid + '/' + rate_descr3,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        console.log('decline resp•••••••••••••••••••••••••••••••••••••');
                        console.log(data);
                        console.log('decline resp•••••••••••••••••••••••••••••••••••••');

                        $('.lloading.' + recid).hide();
                        $('.jtable tbody tr[data-record-key="' + data.recid + '"] td .msggdiv').html("<p class='msgg'>" + data.message + "</p>");
                        $('tbody tr[data-record-key="' + data.recid + '"] td .msggdiv .msgg').css('background', '#ED5565');

                        $('.simpleConfirm.' + data.recid).addClass('inactivated');
                        $('.simpleDecline.' + data.recid).addClass('inactivated');
                        $('.simpleConfirm.' + data.recid).removeClass('simpleConfirm');
                    },
                    error: function () {
                        $('.lloading.' + recid).hide();
                        $('tr#' + recid + ' td .msggdiv').html("<p class='msgg'>Error. Refresh Page</p>");
                        $('tr#' + recid + ' td .msggdiv .msgg').css('background', '#ED5565');
                    }
                });
            } else {
                //alert('validation fail');   
            }
        },
        cancel: function (button) {
            button.fadeOut(2000).fadeIn(2000);
            //alert("You aborted the operation.");
        },
        confirmButton: "Decline Record",
        cancelButton: "Cancel"
    });

}


function auditListen() {


    /** ******  /star rating  *********************** **/

    $(".rate_submit").click(function () {
        var validationCheck = true;
        var btnId = $(this).parent().parent().parent().data('record-key');

        var recordID = $('tbody tr[data-record-key="' + btnId + '"]  td:nth-child(1) ul li.id span').html();

        var rate_val = $('tbody tr[data-record-key="' + btnId + '"]  td .rate_val').val();
        var rate_descr = $('tbody tr[data-record-key="' + btnId + '"]  td .rate_descr').val();
        var rate_descr2 = "___";

        $('tbody tr[data-record-key="' + btnId + '"]  td .auditActionDiv .cchecks .rate_descr2:checked').each(function () {
            rate_descr2 += ',' + $(this).val();
            console.log(' leo leo ----- -- - -- rate_descr2:: ' + rate_descr2);
        });
        
        
        console.log('rate_descr2:: ' + rate_descr2);

        if (rate_descr == "" && rate_val != 3 && rate_val != "") {
            alert("SELECT description for record " + btnId);
            validationCheck = false;
        }

        if (rate_val == "") {
            alert("PLEASE rate the record first! ");
            validationCheck = false;
        }

        if (rate_descr2 == "___" && rate_val != 3) {
            alert("PLEASE check what the issue with reg is! ");
            validationCheck = false;
        }

        if (rate_val === 3) {
            rate_descr = "good_registration";
        }

        if (validationCheck) {
            $('tbody tr[data-record-key="' + btnId + '"]  td .rate_progress.lloading').show();

            //TOP DASH SUMMARY
            jQuery.ajax({
                url: URL_AUDIT_REGISTRATIONS + "/" + recordID + "/" + rate_val + "/" + rate_descr2 + "/" + new Date().getTime(), //This URL is for Json file
                type: "GET",
                dataType: "json",
                success: function (data) {
                    //To-do
                    console.log(data);
                    
                    
        getTallyCountDay();
        getTallyCountMonth();

                    $('tbody tr[data-record-key="' + btnId + '"]  td .rate_progress.lloading').hide();
                    //$('tbody tr[data-record-key="' + btnId + '"]  td .rate_progress').append("<span style='color: #43B153;font-weight: 600;text-rendering: inherit;'>" + data.response_msg + "</span>");
                    $('.jtable tbody tr[data-record-key="' + btnId + '"] td .msggdiv').html("<p class='msgg'>" + data.response_msg + "</p>");
                },
                error: function () {
                    //Do alert is error
                }
            });
        }
    });
}

function getParameterByName(name, url) {
    if (!url)
        url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
    if (!results)
        return null;
    if (!results[2])
        return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

